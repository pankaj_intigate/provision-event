package provision.com.app.fragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.facebook.appevents.AppEventsLogger;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.activity.CartActivity;
import provision.com.app.activity.WebViewActivity;
import provision.com.app.apiResponse.cart_count.CartCountEntity;
import provision.com.app.apiResponse.eventListResponse.Country;
import provision.com.app.apiResponse.eventListResponse.EventListResponse;
import provision.com.app.apiResponse.eventListResponse.Eventlist;
import provision.com.app.apiResponse.eventListResponse.Fee;
import provision.com.app.apiResponse.eventListResponse.RegAttr;
import provision.com.app.apiResponse.eventListResponse.Value;
import provision.com.app.apiResponse.feePriceResponse.FeePriceResponse;
import provision.com.app.apiResponse.getProfileResponse.CountryCode;
import provision.com.app.apiResponse.get_cart_total.GetCartTotal;
import provision.com.app.apiResponse.myWalletResponse.MyWallet;
import provision.com.app.apiResponse.saveParticipent.SaveParticipentResponse;
import provision.com.app.apiResponse.user_info_registration.UserInfoReg;
import provision.com.app.databinding.FragmentRegisterNowBinding;
import provision.com.app.interfaces.RetryListener;
import provision.com.app.interfaces.UpdateCartCount;
import provision.com.app.shop.event_shop_item.ShopItemFromRegistration;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.Utils;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static provision.com.app.app.AppConstant.REGISTRATION_EVENT_TOKEN;
import static provision.com.app.app.AppConstant.SEND_TO_CART_EVENT_TOKEN;
import static provision.com.app.utils.CommonUtils.onTrackEventClick;

/**
 * Created by gauravg on 7/5/2018.
 */

public class RegisterNowFragment extends BaseFragment implements RetryListener, CompoundButton.OnCheckedChangeListener {
    public static final String TYPE_TSHIRT = "tshirt-size";
    public static final String TYPE_DATE_OF_BIRTH = "date-of-birth";
    public static final String TYPE_DISTANCE = "distance";
    public static final String TYPE_HEAR_ABOUT = "how-did-you-hear-about-this-event?";
    public static final String TYPE_ADDRESS = "address";
    public static final String CAR_AND_MAKE_MODEL = "car-make-model";
    public static final String TYPE_PARTICIPATED = "have-you-participated-in-a-running-event-before?";
    public static final String TYPE_BOUBYAN = "boubyan-bank-account";
    private static String eventId;
    private static UpdateCartCount updateCartCount1;
    Calendar myCalendar = Calendar.getInstance();
    BaseActivity context;
    AppEventsLogger logger;
    private ArrayList<String> isShopOpened;
    private int eventItemCheck = 0;
    private double grandTotalCart = 0.0;
    DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, monthOfYear);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
    };
    private String isHolder = "";
    private boolean isErrorOnUserInfo, isErrorOnGetRegistration;
    private double CurrencyValue, teamPrice, doubleIndividualPrice;
    private int Numberformat;
    private String mm = "0", currencyCode;
    private String regestrationType;
    private EventListResponse eventListResponse;
    private FragmentRegisterNowBinding binding;
    private List<CountryCode> countryCodes = new ArrayList<>();
    private int eventPosition = -1;
    private List<Eventlist> eventlists = new ArrayList<>();
    private List<Fee> feeList = new ArrayList<>();
    private String individualPrice = "", mTeamPrice = "", typeEvent;
    private List<Country> countryCodeList = new ArrayList<>();
    private ArrayAdapter<Country> countryCodeAdapter;
    private String[] gender = {"Male | الذكر", "Female | أنثى"};
    private String[] options = {"Delivery التوصيل ", "Pickup الاستلام في الموقع "};
    private String[] haveYouParticipent = {"Yes نعم", "No لا"};
    private List<Value> valueList = new ArrayList<>();
    private List<String> howHearAbout = new ArrayList<>();
    private String selectedEventId = "", selectedCategoryId = "", selectOption = "", selectedGender = "", selectedTeam = "1", eventPrice = "", selectedCountry = "", selectedResidence = "",
            selectedtshirtSize = "", selectHowHearAbout = "", selectHaveYouParticipent = "", eventFeeId = "";
    private boolean isTshirtMandetory = false,
            isHearAboutMandetory = false, isDistanceMandetory = false, isAddressMandetory = false, isParticipentMandetory = false, isCarMakeModel = false, isDateOfBirthMandetory = false,
            isSelectOptionMandetory = false, isClicked = false;
    private boolean isBankHolderMandetory = false;
    private boolean isAddMoreParticapet = false;
    private double eventPriceDouble = 0.0;

    public static RegisterNowFragment newInstance(String event_id, UpdateCartCount updateCartCount) {

        RegisterNowFragment registerNowFragment = new RegisterNowFragment();

        eventId = event_id;
        updateCartCount1 = updateCartCount;
        return registerNowFragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register_now, container, false);
        context = (BaseActivity) getActivity();
        radioButtonClick();
        clickMethod();
        logger = AppEventsLogger.newLogger(context);
        binding.couponInfo.setOnClickListener(v -> {
//            startActivity(new Intent(getContext(), CouponInfoActivity.class));
            couponDetails();
        });
        //setSpinner Adapter
        setEventSpinnerAdapter();
        setCountryAdapter();
        setResidenceAdapter();
        setGenderAdapter();
        setOptionAdapter();
        setTshirtSizeAdapter();
        setHowHearAboutAdapter(-1);
        setCategorySpinnerAdapter();
        setHowyouParticipentAdapter();
        getEventList(eventId);
        binding.addMorePeopleLL.setOnClickListener(v -> {
            if (validateData()) {
                isAddMoreParticapet = true;
                saveParticipent();
                onTrackEventClick(SEND_TO_CART_EVENT_TOKEN);
            }
        });
        binding.shopItemLL.setOnClickListener(v -> {
            Intent intent = new Intent(context, ShopItemFromRegistration.class);
            intent.putExtra("eventID", selectedEventId);
            getActivity().startActivityForResult(intent, 101);
        });
        isShopOpened = new ArrayList<>();
        return binding.getRoot();
    }

    @Override
    public void onRetryCall() {
        super.onRetryCall();
        if (!isErrorOnUserInfo) {
            getUserInfo();
        }
        if (!isErrorOnGetRegistration) {
            getEventList(eventId);
        }
    }

    public void getEventList(String eventId) {
        if (context.isOnline()) {
            setVisibiltiyGone();

            View view = context.showProgressBar();
            String userId = PrefUtils.getFromPrefs(getContext(), PrefUtils.USER_ID);
            String isLogin = "1";
            if (userId.equals("")) {
                userId = "0";
                isLogin = "0";
            }
            AuthApiHelper authApiHelper = ApiClient.getClient(context).create(AuthApiHelper.class);
            retrofit2.Call<EventListResponse> call = authApiHelper.getRegistration("GetRegisteration",
                    userId,
                    isLogin,
                    eventId,
                    "0",

                    "0", PrefUtils.getFromPrefs(getContext(), PrefUtils.COUNTRY_CODE), "1", ApiClient.API_VERSION);
            call.enqueue(new Callback<EventListResponse>() {
                @Override
                public void onResponse(@NotNull Call<EventListResponse> call, @NotNull Response<EventListResponse> response) {
                    eventListResponse = response.body();
                    if (response.body() == null || response.body().getResponse() == null) {
                        Toast.makeText(context, context.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        showViewGroup(RegisterNowFragment.this);
                        isErrorOnGetRegistration = false;
                        return;
                    }
                    isErrorOnGetRegistration = true;
                    binding.radioButtonTshirtPrint.setVisibility(View.GONE);
                    binding.radioButtonTshirtPrint.setChecked(false);
                    binding.radioButtonMadel.setVisibility(View.GONE);
                    binding.radioButtonMadel.setChecked(false);
                    feeList.clear();
                    eventlists.clear();
                    countryCodeList.clear();
                    assert eventListResponse != null;
                    if (eventListResponse != null || eventListResponse.getResponse() != null) {
                        try {
                            feeList.addAll(eventListResponse.getResponse().getEventFee().getFee());

                            eventlists.addAll(eventListResponse.getResponse().getEventlist());
                            countryCodes.addAll(eventListResponse.getResponse().getCountryCodes());
                            countryCodeList.addAll(eventListResponse.getResponse().getCountry());
                            setCountryAdapter();
                            setResidenceAdapter();
                            selectedCategoryId = "";
                            setEventSpinnerAdapter();
                            setCategorySpinnerAdapter();
                            context.cancelProgressBar(view);
                            context.cancelProgressBar(view);
                            regAttribute(eventListResponse);
                        } catch (Exception ignored) {
                            Log.d("dsfs", "dsfds");
                        }
                    } else {
                        isErrorOnGetRegistration = false;
                        Toast.makeText(context, R.string.response_msg, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(@NotNull Call<EventListResponse> call, @NotNull Throwable t) {
                    isErrorOnGetRegistration = false;
                    Log.d("Throwable", t.getLocalizedMessage() + "");
                    showViewGroup(RegisterNowFragment.this);
                }
            });

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        onTrackEventClick(REGISTRATION_EVENT_TOKEN);
    }

    @Override
    public void onStop() {
        super.onStop();
        eventId = "0";
    }


    private void setEventSpinnerAdapter() {
        if (!isClicked && eventlists.size() > 0 && !eventId.equals("0")) {
            for (int i = 0; i < eventlists.size(); i++) {
                if (eventId.equals(eventlists.get(i).getIId())) {

                    binding.spnEvent.setText(eventlists.get(i).getVTitle());
                    binding.spnEvent.setSelected(true);
                    eventPosition = i;
                    selectedEventId = eventlists.get(i).getIId();
                    isClicked = true;
                    getEventList(selectedEventId);
                    break;
                }
            }
        }
        ArrayAdapter<Eventlist> eventAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, eventlists);
        eventAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spnEvent.setAdapter(eventAdapter);
        binding.spnEvent.setOnItemClickListener((parent, arg1, position, id) -> {
            binding.spnCategory.setText("");
            hideViewOnSelectEvent();
            eventPosition = position;
            isClicked = true;
            selectedEventId = eventlists.get(position).getIId();
            clearDataOnSelectEvent();
            getEventList(selectedEventId);

        });

    }


    private void setGenderAdapter() {
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, gender);
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spnGender.setAdapter(genderAdapter);
        binding.spnGender.setOnItemClickListener((parent, arg1, position, id) -> {
            if (position == 0) {
                selectedGender = "male";
            } else {
                selectedGender = "female";
            }
        });
    }

    private void hideViewOnSelectEvent() {
        binding.teamLayout.setVisibility(View.GONE);
        binding.priceLayout.setVisibility(View.GONE);
        binding.radioGroup.setVisibility(View.GONE);
        binding.amountText.setVisibility(View.GONE);
    }


    private void clickMethod() {

//        binding.gotocartText.setOnClickListener(v -> {
//            new Intent(getActivity(), CartActivity.class).putExtra("fromRegisterActivity", true);
////                openFragment();
//        });

//        binding.addNewParticipent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                clearDataOnSelectEvent();
//                invisibleCartLayout();
//            }
//        });

        binding.btnSendToCart.setOnClickListener(v -> {
            if (validateData()) {
                saveParticipent();
                onTrackEventClick(SEND_TO_CART_EVENT_TOKEN);
            }
        });


        binding.spnCategory.setOnClickListener(v -> {
            if (eventPosition < 0) {
                Toast.makeText(context, "Please select event", Toast.LENGTH_SHORT).show();
                binding.spnCategory.dismissDropDown();
                binding.spnEvent.requestFocus();
                binding.spnEvent.showDropDown();
            }
        });
    }

    private void getUserInfo() {
        String userId = PrefUtils.getFromPrefs(context, PrefUtils.USER_ID);
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        if (context.isOnline() && !userId.isEmpty()) {
            View view = context.showProgressBar();
            AuthApiHelper authApiHelper = ApiClient.getClient(context).create(AuthApiHelper.class);
            authApiHelper.getUserInfoForRegistration("userInfo", "1", userId, ApiClient.API_VERSION).enqueue(new CallbackManager<UserInfoReg>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    UserInfoReg userInfoReg = (UserInfoReg) object;
                    context.cancelProgressBar(view);
                    if (userInfoReg == null || userInfoReg.getResponse() == null) {
                        Toast.makeText(context, context.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    binding.firstName.setText(userInfoReg.getResponse().getuInfo().getFirstName());
                    binding.lastName.setText(userInfoReg.getResponse().getuInfo().getLastName());
                    binding.emailEditText.setText(userInfoReg.getResponse().getuInfo().getEmail());
                    binding.addressEditText.setText(userInfoReg.getResponse().getuInfo().getAddress());
                    binding.spnDay.setText(userInfoReg.getResponse().getuInfo().getDd());
                    binding.spnMounth.setText(userInfoReg.getResponse().getuInfo().getMm());
                    mm = userInfoReg.getResponse().getuInfo().getMm();
                    binding.mobileEditText.setText(userInfoReg.getResponse().getuInfo().getPhoneNo());
                    binding.spnYear.setText(userInfoReg.getResponse().getuInfo().getYy());
                    binding.spnTshirtSize.setText(userInfoReg.getResponse().getuInfo().getTsize());
                    selectedtshirtSize = userInfoReg.getResponse().getuInfo().getTsize();
                    selectedCountry = userInfoReg.getResponse().getuInfo().getCountry();
                    selectedResidence = userInfoReg.getResponse().getuInfo().getResidence();
                    if (selectedCountry == null || selectedCountry.isEmpty()) {
                        selectedCountry = "Kuwait";
                    }

                    if (selectedResidence == null || selectedResidence.isEmpty()) {
                        selectedResidence = "Kuwait";
                    }

                    if (userInfoReg.getResponse().getuInfo().getGender().equalsIgnoreCase("Male")) {
                        binding.spnGender.setText(gender[0]);
                        selectedGender = "male";
                    }
                    if (userInfoReg.getResponse().getuInfo().getGender().equalsIgnoreCase("Female")) {
                        binding.spnGender.setText(gender[1]);
                        selectedGender = "female";
                    }
                    String countryFlag = "";
                    binding.spnNationality.setText(selectedCountry);
                    binding.spnResidence.setText(selectedResidence);
                    for (int i = 0; i < countryCodes.size(); i++) {
                        Log.d("selectedResidence", selectedResidence + " " + countryCodes.get(i).getName());
                        if (selectedResidence.equals(countryCodes.get(i).getName())) {

                            int flagOffset = 0x1F1E6;
                            int asciiOffset = 0x41;
                            String country = countryCodes.get(i).getCode();
                            int firstChar = Character.codePointAt(country, 0) - asciiOffset + flagOffset;
                            int secondChar = Character.codePointAt(country, 1) - asciiOffset + flagOffset;
                            String flag = new String(Character.toChars(firstChar))
                                    + new String(Character.toChars(secondChar));
                            String countryCode = countryCodes.get(i).getDialCode();
                            if (countryCode == null || countryCode.equals(null)) {
                                countryCode = "";
                            }
                            binding.spnCountryCode.setText(flag + "  " + countryCode);
                            break;
                        }
                    }


                    if (userInfoReg.getResponse().getuInfo().getCountryCode() != null && !userInfoReg.getResponse().getuInfo().getGender().isEmpty()) {
                        for (int i = 0; i < countryCodes.size(); i++) {
                            Log.d("selectedResidence", selectedResidence + " " + countryCodes.get(i).getName());
                            if (userInfoReg.getResponse().getuInfo().getCountryCode().equals(countryCodes.get(i).getDialCode())) {

                                int flagOffset = 0x1F1E6;
                                int asciiOffset = 0x41;
                                String country = countryCodes.get(i).getCode();
                                int firstChar = Character.codePointAt(country, 0) - asciiOffset + flagOffset;
                                int secondChar = Character.codePointAt(country, 1) - asciiOffset + flagOffset;
                                String flag = new String(Character.toChars(firstChar))
                                        + new String(Character.toChars(secondChar));
                                String countryCode = countryCodes.get(i).getDialCode();
                                if (countryCode == null || countryCode.equals(null)) {
                                    countryCode = "";
                                }
                                binding.spnCountryCode.setText(flag + "  " + countryCode);
                                break;
                            }
                        }
//                        String country = "IN";
//                        int flagOffset = 0x1F1E6;
//                        int asciiOffset = 0x41;
//                        int firstChar = Character.codePointAt(country, 0) - asciiOffset + flagOffset;
//                        int secondChar = Character.codePointAt(country, 1) - asciiOffset + flagOffset;
//                        String flag = new String(Character.toChars(firstChar))
//                                + new String(Character.toChars(secondChar));
//                        String countryCode = userInfoReg.getResponse().getuInfo().getCountryCode();
//                        if (countryCode == null || countryCode.equals(null)) {
//                            countryCode = "";
//                        }
//                        binding.spnCountryCode.setText(flag + "  " + countryCode);

                    }

                   /* for (int i = 0; i < countryCodes.size(); i++) {
                        Log.d("selectedCountry", selectedCountry + " " + countryCodes.get(i).getName());
                        if (selectedCountry.equals(countryCodes.get(i).getName())) {

                            int flagOffset = 0x1F1E6;
                            int asciiOffset = 0x41;
                            String country = countryCodes.get(i).getCode();
                            int firstChar = Character.codePointAt(country, 0) - asciiOffset + flagOffset;
                            int secondChar = Character.codePointAt(country, 1) - asciiOffset + flagOffset;
                            String flag = new String(Character.toChars(firstChar))
                                    + new String(Character.toChars(secondChar));
                            String countryCode = countryCodes.get(i).getDialCode();
                            if (countryCode == null || countryCode.equals(null)) {
                                countryCode = "";
                            }
                            binding.spnCountryCode.setText(flag + "  " + countryCode);
                            break;
                        }
                    }


                    if (userInfoReg.getResponse().getuInfo().getCountryCode() != null && !userInfoReg.getResponse().getuInfo().getGender().isEmpty()) {
                        for (int i = 0; i < countryCodes.size(); i++) {
                            Log.d("selectedCountry", selectedCountry + " " + countryCodes.get(i).getName());
                            if (userInfoReg.getResponse().getuInfo().getCountryCode().equals(countryCodes.get(i).getDialCode())) {

                                int flagOffset = 0x1F1E6;
                                int asciiOffset = 0x41;
                                String country = countryCodes.get(i).getCode();
                                int firstChar = Character.codePointAt(country, 0) - asciiOffset + flagOffset;
                                int secondChar = Character.codePointAt(country, 1) - asciiOffset + flagOffset;
                                String flag = new String(Character.toChars(firstChar))
                                        + new String(Character.toChars(secondChar));
                                String countryCode = countryCodes.get(i).getDialCode();
                                if (countryCode == null || countryCode.equals(null)) {
                                    countryCode = "";
                                }
                                binding.spnCountryCode.setText(flag + "  " + countryCode);
                                break;
                            }
                        }
//                        String country = "IN";
//                        int flagOffset = 0x1F1E6;
//                        int asciiOffset = 0x41;
//                        int firstChar = Character.codePointAt(country, 0) - asciiOffset + flagOffset;
//                        int secondChar = Character.codePointAt(country, 1) - asciiOffset + flagOffset;
//                        String flag = new String(Character.toChars(firstChar))
//                                + new String(Character.toChars(secondChar));
//                        String countryCode = userInfoReg.getResponse().getuInfo().getCountryCode();
//                        if (countryCode == null || countryCode.equals(null)) {
//                            countryCode = "";
//                        }
//                        binding.spnCountryCode.setText(flag + "  " + countryCode);

                    }
*/
                    isErrorOnUserInfo = true;

                }

                @Override
                protected void onError(RetroError retroError) {
                    context.cancelProgressBar(view);
                    isErrorOnUserInfo = false;
                }

                @Override
                protected void onFailure(String retroError) {
                    context.cancelProgressBar(view);
                    isErrorOnUserInfo = false;
                }
            });
        }
    }

    public void getFeePrice(String categoryId, String getId, String priceType) {
        if (context.isOnline()) {
            binding.radioGroup.clearCheck();
            View view = context.showProgressBar();
            AuthApiHelper authApiHelper = ApiClient.getClient(context).create(AuthApiHelper.class);
            retrofit2.Call<FeePriceResponse> call = authApiHelper.getFeePrice("GetFeesPriceAndregistrationType", selectedEventId,
                    categoryId, categoryId + "@@" + priceType, PrefUtils.getFromPrefs(getContext(), PrefUtils.COUNTRY_CODE), ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<FeePriceResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    FeePriceResponse categoryEventResponse = (FeePriceResponse) object;
                    if (categoryEventResponse == null || categoryEventResponse.getResponse() == null) {
                        Toast.makeText(context, context.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    doubleIndividualPrice = categoryEventResponse.getResponse().getEventFee().getFee().getPrice();
                    individualPrice = categoryEventResponse.getResponse().getEventFee().getFee().getPrice() + "";
                    typeEvent = categoryEventResponse.getResponse().getEventFee().getFee().getType();
                    teamPrice = categoryEventResponse.getResponse().getEventFee().getFee().getTprice();
                    mTeamPrice = categoryEventResponse.getResponse().getEventFee().getFee().getTprice() + "";
                    eventFeeId = categoryEventResponse.getResponse().getEventFee().getFee().getId();
                    currencyCode = categoryEventResponse.getCurrrencyCode();
                    eventPrice = individualPrice;
                    if (eventPriceDouble <= 0) {
                        binding.addMorePeopleLL.setVisibility(View.GONE);
                    } else {
                        binding.addMorePeopleLL.setVisibility(View.VISIBLE);
                    }
                    eventPriceDouble = categoryEventResponse.getResponse().getEventFee().getFee().getPrice();

//                    if (categoryEventResponse.getResponse().getEventFee().getFee().getPrice() > 0) {
                    binding.amountText.setText("Amount " + Utils.convertPrice(categoryEventResponse.getCurrencyValue(), categoryEventResponse.getResponse().getEventFee().getFee().getPrice(), categoryEventResponse.getNumberformat()) + " " + categoryEventResponse.getCurrrencyCode() + " (" + typeEvent + ")");
                    binding.priceLayout.setVisibility(View.VISIBLE);
                    binding.amountText.setVisibility(View.VISIBLE);
//                    } else {
//                        binding.priceLayout.setVisibility(View.GONE);
//                    }
                    String minimumParticipent = categoryEventResponse.getResponse().getEventFee().getFeeHeader().getMinPT();
                    CurrencyValue = categoryEventResponse.getCurrencyValue();
                    Numberformat = categoryEventResponse.getNumberformat();

                    binding.particepentText.setText(" Minimum " + minimumParticipent + " participant required.");

                    regestrationType = categoryEventResponse.getResponse().getEventFee().getFeeHeader().getRegistrationType();

                    String isId = categoryEventResponse.getResponse().getEventFee().getFee().getIsD();


                    if (!TextUtils.isEmpty(isId) && isId.equalsIgnoreCase("1")) {
                        binding.spnOption.setVisibility(View.VISIBLE);
                        isSelectOptionMandetory = true;
                    } else {
                        binding.spnOption.setVisibility(View.GONE);
                        isSelectOptionMandetory = false;
                    }

                    if (regestrationType.equalsIgnoreCase("0")) {
                        binding.radioGroup.setVisibility(View.GONE);
                        binding.amountText.setVisibility(View.VISIBLE);
                        binding.teamLayout.setVisibility(View.GONE);
                        binding.teamLayout.setVisibility(View.GONE);
                        binding.priceLayout.setVisibility(View.GONE);
                    } else if (regestrationType.equalsIgnoreCase("1")) {
                        binding.radioGroup.setVisibility(View.VISIBLE);
                        binding.radioButtonIdividual.setVisibility(View.GONE);
                        binding.radioButtonTeam.setChecked(true);
                        if (categoryEventResponse.getResponse().getEventFee().getFee().getTprice() > 0) {
                            binding.amountText.setText("Amount " + Utils.convertPrice(categoryEventResponse.getCurrencyValue(), teamPrice, categoryEventResponse.getNumberformat()) + " " + categoryEventResponse.getCurrrencyCode() + " (" + typeEvent + ")");
                            binding.priceLayout.setVisibility(View.VISIBLE);
                        } else {
                            binding.priceLayout.setVisibility(View.GONE);
                        }
                        binding.teamLayout.setVisibility(View.VISIBLE);
                        binding.teamNameEditText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
                    } else if (regestrationType.equalsIgnoreCase("2")) {
                        binding.radioButtonIdividual.setVisibility(View.VISIBLE);
                        binding.radioGroup.setVisibility(View.VISIBLE);
                        binding.amountText.setVisibility(View.VISIBLE);
                        binding.priceLayout.setVisibility(View.GONE);
                        binding.radioButtonIdividual.setChecked(true);
                    }
                    getCartTotal();
                    context.cancelProgressBar(view);
                }

                @Override
                protected void onError(RetroError retroError) {
                    context.cancelProgressBar(view);

                }

                @Override
                protected void onFailure(String retroError) {
                    context.cancelProgressBar(view);

                }
            });
        } else {
            context.showToast(context, context.getResources().getString(R.string.check_internet));
        }
    }


    private void setCategorySpinnerAdapter() {
        ArrayAdapter<Fee> categoryAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, feeList);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spnCategory.setAdapter(categoryAdapter);


        if (feeList.size() == 1) {
            binding.spnCategory.setText(feeList.get(0).getCategoryName());
            String priceType = feeList.get(0).getPriceType();
            selectedCategoryId = feeList.get(0).getCategoryId();
            String getId = feeList.get(0).getId();
            isSelectOptionMandetory = false;
            binding.spnDay.setSelected(false);
            binding.spnMounth.setSelected(false);
            binding.spnYear.setSelected(false);
//            binding.spnDay.setText("");
//            binding.spnMounth.setText("");
//            binding.spnYear.setText("");
            getFeePrice(selectedCategoryId, getId, priceType);
        }
        binding.spnCategory.setOnItemClickListener((parent, arg1, position, id) -> {
            String priceType = feeList.get(position).getPriceType();
            selectedCategoryId = feeList.get(position).getCategoryId();
            String getId = feeList.get(position).getId();
            isSelectOptionMandetory = false;
            binding.spnDay.setSelected(false);
            binding.spnMounth.setSelected(false);
            binding.spnYear.setSelected(false);
//            binding.spnDay.setText("");
//            binding.spnMounth.setText("");
//            binding.spnYear.setText("");
            getFeePrice(selectedCategoryId, getId, priceType);

        });
    }

    private void setOptionAdapter() {
        ArrayAdapter<String> selectOptionAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, options);
        selectOptionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spnOption.setAdapter(selectOptionAdapter);

        binding.spnOption.setOnItemClickListener((parent, arg1, position, id) -> {
            if (position == 0) {
                selectOption = "1";
                binding.addressLayout.setVisibility(View.VISIBLE);
            } else {
                selectOption = "2";
                binding.addressLayout.setVisibility(View.GONE);
            }
        });
    }

    private void setTshirtSizeAdapter() {
        ArrayAdapter<Value> tshirtAdapter = new ArrayAdapter<Value>(context, android.R.layout.simple_spinner_dropdown_item, valueList);
        tshirtAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spnTshirtSize.setAdapter(tshirtAdapter);

        binding.spnTshirtSize.setOnItemClickListener((parent, arg1, position, id) -> selectedtshirtSize = valueList.get(position).getSize());
    }

    private void setHowHearAboutAdapter(int eventPosition) {
        if (eventPosition >= 0 && eventListResponse != null) {
            howHearAbout.clear();
            for (int i = 0; i < eventListResponse.getResponse().getRegAttr().get(eventPosition).getValue().size(); i++) {
                howHearAbout.add(eventListResponse.getResponse().getRegAttr().get(eventPosition).getValue().get(i).getSocial());
            }
            ArrayAdapter<String> howHearAboutAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, howHearAbout);
            howHearAboutAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            binding.spinHowHearAbout.setAdapter(howHearAboutAdapter);

            binding.spinHowHearAbout.setOnItemClickListener((parent, arg1, position, id) -> selectHowHearAbout = howHearAbout.get(position));
        }
    }

    private void setHowyouParticipentAdapter() {
        ArrayAdapter<String> setHowyouParticipentAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, haveYouParticipent);
        setHowyouParticipentAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.participatedLayout.setAdapter(setHowyouParticipentAdapter);
        binding.participatedLayout.setOnItemClickListener((parent, arg1, position, id) -> selectHaveYouParticipent = haveYouParticipent[position]);
    }

    private void radioButtonClick() {
        binding.radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            View radioButton = binding.radioGroup.findViewById(checkedId);
            int index = binding.radioGroup.indexOfChild(radioButton);
            // Add logic here
            switch (index) {
                case 0: // first button
                    binding.teamLayout.setVisibility(View.GONE);
                    binding.priceLayout.setVisibility(View.GONE);
                    if (doubleIndividualPrice > 0) {
                        binding.amountText.setText("Amount " + Utils.convertPrice(CurrencyValue, doubleIndividualPrice, Numberformat) + " " + currencyCode + " (" + typeEvent + ")");
                        binding.amountText.setVisibility(View.VISIBLE);
                    } else {
                        binding.priceLayout.setVisibility(View.GONE);

                    }
                    selectedTeam = "1";
                    eventPrice = individualPrice;
                    if (eventPriceDouble <= 0) {
                        binding.addMorePeopleLL.setVisibility(View.GONE);
                    } else {
                        binding.addMorePeopleLL.setVisibility(View.VISIBLE);
                    }
                    eventPriceDouble = doubleIndividualPrice;
                    break;
                case 1: // secondbutton
                    binding.teamLayout.setVisibility(View.VISIBLE);
                    binding.teamNameEditText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
                    eventPrice = mTeamPrice;
                    eventPriceDouble = teamPrice;
                    if (eventPriceDouble <= 0) {
                        binding.addMorePeopleLL.setVisibility(View.GONE);
                    } else {
                        binding.addMorePeopleLL.setVisibility(View.VISIBLE);
                    }
                    if (doubleIndividualPrice > 0) {
                        binding.amountText.setText("Amount " + Utils.convertPrice(CurrencyValue, teamPrice, Numberformat) + " " + currencyCode + " (" + typeEvent + ")");
                        binding.amountText.setVisibility(View.VISIBLE);
                        binding.priceLayout.setVisibility(View.VISIBLE);
                    } else {
                        binding.priceLayout.setVisibility(View.GONE);
                    }
                    selectedTeam = "2";
                    break;
            }
        });
    }


    private void setCountryAdapter() {
        countryCodeAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, countryCodeList);
        countryCodeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spnNationality.setAdapter(countryCodeAdapter);
        selectedCountry = "Kuwait";
        binding.spnNationality.setText(selectedCountry);
        /*ArrayAdapter<CountryCode> countryCodeAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, countryCodes);
        countryCodeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spnCountryCode.setAdapter(countryCodeAdapter);
        if (countryCodes.size() > 0) {
            int flagOffset = 0x1F1E6;
            int asciiOffset = 0x41;

            String country = "KW";

            int firstChar = Character.codePointAt(country, 0) - asciiOffset + flagOffset;
            int secondChar = Character.codePointAt(country, 1) - asciiOffset + flagOffset;

            String flag = new String(Character.toChars(firstChar))
                    + new String(Character.toChars(secondChar));
            binding.spnCountryCode.setText(flag + "  +965");
        }
        binding.spnCountryCode.setOnItemClickListener((parent, view, position, id) -> {
            int flagOffset = 0x1F1E6;
            int asciiOffset = 0x41;
            String country = countryCodes.get(position).getCode();
            int firstChar = Character.codePointAt(country, 0) - asciiOffset + flagOffset;
            int secondChar = Character.codePointAt(country, 1) - asciiOffset + flagOffset;
            String flag = new String(Character.toChars(firstChar))
                    + new String(Character.toChars(secondChar));

            binding.mobileEditText.setSelection(binding.mobileEditText.getText().length());
            binding.spnCountryCode.setText(flag + "  " + countryCodes.get(position).getDialCode());
        });
*/
        binding.spnNationality.setOnItemClickListener((parent, arg1, position, id) -> {
                    selectedCountry = countryCodeList.get(position).getName();
                  /*  for (int i = 0; i < countryCodes.size(); i++) {
                        if (selectedCountry.equals(countryCodes.get(i).getName())) {

                            int flagOffset = 0x1F1E6;
                            int asciiOffset = 0x41;
                            String country = countryCodes.get(i).getCode();
                            int firstChar = Character.codePointAt(country, 0) - asciiOffset + flagOffset;
                            int secondChar = Character.codePointAt(country, 1) - asciiOffset + flagOffset;
                            String flag = new String(Character.toChars(firstChar))
                                    + new String(Character.toChars(secondChar));
                            String countryCode = countryCodes.get(i).getDialCode();
                            if (countryCode == null || countryCode.equals(null)) {
                                countryCode = "";
                            }
                            binding.spnCountryCode.setText(flag + "  " + countryCode);
                            break;
                        }
                    }*/
                }
        );

    }

    private void setResidenceAdapter() {
        countryCodeAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, countryCodeList);
        countryCodeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spnResidence.setAdapter(countryCodeAdapter);
        selectedResidence = "Kuwait";
        binding.spnResidence.setText(selectedResidence);

        ArrayAdapter<CountryCode> countryCodeAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, countryCodes);
        countryCodeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spnCountryCode.setAdapter(countryCodeAdapter);
        if (countryCodes.size() > 0) {
            int flagOffset = 0x1F1E6;
            int asciiOffset = 0x41;

            String country = "KW";

            int firstChar = Character.codePointAt(country, 0) - asciiOffset + flagOffset;
            int secondChar = Character.codePointAt(country, 1) - asciiOffset + flagOffset;

            String flag = new String(Character.toChars(firstChar))
                    + new String(Character.toChars(secondChar));
            binding.spnCountryCode.setText(flag + "  +965");
        }
        binding.spnCountryCode.setOnItemClickListener((parent, view, position, id) -> {
            int flagOffset = 0x1F1E6;
            int asciiOffset = 0x41;
            String country = countryCodes.get(position).getCode();
            int firstChar = Character.codePointAt(country, 0) - asciiOffset + flagOffset;
            int secondChar = Character.codePointAt(country, 1) - asciiOffset + flagOffset;
            String flag = new String(Character.toChars(firstChar))
                    + new String(Character.toChars(secondChar));

            binding.mobileEditText.setSelection(binding.mobileEditText.getText().length());
            binding.spnCountryCode.setText(flag + "  " + countryCodes.get(position).getDialCode());
        });


        binding.spnResidence.setOnItemClickListener(((parent, view, position, id) -> {
            selectedResidence = countryCodeList.get(position).getName();
            for (int i = 0; i < countryCodes.size(); i++) {
                if (selectedResidence.equals(countryCodes.get(i).getName())) {

                    int flagOffset = 0x1F1E6;
                    int asciiOffset = 0x41;
                    String country = countryCodes.get(i).getCode();
                    int firstChar = Character.codePointAt(country, 0) - asciiOffset + flagOffset;
                    int secondChar = Character.codePointAt(country, 1) - asciiOffset + flagOffset;
                    String flag = new String(Character.toChars(firstChar))
                            + new String(Character.toChars(secondChar));
                    String countryCode = countryCodes.get(i).getDialCode();
                    if (countryCode == null || countryCode.equals(null)) {
                        countryCode = "";
                    }
                    binding.spnCountryCode.setText(flag + "  " + countryCode);
                    break;
                }
            }
        }));
    }

    private void regAttribute(EventListResponse eventListResponse) {
        valueList.clear();
        binding.spnDay.setSelected(false);
        binding.spnMounth.setSelected(false);
        binding.spnYear.setSelected(false);
        binding.spnDay.setText("");
        binding.spnMounth.setText("");
        binding.spnYear.setText("");
        if (eventListResponse.getResponse().getCoupon() == 1) {
            binding.couponCodeLayout.setVisibility(View.VISIBLE);
            binding.couponEditText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        } else {
            binding.couponCodeLayout.setVisibility(View.GONE);
        }
        if (eventListResponse.getResponse().getEventItemCheck() > 0) {
            binding.shopItemLL.setVisibility(View.VISIBLE);
            eventItemCheck = 1;
        } else {
            binding.shopItemLL.setVisibility(View.GONE);
            eventItemCheck = 0;
        }
        if (eventListResponse.getResponse().getRegAttr() != null && eventListResponse.getResponse().getRegAttr().size() > 0) {
            List<RegAttr> regAttrsList = eventListResponse.getResponse().getRegAttr();
            for (int i = 0; i < regAttrsList.size(); i++) {
                String className = regAttrsList.get(i).getTypeval();
                if (regAttrsList.get(i).getIsMandetory().equalsIgnoreCase("yes")) {
                    setVisibilityAttribute2(i, className, eventListResponse);
                }

            }
            for (int j = 0; j < regAttrsList.size(); j++) {
                if (regAttrsList.get(j).getClass_().equals("date-of-birth")) {
                    ArrayAdapter dayArrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, regAttrsList.get(j).getValueArray().getDates());
                    dayArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spnDay.setAdapter(dayArrayAdapter);
                    ArrayAdapter monthArrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, regAttrsList.get(j).getValueArray().getMonth());
                    monthArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spnMounth.setAdapter(monthArrayAdapter);
                    binding.spnMounth.setOnItemClickListener((parent, view, position, id) -> mm = position + "");
                    ArrayAdapter yearArrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, regAttrsList.get(j).getValueArray().getYear());
                    yearArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spnYear.setAdapter(yearArrayAdapter);
                    break;
                }
            }
        }
        getUserInfo();
    }

    private void setVisibilityAttribute2(int postion, String className, EventListResponse eventListResponse) {
        isHearAboutMandetory = false;
        if (className.equalsIgnoreCase(TYPE_TSHIRT)) {
            binding.spnTshirtSize.setVisibility(View.VISIBLE);
            List<Value> value = eventListResponse.getResponse().getRegAttr().get(postion).getValue();
            if (value != null) {
                valueList.addAll(value);
            }
            String ismandetory = eventListResponse.getResponse().getRegAttr().get(postion).getIsMandetory();
            isTshirtMandetory = (!TextUtils.isEmpty(ismandetory) && ismandetory.equalsIgnoreCase("Yes")) ? true : false;
            setTshirtSizeAdapter();
            return;
        }
        if (className.equalsIgnoreCase(TYPE_DISTANCE)) {
            binding.distanceInputLayout.setVisibility(View.VISIBLE);
            String ismandetory = eventListResponse.getResponse().getRegAttr().get(postion).getIsMandetory();
            isDistanceMandetory = (!TextUtils.isEmpty(ismandetory) && ismandetory.equalsIgnoreCase("Yes")) ? true : false;
            return;
        }
        if (className.equalsIgnoreCase(TYPE_HEAR_ABOUT)) {
            binding.spinHowHearAbout.setVisibility(View.VISIBLE);
            binding.hearInputLayout.setVisibility(View.VISIBLE);
            isHearAboutMandetory = true;
            String ismandetory = eventListResponse.getResponse().getRegAttr().get(postion).getIsMandetory();
            setHowHearAboutAdapter(postion);
            isHearAboutMandetory = !TextUtils.isEmpty(ismandetory) && ismandetory.equalsIgnoreCase("Yes");
            return;
        }
        if (className.equalsIgnoreCase(TYPE_ADDRESS)) {
//            binding.addressLayout.setVisibility(View.VISIBLE);
            String ismandetory = eventListResponse.getResponse().getRegAttr().get(postion).getIsMandetory();
            isAddressMandetory = !TextUtils.isEmpty(ismandetory) && ismandetory.equalsIgnoreCase("Yes");
            return;
        }
        if (className.equalsIgnoreCase(TYPE_DATE_OF_BIRTH)) {
            binding.dataOfBirthLayout.setVisibility(View.VISIBLE);
            String ismandetory = eventListResponse.getResponse().getRegAttr().get(postion).getIsMandetory();
            isDateOfBirthMandetory = !TextUtils.isEmpty(ismandetory) && ismandetory.equalsIgnoreCase("Yes");

            return;
        }
        if (className.equalsIgnoreCase(TYPE_PARTICIPATED)) {
            binding.participatedLayout.setVisibility(View.VISIBLE);
            binding.participatedInputLayout.setVisibility(View.VISIBLE);
            isParticipentMandetory = true;
            String ismandetory = eventListResponse.getResponse().getRegAttr().get(postion).getIsMandetory();
            isParticipentMandetory = !TextUtils.isEmpty(ismandetory) && ismandetory.equalsIgnoreCase("Yes");

            return;
        }
        if (className.equalsIgnoreCase(CAR_AND_MAKE_MODEL)) {
            binding.carMakeModelLL.setVisibility(View.VISIBLE);
            String ismandetory = eventListResponse.getResponse().getRegAttr().get(postion).getIsMandetory();
            isCarMakeModel = !TextUtils.isEmpty(ismandetory) && ismandetory.equalsIgnoreCase("Yes");
            return;
        }
        if (className.equalsIgnoreCase("t-shirt_printing")) {
            binding.radioButtonTshirtPrint.setVisibility(View.VISIBLE);
            binding.radioButtonTshirtPrint.setText(eventListResponse.getResponse().getRegAttr().get(postion).getLabel() + " ( " + Utils.convertPrice(eventListResponse.getCurrencyValue(), eventListResponse.getResponse().getRegAttr().get(postion).getValue().get(0).getPrice(), eventListResponse.getNumberformat(), "") + " " + eventListResponse.getCurrrencyCode() + " )");
//            binding.tvAmountTshirtPrinting.setText("Price : " + Utils.convertPrice(eventListResponse.getCurrencyValue(), eventListResponse.getResponse().getRegAttr().get(postion).getValue().get(0).getPrice(), eventListResponse.getNumberformat(), "") + " " + eventListResponse.getCurrrencyCode());
            return;
        }
        if (className.equalsIgnoreCase("medal_engraving")) {
            binding.radioButtonMadel.setText(eventListResponse.getResponse().getRegAttr().get(postion).getLabel() + " ( " + Utils.convertPrice(eventListResponse.getCurrencyValue(), eventListResponse.getResponse().getRegAttr().get(postion).getValue().get(0).getPrice(), eventListResponse.getNumberformat(), "") + " " + eventListResponse.getCurrrencyCode() + " )");
            binding.radioButtonMadel.setVisibility(View.VISIBLE);
//            binding.tvAmountMedalEngraving.setText("Price : " + Utils.convertPrice(eventListResponse.getCurrencyValue(), eventListResponse.getResponse().getRegAttr().get(postion).getValue().get(0).getPrice(), eventListResponse.getNumberformat(), "") + " " + eventListResponse.getCurrrencyCode());
            return;
        }

        if (className.equalsIgnoreCase("civil_id")) {
            binding.civilIdLayout.setVisibility(View.VISIBLE);
            return;
        }
        if (className.equalsIgnoreCase("gender-avalibility")) {
            gender = new String[eventListResponse.getResponse().getRegAttr().get(postion).getValue().size()];
            for (int i = 0; i < eventListResponse.getResponse().getRegAttr().get(postion).getValue().size(); i++) {
                gender[i] = eventListResponse.getResponse().getRegAttr().get(postion).getValue().get(i).getGenderStr();
            }
            setGenderAdapter();
            return;
        }

        if (className.equalsIgnoreCase(TYPE_BOUBYAN)) {
            binding.llBankAccountHolder.setVisibility(View.VISIBLE);
            String label = eventListResponse.getResponse().getRegAttr().get(postion).getLabel();
            binding.tvBankAccountHolder.setText(label);
            String ismandetory = eventListResponse.getResponse().getRegAttr().get(postion).getIsMandetory();
            if (!TextUtils.isEmpty(ismandetory) && ismandetory.equalsIgnoreCase("Yes")) {
                isBankHolderMandetory = true;
            } else {
                isHolder = "";
                isBankHolderMandetory = false;
            }

            binding.rbBankAccountYes.setOnCheckedChangeListener(this);
            binding.rbBankAccountNo.setOnCheckedChangeListener(this);

        }

//        else {

//        }
    }

    private void setVisibiltiyGone() {
        binding.spnTshirtSize.setVisibility(View.GONE);
        binding.distanceInputLayout.setVisibility(View.GONE);
        binding.carMakeModelLL.setVisibility(View.GONE);
        binding.spinHowHearAbout.setVisibility(View.GONE);
        binding.hearInputLayout.setVisibility(View.GONE);
        isHearAboutMandetory = false;
        binding.addressLayout.setVisibility(View.GONE);
        binding.dataOfBirthLayout.setVisibility(View.GONE);
        binding.participatedLayout.setVisibility(View.GONE);
        binding.participatedInputLayout.setVisibility(View.GONE);
        binding.civilIdLayout.setVisibility(View.GONE);
        isParticipentMandetory = false;
        isCarMakeModel = false;

    }

    private void cartItemCount() {
        String sessionId = PrefUtils.getFromPrefs(context, PrefUtils.SESSION_ID);
        AuthApiHelper authApiHelper = ApiClient.getClient(context).create(AuthApiHelper.class);
        authApiHelper.getCartCount("GetCartCount", sessionId, PrefUtils.getFromPrefs(context, PrefUtils.USER_ID), ApiClient.API_VERSION).enqueue(new CallbackManager<CartCountEntity>() {
            @Override
            protected void onSuccess(Object object, String message) {
                CartCountEntity baseResponce = (CartCountEntity) object;
                if (baseResponce.getCartCountResponse() != null) {
                    PrefUtils.saveToPrefs(context, PrefUtils.CART_ITEM, baseResponce.getCartCountResponse().getCart_count() + "");
                    updateCartCount1.upDateCartCount();
                }
            }

            @Override
            protected void onError(RetroError retroError) {

            }

            @Override
            protected void onFailure(String retroError) {

            }
        });
    }

    public void saveParticipent() {
        String teamName = binding.teamNameEditText.getText().toString();
        String firstName = binding.firstName.getText().toString();
        String lastName = binding.lastName.getText().toString();
//        String dateofBirth = binding.dateOfBirthEditText.getText().toString();
        String mobileNumber = binding.mobileEditText.getText().toString();
        String eamilId = binding.emailEditText.getText().toString();
        String carMakeMode = binding.carMakeModelEdt.getText().toString();

        String distance = binding.distanceEditText.getText().toString();
        String address = binding.addressEditText.getText().toString();
        String sessionId = PrefUtils.getFromPrefs(context, PrefUtils.SESSION_ID);
        String userId = PrefUtils.getFromPrefs(context, PrefUtils.USER_ID);
        String isLoggedIn = "1";
        if (TextUtils.isEmpty(userId)) {
            userId = "0";
            isLoggedIn = "0";
        }
        String dd, month, yy;
        month = binding.spnMounth.getText().toString();
        yy = binding.spnYear.getText().toString();
        dd = binding.spnDay.getText().toString();
        if (binding.dataOfBirthLayout.getVisibility() == View.VISIBLE) {
            if (dd.isEmpty()) {
                Toast.makeText(context, "Please Select day of birth", Toast.LENGTH_SHORT).show();
                return;
            }
            if (month.isEmpty()) {
                Toast.makeText(context, "Please Select day of month", Toast.LENGTH_SHORT).show();
                return;
            }
            if (yy.isEmpty()) {
                Toast.makeText(context, "Please Select year of year", Toast.LENGTH_SHORT).show();
                return;
            }
        } else {
            binding.spnMounth.setText("");
            binding.spnYear.setText("");
            binding.spnDay.setText("");
            month = binding.spnMounth.getText().toString();
            yy = binding.spnYear.getText().toString();
            dd = binding.spnDay.getText().toString();
        }

        String dob = dd + "-" + mm + "-" + yy;
        if (context.isOnline()) {
            View view = context.showProgressBar();
            String medalEngraving = "0", printingTshirts = "0";
            if (binding.radioButtonTshirtPrint.isChecked()) {
                printingTshirts = "1";
            }
            if (binding.radioButtonMadel.isChecked()) {
                medalEngraving = "1";
            }

            AuthApiHelper authApiHelper = ApiClient.getClient(context).create(AuthApiHelper.class);
            retrofit2.Call<SaveParticipentResponse> call = authApiHelper.saveParticipent(
                    "SaveParticipant",
                    "0",
                    selectedCategoryId,
                    "0",
                    "0",
                    selectedTeam,
                    teamName,
                    eventPrice,
                    eventFeeId,
                    selectedEventId,
                    firstName,
                    lastName,
                    dob,
                    dd,
                    mm,
                    yy,
                    selectedGender,
                    mobileNumber,
                    eamilId,
                    selectedCountry,
                    selectOption,
                    selectedtshirtSize,
                    distance,
                    selectHowHearAbout,
                    address,
                    selectHaveYouParticipent,
                    "",
                    "",
                    "",
                    "",
                    "",
                    binding.couponEditText.getText().toString(),
                    sessionId,
                    isLoggedIn,
                    userId,
                    printingTshirts,
                    medalEngraving,
                    binding.civilIdEditText.getText().toString(),
                    isHolder,
                    selectedResidence,
                    carMakeMode,
                    ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<SaveParticipentResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    context.cancelProgressBar(view);
                    SaveParticipentResponse categoryEventResponse = (SaveParticipentResponse) object;
                    if (categoryEventResponse == null || categoryEventResponse.getResponse() == null) {
                        Toast.makeText(context, context.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (categoryEventResponse.getResponse().getStatus() == 1) {
                        binding.spnDay.setSelected(false);
                        binding.spnMounth.setSelected(false);
                        binding.spnYear.setSelected(false);
                        binding.spnDay.setText("");
                        binding.spnMounth.setText("");
                        binding.spnYear.setText("");
                        binding.couponEditText.setText("");
//                        selectedCategoryId = "";

//                        Toast.makeText(context, categoryEventResponse.getResponseMessege(), Toast.LENGTH_LONG).show();
                        resetBankHolder();
                        double eventPriceInDouble = Double.parseDouble(eventPrice);
                        Utils.logAddToCartEvent(binding.spnEvent.getText().toString().trim(), selectedEventId, binding.spnCategory.getText().toString().trim(), "KWD", eventPriceInDouble, logger);
                        boolean shoudOpenShopScreen = true;
                        for (int i = 0; i < isShopOpened.size(); i++) {
                            if (selectedEventId.equals(isShopOpened.get(i))) {
                                shoudOpenShopScreen = false;
                            }
                        }


                        refreshView();
                        if (regestrationType.equalsIgnoreCase("0")) {
                            binding.radioGroup.setVisibility(View.GONE);
                            binding.amountText.setVisibility(View.VISIBLE);
                            binding.teamLayout.setVisibility(View.GONE);
                            binding.priceLayout.setVisibility(View.GONE);
                        } else if (regestrationType.equalsIgnoreCase("1")) {
                            binding.radioGroup.setVisibility(View.VISIBLE);
                            binding.radioButtonTeam.setChecked(true);
                            binding.radioButtonIdividual.setVisibility(View.GONE);
                            binding.amountText.setVisibility(View.VISIBLE);
                        } else if (regestrationType.equalsIgnoreCase("2")) {
                            binding.radioGroup.setVisibility(View.VISIBLE);
                            binding.amountText.setVisibility(View.VISIBLE);
                            if (selectedTeam.equals("1")) {
                                binding.radioButtonIdividual.setChecked(true);
                            }
                        }
                        if (grandTotalCart <= 0 && eventPriceInDouble <= 0) {
//                            hitAddWalletService();
                            saveBookingData();
                            return;
                        }
                        cartItemCount();
                        if (isAddMoreParticapet) {
                            visibleCartLayout();
                            return;
                        }
                        if (shoudOpenShopScreen && eventItemCheck == 1) {
                            Intent intent = new Intent(context, ShopItemFromRegistration.class);
                            intent.putExtra("eventID", selectedEventId);
                            getActivity().startActivityForResult(intent, 101);
                            isShopOpened.add(selectedEventId);
                        } else {
                            visibleCartLayout();
                        }
                    } else {
                        Toast.makeText(context, categoryEventResponse.getResponseMessege(), Toast.LENGTH_LONG).show();
                    }

                }

                @Override
                protected void onError(RetroError retroError) {
                    context.cancelProgressBar(view);

                }

                @Override
                protected void onFailure(String retroError) {
                    context.cancelProgressBar(view);

                }
            });
        } else {
            context.showToast(context, context.getResources().getString(R.string.check_internet));
        }
    }


    private boolean validateData() {
        String teamName = binding.teamNameEditText.getText().toString();
        String firstName = binding.firstName.getText().toString();
        String lastName = binding.lastName.getText().toString();
        String mobileNumber = binding.mobileEditText.getText().toString();
        String eamilId = binding.emailEditText.getText().toString();
        String carMakeModelStr = binding.carMakeModelEdt.getText().toString();
        String distance = binding.distanceEditText.getText().toString();
        String address = binding.addressEditText.getText().toString();
        selectedtshirtSize = binding.spnTshirtSize.getText().toString();
        if (TextUtils.isEmpty(selectedEventId)) {
            binding.spnEvent.requestFocus();
            Toast.makeText(context, "Please select event", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(selectedCategoryId)) {
            binding.spnCategory.requestFocus();
            Toast.makeText(context, "Please select category", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (binding.teamLayout.getVisibility() == View.VISIBLE && TextUtils.isEmpty(teamName)) {
            Toast.makeText(context, "Please enter team name", Toast.LENGTH_SHORT).show();
            binding.teamNameEditText.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(firstName)) {
            binding.firstName.requestFocus();
            Toast.makeText(context, "Please enter first name", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(lastName)) {
            binding.lastName.requestFocus();
            Toast.makeText(context, "Please enter last name", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(selectedGender)) {
            binding.spnGender.requestFocus();
            Toast.makeText(context, "Please select gender", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (mobileNumber.length() < 8) {
            Toast.makeText(context, "Please enter 8 to 12 digit mobile number.", Toast.LENGTH_SHORT).show();
            binding.mobileEditText.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(selectedCountry)) {
            Toast.makeText(context, "Please select nationality", Toast.LENGTH_SHORT).show();
            binding.spnNationality.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(eamilId)) {
            binding.emailEditText.requestFocus();
            Toast.makeText(context, "Please enter email", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!Utils.isValidEmail(eamilId)) {
            Toast.makeText(context, "Please enter valid email", Toast.LENGTH_SHORT).show();
            binding.emailEditText.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(selectedResidence)) {
            Toast.makeText(context, "Please select country of residence", Toast.LENGTH_SHORT).show();
            binding.spnResidence.requestFocus();
            return false;
        }

        if (isSelectOptionMandetory && TextUtils.isEmpty(selectOption)) {
            binding.spnOption.requestFocus();
            Toast.makeText(context, "Please select Race Kit Delivery Option", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (isTshirtMandetory) {
            if (TextUtils.isEmpty(selectedtshirtSize)) {
                binding.spnTshirtSize.requestFocus();
                Toast.makeText(context, "Please select T-shirt size", Toast.LENGTH_SHORT).show();
                return false;
            }
        } else {
            selectedtshirtSize = "";
        }
        if (isDistanceMandetory && TextUtils.isEmpty(distance)) {
            binding.distanceEditText.requestFocus();
            Toast.makeText(context, "Please enter distance", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (isHearAboutMandetory && TextUtils.isEmpty(selectHowHearAbout)) {
            binding.spinHowHearAbout.requestFocus();
            Toast.makeText(context, "Please select Option ", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (isAddressMandetory && TextUtils.isEmpty(address) && selectOption.equals("1")) {
            binding.addressEditText.requestFocus();
            Toast.makeText(context, "Please enter address ", Toast.LENGTH_SHORT).show();

            return false;
        }
        if (isParticipentMandetory && TextUtils.isEmpty(selectHaveYouParticipent)) {
            Toast.makeText(context, "Please select participated", Toast.LENGTH_SHORT).show();
            binding.participatedLayout.requestFocus();
            return false;
        }
        if (isCarMakeModel && TextUtils.isEmpty(carMakeModelStr)) {
            Toast.makeText(context, "Please Enter Car make and model", Toast.LENGTH_SHORT).show();
            binding.carMakeModelLL.requestFocus();
            return false;
        }
        if (binding.civilIdLayout.getVisibility() == View.VISIBLE && binding.civilIdEditText.getText().length() < 12) {
            binding.civilIdEditText.requestFocus();
            Toast.makeText(context, "Please enter 12 digits number only", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (binding.llBankAccountHolder.getVisibility() == View.VISIBLE && isBankHolderMandetory) {
            if (TextUtils.isEmpty(isHolder)) {
                Toast.makeText(context, "Please Select bank account holder", Toast.LENGTH_SHORT).show();
                return false;
            }
        } else {
            resetBankHolder();
        }
        return true;
    }

    private void resetBankHolder() {
        binding.rbBankAccountYes.setChecked(false);
        binding.rbBankAccountNo.setChecked(false);
        isHolder = "";
    }

    private void clearDataOnSelectEvent() {
        selectOption = "";
        selectedGender = "";
        selectedTeam = "1";
        eventPrice = "";
        eventPriceDouble = 0.0;
        selectedCountry = "";
        selectedResidence = "";
        selectedtshirtSize = "";
        selectHowHearAbout = "";
        eventFeeId = "";

        boolean isGenderMandetory = false;
        boolean isNationalityMandetoory = false;
        isTshirtMandetory = false;
        isHearAboutMandetory = false;
        isDistanceMandetory = false;
        isAddressMandetory = false;
        isParticipentMandetory = false;
        isCarMakeModel = false;
        isDateOfBirthMandetory = false;
        //countryCodeList.clear();
        // valueList.clear();
        binding.llBankAccountHolder.setVisibility(View.GONE);
        resetBankHolder();
        setCountryAdapter();
        setResidenceAdapter();
        setGenderAdapter();
        setOptionAdapter();
        setTshirtSizeAdapter();
        setHowHearAboutAdapter(-1);
        setHowyouParticipentAdapter();

        binding.firstName.setText("");
        binding.lastName.setText("");
        binding.mobileEditText.setText("");
//        binding.spnCountryCode.setText("");
        binding.emailEditText.setText("");
        binding.spnNationality.setText("");
        binding.spnResidence.setText("");
        selectedCountry = "Kuwait";
        binding.spnNationality.setText(selectedCountry);
        selectedResidence = "Kuwait";
        binding.spnResidence.setText(selectedResidence);
        binding.spinHowHearAbout.setText("");
        binding.spnOption.setText("");
        binding.spnTshirtSize.setText("");
        binding.distanceEditText.setText("");
        binding.addressEditText.setText("");

        binding.radioGroup.setVisibility(View.GONE);
        binding.spnGender.setText("");

    }

    private void visibleCartLayout() {
        showDialogBox();
    }

    private void showDialogBox() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.go_cart_layout, null);

        TextView addNewParticipent = alertLayout.findViewById(R.id.add_new_participent);
        TextView gotocartText = alertLayout.findViewById(R.id.gotocartText);

        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        AlertDialog dialog = alert.create();
        addNewParticipent.setOnClickListener(v -> {
            refreshView();
            dialog.dismiss();
        });
        gotocartText.setOnClickListener(v -> {
            dialog.dismiss();
            startActivity(new Intent(getActivity(), CartActivity.class).putExtra("fromRegisterActivity", true));
            context.overridePendingTransition(R.anim.bottom_up, R.anim.stay);
            getUserInfo();
        });
        dialog.show();
    }

    private void couponDetails() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.coupon_info_layout, null);

        TextView firstTerms = alertLayout.findViewById(R.id.firstTerms);
        TextView secondTermsTv = alertLayout.findViewById(R.id.secondTerms);
        ImageView ivClose = alertLayout.findViewById(R.id.ivClose);

        String firstTearms = "Regular Coupon code users: Once attempted to use the coupon code, it will be frozen for 60 minutes until you can retry.للعلم: لدى محاولة استعمال رقم الخصم الخاص بك، سيتم تجميده لفترة 60 دقيقة قبل الاستطاعة من محاولة استخدامه مرة أخرى\n";
        SpannableStringBuilder sb = new SpannableStringBuilder(firstTearms);
        StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
        StyleSpan iss = new StyleSpan(Typeface.NORMAL); //Span to make text italic
        sb.setSpan(bss, 0, 26, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
        sb.setSpan(iss, 27, firstTearms.length() - 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make last 2 characters Italic

        firstTerms.setText(sb);
        String secondTerms = "Gulf Bank Staff: Please enter your staff ID number followed by Civil I.D number to avail your special discount. Example 000000-000000000000 موظفي بنك الخليج: يرجى ادخال الرقم الوظيفي والرقم المدني للحصول على الخصم. 000000-000000000000 مثال:";
        sb = new SpannableStringBuilder(secondTerms);
        sb.setSpan(bss, 0, 17, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
        sb.setSpan(iss, 21, secondTerms.length() - 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make last 2 characters Italic
        secondTermsTv.setText(sb);
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(true);
        AlertDialog dialog = alert.create();

        ivClose.setOnClickListener(v -> {
            dialog.dismiss();
        });
        dialog.show();
    }

    private void refreshView() {
        selectOption = "";
        selectedGender = "";
        selectedCountry = "";
        selectedResidence = "";
        selectedtshirtSize = "";
        selectHowHearAbout = "";
        selectHaveYouParticipent = "";
        binding.participatedLayout.setText("");
        setCountryAdapter();
        setResidenceAdapter();
        setGenderAdapter();
        setOptionAdapter();
        setTshirtSizeAdapter();
        setHowHearAboutAdapter(-1);
        setHowyouParticipentAdapter();
        binding.spnGender.setText("");
        binding.firstName.setText("");
        binding.lastName.setText("");
        binding.mobileEditText.setText("");
//        binding.spnCountryCode.setText("");
        binding.emailEditText.setText("");
        binding.spnNationality.setText("");
        binding.spnResidence.setText("");
        selectedCountry = "Kuwait";
        binding.spnNationality.setText(selectedCountry);
        selectedResidence = "Kuwait";
        binding.spnResidence.setText(selectedResidence);
        binding.spinHowHearAbout.setText("");
        binding.spnOption.setText("");
        binding.spnTshirtSize.setText("");
        binding.distanceEditText.setText("");
        binding.carMakeModelEdt.setText("");
        binding.addressEditText.setText("");
        binding.civilIdEditText.setText("");


//        binding.radioButtonMadel.setVisibility(View.GONE);
//        binding.radioButtonMadel.setChecked(false);
//        binding.radioButtonTshirtPrint.setVisibility(View.GONE);
//        binding.radioButtonTshirtPrint.setChecked(false);
    }


    @Override
    public void onRetry() {
        getEventList(eventId);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int id = buttonView.getId();
        if (id == binding.rbBankAccountYes.getId() && isChecked) {
            binding.rbBankAccountNo.setChecked(false);
            isHolder = "1";
            return;
        }
        if (id == binding.rbBankAccountNo.getId() && isChecked) {
            binding.rbBankAccountYes.setChecked(false);
            isHolder = "0";
            return;
        }
    }

    private void getCartTotal() {
        if (isOnline()) {
            View progressBar = showProgressBar();
            ApiClient.getAPiAuthHelper(context).getCartTotal("getCartTotal", ApiClient.API_VERSION, PrefUtils.getFromPrefs(context, PrefUtils.SESSION_ID), "1").enqueue(new CallbackManager<GetCartTotal>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    GetCartTotal getCartTotal = (GetCartTotal) object;
                    if (getCartTotal.getStatus().equals("1")) {
                        grandTotalCart = getCartTotal.getResponse().getGrandTotal();
                        if (eventPriceDouble <= 0) {
                            binding.addMorePeopleLL.setVisibility(View.GONE);
                        } else {
                            binding.addMorePeopleLL.setVisibility(View.VISIBLE);
                        }
                        if (grandTotalCart <= 0 && eventPriceDouble <= 0) {
                            binding.btnSendToCart.setText("CONFIRM BOOKING");
                        } else {
                            binding.btnSendToCart.setText("SEND TO CART");
                        }
                    }
                    cancelProgressBar(progressBar);
                }

                @Override
                protected void onError(RetroError retroError) {
                    cancelProgressBar(progressBar);
                }

                @Override
                protected void onFailure(String retroError) {
                    cancelProgressBar(progressBar);
                }
            });
        }
    }

    private void saveBookingData() {
        if (isOnline()) {
            View view = showProgressBar();
            String sessionId = PrefUtils.getFromPrefs(context, PrefUtils.SESSION_ID);
            int isLoggedIn = 0;
            String userId = PrefUtils.getFromPrefs(context, PrefUtils.USER_ID);
            if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(context, PrefUtils.USER_ID))) {
                isLoggedIn = 1;
            } else {
                userId = "0";
            }

            String currencyCode = PrefUtils.getFromPrefs(context, PrefUtils.COUNTRY_CODE);
            String apiVersion = ApiClient.API_VERSION;

            String parms = currencyCode + "/" + sessionId + "/" + "2" + "/" + isLoggedIn + "/" + userId + "/" + apiVersion + "/2";
            ApiClient.getAPiAuthHelper(context).saveBookingData("SaveBooking_Data", sessionId, userId, 0, "2", ApiClient.API_VERSION).enqueue(new CallbackManager<MyWallet>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    MyWallet myWallet = (MyWallet) object;
                    if (myWallet.getStatus().equals("1")) {
                        startActivity(new Intent(context, WebViewActivity.class).putExtra("URL", myWallet.getResponse().getPaymentUrl() ).putExtra("TitleName", "").putExtra("isCartReset", true));

                    }
                    cancelProgressBar(view);
                }

                @Override
                protected void onError(RetroError retroError) {
                    cancelProgressBar(view);
                }

                @Override
                protected void onFailure(String retroError) {
                    cancelProgressBar(view);
                }
            });
        } else {
            showToast(context, getResources().getString(R.string.check_internet));
        }
    }

    private void hitAddWalletService() {
        if (isOnline()) {
            View view = showProgressBar();
            String sessionId = PrefUtils.getFromPrefs(context, PrefUtils.SESSION_ID);
            int isLoggedIn = 0;
            String userId = PrefUtils.getFromPrefs(context, PrefUtils.USER_ID);
            if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(context, PrefUtils.USER_ID))) {
                isLoggedIn = 1;
            } else {
                userId = "0";
            }

            String currencyCode = PrefUtils.getFromPrefs(context, PrefUtils.COUNTRY_CODE);
            String apiVersion = ApiClient.API_VERSION;

            String parms = currencyCode + "/" + sessionId + "/" + "2" + "/" + isLoggedIn + "/" + userId + "/" + apiVersion + "/2";
            ApiClient.getAPiAuthHelper(context).addMoney("getapplogin", ApiClient.API_VERSION).enqueue(new CallbackManager<MyWallet>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    MyWallet myWallet = (MyWallet) object;
                    if (myWallet.getStatus().equals("1")) {
                        startActivity(new Intent(context, WebViewActivity.class).putExtra("URL", myWallet.getResponse().getPaymentUrl() + parms).putExtra("TitleName", "").putExtra("isCartReset", true));

                    }
                    cancelProgressBar(view);
                }

                @Override
                protected void onError(RetroError retroError) {
                    cancelProgressBar(view);
                }

                @Override
                protected void onFailure(String retroError) {
                    cancelProgressBar(view);
                }
            });
        } else {
            showToast(context, getResources().getString(R.string.check_internet));
        }
    }
}
