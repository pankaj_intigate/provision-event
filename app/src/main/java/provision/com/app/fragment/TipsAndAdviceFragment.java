package provision.com.app.fragment;

import android.app.DownloadManager;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.adapter.TipsAndAdviceAdapter;
import provision.com.app.apiResponse.tipsAdviceResponse.Tip;
import provision.com.app.apiResponse.tipsAdviceResponse.TipsAdviceResponse;
import provision.com.app.databinding.FragmentTipsAndAdviceBinding;
import provision.com.app.interfaces.RetryListener;
import provision.com.app.utils.AppConstant;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;

import static android.content.Context.DOWNLOAD_SERVICE;

/**
 * Created by gauravg on 7/12/2018.
 */

public class TipsAndAdviceFragment extends BaseFragment implements TipsAndAdviceAdapter.CallbackInterface, RetryListener {
    public static TipsAndAdviceFragment tipsAndAdviceFragment;
    private FragmentTipsAndAdviceBinding binding;
    private TipsAndAdviceAdapter mAdapter;
    private BaseActivity mActivity;
    private List<Tip> tipsList = new ArrayList<>();
    private DownloadManager downloadManager;


    public static TipsAndAdviceFragment getInstance() {
        tipsAndAdviceFragment = new TipsAndAdviceFragment();
        return tipsAndAdviceFragment;
    }

    @Override
    public void onRetryCall() {
        Bundle bundle = getArguments();
        String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
        String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
        String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);
        getTipsAdviceData(eventId, tabId, tabName);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_tips_and_advice, container, false);
            mActivity = (BaseActivity) getActivity();


            Bundle bundle = getArguments();
            String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
            String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
            String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);

            setTipsAdviceAdapter();
            getTipsAdviceData(eventId, tabId, tabName);
        }

        return binding.getRoot();
    }

    private void setTipsAdviceAdapter() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        } else {
            binding.rcvTipsAndAdvice.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true));
            mAdapter = new TipsAndAdviceAdapter(mActivity, tipsList, this);
            binding.rcvTipsAndAdvice.setAdapter(mAdapter);
        }
    }


    private void getTipsAdviceData(String eventId, String tabId, String tabName) {
        if (mActivity.isOnline()) {
            View view = mActivity.showProgressBar();
            AuthApiHelper authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper.class);
            retrofit2.Call<TipsAdviceResponse> call = authApiHelper.tipsAdviceResponse("GetEventTab", eventId, tabId, tabName,ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<TipsAdviceResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    mActivity.cancelProgressBar(view);
                    TipsAdviceResponse organiserResponse = (TipsAdviceResponse) object;
                    if (organiserResponse == null || organiserResponse.getResponse() == null) {
                        Toast.makeText(mActivity, mActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        showViewGroup(TipsAndAdviceFragment.this);
                        return;
                    }
                    List<Tip> tips = organiserResponse.getResponse().getTips();
                    tipsList.addAll(tips);
                    setTipsAdviceAdapter();
                }

                @Override
                protected void onError(RetroError retroError) {
                    mActivity.cancelProgressBar(view);
                    showViewGroup(TipsAndAdviceFragment.this);
                }

                @Override
                protected void onFailure(String retroError) {
                    mActivity.cancelProgressBar(view);
                    showViewGroup(TipsAndAdviceFragment.this);
                }
            });
        } else {
            showViewGroup(TipsAndAdviceFragment.this);
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
        }
    }


    private long DownloadData(String uri) {
        long downloadReference;
        // Create request for android download manager
        downloadManager = (DownloadManager) mActivity.getSystemService(DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse
                (uri));
        //Setting title of request
        request.setTitle("Data Download");

        //Setting description of request
        request.setDescription("Android Data download using DownloadManager.");

        //Set the local destination for the downloaded file to a path
        //within the application's external files directory

        //Enqueue download and save into referenceId
        downloadReference = downloadManager.enqueue(request);


        return downloadReference;
    }


    @Override
    public void callBack(String fileUrl) {
        Toast.makeText(mActivity, "File downloading", Toast.LENGTH_SHORT).show();
        DownloadData(fileUrl);
    }

    @Override
    public void onRetry() {
        Bundle bundle = getArguments();
        String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
        String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
        String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);
        getTipsAdviceData(eventId, tabId, tabName);
    }
}
