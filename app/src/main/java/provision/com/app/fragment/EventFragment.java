package provision.com.app.fragment;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.activity.FilterActivity;
import provision.com.app.activity.SearchEventsActivity;
import provision.com.app.adapter.EventAdapter;
import provision.com.app.apiResponse.eventSearchResponse.EventSearchResponse;
import provision.com.app.apiResponse.eventSearchResponse.ProductList;
import provision.com.app.databinding.FragmentEventBinding;
import provision.com.app.model.DataModel;
import provision.com.app.utils.AppConstant;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;

/**
 * Created by gauravg on 7/5/2018.
 */

public class EventFragment extends BaseFragment {
    private static EventFragment eventFragment;
    private final int REQUEST_CODE = 103;
    private FragmentEventBinding binding;
    private EventAdapter mAdapter;
    private List<DataModel> list;
    private BaseActivity mActivity;
    private List<ProductList> productLists = new ArrayList<>();
    private int year;
    private boolean isFilterActive = false;

    public static EventFragment newInstance() {
        if (eventFragment == null) {
            eventFragment = new EventFragment();
        }
        return eventFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_event, container, false);
            mActivity = (BaseActivity) getActivity();
            year = Calendar.getInstance().get(Calendar.YEAR);
            getEventDetail();
            setEventAdapter();
            searchEvent();

            binding.icFilter.setOnClickListener(v -> {
                Intent intent = new Intent(getActivity(), FilterActivity.class);
                startActivityForResult(intent, REQUEST_CODE);
            });
        }
        if (isFilterActive) {
            binding.tvSearch.setText("");
            getEventDetail();
        }
        return binding.getRoot();
    }


    public void getEventDetail() {
        if (mActivity.isOnline()) {
            productLists.clear();
            View view = mActivity.showProgressBar();
            AuthApiHelper authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper.class);
            retrofit2.Call<EventSearchResponse> call = authApiHelper.searchEventResponse("GetAjaxEventsListing", "", "", year, "", "",ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<EventSearchResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    mActivity.cancelProgressBar(view);
                    EventSearchResponse eventDetailResponse = (EventSearchResponse) object;
                    if (eventDetailResponse == null || eventDetailResponse.getResponse() == null) {
                        Toast.makeText(mActivity, mActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    productLists.addAll(eventDetailResponse.getResponse().getProductList());
                    isFilterActive = false;
                    if (productLists != null && productLists.size() > 0) {
                        dataFound();
                    } else {
                        noDataFound();
                    }
                    setEventAdapter();
                }

                @Override
                protected void onError(RetroError retroError) {
                    mActivity.cancelProgressBar(view);
                }

                @Override
                protected void onFailure(String retroError) {
                    mActivity.cancelProgressBar(view);
                }
            });
        } else {
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
        }
    }


    public void getSearchEventData(String searchText, String categoryId, int yearId, String monthId, String statusId) {
        if (mActivity.isOnline()) {
            productLists.clear();
            View view = mActivity.showProgressBar();
            AuthApiHelper authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper.class);
            retrofit2.Call<EventSearchResponse> call = authApiHelper.searchEventResponse("GetAjaxEventsListing", searchText, categoryId, yearId, monthId, statusId,ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<EventSearchResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    mActivity.cancelProgressBar(view);
                    EventSearchResponse eventDetailResponse = (EventSearchResponse) object;
                    if (eventDetailResponse.getResponse().getProductList() != null) {
                        productLists.clear();
                        productLists.addAll(eventDetailResponse.getResponse().getProductList());
                    }

                    if (productLists != null && productLists.size() > 0) {
                        dataFound();
                    } else {
                        noDataFound();
                    }

                    setEventAdapter();
                }

                @Override
                protected void onError(RetroError retroError) {
                    mActivity.cancelProgressBar(view);
                }

                @Override
                protected void onFailure(String retroError) {
                    mActivity.cancelProgressBar(view);
                }
            });
        } else {
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
        }
    }


    private void setEventAdapter() {
       /* if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        } else {
            binding.rcvEvents.setLayoutManager(new LinearLayoutManager(getActivity()));
            mAdapter = new EventAdapter(mActivity, productLists);
            binding.rcvEvents.setAdapter(mAdapter);
        }*/

        binding.rcvEvents.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new EventAdapter(mActivity, productLists);
        binding.rcvEvents.setAdapter(mAdapter);
    }

    private void searchEvent() {
        binding.tvSearch.setOnClickListener(v -> {
//            startActivity(new Intent(getActivity(), SearchEventsActivity.class));
            Intent cartIntent = new Intent(getActivity(), SearchEventsActivity.class);
            getActivity().startActivityForResult(cartIntent, 101);
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == requestCode && resultCode == resultCode) {
            if (resultCode == REQUEST_CODE) {
                String categoryId = intent.getStringExtra(AppConstant.KEY_CATEGORY);
                String yearId = intent.getStringExtra(AppConstant.KEY_YEAR);
                String monthId = intent.getStringExtra(AppConstant.KEY_MONTH);
                String statusId = intent.getStringExtra(AppConstant.KEY_STATUS);
                if (yearId.isEmpty() && categoryId.isEmpty() && monthId.isEmpty() && statusId.isEmpty()) {
                    yearId = Calendar.getInstance().get(Calendar.YEAR) + "";
                }
                isFilterActive = true;
                getSearchEventData(binding.tvSearch.getText().toString(), categoryId, Integer.parseInt(yearId), monthId, statusId);
            } else {
                isFilterActive = true;
                getSearchEventData(binding.tvSearch.getText().toString(), "", Calendar.getInstance().get(Calendar.YEAR), "", "");
            }
        } else {

        }


    }

    private void dataFound() {
        binding.rcvEvents.setVisibility(View.VISIBLE);
        binding.noDataFoundText.setVisibility(View.GONE);
    }

    private void noDataFound() {
        binding.rcvEvents.setVisibility(View.GONE);
        binding.noDataFoundText.setVisibility(View.VISIBLE);
    }
}
