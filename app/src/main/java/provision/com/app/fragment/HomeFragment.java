package provision.com.app.fragment;

import android.app.Activity;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.UUID;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.binder.HomeFragmentBinder;
import provision.com.app.databinding.FragmentHomeBinding;
import provision.com.app.utils.AppConstant;
import provision.com.app.utils.PrefUtils;

import static provision.com.app.app.AppConstant.HOME_EVENT_TOKEN;
import static provision.com.app.utils.CommonUtils.onTrackEventClick;
/*
 * Created by gauravg on 7/5/2018.
 */

public class HomeFragment extends BaseFragment {
    private static HomeFragment homeFragment;
    private FragmentHomeBinding binding;
    private BaseActivity mActivity;
    private HomeFragmentBinder homeFragmentBinder;

    public static HomeFragment newInstance() {
        if (homeFragment == null) {
            homeFragment = new HomeFragment();
        }
        return homeFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
            mActivity = (BaseActivity) getActivity();
            homeFragmentBinder = new HomeFragmentBinder(mActivity, binding);
            binding.setHomeFragment(homeFragmentBinder);
        }
        return binding.getRoot();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_CANCELED) {
            Toast.makeText(mActivity, "" + resultCode, Toast.LENGTH_SHORT).show();
            return;
        }
        if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
            if (data != null && data.hasExtra(AppConstant.KEY_EVENT_ID)) {
                String eventId = data.getStringExtra(AppConstant.KEY_EVENT_ID);
                Toast.makeText(mActivity, "" + eventId, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (TextUtils.isEmpty(PrefUtils.getFromPrefs(getActivity(), PrefUtils.SESSION_ID))) {
            String timeInMillis = System.currentTimeMillis() + "-" + UUID.randomUUID().toString();
            PrefUtils.saveToPrefs(getActivity(), PrefUtils.SESSION_ID, timeInMillis);
        }

        onTrackEventClick(HOME_EVENT_TOKEN);
    }

    @Override
    public void onRetryCall() {
        super.onRetryCall();
        homeFragmentBinder.retry();
    }
}
