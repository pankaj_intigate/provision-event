package provision.com.app.fragment;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.adapter.OutletAdapter;
import provision.com.app.apiResponse.outletResponse.OutLetResponse;
import provision.com.app.apiResponse.outletResponse.Outlet;
import provision.com.app.databinding.FragmentOutletBinding;
import provision.com.app.interfaces.RetryListener;
import provision.com.app.utils.AppConstant;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;

/**
 * Created by gauravg on 7/12/2018.
 */

public class OutletFragment extends BaseFragment implements RetryListener {
    public static OutletFragment outletFragment;
    private FragmentOutletBinding binding;
    private OutletAdapter mAdapter;
    private List<Outlet> mOutLetlist = new ArrayList<>();
    private BaseActivity mActivity;

    public static OutletFragment getInstance() {
        outletFragment = new OutletFragment();
        return outletFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_outlet, container, false);
            mActivity = (BaseActivity) getActivity();
            Bundle bundle = getArguments();
            String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
            String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
            String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);
            setOutletAdapter();
            getOutLetData(eventId, tabId, tabName);
        }
        return binding.getRoot();
    }

    private void setOutletAdapter() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        } else {
            binding.rcvOutlet.setLayoutManager(new LinearLayoutManager(getActivity()));
            mAdapter = new OutletAdapter(mActivity, mOutLetlist);
            binding.rcvOutlet.setAdapter(mAdapter);
        }
    }


    private void getOutLetData(String eventId, String tabId, String tabName) {
        if (mActivity.isOnline()) {
            View view = mActivity.showProgressBar();
            AuthApiHelper authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper.class);
            retrofit2.Call<OutLetResponse> call = authApiHelper.outLetResponse("GetEventTab", eventId, tabId, tabName,ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<OutLetResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    mActivity.cancelProgressBar(view);
                    OutLetResponse feeResponse = (OutLetResponse) object;
                    if (feeResponse == null || feeResponse.getResponse() == null) {
                        Toast.makeText(mActivity, mActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        showViewGroup(OutletFragment.this::onRetryCall);
                        return;
                    }
                    List<Outlet> outlet = feeResponse.getResponse().getOutlet();
                    if (outlet !=null){
                        mOutLetlist.addAll(outlet);
                    }
                    setOutletAdapter();
                }

                @Override
                protected void onError(RetroError retroError) {
                    mActivity.cancelProgressBar(view);
                    showViewGroup(OutletFragment.this::onRetryCall);
                }

                @Override
                protected void onFailure(String retroError) {
                    mActivity.cancelProgressBar(view);
                    showViewGroup(OutletFragment.this::onRetryCall);
                }
            });
        } else {
            showViewGroup(OutletFragment.this::onRetryCall);
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
        }
    }

    @Override
    public void onRetryCall() {
        Bundle bundle = getArguments();
        String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
        String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
        String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);
        getOutLetData(eventId, tabId, tabName);
    }

    @Override
    public void onRetry() {
        Bundle bundle = getArguments();
        String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
        String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
        String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);
        getOutLetData(eventId, tabId, tabName);
    }
}
