package provision.com.app.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.activity.LoginActivity;
import provision.com.app.activity.PaymentWebViewActivity;
import provision.com.app.activity.WebViewActivity;
import provision.com.app.adapter.CartAdapter;
import provision.com.app.apiResponse.BaseResponce;
import provision.com.app.apiResponse.cartDataResponse.CartDataResponse;
import provision.com.app.apiResponse.cartDataResponse.DeliveryInfo;
import provision.com.app.apiResponse.cartDataResponse.EventsData;
import provision.com.app.apiResponse.cartDataResponse.ParticipateData;
import provision.com.app.apiResponse.cartDeleteResponse.CartDeleteResponse;
import provision.com.app.apiResponse.cart_count.CartCountEntity;
import provision.com.app.apiResponse.check_participate.CheckParticipatEntity;
import provision.com.app.apiResponse.check_participate.save_booking.SaveBookingEntity;
import provision.com.app.apiResponse.myWalletResponse.MyWallet;
import provision.com.app.databinding.FragmentCartBinding;
import provision.com.app.interfaces.RetryListener;
import provision.com.app.shop.cart.CartParticipateDataAdapter;
import provision.com.app.shop.checkout.CheckoutActivity;
import provision.com.app.utils.CustomAlertDialog;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.Utils;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static provision.com.app.app.AppConstant.DISCLAIMER_EVENT_TOKEN;
import static provision.com.app.app.AppConstant.PROCEED_TO_PAYMENT_EVENT_TOKEN;
import static provision.com.app.utils.CommonUtils.onTrackEventClick;

/**
 * Created by gauravg on 7/5/2018.
 */

public class CartFragment extends BaseFragment implements CartAdapter.RemoveCartItem, RetryListener {
    private static final int LOGIN_REQUEST_CODE = 555;
    private static CartFragment cartFragment;
    private static boolean iisFinishOnAddClick = false;
    String cartAmount = "0.0";
    double currencyValue = 0.0;
    String currencyCode = "KD";
    private FragmentCartBinding binding;
    private CartAdapter adapter;
    private BaseActivity mActivity;
    private CartCountInterface cartCountInterface;
    private String mPaymentType = "0", name, eventIds = "";
    private boolean isError0nCart = false;
    private ArrayList<EventsData> cartDataList;
    ArrayList<DeliveryInfo> shopDataList;
    private ArrayList<ParticipateData> participateData;
    private CartParticipateDataAdapter cartParticipateDataAdapter;
    private String couponCodeValue = "";
    private String couponId = "";

    public static CartFragment newInstance(boolean isFinishOnAddClick) {
        iisFinishOnAddClick = isFinishOnAddClick;
        //  if (cartFragment == null) {
        cartFragment = new CartFragment();
        //}
        return cartFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cart, container, false);
        mActivity = (BaseActivity) getActivity();
        cartDataList = new ArrayList<>();
        shopDataList = new ArrayList<>();
        participateData = new ArrayList<>();
        binding.rcvCart.setLayoutManager(new LinearLayoutManager(mActivity));
        binding.rcvCart.setNestedScrollingEnabled(false);
        adapter = new CartAdapter(mActivity, cartDataList, shopDataList, this, currencyValue, currencyCode, 3);
        binding.rcvCart.setAdapter(adapter);
        binding.participateDataList.setLayoutManager(new LinearLayoutManager(mActivity));
        binding.participateDataList.setNestedScrollingEnabled(false);
        cartParticipateDataAdapter = new CartParticipateDataAdapter(mActivity, participateData, 1.0, "");
        binding.participateDataList.setAdapter(cartParticipateDataAdapter);
        binding.btnContinue.setOnClickListener(v -> {
            if (TextUtils.isEmpty(PrefUtils.getFromPrefs(getContext(), PrefUtils.USER_ID))) {
                getActivity().startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("IsCheckOut", "").putExtra("isAddressRequired", shopDataList.size()), LOGIN_REQUEST_CODE);
            } else {
                getActivity().startActivityForResult(new Intent(getActivity(), CheckoutActivity.class), 999);

            }

        });
        binding.couponCodeTV.setOnClickListener(v -> {
            removeCouponCode();
        });
        binding.emptyCart.btnRegisterEvent.setOnClickListener(v -> {

            getActivity().setResult(Activity.RESULT_OK);
            getActivity().finish();
        });
        return binding.getRoot();
    }

    private void removeCouponCode() {
        View view = showProgressBar();
        ApiClient.getAPiAuthHelper(getContext()).deleteItemCouponFromCart("DeleteItemCouponFromCart",
                ApiClient.API_VERSION, couponCodeValue, PrefUtils.getFromPrefs(getContext(), PrefUtils.SESSION_ID), couponId,
                PrefUtils.getFromPrefs(getContext(), PrefUtils.USER_ID)).enqueue(new CallbackManager<BaseResponce>() {
            @Override
            protected void onSuccess(Object object, String message) {
                BaseResponce baseResponce = (BaseResponce) object;
                if (baseResponce.getStatus().equals("1")) {
                    getCartData();
                }
                cancelProgressBar(view);
            }

            @Override
            protected void onError(RetroError retroError) {
                cancelProgressBar(view);
            }

            @Override
            protected void onFailure(String retroError) {
                cancelProgressBar(view);
            }
        });
    }

    private void updateCartQtyApi(String qty, String bookingId) {
        View view = showProgressBar();
        ApiClient.getAPiAuthHelper(getContext()).updateCartQty("updatecartqty",
                ApiClient.API_VERSION, PrefUtils.getFromPrefs(getContext(), PrefUtils.SESSION_ID), bookingId,
                qty).enqueue(new CallbackManager<BaseResponce>() {
            @Override
            protected void onSuccess(Object object, String message) {
                BaseResponce baseResponce = (BaseResponce) object;
                if (baseResponce.getStatus().equals("1")) {
                    getCartData();
                }
                cancelProgressBar(view);
            }

            @Override
            protected void onError(RetroError retroError) {
                cancelProgressBar(view);
            }

            @Override
            protected void onFailure(String retroError) {
                cancelProgressBar(view);
            }
        });
    }

    private void setSpannable(TextView tv, String text, int start, int end) {
        SpannableString ss = new SpannableString(text);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                startActivity(new Intent(getActivity(), WebViewActivity.class).putExtra("URL", ApiClient.TERMS_N_CONDITIONS_URL).putExtra("TitleName", "Disclaimer"));
                onTrackEventClick(DISCLAIMER_EVENT_TOKEN);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
            }
        };

        ss.setSpan(clickableSpan, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv.setMovementMethod(LinkMovementMethod.getInstance());
        tv.setText(ss, TextView.BufferType.SPANNABLE);
    }

    private void checkParticipate() {
        if (mActivity.isOnline()) {
            String sessionId = PrefUtils.getFromPrefs(getActivity(), PrefUtils.SESSION_ID);
            View view = mActivity.showProgressBar();
            AuthApiHelper authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper.class);
            retrofit2.Call<CheckParticipatEntity> call = authApiHelper.checkPaymentProceed("checkParticipate", sessionId, ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<CheckParticipatEntity>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    CheckParticipatEntity checkParticipatEntity = (CheckParticipatEntity) object;
                    if (checkParticipatEntity == null) {
                        Toast.makeText(mActivity, mActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (checkParticipatEntity.getCheckParticipateResponce().getStatus() == 1) {
//                        doPayment();
                        hitAddWalletService();
                    } else {
                        Toast.makeText(mActivity, checkParticipatEntity.getCheckParticipateResponce().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                    mActivity.cancelProgressBar(view);
                }

                @Override
                protected void onError(RetroError retroError) {
                    mActivity.cancelProgressBar(view);
                }

                @Override
                protected void onFailure(String retroError) {
                    mActivity.cancelProgressBar(view);
                }
            });
        } else {
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
        }
    }

    private void getCartData() {
        if (mActivity.isOnline()) {
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }
            String sessionId = PrefUtils.getFromPrefs(getActivity(), PrefUtils.SESSION_ID);
//            sessionId = "1539773516668-7677c37b-c0a4-4752-af45-e4f686b5cda5";
            int isLoggedIn = 0;
            if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(getActivity(), PrefUtils.USER_ID))) {
                isLoggedIn = 1;
            }
            String emailId = TextUtils.isEmpty(PrefUtils.getFromPrefs(getActivity(), PrefUtils.EMAIL_ID)) ? "" : PrefUtils.getFromPrefs(getActivity(), PrefUtils.EMAIL_ID);

            View view = mActivity.showProgressBar();
            AuthApiHelper authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper.class);
            retrofit2.Call<CartDataResponse> call = authApiHelper.cartDataResponse("GetCartDetails", sessionId, "", isLoggedIn, TextUtils.isEmpty(PrefUtils.getFromPrefs(getActivity(), PrefUtils.COUNTRY_CODE)) ? "KWD" : PrefUtils.getFromPrefs(getActivity(), PrefUtils.COUNTRY_CODE), emailId, "", PrefUtils.getFromPrefs(getActivity(), PrefUtils.USER_ID), PrefUtils.getFromPrefs(getActivity(), PrefUtils.USER_TYPE), "registration/shop", ApiClient.API_VERSION);//KEY_WEB_CALL
            call.enqueue(new CallbackManager<CartDataResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    mActivity.cancelProgressBar(view);
                    CartDataResponse cartDataResponse = (CartDataResponse) object;
                    if (cartDataResponse == null || cartDataResponse.getResponse() == null) {
                        Toast.makeText(mActivity, mActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        showViewGroup(CartFragment.this::onRetryCall);
                        isError0nCart = true;
                        return;
                    }
                    isError0nCart = false;
                    if (cartDataResponse.getStatus().equals("1")) {
                        shopDataList.clear();
                        cartDataList.clear();
                        participateData.clear();
                        participateData.addAll(cartDataResponse.getResponse().getParticipateData());
                        cartDataList.addAll(cartDataResponse.getResponse().getEventsData());
                        shopDataList.addAll(cartDataResponse.getResponse().getItemsGrouping().getDeliveryInfo());
                        currencyValue = cartDataResponse.getCurrencyValue();
                        currencyCode = cartDataResponse.getCurrrencyCode();
                        adapter = new CartAdapter(mActivity, cartDataList, shopDataList, CartFragment.this, currencyValue, currencyCode, 3);
                        binding.rcvCart.setAdapter(adapter);
                        cartParticipateDataAdapter = new CartParticipateDataAdapter(mActivity, participateData, cartDataResponse.getCurrencyValue(), cartDataResponse.getCurrrencyCode());
                        binding.participateDataList.setAdapter(cartParticipateDataAdapter);
                        if (participateData.isEmpty()) {
                            binding.llRegistration.setVisibility(View.GONE);
                        } else {
                            binding.llRegistration.setVisibility(View.VISIBLE);
                        }
                        if (cartDataResponse.getResponse().getAddonsTotal() > 0) {
                            binding.addOnsPriceTv.setText(Utils.convertPrice(cartDataResponse.getResponse().getAddonsTotal(), cartDataResponse.getCurrencyValue(), 3, "") + " " + cartDataResponse.getCurrrencyCode());
                        } else {
                            binding.addOnsLayout.setVisibility(View.GONE);
                        }

                        if (cartDataResponse.getResponse().getTotalEventDeliveryAmount() > 0) {
                            binding.raceKitDeliveryChargeTV.setText(Utils.convertPrice(cartDataResponse.getResponse().getTotalEventDeliveryAmount(), cartDataResponse.getCurrencyValue(), 3, "") + " " + cartDataResponse.getCurrrencyCode());
                        } else {
                            binding.raceKitDeliveryChargeTr.setVisibility(View.GONE);
                        }

                        if (cartDataResponse.getResponse().getTotalEventDiscount() > 0) {
                            binding.dicountChargeTV.setText(Utils.convertPrice(cartDataResponse.getResponse().getTotalEventDiscount(), cartDataResponse.getCurrencyValue(), 3, "") + " " + cartDataResponse.getCurrrencyCode());
                        } else {
                            binding.dicountChargeTR.setVisibility(View.GONE);
                        }

                        if (cartDataResponse.getResponse().getTotalRefundAmount() > 0) {
                            binding.eventRefundableAmountTV.setText(Utils.convertPrice(cartDataResponse.getResponse().getTotalRefundAmount(), cartDataResponse.getCurrencyValue(), 3, "") + " " + cartDataResponse.getCurrrencyCode());
                        } else {
                            binding.eventRefundableAmountTR.setVisibility(View.GONE);
                        }

                        if (cartDataResponse.getResponse().getTotalItemAmount() > 0) {
                            binding.shopItemCountTV.setText("Item (" + cartDataResponse.getResponse().getTotalItemCount() + ")");
                            binding.shopItemPriceTV.setText(Utils.convertPrice(cartDataResponse.getResponse().getTotalItemAmount(), cartDataResponse.getCurrencyValue(), 3, "") + " " + cartDataResponse.getCurrrencyCode());
                        } else {
                            binding.shopItemDetails.setVisibility(View.GONE);
                        }
                        if (cartDataResponse.getResponse().isAppliedShopCoupon() == 0) {
                            binding.llRemoveCoupon.setVisibility(View.GONE);
                            binding.shopItemDiscountTR.setVisibility(View.GONE);
                        } else {
                            binding.llRemoveCoupon.setVisibility(View.VISIBLE);
                            binding.shopItemDiscountTR.setVisibility(View.VISIBLE);
                            couponCodeValue = cartDataResponse.getResponse().getShopCouponDetail().getCouponCode();
                            couponId = cartDataResponse.getResponse().getShopCouponDetail().getCouponCode();
                            binding.couponCodeTV.setText(couponCodeValue);
                            binding.shopItemDiscountPricesTV.setText(Utils.convertPrice(cartDataResponse.getResponse().getShopCouponDetail().getCouponValue(), cartDataResponse.getCurrencyValue(), 3, "") + " " + cartDataResponse.getCurrrencyCode());
                        }
                        binding.totalPriceTv.setText(Utils.convertPrice(cartDataResponse.getResponse().getSubtotal(), cartDataResponse.getCurrencyValue(), 3, "") + " " + cartDataResponse.getCurrrencyCode());
                        binding.totalDeliveryChargeTV.setText(Utils.convertPrice(cartDataResponse.getResponse().getTotalDeliveryAmount(), cartDataResponse.getCurrencyValue(), 3, "") + " " + cartDataResponse.getCurrrencyCode());
                        binding.totalDiscountTV.setText(Utils.convertPrice(cartDataResponse.getResponse().getTotalDiscount(), cartDataResponse.getCurrencyValue(), 3, "") + " " + cartDataResponse.getCurrrencyCode());
                        binding.grandTotalTextView.setText(Utils.convertPrice(cartDataResponse.getResponse().getGrandTotal(), cartDataResponse.getCurrencyValue(), 3, "") + " " + cartDataResponse.getCurrrencyCode());
                        if (cartDataResponse.getResponse().getTotalDiscount() > 0) {
                            binding.totalDiscountTR.setVisibility(View.VISIBLE);
                        } else {
                            binding.totalDiscountTR.setVisibility(View.GONE);
                        }
                        if (cartDataResponse.getResponse().getTotalDeliveryAmount() > 0) {
                            binding.totalDeliveryChargeTR.setVisibility(View.VISIBLE);
                        } else {
                            binding.totalDeliveryChargeTR.setVisibility(View.GONE);
                        }

                    } else {
                        binding.cartMainLayout.setVisibility(View.GONE);
                        binding.emptyCart.rlParent.setVisibility(View.VISIBLE);

                    }
                    if (cartDataList.isEmpty() && shopDataList.isEmpty()) {
                        binding.cartMainLayout.setVisibility(View.GONE);
                        binding.emptyCart.rlParent.setVisibility(View.VISIBLE);
                        PrefUtils.saveToPrefs(getActivity(), PrefUtils.CART_ITEM, "0");
                    }
                }

                @Override
                protected void onError(RetroError retroError) {
                    mActivity.cancelProgressBar(view);
                    showViewGroup(CartFragment.this::onRetryCall);
                    isError0nCart = true;
//                    PrefUtils.saveToPrefs(getActivity(), PrefUtils.CART_ITEM, cartDataList.size() + "");
//                    binding.cartMainLayout.setVisibility(View.GONE);
//                    binding.emptyCart.rlParent.setVisibility(View.VISIBLE);
//                    binding.emptyCart.btnRegisterEvent.setOnClickListener(v -> {
////                            Intent intent1 = new Intent(getActivity(), RegisterEventActivity.class);
////                            intent1.putExtra("eventId", "0");
////                            startActivityForResult(intent1, 101);
//                        getActivity().setResult(Activity.RESULT_OK);
//                        getActivity().finish();
//                    });

                }

                @Override
                protected void onFailure(String retroError) {
                    mActivity.cancelProgressBar(view);
                    showViewGroup(CartFragment.this::onRetryCall);
                    isError0nCart = true;
                }
            });
        } else {
            showViewGroup(CartFragment.this::onRetryCall);
            isError0nCart = true;
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
        }
    }

    private void hitAddWalletService() {
        if (mActivity.isOnline()) {
            View view = mActivity.showProgressBar();
            String sessionId = PrefUtils.getFromPrefs(getContext(), PrefUtils.SESSION_ID);
            int isLoggedIn = 0;
            String userId = PrefUtils.getFromPrefs(getContext(), PrefUtils.USER_ID);
            if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(getContext(), PrefUtils.USER_ID))) {
                isLoggedIn = 1;
            } else {
                userId = "0";
            }

            String currencyCode = PrefUtils.getFromPrefs(getContext(), PrefUtils.COUNTRY_CODE);
            String parms = currencyCode + "/" + sessionId + "/" + mPaymentType + "/" + isLoggedIn + "/" + userId;//+"?androidcall=2"


            ApiClient.getAPiAuthHelper(getActivity()).addMoney("getapplogin", ApiClient.API_VERSION).enqueue(new Callback<MyWallet>() {
                @Override
                public void onResponse(Call<MyWallet> call, Response<MyWallet> response) {
                    if (response.body() == null || response.body().getResponse() == null) {
                        Toast.makeText(mActivity, mActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (response.body().getStatus().equals("1")) {
                        getActivity().startActivity(new Intent(getActivity(), WebViewActivity.class).putExtra("URL", response.body().getResponse().getPaymentUrl() + parms).putExtra("TitleName", "").putExtra("isCartReset", true));
                        onTrackEventClick(PROCEED_TO_PAYMENT_EVENT_TOKEN);
                    } else {
                        mActivity.showToast(getActivity(), response.body().getResponseMessege());
                    }
                    mActivity.cancelProgressBar(view);
                }

                @Override
                public void onFailure(Call<MyWallet> call, Throwable t) {
                    mActivity.cancelProgressBar(view);
                }
            });
        } else {
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
        }
    }

    private void doPayment() {
        if (mActivity.isOnline()) {
            String sessionId = PrefUtils.getFromPrefs(getContext(), PrefUtils.SESSION_ID);
            int isLoggedIn = 0;
            String userId = PrefUtils.getFromPrefs(getContext(), PrefUtils.USER_ID);
            if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(getContext(), PrefUtils.USER_ID))) {
                isLoggedIn = 1;
            }
            String emailId = TextUtils.isEmpty(PrefUtils.getFromPrefs(getActivity(), PrefUtils.EMAIL_ID)) ? "" : PrefUtils.getFromPrefs(getActivity(), PrefUtils.EMAIL_ID);
            String name = TextUtils.isEmpty(PrefUtils.getFromPrefs(getActivity(), PrefUtils.USER_NAME)) ? "" : PrefUtils.getFromPrefs(getActivity(), PrefUtils.USER_NAME);

            View view = mActivity.showProgressBar();
            AuthApiHelper authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper.class);
            retrofit2.Call<SaveBookingEntity> call = authApiHelper.doPayment("SaveBooking", sessionId, mPaymentType, userId, emailId, name, cartAmount + "", isLoggedIn, 2, ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<SaveBookingEntity>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    SaveBookingEntity saveBookingEntity = (SaveBookingEntity) object;
                    mActivity.cancelProgressBar(view);
                    if (saveBookingEntity == null || saveBookingEntity.getResponse() == null) {
                        Toast.makeText(mActivity, mActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (saveBookingEntity.getResponse().getStatus() == 1) {
                        Intent intent = new Intent(getActivity(), PaymentWebViewActivity.class);
                        intent.putExtra("title", "Payment");
                        intent.putExtra("url", saveBookingEntity.getResponse().getUrl());
                        startActivity(intent);
                    } else {
                        Toast.makeText(mActivity, saveBookingEntity.getResponse().getMsg(), Toast.LENGTH_SHORT).show();
                    }


                }

                @Override
                protected void onError(RetroError retroError) {
                    mActivity.cancelProgressBar(view);
                }

                @Override
                protected void onFailure(String retroError) {
                    mActivity.cancelProgressBar(view);
                }
            });
        } else {
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        getCartData();
    }

    @Override
    public void callbackClick(String cartItemId, String participantId) {
        removeCartDialog(cartItemId, participantId);
    }

    private void removeCartDialog(String cartItemId, String participantId) {
        new CustomAlertDialog(getActivity(), getString(R.string.delete_cart_confirmation_msg), getString(R.string.dialog_ok),
                new CustomAlertDialog.onAlertDialogCustomListener(
                ) {
                    @Override
                    public void onSuccessListener(DialogInterface dialog) {
                        dialog.dismiss();
                        removeCartItem(cartItemId, participantId);
                    }

                    @Override
                    public void onCancelListener() {
                    }
                });
    }

    private void removeCartItem(String bookingId, String participantId) {
        if (mActivity.isOnline()) {
            String sessionId = PrefUtils.getFromPrefs(getActivity(), PrefUtils.SESSION_ID);
            View view = mActivity.showProgressBar();
            AuthApiHelper authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper.class);
            retrofit2.Call<CartDeleteResponse> call = authApiHelper.cartDeleteResponse("DeleteParticipantFromCart", bookingId, participantId, ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<CartDeleteResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    mActivity.cancelProgressBar(view);
                    CartDeleteResponse cartDataResponse = (CartDeleteResponse) object;
                    getCartData();
//                    Toast.makeText(mActivity, cartDataResponse.getResponseMessege(), Toast.LENGTH_SHORT).show();
                }

                @Override
                protected void onError(RetroError retroError) {
                    mActivity.cancelProgressBar(view);
                }

                @Override
                protected void onFailure(String retroError) {
                    mActivity.cancelProgressBar(view);
                }
            });
        } else {
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
        }
    }


    private void infoDialog(String message, String title) {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.info_layout, null);

        TextView tvMessage = alertLayout.findViewById(R.id.tvMessage);
        tvMessage.setText(message);

        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        alert.setPositiveButton("Ok", (dialog, which) -> {
            dialog.dismiss();
        });
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(true);
        AlertDialog dialog = alert.create();

        dialog.show();
    }



    @Override
    public void callbackClick(String cartItemId) {
        removeCartItemDialog(cartItemId);
    }

    @Override
    public void updateCartQty(String bookingId, String qty) {
        updateCartQtyApi(qty, bookingId);
    }

    private void removeCartItemDialog(String cartItemId) {
        new CustomAlertDialog(getActivity(), getString(R.string.delete_cart_confirmation_msg_itme), getString(R.string.dialog_ok),
                new CustomAlertDialog.onAlertDialogCustomListener(
                ) {
                    @Override
                    public void onSuccessListener(DialogInterface dialog) {
                        dialog.dismiss();
                        removeCartItem(cartItemId);
                    }

                    @Override
                    public void onCancelListener() {
                    }
                });
    }

    private void cartItemCount() {
        String sessionId = PrefUtils.getFromPrefs(getContext(), PrefUtils.SESSION_ID);
        AuthApiHelper authApiHelper = ApiClient.getClient(getContext()).create(AuthApiHelper.class);
        authApiHelper.getCartCount("GetCartCount", sessionId, PrefUtils.getFromPrefs(getContext(), PrefUtils.USER_ID), ApiClient.API_VERSION).enqueue(new CallbackManager<CartCountEntity>() {
            @Override
            protected void onSuccess(Object object, String message) {
                CartCountEntity baseResponce = (CartCountEntity) object;
                if (baseResponce.getCartCountResponse() != null) {
                    PrefUtils.saveToPrefs(getContext(), PrefUtils.CART_ITEM, baseResponce.getCartCountResponse().getCart_count() + "");
                }
            }

            @Override
            protected void onError(RetroError retroError) {

            }

            @Override
            protected void onFailure(String retroError) {

            }
        });
    }

    private void removeCartItem(String itemId) {
        if (mActivity.isOnline()) {
            View view = mActivity.showProgressBar();
            AuthApiHelper authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper.class);
            retrofit2.Call<BaseResponce> call = authApiHelper.removeRecodedItemFromCart("del_item", itemId, PrefUtils.getFromPrefs(getContext(), PrefUtils.SESSION_ID), ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<BaseResponce>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    mActivity.cancelProgressBar(view);
                    BaseResponce cartDataResponse = (BaseResponce) object;
                    if (cartDataResponse == null || cartDataResponse == null) {
                        cartItemCount();
                        Toast.makeText(mActivity, mActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    getCartData();
//                    Toast.makeText(mActivity, cartDataResponse.getResponseMessege(), Toast.LENGTH_SHORT).show();
                }

                @Override
                protected void onError(RetroError retroError) {
                    mActivity.cancelProgressBar(view);
                }

                @Override
                protected void onFailure(String retroError) {
                    mActivity.cancelProgressBar(view);
                }
            });
        } else {
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
        }
    }


    @Override
    public void onRetryCall() {
        if (isError0nCart) {
            getCartData();
        }

    }

    @Override
    public void onRetry() {
        if (isError0nCart) {
            getCartData();
        }
    }

    public interface CartCountInterface {
        void cartCountValue(int cartCount);
    }
}
