package provision.com.app.fragment;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.adapter.FeeAdapter;
import provision.com.app.apiResponse.feeDataResponse.EventFee;
import provision.com.app.apiResponse.feeDataResponse.FeeResponse;
import provision.com.app.databinding.FragmentFeeBinding;
import provision.com.app.interfaces.RetryListener;
import provision.com.app.utils.AppConstant;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;

/**
 * Created by gauravg on 7/12/2018.
 */

public class FeeFragment extends BaseFragment implements RetryListener {
    private static FeeFragment feeFragment;
    private FragmentFeeBinding binding;
    private FeeAdapter adapter;
    private BaseActivity mActivity;
    private List<EventFee> eventFeeList = new ArrayList<>();

    public static FeeFragment getInstance() {
        feeFragment = new FeeFragment();
        return feeFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_fee, container, false);
            mActivity = (BaseActivity) getActivity();
            Bundle bundle = getArguments();
            String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
            String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
            String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);

            setFeeAdapter();
            getFeeData(eventId, tabId, tabName);
        }
        return binding.getRoot();
    }


    private void getFeeData(String eventId, String tabId, String tabName) {
        if (mActivity.isOnline()) {
            View view = mActivity.showProgressBar();
            AuthApiHelper authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper.class);
            retrofit2.Call<FeeResponse> call = authApiHelper.feeData("GetEventTab", eventId, tabId, tabName, PrefUtils.getFromPrefs(getContext(), PrefUtils.COUNTRY_CODE),ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<FeeResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    mActivity.cancelProgressBar(view);
                    FeeResponse feeResponse = (FeeResponse) object;
                    if (feeResponse == null || feeResponse.getResponse() == null) {
                        showViewGroup(FeeFragment.this::onRetryCall);
                        Toast.makeText(mActivity, mActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    List<EventFee> eventFee = feeResponse.getResponse().getEventFee();
                    eventFeeList.clear();
                    eventFeeList.addAll(eventFee);
                    adapter.setCurrencyValue(feeResponse.getCurrencyValue());
                    adapter.setNumberFormat(feeResponse.getNumberformat());
                    adapter.setCurrencyCode(feeResponse.getCurrrencyCode());
                    adapter.notifyDataSetChanged();
                }

                @Override
                protected void onError(RetroError retroError) {
                    mActivity.cancelProgressBar(view);
                    showViewGroup(FeeFragment.this::onRetryCall);
                }

                @Override
                protected void onFailure(String retroError) {
                    mActivity.cancelProgressBar(view);
                    showViewGroup(FeeFragment.this::onRetryCall);
                }
            });
        } else {
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
            showViewGroup(FeeFragment.this::onRetryCall);
        }
    }

    private void setFeeAdapter() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        } else {
            binding.rcvFeeList.setLayoutManager(new LinearLayoutManager(getActivity()));
            adapter = new FeeAdapter(getActivity(), eventFeeList);
            binding.rcvFeeList.setAdapter(adapter);
        }

    }


    @Override
    public void onRetryCall() {
        Bundle bundle = getArguments();
        String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
        String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
        String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);
        getFeeData(eventId, tabId, tabName);
    }

    @Override
    public void onRetry() {
        Bundle bundle = getArguments();
        String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
        String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
        String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);
        getFeeData(eventId, tabId, tabName);
    }
}
