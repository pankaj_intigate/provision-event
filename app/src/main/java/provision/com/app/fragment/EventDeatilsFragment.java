package provision.com.app.fragment;


import android.content.Intent;
import android.content.res.Resources;
import androidx.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.CalendarContract;
import androidx.fragment.app.Fragment;
import androidx.core.text.HtmlCompat;
import androidx.viewpager.widget.ViewPager;

import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.adapter.EventDetailViewPagerAdapter;
import provision.com.app.apiResponse.eventDetailResponse.EventDetail;
import provision.com.app.apiResponse.eventDetailResponse.EventDetailResponse;
import provision.com.app.apiResponse.eventDetailResponse.EventImg;
import provision.com.app.databinding.FragmentEventDeatilsBinding;
import provision.com.app.interfaces.RetryListener;
import provision.com.app.utils.AppConstant;
import provision.com.app.utils.Utils;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventDeatilsFragment extends BaseFragment implements RetryListener {

    public static EventDeatilsFragment eventDeatilsFragment;
    private BaseActivity mActivity;
    private FragmentEventDeatilsBinding mBinding;
    private EventDetailViewPagerAdapter eventDetailViewPagerAdapter;
    private CountDownTimer cutdownTimer;
    private List<ImageView> dots;
    private String date = "";

    public static EventDeatilsFragment getInstance() {
        eventDeatilsFragment = new EventDeatilsFragment();
        return eventDeatilsFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_event_deatils, container, false);
        mActivity = (BaseActivity) getActivity();
        Bundle bundle = getArguments();
        String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);
        getEventDetail(eventId);
        return mBinding.getRoot();
    }


    public void getEventDetail(String eventId) {
        AuthApiHelper authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper.class);
        retrofit2.Call<EventDetailResponse> call = authApiHelper.eventDetail("GetEventdetail", eventId, ApiClient.API_VERSION);
        if (mActivity.isOnline()) {
            View view = mActivity.showSplashProgress();


            call.enqueue(new CallbackManager<EventDetailResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {

                    EventDetailResponse eventDetailResponse = (EventDetailResponse) object;
                    if (eventDetailResponse == null || eventDetailResponse.getResponse() == null) {
                        showViewGroup(EventDeatilsFragment.this);
                        Toast.makeText(mActivity, mActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    EventDetail eventDetail = eventDetailResponse.getResponse().getEventDetail();
                    setEventData(eventDetail);
                    List<EventImg> eventImageList = eventDetailResponse.getResponse().getEventImg();
                    setViewPagerDot(eventImageList);
                    mActivity.cancelProgressBar(view);

                }

                @Override
                protected void onError(RetroError retroError) {
                    Log.d("retroError", retroError.getErrorMessage());
                    showViewGroup(EventDeatilsFragment.this);
                    mActivity.cancelProgressBar(view);
                }

                @Override
                protected void onFailure(String retroError) {
                    Log.d("retroError", retroError);
                    showViewGroup(EventDeatilsFragment.this);
                    mActivity.cancelProgressBar(view);
                }
            });
        } else {
            showViewGroup(EventDeatilsFragment.this);
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
        }
    }

    private void addToCalender() {
        Calendar beginTime = Calendar.getInstance();
        beginTime.set(Utils.getSaprateDate(date, "yyyy"), Utils.getSaprateDate(date, "MM") - 1, Utils.getSaprateDate(date, "dd"),
                Utils.getSaprateDate(date, "HH"), Utils.getSaprateDate(date, "mm"));
//        Calendar endTime = Calendar.getInstance();
//        endTime.set(2012, 0, 19, 8, 30);
        if (!Utils.isPackageInstalled("com.google.android.calendar", getContext())) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.calendar")));
            return;
        }
        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
//                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis())
                .putExtra(CalendarContract.Events.TITLE, mBinding.eventName.getText())
                .putExtra(CalendarContract.Events.DESCRIPTION, mBinding.eventDescription.getText())
                .putExtra(CalendarContract.Events.EVENT_LOCATION, mBinding.eventAddress.getText())
                .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY)
                .putExtra(Intent.EXTRA_EMAIL, "");
        startActivity(intent);
    }

    private void setEventData(EventDetail eventDetail) {
        if (eventDetail.getVAddress().length() > 0) {
            StringBuilder sb = new StringBuilder(eventDetail.getVAddress());
            sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
            mBinding.eventAddress.setText(sb.toString());
            mBinding.eventAddress.setOnClickListener(view -> {
                String strUri = "http://maps.google.com/maps?q=loc:" + eventDetail.getVAddressLat() + "," + eventDetail.getVAddressLong() + " (" + eventDetail.getVAddress() + ")";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUri));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);

            });
        }
        if (eventDetail.getVirtualEvent() == 1) {
            mBinding.eventDate.setText("Start Date : " + Utils.parseDateToddMMyyyy(eventDetail.getEventStartDate()));
            mBinding.eventEndDate.setText("End Date : " + Utils.parseDateToddMMyyyy(eventDetail.getEventEndDate()));
            mBinding.eventEndDate.setVisibility(View.VISIBLE);
        } else {
            mBinding.eventDate.setText(Utils.parseDateToddMMyyyy(eventDetail.getEventDate()));
        }

        mBinding.tvTime.setText(Utils.parseDateToHHmm(eventDetail.getEventDate()));
        mBinding.eventName.setText(eventDetail.getVTitle());
        date = eventDetail.getEventDate();
        mBinding.eventDescription.setText(HtmlCompat.fromHtml(HtmlCompat.fromHtml(eventDetail.getVDescription(), HtmlCompat.FROM_HTML_MODE_LEGACY).toString(), HtmlCompat.FROM_HTML_MODE_LEGACY));
        mBinding.eventDescription.setMovementMethod(LinkMovementMethod.getInstance());
        mBinding.tvCalendet.setOnClickListener(v -> addToCalender());
    }

    @Override
    public void onRetryCall() {
        Bundle bundle = getArguments();
        String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);
        getEventDetail(eventId);
    }

    private void setHourDays(long timeDifference) {
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = timeDifference / daysInMilli;
        timeDifference = timeDifference % daysInMilli;
        long elapsedHours = timeDifference / hoursInMilli;
        timeDifference = timeDifference % hoursInMilli;


        long elapsedMinutes = timeDifference / minutesInMilli;
        timeDifference = timeDifference % minutesInMilli;

        long elapsedSeconds = timeDifference / secondsInMilli;

        mBinding.eventHours.setText("" + elapsedHours);
        mBinding.dayTextView.setText("" + elapsedDays);
        mBinding.eventMinute.setText("" + elapsedMinutes);

        if (elapsedHours < 10) {
            mBinding.eventHours.setText("0" + elapsedHours);
        } else {
            mBinding.eventHours.setText("" + elapsedHours);
        }

        if (elapsedDays < 10) {
            mBinding.dayTextView.setText("0" + elapsedDays);
        } else {
            mBinding.dayTextView.setText("" + elapsedDays);
        }

        if (elapsedSeconds < 10) {
            mBinding.eventMinute.setText("0" + elapsedSeconds);
        } else {
            mBinding.eventMinute.setText("" + elapsedSeconds);
        }
    }

    private void setEventFeaturesAdapter(List<EventImg> eventImageList) {
        dots = new ArrayList<>();
        eventDetailViewPagerAdapter = new EventDetailViewPagerAdapter(mActivity, eventImageList);
        mBinding.viewPager.setAdapter(eventDetailViewPagerAdapter);


        mBinding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                selectDot(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }


    public void selectDot(int idx) {
        Resources res = getResources();
        for (int i = 0; i < dots.size(); i++) {
            int drawableId = (i == idx) ? (R.drawable.selected_circle) : (R.drawable.unselected_circle);
            Drawable drawable = res.getDrawable(drawableId);
            dots.get(i).setImageDrawable(drawable);
        }
    }


    private void setViewPagerDot(List<EventImg> eventImageList) {
        mBinding.viewPager.setAdapter(new EventDetailViewPagerAdapter(mActivity, eventImageList));
        if (eventImageList.size() <= 1) {
            mBinding.tabLayout.setVisibility(View.GONE);
            return;
        }
        mBinding.tabLayout.setupWithViewPager(mBinding.viewPager, true);
        ViewGroup slidingTabStrip = (ViewGroup) mBinding.tabLayout.getChildAt(0);

        for (int i = 0; i < slidingTabStrip.getChildCount() - 1; i++) {
            View tab = ((ViewGroup) mBinding.tabLayout.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            p.setMargins(0, 0, 0, 0);

            tab.requestLayout();
        }
    }

    private void setCutDownTimer(long timeInSeconds) {
        cutdownTimer = new CountDownTimer(timeInSeconds, 1000) {
            public void onTick(long millisUntilFinished) {
                setHourDays(millisUntilFinished);
            }

            public void onFinish() {
                // mTextField.setText("done!");
            }

        }.start();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (cutdownTimer != null) {
            cutdownTimer.cancel();
        }
    }

    @Override
    public void onRetry() {
        Bundle bundle = getArguments();
        String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);
        getEventDetail(eventId);
    }
}