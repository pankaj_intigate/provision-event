package provision.com.app.fragment;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.adapter.RaceTrackAdapter;
import provision.com.app.apiResponse.raceTrackResponse.RaceTrackResponse;
import provision.com.app.apiResponse.raceTrackResponse.Racetrack;
import provision.com.app.databinding.FragmentRaceTrackBinding;
import provision.com.app.interfaces.RetryListener;
import provision.com.app.utils.AppConstant;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;

/**
 * Created by gauravg on 7/12/2018.
 */

public class RaceTrackFragment extends BaseFragment implements RetryListener {
    public static RaceTrackFragment raceTrackFragment;
    private FragmentRaceTrackBinding binding;
    private RaceTrackAdapter mAdapter;
    private BaseActivity mActivity;
    private List<Racetrack> racetrackList = new ArrayList<>();

    public static RaceTrackFragment getInstance() {
        raceTrackFragment = new RaceTrackFragment();
        return raceTrackFragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_race_track, container, false);
            mActivity = (BaseActivity) getActivity();

            Bundle bundle = getArguments();
            String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
            String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
            String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);
            getRaceTrackData(eventId, tabId, tabName);

            setRaceTrackAdapter();
        } else {
            setRaceTrackAdapter();
        }


        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void getRaceTrackData(String eventId, String tabId, String tabName) {
        if (mActivity.isOnline()) {
            View view = mActivity.showProgressBar();
            AuthApiHelper authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper.class);
            retrofit2.Call<RaceTrackResponse> call = authApiHelper.raceTrackResponse("GetEventTab", eventId, tabId, tabName,ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<RaceTrackResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    mActivity.cancelProgressBar(view);
                    RaceTrackResponse organiserResponse = (RaceTrackResponse) object;
                    if (organiserResponse == null || organiserResponse.getResponse() == null) {
                        showViewGroup(RaceTrackFragment.this::onRetryCall);
                        Toast.makeText(mActivity, mActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    List<Racetrack> racetrack = organiserResponse.getResponse().getRacetrack();
                    racetrackList.addAll(racetrack);
                    setRaceTrackAdapter();
                }

                @Override
                protected void onError(RetroError retroError) {
                    mActivity.cancelProgressBar(view);
                    showViewGroup(RaceTrackFragment.this::onRetryCall);
                }

                @Override
                protected void onFailure(String retroError) {
                    mActivity.cancelProgressBar(view);
                    showViewGroup(RaceTrackFragment.this::onRetryCall);
                }
            });
        } else {
            showViewGroup(RaceTrackFragment.this::onRetryCall);
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
        }
    }


    private void setRaceTrackAdapter() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        } else {
            binding.rcvRaceTrack.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, true));
            mAdapter = new RaceTrackAdapter(mActivity, racetrackList);
            binding.rcvRaceTrack.setAdapter(mAdapter);
        }

    }

    @Override
    public void onRetryCall() {
        Bundle bundle = getArguments();
        String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
        String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
        String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);
        getRaceTrackData(eventId, tabId, tabName);
    }

    @Override
    public void onRetry() {
        Bundle bundle = getArguments();
        String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
        String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
        String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);
        getRaceTrackData(eventId, tabId, tabName);
    }
}
