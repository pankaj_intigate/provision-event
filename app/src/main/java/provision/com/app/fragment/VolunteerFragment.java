package provision.com.app.fragment;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.adapter.VolunteerAdapter;
import provision.com.app.apiResponse.volunteerResponse.Volunteer;
import provision.com.app.apiResponse.volunteerResponse.VolunteerResponse;
import provision.com.app.databinding.FragmentVolunteerBinding;
import provision.com.app.utils.AppConstant;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;

/**
 * Created by gauravg on 7/12/2018.
 */

public class VolunteerFragment extends BaseFragment {
    public static VolunteerFragment volunteerFragment;
    private FragmentVolunteerBinding binding;
    private VolunteerAdapter mAdapter;
    private BaseActivity mActivity;
    private List<Volunteer> volunteerList = new ArrayList<>();

    public static VolunteerFragment getInstance() {
        volunteerFragment = new VolunteerFragment();
        return volunteerFragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_volunteer, container, false);
            mActivity = (BaseActivity) getActivity();
            Bundle bundle = volunteerFragment.getArguments();
            String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
            String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
            String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);

            setVolunteerAdapter();
            getVolunteerData(eventId, tabId, tabName);
        }
        return binding.getRoot();
    }


    private void setVolunteerAdapter() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        } else {
            binding.rcvVolunteer.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true));
            mAdapter = new VolunteerAdapter(mActivity, volunteerList);
            binding.rcvVolunteer.setAdapter(mAdapter);
        }
    }


    private void getVolunteerData(String eventId, String tabId, String tabName) {
        if (mActivity.isOnline()) {
            View view = mActivity.showProgressBar();
            AuthApiHelper authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper.class);
            retrofit2.Call<VolunteerResponse> call = authApiHelper.volunteerResponse("GetEventTab", eventId, tabId, tabName,ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<VolunteerResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    mActivity.cancelProgressBar(view);
                    VolunteerResponse organiserResponse = (VolunteerResponse) object;
                    if (organiserResponse == null || organiserResponse.getResponse() == null) {
                        Toast.makeText(mActivity, mActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    List<Volunteer> volunteer = organiserResponse.getResponse().getVolunteer();
                    volunteerList.addAll(volunteer);
                    setVolunteerAdapter();
                }

                @Override
                protected void onError(RetroError retroError) {
                    mActivity.cancelProgressBar(view);
                }

                @Override
                protected void onFailure(String retroError) {
                    mActivity.cancelProgressBar(view);
                }
            });
        } else {
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
        }
    }

}
