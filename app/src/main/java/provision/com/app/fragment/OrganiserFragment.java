package provision.com.app.fragment;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.adapter.OrganiserAdapter;
import provision.com.app.apiResponse.organiserResponse.OrganiserResponse;
import provision.com.app.apiResponse.organiserResponse.Organizer;
import provision.com.app.databinding.FragmentOrganiserBinding;
import provision.com.app.interfaces.RetryListener;
import provision.com.app.utils.AppConstant;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;

/**
 * Created by gauravg on 7/12/2018.
 */

public class OrganiserFragment extends BaseFragment implements RetryListener{
    private static OrganiserFragment organiserFragment;
    private FragmentOrganiserBinding binding;
    private OrganiserAdapter mAdapter;
    private List<Organizer> organizerList = new ArrayList<>();
    private BaseActivity mActivity;

    public static OrganiserFragment getInstance() {
        organiserFragment = new OrganiserFragment();
        return organiserFragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_organiser, container, false);
            mActivity = (BaseActivity) getActivity();

            Bundle bundle = getArguments();
            String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
            String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
            String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);

            getOrganiserData(eventId, tabId, tabName);
            setOrganiserAdapter();
        } else {
            setOrganiserAdapter();
        }

        return binding.getRoot();
    }


    private void setOrganiserAdapter() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        } else {
            binding.rcvOrganiser.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            mAdapter = new OrganiserAdapter(mActivity, organizerList);
            binding.rcvOrganiser.setAdapter(mAdapter);
        }
    }


    private void getOrganiserData(String eventId, String tabId, String tabName) {
        if (mActivity.isOnline()) {
            View view = mActivity.showProgressBar();
            AuthApiHelper authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper.class);
            retrofit2.Call<OrganiserResponse> call = authApiHelper.organiseResponse("GetEventTab", eventId, tabId, tabName,ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<OrganiserResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    mActivity.cancelProgressBar(view);
                    OrganiserResponse organiserResponse = (OrganiserResponse) object;
                    if (organiserResponse == null || organiserResponse.getResponse() == null) {
                        Toast.makeText(mActivity, mActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        showViewGroup(OrganiserFragment.this::onRetryCall);
                        return;
                    }
                    List<Organizer> organizer = organiserResponse.getResponse().getOrganizer();
                    organizerList.addAll(organizer);
                    setOrganiserAdapter();
                }

                @Override
                protected void onError(RetroError retroError) {
                    mActivity.cancelProgressBar(view);
                    showViewGroup(OrganiserFragment.this::onRetryCall);
                }

                @Override
                protected void onFailure(String retroError) {
                    mActivity.cancelProgressBar(view);
                    showViewGroup(OrganiserFragment.this::onRetryCall);
                }
            });
        } else {
            showViewGroup(OrganiserFragment.this::onRetryCall);
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
        }
    }

    @Override
    public void onRetryCall() {
        Bundle bundle = getArguments();
        String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
        String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
        String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);

        getOrganiserData(eventId, tabId, tabName);
    }

    @Override
    public void onRetry() {
        Bundle bundle = getArguments();
        String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
        String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
        String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);

        getOrganiserData(eventId, tabId, tabName);
    }
}
