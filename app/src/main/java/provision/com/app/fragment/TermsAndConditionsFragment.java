package provision.com.app.fragment;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.text.HtmlCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.apiResponse.termsConditionsResponse.TermsConditionsResponse;
import provision.com.app.databinding.FragmentTermsConditionsBinding;
import provision.com.app.utils.AppConstant;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;

/**
 * Created by gauravg on 8/13/2018.
 */

public class TermsAndConditionsFragment extends BaseFragment {
    public static TermsAndConditionsFragment termsAndConditionsFragment;
    private FragmentTermsConditionsBinding binding;
    private BaseActivity baseActivity;

    public static TermsAndConditionsFragment getInstance() {
        termsAndConditionsFragment = new TermsAndConditionsFragment();
        return termsAndConditionsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_terms_conditions, container, false);
            baseActivity = (BaseActivity) getActivity();
            Bundle bundle = termsAndConditionsFragment.getArguments();
            String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
            String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
            String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);
            if (baseActivity.isOnline()) {
                getTermsAndConditions(eventId, tabId, tabName);
            } else {

            }
        }

        return binding.getRoot();
    }

    private void getTermsAndConditions(String eventId, String tabId, String tabName) {
        View view = baseActivity.showProgressBar();
        AuthApiHelper authApiHelper = ApiClient.getClient(baseActivity).create(AuthApiHelper.class);
        retrofit2.Call<TermsConditionsResponse> call = authApiHelper.termsConditionsResponse("GetEventTab", eventId, tabId, tabName,ApiClient.API_VERSION);
        call.enqueue(new CallbackManager<TermsConditionsResponse>() {
            @Override
            protected void onSuccess(Object object, String message) {
                baseActivity.cancelProgressBar(view);
                TermsConditionsResponse response = (TermsConditionsResponse) object;
                if (response == null || response.getResponse() == null) {
                    Toast.makeText(baseActivity, baseActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (response.getStatus().equals("1")) {
                    //binding.tvTitle.setText(response.getResponse().getTermcondition().getVTitle());
                    binding.tvDescription.setText(HtmlCompat.fromHtml(response.getResponse().getTermcondition().getTTerms(),HtmlCompat.FROM_HTML_MODE_LEGACY));
                }
            }

            @Override
            protected void onError(RetroError retroError) {
                baseActivity.cancelProgressBar(view);
            }

            @Override
            protected void onFailure(String retroError) {
                baseActivity.cancelProgressBar(view);
            }
        });
    }


}
