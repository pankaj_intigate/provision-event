package provision.com.app.fragment;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.adapter.SponsarAdapter;
import provision.com.app.apiResponse.sponserResponse.Sponser;
import provision.com.app.apiResponse.sponserResponse.SponsorsResponse;
import provision.com.app.databinding.FragmentOrganiserBinding;
import provision.com.app.interfaces.RetryListener;
import provision.com.app.utils.AppConstant;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;

/**
 * Created by gauravg on 7/12/2018.
 */

public class SponsorsFragment extends BaseFragment implements RetryListener{
    public static SponsorsFragment sponsosFragment;
    private FragmentOrganiserBinding binding;
    private SponsarAdapter adapter;
    private BaseActivity mActivity;
    private List<Sponser> sponserList = new ArrayList<>();

    public static SponsorsFragment getInstance() {
        sponsosFragment = new SponsorsFragment();
        return sponsosFragment;
    }

    @Nullable
    @Override

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_organiser, container, false);
            mActivity = (BaseActivity) getActivity();

            Bundle bundle = getArguments();
            String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
            String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
            String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);
            getOrganiserData(eventId, tabId, tabName);

            setSponsorAdapter();
        }
        return binding.getRoot();
    }


    private void getOrganiserData(String eventId, String tabId, String tabName) {
        if (mActivity.isOnline()) {
            View view = mActivity.showProgressBar();
            AuthApiHelper authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper.class);
            retrofit2.Call<SponsorsResponse> call = authApiHelper.sponserResponse("GetEventTab", eventId, tabId, tabName,ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<SponsorsResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    mActivity.cancelProgressBar(view);
                    SponsorsResponse organiserResponse = (SponsorsResponse) object;
                    if (organiserResponse == null || organiserResponse.getResponse() == null) {
                        Toast.makeText(mActivity, mActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        showViewGroup(SponsorsFragment.this);
                        return;
                    }
                    List<Sponser> sponsar = organiserResponse.getResponse().getSponser();
                    sponserList.addAll(sponsar);
                    setSponsorAdapter();
                }

                @Override
                protected void onError(RetroError retroError) {
                    mActivity.cancelProgressBar(view);
                    showViewGroup(SponsorsFragment.this);
                }

                @Override
                protected void onFailure(String retroError) {
                    mActivity.cancelProgressBar(view);
                    showViewGroup(SponsorsFragment.this);
                }
            });
        } else {
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
            showViewGroup(SponsorsFragment.this);
        }
    }

    private void setSponsorAdapter() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        } else {
            binding.rcvOrganiser.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            adapter = new SponsarAdapter(mActivity, sponserList);
            binding.rcvOrganiser.setAdapter(adapter);
        }

    }

    @Override
    public void onRetryCall() {
        Bundle bundle = getArguments();
        String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
        String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
        String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);
        getOrganiserData(eventId, tabId, tabName);
    }

    @Override
    public void onRetry() {
        Bundle bundle = getArguments();
        String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
        String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
        String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);
        getOrganiserData(eventId, tabId, tabName);
    }
}
