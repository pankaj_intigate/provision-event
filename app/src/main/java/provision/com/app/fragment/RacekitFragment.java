package provision.com.app.fragment;


import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.adapter.RaceKitAdapter;
import provision.com.app.apiResponse.RacekitResponse.Racekit;
import provision.com.app.apiResponse.RacekitResponse.RacekitResponse;
import provision.com.app.databinding.FragmentRacekitBinding;
import provision.com.app.interfaces.RetryListener;
import provision.com.app.model.DataModel;
import provision.com.app.utils.AppConstant;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;

/**
 * A simple {@link Fragment} subclass.
 */
public class RacekitFragment extends BaseFragment implements RetryListener {
    public static RacekitFragment racekitFragment;
    private List<DataModel> dataModelList;
    private RaceKitAdapter adapter;
    private BaseActivity mActivity;
    private FragmentRacekitBinding binding;
    private List<Racekit> racekitList = new ArrayList<>();

    public static RacekitFragment getInstance() {
        racekitFragment = new RacekitFragment();
        return racekitFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_racekit, container, false);
            mActivity = (BaseActivity) getActivity();
            Bundle bundle = getArguments();
            String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
            String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
            String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);

            getRecekitData(eventId, tabId, tabName);
            setReckitAdapter();
        } else {
            setReckitAdapter();
        }
        return binding.getRoot();
    }


    private void getRecekitData(String eventId, String tabId, String tabName) {
        if (mActivity.isOnline()) {
            View view = mActivity.showProgressBar();
            AuthApiHelper authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper.class);
            retrofit2.Call<RacekitResponse> call = authApiHelper.racekitResponse("GetEventTab", eventId, tabId, tabName,ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<RacekitResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    mActivity.cancelProgressBar(view);
                    RacekitResponse racekitResponse = (RacekitResponse) object;
                    if (racekitResponse == null || racekitResponse.getResponse() == null) {
                        showViewGroup(RacekitFragment.this::onRetryCall);
                        Toast.makeText(mActivity, mActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    List<Racekit> racekit = racekitResponse.getResponse().getRacekit();
                    if (racekit!=null){
                        racekitList.addAll(racekit);
                        setReckitAdapter();
                    }
                    assert racekit != null;
                    racekitList.addAll(racekit);
                    setReckitAdapter();
                }

                @Override
                protected void onError(RetroError retroError) {
                    mActivity.cancelProgressBar(view);
                    showViewGroup(RacekitFragment.this::onRetryCall);
                }

                @Override
                protected void onFailure(String retroError) {
                    mActivity.cancelProgressBar(view);
                    showViewGroup(RacekitFragment.this::onRetryCall);
                }
            });
        } else {
            showViewGroup(RacekitFragment.this::onRetryCall);
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
        }
    }


    private void setReckitAdapter() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        } else {
            binding.racekitRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
            adapter = new RaceKitAdapter(mActivity, racekitList);
            binding.racekitRecyclerView.setAdapter(adapter);
        }
    }


    @Override
    public void onRetryCall() {
        Bundle bundle = getArguments();
        String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
        String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
        String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);
        getRecekitData(eventId, tabId, tabName);
    }

    @Override
    public void onRetry() {
        Bundle bundle = getArguments();
        String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
        String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
        String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);
        getRecekitData(eventId, tabId, tabName);
    }
}
