package provision.com.app.fragment;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.activity.ResultActivity;
import provision.com.app.binder.SettingFragmentBinder;
import provision.com.app.databinding.FragmentSettingBinding;
import provision.com.app.utils.PrefUtils;

/**
 * Created by gauravg on 7/17/2018.
 */

public class
SettingFragment extends BaseFragment {
    private static SettingFragment settingFragment;
    private FragmentSettingBinding binding;
    private BaseActivity activity;

    public static SettingFragment newInstance() {
        if (settingFragment == null) {
            settingFragment = new SettingFragment();
        }
        return settingFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_setting, container, false);
        activity = (BaseActivity) getActivity();
        SettingFragmentBinder settingFragmentBinder = new SettingFragmentBinder(activity, binding);
        binding.setSetting(settingFragmentBinder);
        binding.rlMyWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, ResultActivity.class);
                startActivity(intent);
            }
        });

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(activity, PrefUtils.USER_ID))) {
//            binding.rlSignInSignUp.setVisibility(View.GONE);
//            binding.rlUserProfile.setVisibility(View.VISIBLE);
//            binding.tvUserProfile.setText(PrefUtils.getFromPrefs(activity, PrefUtils.USER_NAME));
        }
    }
}
