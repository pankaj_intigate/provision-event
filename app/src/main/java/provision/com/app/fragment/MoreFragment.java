package provision.com.app.fragment;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.activity.ChangePasswordActivity;
import provision.com.app.activity.HomeActivity;
import provision.com.app.activity.InboxActivity;
import provision.com.app.activity.LoginActivity;
import provision.com.app.activity.MyEventsActivity;
import provision.com.app.activity.MyProfileActivity;
import provision.com.app.activity.MyWalletActivity;
import provision.com.app.activity.ResultActivity;
import provision.com.app.activity.WebViewActivity;
import provision.com.app.apiResponse.BaseResponce;
import provision.com.app.apiResponse.getCurrencyCodeResponse.GetCurrencyCodeResponse;
import provision.com.app.apiResponse.getProfileResponse.CountryCode;
import provision.com.app.databinding.FragmentMoreBinding;
import provision.com.app.interfaces.UpdateCartCount;
import provision.com.app.shop.AddAddressActivity;
import provision.com.app.shop.ShopProductListActivity;
import provision.com.app.shop.my_order.MyOrderActivity;
import provision.com.app.shop.wishlist.WishlistActivity;
import provision.com.app.utils.CustomAlertDialog;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.network.api.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static provision.com.app.app.AppConstant.ABOUT_US_EVENT_TOKEN;
import static provision.com.app.app.AppConstant.CHANGE_PASSWORD_EVENT_TOKEN;
import static provision.com.app.app.AppConstant.CONTACT_US_EVENT_TOKEN;
import static provision.com.app.app.AppConstant.INSTAGRAM_EVENT_TOKEN;
import static provision.com.app.app.AppConstant.LOGOUT_EVENT_TOKEN;
import static provision.com.app.app.AppConstant.MY_ACCOUNT_EVENT_TOKEN;
import static provision.com.app.app.AppConstant.MY_LOCATION_EVENT_TOKEN;
import static provision.com.app.app.AppConstant.PRIVACY_POLICY_EVENT_TOKEN;
import static provision.com.app.app.AppConstant.TERMS_AND_CONDITIONS_EVENT_TOKEN;
import static provision.com.app.utils.CommonUtils.onTrackEventClick;

/**
 * Created by gauravg on 7/5/2018.
 */

public class MoreFragment extends BaseFragment {
    public static final int LOGIN_REQUEST_CODE = 1002;
    private static MoreFragment moreFragment;
    private static UpdateCartCount mupdateCartCount;
    int selectedPosition = 0;
    private FragmentMoreBinding binding;
    private String[] categoryArray;
    private String[] countryArray;
    private List<provision.com.app.apiResponse.getCurrencyCodeResponse.CountryCode> countryCodeList;
    private BaseActivity activity;

    public static MoreFragment newInstance(UpdateCartCount updateCartCount) {
        mupdateCartCount = updateCartCount;
        moreFragment = new MoreFragment();
        return moreFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_more, container, false);
        activity = (BaseActivity) getActivity();
        binding.emailTextView.setText(PrefUtils.getFromPrefs(activity, PrefUtils.EMAIL_ID));
       /* binding.nameTextView.setText(PrefUtils.getFromPrefs(activity,PrefUtils.USER_NAME));
        binding.mobileTextView.setText(PrefUtils.getFromPrefs(activity,PrefUtils.MOBILE_NO));*/
        getCountryCode(false);
        if (PrefUtils.getFromPrefs(activity, PrefUtils.NOTIFICATION_ENABLE).equals("1")) {
            binding.notificationSwitch.setChecked(true);
        } else {
            binding.notificationSwitch.setChecked(false);
        }

        try {
            PackageManager manager = getContext().getPackageManager();
            PackageInfo info = manager.getPackageInfo(
                    getContext().getPackageName(), 0);
            String verName = info.versionName;
            binding.versionNameTxt.setText("Version " + verName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        binding.notificationSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            enableDissableNotification(PrefUtils.getFromPrefs(activity, PrefUtils.USER_ID));

        });

        binding.tvCountry.setText(PrefUtils.getFromPrefs(activity, PrefUtils.COUNTRY_NAME) + " (" + PrefUtils.getFromPrefs(activity, PrefUtils.COUNTRY_CODE) + ")");
        binding.rlProfile.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(activity, PrefUtils.USER_ID))) {
                startActivity(new Intent(activity, MyProfileActivity.class));
            } else {
                activity.startActivityForResult(new Intent(activity, LoginActivity.class), LOGIN_REQUEST_CODE);
            }
        });
        binding.rlMyEvents.setOnClickListener(v -> {
            Intent eventDetailIntent = new Intent(activity, MyEventsActivity.class);
            activity.startActivityForResult(eventDetailIntent, 101);
        });

        binding.rlMyWallet.setOnClickListener(v -> {
            Intent eventDetailIntent = new Intent(activity, MyWalletActivity.class);
            activity.startActivityForResult(eventDetailIntent, 101);
        });

        binding.rlChangePassword.setOnClickListener(v -> {
            startActivity(new Intent(activity, ChangePasswordActivity.class));
            onTrackEventClick(CHANGE_PASSWORD_EVENT_TOKEN);
        });

        binding.rlInbox.setOnClickListener(v -> {
            startActivity(new Intent(activity, InboxActivity.class));
        });

        binding.rlShop.setOnClickListener(v -> {
            startActivity(new Intent(activity, ShopProductListActivity.class));
        });

        binding.rlLogout.setOnClickListener(v -> {
            logOutDialog();
        });
        binding.rlCountry.setOnClickListener(v -> {
            if (countryCodeList == null || countryCodeList.size() == 0) {
                getCountryCode(true);
            } else {
                visiblePickerAnimation();
            }

            onTrackEventClick(MY_LOCATION_EVENT_TOKEN);
        });
        binding.rlContactUs.setOnClickListener(v -> {
            try {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + "hi@wesuffix.com"));
                intent.putExtra(Intent.EXTRA_SUBJECT, "");
                intent.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                //TODO smth
            }

            onTrackEventClick(CONTACT_US_EVENT_TOKEN);
        });
        binding.rlResult.setOnClickListener(v -> {

            Intent intent = new Intent(activity, ResultActivity.class);
            startActivity(intent);
        });

        binding.rlInstagram.setOnClickListener(v -> {
            activity.startActivity(new Intent(activity, WebViewActivity.class).putExtra("URL", ApiClient.INSTAGRAM).putExtra("TitleName", "Instagram"));
            onTrackEventClick(INSTAGRAM_EVENT_TOKEN);
        });
        binding.rlYouTube.setOnClickListener(v -> {
            activity.startActivity(new Intent(activity, WebViewActivity.class).putExtra("URL", ApiClient.YOUTUBE).putExtra("TitleName", "Youtube"));
        });
        binding.rlfacebook.setOnClickListener(v -> {
            activity.startActivity(new Intent(activity, WebViewActivity.class).putExtra("URL", ApiClient.FACEBOOK).putExtra("TitleName", "Facebook"));
        });
        binding.rlHelp.setOnClickListener(v -> {
            activity.startActivity(new Intent(activity, WebViewActivity.class).putExtra("URL", ApiClient.HELP_URL).putExtra("TitleName", "Help"));
        });

        binding.rlAboutUs.setOnClickListener(v -> {
            activity.startActivity(new Intent(activity, WebViewActivity.class).putExtra("URL", ApiClient.ABOUT_US_URL).putExtra("TitleName", "About Us"));
            onTrackEventClick(ABOUT_US_EVENT_TOKEN);
        });

        binding.rlPrivacyPolicy.setOnClickListener(v -> {
            activity.startActivity(new Intent(activity, WebViewActivity.class).putExtra("URL", ApiClient.PRIVACY_POLICY_URL).putExtra("TitleName", "Privacy Policy"));
            onTrackEventClick(PRIVACY_POLICY_EVENT_TOKEN);
        });

        binding.rlTermsAndConditions.setOnClickListener(v -> {
            activity.startActivity(new Intent(activity, WebViewActivity.class).putExtra("URL", ApiClient.TERMS_N_CONDITIONS_URL).putExtra("TitleName", "Terms & Conditions"));
            onTrackEventClick(TERMS_AND_CONDITIONS_EVENT_TOKEN);
        });
        binding.confirmPicker.setOnClickListener(v -> {
            inVisiblePickerAnimation();
            int position = binding.numberPicker.getValue();
            PrefUtils.saveToPrefs(activity, PrefUtils.COUNTRY_CODE, countryCodeList.get(position).getCode());
            PrefUtils.saveToPrefs(activity, PrefUtils.COUNTRY_NAME, countryCodeList.get(position).getTitle());
            binding.tvCountry.setText(PrefUtils.getFromPrefs(activity, PrefUtils.COUNTRY_NAME) + " (" + PrefUtils.getFromPrefs(activity, PrefUtils.COUNTRY_CODE) + ")");
        });
        binding.cancelPicker.setOnClickListener(v -> {
            inVisiblePickerAnimation();
        });

        binding.rlMyOrder.setOnClickListener(v->{
             startActivity(new Intent(getActivity(), MyOrderActivity.class));
        });

        binding.rlAddress.setOnClickListener(v->{
            startActivity(new Intent(getActivity(), AddAddressActivity.class).putExtra("isMore","true"));
        });

        binding.rlWishList.setOnClickListener(v->{
            startActivity(new Intent(getActivity(), WishlistActivity.class));
        });
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(activity, PrefUtils.USER_ID)) && !PrefUtils.getFromPrefs(activity, PrefUtils.USER_TYPE).equals("5")) {

            binding.nameTextView.setText(PrefUtils.getFromPrefs(activity, PrefUtils.USER_NAME));
            binding.mobileTextView.setText(PrefUtils.getFromPrefs(activity, PrefUtils.MOBILE_NO));
            binding.userDetailsLayout.setVisibility(View.VISIBLE);
            binding.ivEdit.setVisibility(View.VISIBLE);
            binding.loginLayout.setVisibility(View.GONE);
            binding.withoutLoginArrow.setVisibility(View.GONE);
            binding.loginUserOptionLayout.setVisibility(View.VISIBLE);
            binding.rlLogout.setVisibility(View.VISIBLE);
//            binding.rlNotification.setVisibility(View.VISIBLE);
//            binding.actionSettings.setVisibility(View.VISIBLE);


            if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(activity, PrefUtils.USER_IMAGE))) {
                Picasso.get()
                        .load(PrefUtils.getFromPrefs(activity, PrefUtils.USER_IMAGE))
                        .placeholder(R.drawable.profile_p)
                        .into(binding.ivProfile);
            } else {
                binding.ivProfile.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.profile_p));
            }
        } else {
//            binding.rlNotification.setVisibility(View.GONE);
//            binding.actionSettings.setVisibility(View.GONE);
            binding.rlLogout.setVisibility(View.GONE);
            binding.loginUserOptionLayout.setVisibility(View.GONE);
            binding.userDetailsLayout.setVisibility(View.GONE);
            binding.ivEdit.setVisibility(View.GONE);
            binding.loginLayout.setVisibility(View.VISIBLE);
            binding.withoutLoginArrow.setVisibility(View.VISIBLE);
        }

        onTrackEventClick(MY_ACCOUNT_EVENT_TOKEN);
    }

    private void enableDissableNotification(String userId) {

                String newToken = PrefUtils.getFromPrefs(getContext() ,PrefUtils.TOKEN);;
                Log.d("MyFirebaseMsgService", "Refreshed token: " + newToken);

                View view = activity.showProgressBar();
                ApiClient.getAPiAuthHelper(activity).updateNotify("updateNotify", newToken, ApiClient.API_VERSION).enqueue(new Callback<BaseResponce>() {
                    @Override
                    public void onResponse(Call<BaseResponce> call, Response<BaseResponce> response) {
                        if (PrefUtils.getFromPrefs(activity, PrefUtils.NOTIFICATION_ENABLE).equals("0")) {
                            PrefUtils.saveToPrefs(activity, PrefUtils.NOTIFICATION_ENABLE, "1");
                        } else {
                            PrefUtils.saveToPrefs(activity, PrefUtils.NOTIFICATION_ENABLE, "0");
                        }
                        activity.cancelProgressBar(view);
                    }

                    @Override
                    public void onFailure(Call<BaseResponce> call, Throwable t) {
                        activity.cancelProgressBar(view);
                    }
                });

    }

    private void logOutDialog() {
        new CustomAlertDialog(activity, getString(R.string.logout_confirmation_msg), getString(R.string.logout),
                new CustomAlertDialog.onAlertDialogCustomListener(
                ) {
                    @Override
                    public void onSuccessListener(DialogInterface dialog) {
                        dialog.dismiss();
                        String timeInMillis = PrefUtils.getFromPrefs(activity, PrefUtils.SESSION_ID);
                        String cartCount = PrefUtils.getFromPrefs(activity, PrefUtils.CART_ITEM);
                        String notificationSettings = PrefUtils.getFromPrefs(activity, PrefUtils.NOTIFICATION_ENABLE);
                        PrefUtils.logout(activity);
                        PrefUtils.saveToPrefs(activity, PrefUtils.COUNTRY_CODE, "KWD");
                        PrefUtils.saveToPrefs(activity, PrefUtils.COUNTRY_NAME, "Kuwait");
                        PrefUtils.saveToPrefs(activity, PrefUtils.SESSION_ID, timeInMillis);
                        PrefUtils.saveToPrefs(activity, PrefUtils.CART_ITEM, cartCount);
                        PrefUtils.saveToPrefs(activity, PrefUtils.NOTIFICATION_ENABLE, notificationSettings);
                        ((HomeActivity) activity).openFragment(HomeFragment.newInstance());
                        ((HomeActivity) activity).onBottomItemSelected(R.id.action_home);
                        mupdateCartCount.upDateCartCount();
                        onTrackEventClick(LOGOUT_EVENT_TOKEN);
                    }

                    @Override
                    public void onCancelListener() {

                    }
                });
    }

    private void getCountryCode(boolean isShow) {
        ApiClient.getAPiAuthHelper(activity).getCurrencyCode("getCurrencyCode", ApiClient.API_VERSION).enqueue(new Callback<GetCurrencyCodeResponse>() {
            @Override
            public void onResponse(Call<GetCurrencyCodeResponse> call, Response<GetCurrencyCodeResponse> response) {
                if (response.body() == null || response.body().getResponse() == null) {
                    Toast.makeText(activity, activity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (response.body().getStatus().equals("1")) {

                    if (response.body().getResponse().getCountryCode() != null && response.body().getResponse().getCountryCode().size() > 0) {
                        //Set Country-Code'Data.

                        countryArray = new String[response.body().getResponse().getCountryCode().size()];
                        countryCodeList = new ArrayList<>();
                        provision.com.app.apiResponse.getCurrencyCodeResponse.CountryCode countryCode;
                        for (int i = 0; i < response.body().getResponse().getCountryCode().size(); i++) {
                            countryArray[i] = response.body().getResponse().getCountryCode().get(i).getTitle();
                            countryCode = new provision.com.app.apiResponse.getCurrencyCodeResponse.CountryCode();
                            countryCode.setCode(response.body().getResponse().getCountryCode().get(i).getCode());
                            countryCode.setTitle(response.body().getResponse().getCountryCode().get(i).getTitle());
                            countryCodeList.add(countryCode);
                            if (PrefUtils.getFromPrefs(activity, PrefUtils.COUNTRY_NAME).equalsIgnoreCase(response.body().getResponse().getCountryCode().get(i).getTitle())) {
                                selectedPosition = i;
                            }
                        }
                    }
                    if (isShow) {
                        visiblePickerAnimation();
                    }
                    binding.numberPicker.setMinValue(0);
                    binding.numberPicker.setMaxValue(countryArray.length - 1);
                    binding.numberPicker.setDisplayedValues(countryArray);
                    binding.numberPicker.setValue(selectedPosition);
                }
            }

            @Override
            public void onFailure(Call<GetCurrencyCodeResponse> call, Throwable t) {

            }
        });
    }

    private void getCountryCode1(boolean isShow) {
//        ApiClient.getAPiAuthHelper(activity).getCountryCode("getCountryCode").enqueue(new Callback<MyAccountDetails>() {
//            @Override
//            public void onResponse(Call<MyAccountDetails> call, ShopItemEventResponse<MyAccountDetails> response) {
//                if (response.body().getStatus().equals("1")) {
//                    CountryCode countryCode;
//                    for (int i = 0; i < response.body().getResponse().getCountryCode().size(); i++) {
//                        countryCode = new CountryCode();
//                        countryCode.setDialCode(response.body().getResponse().getCountryCode().get(i).getDialCode());
//                        countryCode.setCode(response.body().getResponse().getCountryCode().get(i).getCode());
//                        countryCode.setName(response.body().getResponse().getCountryCode().get(i).getName());
//                        countryCode.setCountryCodeEnable(true);
//                        countryCodeList.add(countryCode);
//                    }
//                    getCategoryArray(countryCodeList);
//                    if (isShow) {
//                        visiblePickerAnimation();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<MyAccountDetails> call, Throwable t) {
//
//            }
//        });
    }

    private void visiblePickerAnimation() {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(),
                R.anim.bottom_up);
        binding.selectedLayout.startAnimation(bottomUp);
        binding.selectedLayout.setVisibility(View.VISIBLE);
        binding.numberPicker.setValue(selectedPosition);
    }

    private void getCategoryArray(List<CountryCode> countryCodeList) {

        categoryArray = new String[countryCodeList.size()];
        for (int i = 0; i < countryCodeList.size(); i++) {
            categoryArray[i] = countryCodeList.get(i).getName();
        }

        binding.numberPicker.setMinValue(0);
        binding.numberPicker.setMaxValue(categoryArray.length - 1);
        binding.numberPicker.setDisplayedValues(categoryArray);
        binding.numberPicker.setValue(0);
    }

    private void inVisiblePickerAnimation() {
        if (binding.selectedLayout.getVisibility() == View.VISIBLE) {
            Animation bottomUp = AnimationUtils.loadAnimation(activity,
                    R.anim.bottom_down);
            binding.selectedLayout.startAnimation(bottomUp);
            binding.selectedLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRetryCall() {
        super.onRetryCall();
        Toast.makeText(activity, "working", Toast.LENGTH_SHORT).show();
    }
}
