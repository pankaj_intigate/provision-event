package provision.com.app.fragment;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.adapter.FAQAdapter;
import provision.com.app.apiResponse.faqResponse.EventFaq;
import provision.com.app.apiResponse.faqResponse.FAQResponse;
import provision.com.app.databinding.FragmentFaqBinding;
import provision.com.app.interfaces.RetryListener;
import provision.com.app.utils.AppConstant;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;

/**
 * Created by gauravg on 7/12/2018.
 */

public class FAQFragment extends BaseFragment implements RetryListener {
    public static FAQFragment faqFragment;
    List<EventFaq> dataModelList = new ArrayList<>();
    private FragmentFaqBinding binding;
    private FAQAdapter adapter;
    private BaseActivity baseActivity;

    public static FAQFragment getInstance() {
        faqFragment = new FAQFragment();
        return faqFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_faq, container, false);
            baseActivity = (BaseActivity) getActivity();
            Bundle bundle = getArguments();
            String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
            String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
            String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);

            setFaqAdapter();

            if (baseActivity.isOnline()) {
                getFAQData(eventId, tabId, tabName);
            } else {
                showViewGroup(FAQFragment.this);
                baseActivity.showToast(baseActivity, baseActivity.getResources().getString(R.string.check_internet));
            }
        }
        return binding.getRoot();
    }

    private void getFAQData(String eventId, String tabId, String tabName) {
        View view = baseActivity.showProgressBar();
        AuthApiHelper authApiHelper = ApiClient.getClient(baseActivity).create(AuthApiHelper.class);
        retrofit2.Call<FAQResponse> call = authApiHelper.faqResponse("GetEventTab", eventId, tabId, tabName,ApiClient.API_VERSION);
        call.enqueue(new CallbackManager<FAQResponse>() {
            @Override
            protected void onSuccess(Object object, String message) {
                baseActivity.cancelProgressBar(view);
                FAQResponse faqResponse = (FAQResponse) object;
                if (faqResponse == null || faqResponse.getResponse() == null) {
                    Toast.makeText(baseActivity, baseActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    showViewGroup(FAQFragment.this);
                    return;
                }
                if (faqResponse.getStatus().equals("1")) {
                    List<EventFaq> faqList = faqResponse.getResponse().getEventFaq();
                    if (faqList != null && faqList.size() > 0) {
                        dataModelList.addAll(faqList);
                        setFaqAdapter();
                    }
                } else {
                    baseActivity.showToast(baseActivity, faqResponse.getResponseMessege());
                }
            }

            @Override
            protected void onError(RetroError retroError) {
                baseActivity.cancelProgressBar(view);
                showViewGroup(FAQFragment.this);
            }

            @Override
            protected void onFailure(String retroError) {
                baseActivity.cancelProgressBar(view);
                showViewGroup(FAQFragment.this);
            }

        });
    }

    private void setFaqAdapter() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        } else {
            binding.rcvFaqList.setLayoutManager(new LinearLayoutManager(getActivity()));
            adapter = new FAQAdapter(getActivity(), dataModelList);
            binding.rcvFaqList.setAdapter(adapter);
        }
    }

    @Override
    public void onRetryCall() {

        if (baseActivity.isOnline()) {
            Bundle bundle = getArguments();
            String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
            String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
            String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);
            getFAQData(eventId, tabId, tabName);
        } else {
            showViewGroup(FAQFragment.this);
            baseActivity.showToast(baseActivity, baseActivity.getResources().getString(R.string.check_internet));
        }
    }

    @Override
    public void onRetry() {
        if (baseActivity.isOnline()) {
            Bundle bundle = getArguments();
            String tabName = bundle.getString(AppConstant.KEY_SELECTED_TAB_NAME);
            String tabId = bundle.getString(AppConstant.KEY_SELECTED_TAB_ID);
            String eventId = bundle.getString(AppConstant.KEY_SELECTED_TAB_EVENT_ID);
            getFAQData(eventId, tabId, tabName);
        } else {
            showViewGroup(FAQFragment.this);
            baseActivity.showToast(baseActivity, baseActivity.getResources().getString(R.string.check_internet));
        }
    }
}
