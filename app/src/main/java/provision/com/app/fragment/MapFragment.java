package provision.com.app.fragment;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import provision.com.app.R;
import provision.com.app.databinding.FragmentMapBinding;

/**
 * Created by gauravg on 7/12/2018.
 */

public class MapFragment extends BaseFragment implements OnMapReadyCallback{
    double vAddressLong, vAddressLat;
    private FragmentMapBinding binding;
    private GoogleMap mMap;
    private FragmentActivity mActivity;

    public MapFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_map, container, false);
        mActivity = getActivity();
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(this);
        mapFragment.getMapAsync(this);
        return binding.getRoot();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Bundle bundle = getArguments();
        try {
            if (!bundle.getString("latitude").isEmpty() && !bundle.getString("longitude").isEmpty()) {
                double lat = Double.parseDouble(bundle.getString("latitude"));
                double longi = Double.parseDouble(bundle.getString("longitude"));
                LatLng noida = new LatLng(lat, longi);
                mMap.addMarker(new MarkerOptions().position(noida).title(bundle.getString("address")));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(noida));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(noida, 13.0f));
            }
        } catch (Exception e) {
            Log.d("sdfhdsjf",e.getLocalizedMessage());
        }

    }

    @Override
    public void onRetryCall() {

    }
}
