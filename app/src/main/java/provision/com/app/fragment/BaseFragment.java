package provision.com.app.fragment;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import provision.com.app.R;
import provision.com.app.interfaces.RetryListener;

/**
 * Created by gauravg on 7/5/2018.
 */

public class BaseFragment extends Fragment {
    ViewGroup viewGroup;

    void showViewGroup(RetryListener retryListener) {
        if (getActivity() != null &&viewGroup == null) {
            viewGroup = getActivity().findViewById(android.R.id.content);
        }
        if (getActivity() != null) {

            View view = LayoutInflater.from(getActivity()).inflate(R.layout.error_layout, null);
            view.findViewById(R.id.retryTextView).setOnClickListener(v -> {
                if (isOnline()) {
                    retryListener.onRetry();
                    viewGroup.removeView(view);
                }
            });
            viewGroup.addView(view);
        }
    }

    public View showProgressBar() {
        viewGroup = getActivity().findViewById(android.R.id.content);
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.progress_layout, null);
        viewGroup.addView(view);
        return view;
    }

    public void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
    public void cancelProgressBar(View view) {
        if (view != null) {
            viewGroup.removeView(view);
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    void onRetryCall() {
    }
}
