package provision.com.app.fragment;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.activity.FilterActivity;
import provision.com.app.adapter.ResultAdapter;
import provision.com.app.apiResponse.resultResponse.EventList;
import provision.com.app.apiResponse.resultResponse.ResultResponse;
import provision.com.app.databinding.FragmentResultBinding;
import provision.com.app.utils.AppConstant;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;

import static provision.com.app.app.AppConstant.RESULTS_EVENT_TOKEN;
import static provision.com.app.utils.CommonUtils.onTrackEventClick;

public class ResultFragment extends BaseFragment {
    private static ResultFragment fragment;
    private final int REQUEST_CODE = 103;
    private FragmentResultBinding binding;
    private BaseActivity mActivity;
    private List<EventList> productLists = new ArrayList<>();
    private ResultAdapter mAdapter;
    private String year;
    private retrofit2.Call<ResultResponse> call;
    private boolean isFilterActive = false;

    public ResultFragment() {
    }

    public static ResultFragment newInstance() {
        fragment = new ResultFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_result, container, false);
            mActivity = (BaseActivity) getActivity();
//            year = Calendar.getInstance().get(Calendar.YEAR);
            getEventDetail();
            setEventAdapter();
            searchEvent();
            binding.icFilter.setOnClickListener(v -> {
                Intent intent = new Intent(mActivity, FilterActivity.class);
                startActivityForResult(intent, REQUEST_CODE);
            });
        }
        if (isFilterActive) {
            binding.tvSearch.setText("");
            getEventDetail();
        }
        return binding.getRoot();
//                inflater.inflate(R.layout.activity_result_layout, container, false);
    }

    @Override
    public void onRetryCall() {
        super.onRetryCall();
        getEventDetail();
    }

    public void getEventDetail() {
        if (mActivity.isOnline()) {
            productLists.clear();
            View view = mActivity.showProgressBar();
            AuthApiHelper authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper.class);
            retrofit2.Call<ResultResponse> call = authApiHelper.getResultResponse("GetEventsResult", "", "", year, "", "",ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<ResultResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    mActivity.cancelProgressBar(view);
                    ResultResponse eventDetailResponse = (ResultResponse) object;
                    productLists.clear();
                    productLists.addAll(eventDetailResponse.getResponse().getEventList());
                    setEventAdapter();
                    isFilterActive = false;
                    if (productLists != null && productLists.size() > 0) {
                        dataFound();
                    } else {
                        noDataFound();
                    }
                }

                @Override
                protected void onError(RetroError retroError) {
                    mActivity.cancelProgressBar(view);
                }

                @Override
                protected void onFailure(String retroError) {
                    mActivity.cancelProgressBar(view);
                }
            });
        } else {
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
        }
    }


    public void getSearchEventData(String searchText, String categoryId, String yearId, String monthId, String statusId) {
        if (mActivity.isOnline()) {
            productLists.clear();
            View view = mActivity.showProgressBar();
            AuthApiHelper authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper.class);
            call = authApiHelper.getResultResponse("GetEventsResult", searchText, categoryId, yearId, monthId, statusId,ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<ResultResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    mActivity.cancelProgressBar(view);
                    ResultResponse eventDetailResponse = (ResultResponse) object;
                    if (eventDetailResponse.getResponse().getEventList() != null) {
                        productLists.clear();
                        productLists.addAll(eventDetailResponse.getResponse().getEventList());
                    }
                    setEventAdapter();
                    if (productLists != null && productLists.size() > 0) {
                        dataFound();
                    } else {
                        noDataFound();
                    }
                }

                @Override
                protected void onError(RetroError retroError) {
                    Log.d("retroError", retroError.getErrorMessage());
                    mActivity.cancelProgressBar(view);
                }

                @Override
                protected void onFailure(String retroError) {
                    Log.d("retroError1", retroError);
                    mActivity.cancelProgressBar(view);
                }
            });
        } else {
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        onTrackEventClick(RESULTS_EVENT_TOKEN);
//        if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(mActivity, PrefUtils.CART_ITEM)) && !PrefUtils.getFromPrefs(mActivity, PrefUtils.CART_ITEM).equals("0")) {
//            binding.headerLayoutId.cartRL.setVisibility(View.VISIBLE);
//            binding.headerLayoutId.cartCountTextView.setText(PrefUtils.getFromPrefs(mActivity, PrefUtils.CART_ITEM));
//        } else {
//            binding.headerLayoutId.cartRL.setVisibility(View.GONE);
//        }
    }

    private void setEventAdapter() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        } else {
            binding.rcvEvents.setLayoutManager(new LinearLayoutManager(mActivity));
            mAdapter = new ResultAdapter(mActivity, productLists);
            binding.rcvEvents.setAdapter(mAdapter);
        }


    }

    private void searchEvent() {
        binding.tvSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable text) {
                String searchText = text.toString();
                if (!TextUtils.isEmpty(searchText) && searchText.length() > 2) {
                    getSearchEventData(searchText, "", year, "", "");
                } else if (TextUtils.isEmpty(searchText)) {
                    getSearchEventData("", "", year, "", "");
                }

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == REQUEST_CODE) {
            String categoryId = intent.getStringExtra(AppConstant.KEY_CATEGORY);
            String yearId = intent.getStringExtra(AppConstant.KEY_YEAR);
            String monthId = intent.getStringExtra(AppConstant.KEY_MONTH);
            String statusId = intent.getStringExtra(AppConstant.KEY_STATUS);
            isFilterActive = true;
            getSearchEventData(binding.tvSearch.getText().toString(), categoryId, yearId, monthId, statusId);
        } else {
            isFilterActive = true;
            getSearchEventData(binding.tvSearch.getText().toString(), "", "", "", "");
        }

    }

    private void dataFound() {
        binding.rcvEvents.setVisibility(View.VISIBLE);
        binding.noDataFoundText.setVisibility(View.GONE);
    }

    private void noDataFound() {
        binding.rcvEvents.setVisibility(View.GONE);
        binding.noDataFoundText.setVisibility(View.VISIBLE);
    }

}
