package provision.com.app.apiResponse.receipt_details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pankajk on 7/18/2018.
 * All rights reserved by Intigate Technologies.
 */

public class InvoiceInfo {

    @SerializedName("BookingDate")
    @Expose
    private String bookingDate;
    @SerializedName("RegistrationNo")
    @Expose
    private String registrationNo;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("PhoneNo")
    @Expose
    private String phoneNo;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("PaymentType")
    @Expose
    private String paymentType;
    @SerializedName("BarCode")
    @Expose
    private String barCode;
    @SerializedName("grandTotal")
    @Expose
    private double grandTotal;
    @SerializedName("subTotal")
    @Expose
    private double subTotal;

    public String getBookingDate() {
        return bookingDate;
    }

    public String getRegistrationNo() {
        return registrationNo;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public String getStatus() {
        return status;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public String getBarCode() {
        return barCode;
    }

    public double getGrandTotal() {
        return grandTotal;
    }

    public double getSubTotal() {
        return subTotal;
    }
}
