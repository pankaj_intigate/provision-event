
package provision.com.app.apiResponse.recomded_itm;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecomdedItem {

    @SerializedName("Version")
    @Expose
    private String version;
    @SerializedName("AppBlock")
    @Expose
    private String appBlock;
    @SerializedName("CurrrencyCode")
    @Expose
    private String currrencyCode;
    @SerializedName("CurrencyValue")
    @Expose
    private double currencyValue;
    @SerializedName("Numberformat")
    @Expose
    private int numberformat;
    @SerializedName("cmd")
    @Expose
    private String cmd;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("response")
    @Expose
    private List<Response> response = null;
    @SerializedName("response_messege")
    @Expose
    private String responseMessege;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAppBlock() {
        return appBlock;
    }

    public void setAppBlock(String appBlock) {
        this.appBlock = appBlock;
    }

    public String getCurrrencyCode() {
        return currrencyCode;
    }

    public void setCurrrencyCode(String currrencyCode) {
        this.currrencyCode = currrencyCode;
    }

    public double getCurrencyValue() {
        return currencyValue;
    }

    public void setCurrencyValue(double currencyValue) {
        this.currencyValue = currencyValue;
    }

    public int getNumberformat() {
        return numberformat;
    }

    public void setNumberformat(int numberformat) {
        this.numberformat = numberformat;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }

    public String getResponseMessege() {
        return responseMessege;
    }

    public void setResponseMessege(String responseMessege) {
        this.responseMessege = responseMessege;
    }

}
