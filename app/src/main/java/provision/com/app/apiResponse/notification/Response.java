package provision.com.app.apiResponse.notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pankajk on 9/24/2018.
 * All rights reserved by Intigate Technologies.
 */
public class Response {
    @SerializedName("list")
    @Expose
    private List<NotificationData> list = null;

    public List<NotificationData> getList() {
        return list;
    }
}
