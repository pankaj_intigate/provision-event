package provision.com.app.apiResponse.participate_info;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pankajk on 7/18/2018.
 * All rights reserved by Intigate Technologies.
 */

public class ParticipateDetails {
    @SerializedName("participateId")
    @Expose
    private String participateId;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("eventName")
    @Expose
    private String eventName;
    @SerializedName("RegistrationNo")
    @Expose
    private String registrationNo;
    @SerializedName("BarCode")
    @Expose
    private String barCode;
    @SerializedName("Price")
    @Expose
    private double price;
    @SerializedName("Age")
    @Expose
    private String age;
    @SerializedName("Tshirt")
    @Expose
    private String tshirt;
    @SerializedName("TeamName")
    @Expose
    private String teamName;
    @SerializedName("feename")
    @Expose
    private String feename;
    @SerializedName("eventDate")
    @Expose
    private String eventDate;
    @SerializedName("eventAddress")
    @Expose
    private String eventAddress;

    @SerializedName("qrCode")
    @Expose
    private String qrCode;

    @SerializedName("Iscancelled")
    @Expose
    private int isCancelled;
    @SerializedName("Cancelled")
    @Expose
    private int cancelled;
    @SerializedName("event_start_date")
    @Expose
    private String eventStartDate;
    @SerializedName("event_end_date")
    @Expose
    private String eventEndDate;
    @SerializedName("VirtualEvent")
    @Expose
    private int virtualEvent=0;

    public String getEventStartDate() {
        return eventStartDate;
    }

    public String getEventEndDate() {
        return eventEndDate;
    }

    public int getVirtualEvent() {
        return virtualEvent;
    }

    public int getCancelled() {
        return cancelled;
    }

    public int getIsCancelled() {
        return isCancelled;
    }

    public String getQrCode() {
        return qrCode;
    }

    public String getParticipateId() {
        return participateId;
    }

    public String getFullName() {
        return fullName;
    }

    public String getEventName() {
        return eventName;
    }

    public String getRegistrationNo() {
        return registrationNo;
    }

    public String getBarCode() {
        return barCode;
    }

    public double getPrice() {
        return price;
    }

    public String getAge() {
        return age;
    }

    public String getTshirt() {
        return tshirt;
    }

    public String getTeamName() {
        return teamName;
    }

    public String getFeename() {
        return feename;
    }

    public String getEventDate() {
        return eventDate;
    }

    public String getEventAddress() {
        return eventAddress;
    }
}
