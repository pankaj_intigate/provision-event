package provision.com.app.apiResponse.address.country


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import provision.com.app.apiResponse.BaseResponce

@Keep
final class ShopCountryRes(

    @SerializedName("response")
    var response: Response = Response()

):BaseResponce(){

}