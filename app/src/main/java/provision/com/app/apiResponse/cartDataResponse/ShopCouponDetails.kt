package provision.com.app.apiResponse.cartDataResponse


import com.google.gson.annotations.SerializedName

data class ShopCouponDetails(
    @SerializedName("Id")
    var id: String? = "",
    @SerializedName("SessionId")
    var sessionId: String? = "",
    @SerializedName("CouponId")
    var couponId: String? = "",
    @SerializedName("CouponValue")
    var couponValue: Double? = 0.0,
    @SerializedName("CouponCode")
    var couponCode: String? = "",
    @SerializedName("status")
    var status: String? = ""
)