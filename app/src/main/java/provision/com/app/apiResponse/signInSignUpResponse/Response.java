package provision.com.app.apiResponse.signInSignUpResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("cmd")
    @Expose
    private String cmd;
    @SerializedName("response_messege")
    @Expose
    private String responseMessege;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getResponseMessege() {
        return responseMessege;
    }

    public void setResponseMessege(String responseMessege) {
        this.responseMessege = responseMessege;
    }

}
