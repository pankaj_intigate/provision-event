package provision.com.app.apiResponse.participate_info;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pankajk on 7/18/2018.
 * All rights reserved by Intigate Technologies.
 */

public class Response {
    @SerializedName("participateDetails")
    @Expose
    private ParticipateDetails participateDetails;

    public ParticipateDetails getParticipateDetails() {
        return participateDetails;
    }
}
