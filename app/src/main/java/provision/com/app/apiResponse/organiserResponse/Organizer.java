package provision.com.app.apiResponse.organiserResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Organizer {

    @SerializedName("KitImg")
    @Expose
    private String kitImg;
    @SerializedName("ThumbImg")
    @Expose
    private String thumbImg;

    public String getKitImg() {
        return kitImg;
    }

    public void setKitImg(String kitImg) {
        this.kitImg = kitImg;
    }

    public String getThumbImg() {
        return thumbImg;
    }

    public void setThumbImg(String thumbImg) {
        this.thumbImg = thumbImg;
    }

}
