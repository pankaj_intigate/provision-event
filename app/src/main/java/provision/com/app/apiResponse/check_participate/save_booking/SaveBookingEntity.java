package provision.com.app.apiResponse.check_participate.save_booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import provision.com.app.apiResponse.BaseResponce;

/**
 * Created by pankajk on 8/23/2018.
 * All rights reserved by Intigate Technologies.
 */

public class SaveBookingEntity extends BaseResponce {
    @SerializedName("response")
    @Expose
    private SaveBookingResponse response;

    public SaveBookingResponse getResponse() {
        return response;
    }
}
