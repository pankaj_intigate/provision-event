package provision.com.app.apiResponse.feePriceResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EventFee {

    @SerializedName("FeeHeader")
    @Expose
    private FeeHeader feeHeader;
    @SerializedName("Fee")
    @Expose
    private Fee fee;

    public FeeHeader getFeeHeader() {
        return feeHeader;
    }

    public void setFeeHeader(FeeHeader feeHeader) {
        this.feeHeader = feeHeader;
    }

    public Fee getFee() {
        return fee;
    }

    public void setFee(Fee fee) {
        this.fee = fee;
    }

}
