package provision.com.app.apiResponse.raceTrackResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {

    @SerializedName("racetrack")
    @Expose
    private List<Racetrack> racetrack = null;

    public List<Racetrack> getRacetrack() {
        return racetrack;
    }

    public void setRacetrack(List<Racetrack> racetrack) {
        this.racetrack = racetrack;
    }

}
