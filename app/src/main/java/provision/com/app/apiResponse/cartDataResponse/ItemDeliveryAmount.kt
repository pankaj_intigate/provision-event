package provision.com.app.apiResponse.cartDataResponse


import com.google.gson.annotations.SerializedName

data class ItemDeliveryAmount(
    @SerializedName("DeliveryCharges")
    var deliveryCharges: Double=0.0,
    @SerializedName("ExprDeliveryCharges")
    var exprDeliveryCharges: String? = "",
    @SerializedName("DeliveryText")
    var deliveryText: String? = ""
)