package provision.com.app.apiResponse.event_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pankajk on 7/18/2018.
 * All rights reserved by Intigate Technologies.
 */

public class Response {
    @SerializedName("events_info_data")
    @Expose
    private List<EventList> eventsInfo = null;

    public List<EventList> getEventsInfo() {
        return eventsInfo;
    }
}
