package provision.com.app.apiResponse.my_account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import provision.com.app.apiResponse.BaseResponce;
import provision.com.app.apiResponse.getProfileResponse.Country;
import provision.com.app.apiResponse.getProfileResponse.CountryCode;
import provision.com.app.apiResponse.getProfileResponse.Gender;
import provision.com.app.apiResponse.getProfileResponse.Tshirt;

/**
 * Created by pankajk on 7/13/2018.
 * All rights reserved by Intigate Technologies.
 */

public class MyAccount extends BaseResponce {
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("userInfo")
    @Expose
    private UserInfo userInfoForProfile;

    @SerializedName("user_info")
    @Expose
    private UserInfo userInfo;
    @SerializedName("event_info")
    @Expose
    private String eventInfo;
    @SerializedName("Gender")
    @Expose
    private List<Gender> gender = null;
    @SerializedName("Country")
    @Expose
    private List<Country> country = null;
    @SerializedName("Tshirts")
    @Expose
    private List<Tshirt> tshirts = null;
    @SerializedName("Country_code")
    @Expose
    private List<CountryCode> countryCode = null;
    @SerializedName("WalletAmount")
    @Expose
    private String walletAmount;

    public String getUserId() {
        return userId;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public String getEventInfo() {
        return eventInfo;
    }

    public String getWalletAmount() {
        return walletAmount;
    }

    public List<Gender> getGender() {
        return gender;
    }

    public List<Country> getCountry() {
        return country;
    }

    public List<Tshirt> getTshirts() {
        return tshirts;
    }

    public List<CountryCode> getCountryCode() {
        return countryCode;
    }

    public UserInfo getUserInfoForProfile() {
        return userInfoForProfile;
    }
}
