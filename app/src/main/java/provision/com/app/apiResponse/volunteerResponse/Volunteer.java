package provision.com.app.apiResponse.volunteerResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Volunteer {

    @SerializedName("Url")
    @Expose
    private String url;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("VolImg")
    @Expose
    private String volImg;
    @SerializedName("ThumbImg")
    @Expose
    private String thumbImg;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVolImg() {
        return volImg;
    }

    public void setVolImg(String volImg) {
        this.volImg = volImg;
    }

    public String getThumbImg() {
        return thumbImg;
    }

    public void setThumbImg(String thumbImg) {
        this.thumbImg = thumbImg;
    }

}
