package provision.com.app.apiResponse.inbox;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pankajk on 7/18/2018.
 * All rights reserved by Intigate Technologies.
 */

public class UserInbox {
    @SerializedName("user_data")
    @Expose
    private List<InboxList> userData;

    public List<InboxList> getUserData() {
        return userData;
    }
}
