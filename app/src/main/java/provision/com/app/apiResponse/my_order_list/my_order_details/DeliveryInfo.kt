package provision.com.app.apiResponse.my_order_list.my_order_details


import com.google.gson.annotations.SerializedName

data class DeliveryInfo(
    @SerializedName("itemId")
    var itemId: String? = "",
    @SerializedName("itemImg")
    var itemImg: String? = "",
    @SerializedName("ItemName")
    var itemName: String? = "",
    @SerializedName("Price")
    var price: Double,
    @SerializedName("quantity")
    var quantity: String? = "",
    @SerializedName("Type")
    var type: String? = "",
    @SerializedName("size")
    var size: String? = "",
    @SerializedName("color_code")
    var colorCode: String? = "",
    @SerializedName("order_status")
    var orderStatus: String? = "",
    @SerializedName("is_delivery")
    var isDelivery: String? = "",
    @SerializedName("DeliveryType")
    var deliveryType: String? = "",
    @SerializedName("address_note")
    var addressNote: String? = "",
    @SerializedName("size_id")
    var sizeId: String? = "",
    @SerializedName("color_id")
    var colorId: String? = ""
)