package provision.com.app.apiResponse.eventDetailResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EventImg {

    @SerializedName("Image")
    @Expose
    private String image;
    @SerializedName("AppImg")
    @Expose
    private String appImg;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAppImg() {
        return appImg;
    }

    public void setAppImg(String appImg) {
        this.appImg = appImg;
    }

}
