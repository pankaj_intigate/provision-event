package provision.com.app.apiResponse.payment_opt


import com.google.gson.annotations.SerializedName

data class Response(
    @SerializedName("Title")
    var title: String? = "",
    @SerializedName("paymentvalue")
    var paymentvalue: Int = 0,
    @SerializedName("IsSelected")
    var isSelected: Int? = 0,
    @SerializedName("image")
    var image: String? = ""
)