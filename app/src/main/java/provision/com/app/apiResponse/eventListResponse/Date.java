package provision.com.app.apiResponse.eventListResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

public class Date {
    @SerializedName("Days")
    @Expose
    private Integer days;

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }



    @NotNull
    @Override
    public String toString() {
        return days.toString();
    }
}
