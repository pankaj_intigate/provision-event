package provision.com.app.apiResponse.my_order_list


import com.google.gson.annotations.SerializedName

data class Response(
    @SerializedName("user_info")
    var userInfo: UserInfo? = UserInfo(),
    @SerializedName("items_info_data")
    var itemsInfoData: ItemsInfoData? = ItemsInfoData()
)