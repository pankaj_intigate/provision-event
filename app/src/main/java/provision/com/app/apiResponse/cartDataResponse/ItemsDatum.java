
package provision.com.app.apiResponse.cartDataResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemsDatum {

    @SerializedName("ItemAttrVal")
    @Expose
    private String itemAttrVal;
    @SerializedName("ItemAmount")
    @Expose
    private double itemAmount;
    @SerializedName("ItemName")
    @Expose
    private String itemName;
    @SerializedName("BookingItemId")
    @Expose
    private String bookingItemId;
    @SerializedName("SessionId")
    @Expose
    private String sessionId;

    public String getItemAttrVal() {
        return itemAttrVal;
    }

    public void setItemAttrVal(String itemAttrVal) {
        this.itemAttrVal = itemAttrVal;
    }

    public double getItemAmount() {
        return itemAmount;
    }


    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getBookingItemId() {
        return bookingItemId;
    }

    public void setBookingItemId(String bookingItemId) {
        this.bookingItemId = bookingItemId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

}
