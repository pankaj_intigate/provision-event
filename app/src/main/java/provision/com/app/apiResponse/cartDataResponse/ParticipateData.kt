package provision.com.app.apiResponse.cartDataResponse


import com.google.gson.annotations.SerializedName

data class ParticipateData(
    @SerializedName("EventName")
    var eventName: String? = "",
    @SerializedName("participateCount")
    var participateCount: Int? = 0,
    @SerializedName("TotalFeePrice")
    var totalFeePrice: Double= 0.0
)