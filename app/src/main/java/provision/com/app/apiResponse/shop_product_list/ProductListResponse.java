package provision.com.app.apiResponse.shop_product_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import provision.com.app.apiResponse.BaseResponce;

public class ProductListResponse extends BaseResponce {
    @SerializedName("response")
    @Expose
    Response response;

    public Response getResponse() {
        return response;
    }
}
