package provision.com.app.apiResponse.create_token;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import provision.com.app.apiResponse.BaseResponce;

/**
 * Created by pankajk on 10/4/2018.
 * All rights reserved by Intigate Technologies.
 */
public class NotificateTokenUpdate extends BaseResponce {
    @SerializedName("response")
    @Expose
    private Responce response;

    public Responce getResponse() {
        return response;
    }
}
