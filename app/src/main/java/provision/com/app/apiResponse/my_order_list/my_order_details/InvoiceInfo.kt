package provision.com.app.apiResponse.my_order_list.my_order_details


import com.google.gson.annotations.SerializedName

data class InvoiceInfo(
    @SerializedName("i_id")
    var iId: String? = "",
    @SerializedName("BookingDate")
    var bookingDate: String? = "",
    @SerializedName("RegistrationNo")
    var registrationNo: String? = "",
    @SerializedName("Name")
    var name: String? = "",
    @SerializedName("Email")
    var email: String? = "",
    @SerializedName("PhoneNo")
    var phoneNo: String? = "",
    @SerializedName("Status")
    var status: String? = "",
    @SerializedName("PaymentType")
    var paymentType: String? = "",
    @SerializedName("i_txn_id")
    var iTxnId: String? = "",
    @SerializedName("pay_id")
    var payId: String? = "",
    @SerializedName("auth")
    var auth: String? = "",
    @SerializedName("track_id")
    var trackId: String? = "",
    @SerializedName("ref_no")
    var refNo: String? = "",
    @SerializedName("r")
    var r: String? = "",
    @SerializedName("BarCode")
    var barCode: String? = "",
    @SerializedName("qrCode")
    var qrCode: String? = "",
    @SerializedName("grandTotal")
    var grandTotal: String? = "",
    @SerializedName("subTotal")
    var subTotal: String? = "",
    @SerializedName("SendCampaign")
    var sendCampaign: String? = "",
    @SerializedName("IsItemData")
    var isItemData: String? = "",
    @SerializedName("DeliveryStatus")
    var deliveryStatus: String? = ""
)