package provision.com.app.apiResponse.getCurrencyCodeResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {

    @SerializedName("Country_code")
    @Expose
    private List<CountryCode> countryCode = null;

    public List<CountryCode> getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(List<CountryCode> countryCode) {
        this.countryCode = countryCode;
    }

}
