package provision.com.app.apiResponse.eventListResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EventListResponse {
    @SerializedName("Version")
    @Expose
    private String version;
    @SerializedName("AppBlock")
    @Expose
    private String appBlock;
    @SerializedName("CurrrencyCode")
    @Expose
    private String currrencyCode;
    @SerializedName("CurrencyValue")
    @Expose
    private double currencyValue;
    @SerializedName("Numberformat")
    @Expose
    private int numberformat;
    @SerializedName("cmd")
    @Expose
    private String cmd;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("response")
    @Expose
    private Response response;
    @SerializedName("response_messege")
    @Expose
    private String responseMessege;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAppBlock() {
        return appBlock;
    }

    public void setAppBlock(String appBlock) {
        this.appBlock = appBlock;
    }

    public String getCurrrencyCode() {
        return currrencyCode;
    }

    public void setCurrrencyCode(String currrencyCode) {
        this.currrencyCode = currrencyCode;
    }

    public double getCurrencyValue() {
        return currencyValue;
    }

    public void setCurrencyValue(double currencyValue) {
        this.currencyValue = currencyValue;
    }

    public int getNumberformat() {
        return numberformat;
    }

    public void setNumberformat(int numberformat) {
        this.numberformat = numberformat;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public String getResponseMessege() {
        return responseMessege;
    }

    public void setResponseMessege(String responseMessege) {
        this.responseMessege = responseMessege;
    }

}
