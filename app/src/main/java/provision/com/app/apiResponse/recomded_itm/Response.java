
package provision.com.app.apiResponse.recomded_itm;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("i_id")
    @Expose
    private String iId;
    @SerializedName("i_type_id")
    @Expose
    private String iTypeId;
    @SerializedName("itemImg")
    @Expose
    private String itemImg;
    @SerializedName("itemTitle")
    @Expose
    private String itemTitle;
    @SerializedName("itemPrice")
    @Expose
    private double itemPrice;
    @SerializedName("itemSize")
    @Expose
    private List<ItemSize> itemSize = null;

    public String getIId() {
        return iId;
    }

    public void setIId(String iId) {
        this.iId = iId;
    }

    public String getITypeId() {
        return iTypeId;
    }

    public void setITypeId(String iTypeId) {
        this.iTypeId = iTypeId;
    }

    public String getItemImg() {
        return itemImg;
    }

    public void setItemImg(String itemImg) {
        this.itemImg = itemImg;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public List<ItemSize> getItemSize() {
        return itemSize;
    }

    public void setItemSize(List<ItemSize> itemSize) {
        this.itemSize = itemSize;
    }

}
