package provision.com.app.apiResponse.participate_info;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import provision.com.app.apiResponse.BaseResponce;

/**
 * Created by pankajk on 7/18/2018.
 * All rights reserved by Intigate Technologies.
 */

public class ParticipateInfoEntity extends BaseResponce {
    @SerializedName("response")
    @Expose
    private Response response;

    public Response getResponse() {
        return response;
    }
}
