package provision.com.app.apiResponse.inbox;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pankajk on 7/18/2018.
 * All rights reserved by Intigate Technologies.
 */

public class InboxList {
    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("eventname")
    @Expose
    private String eventname;
    @SerializedName("MailTypeId")
    @Expose
    private String mailTypeId;
    @SerializedName("MailConversationId")
    @Expose
    private String mailConversationId;
    @SerializedName("Count")
    @Expose
    private String count;
    @SerializedName("SendBy")
    @Expose
    private String sendBy;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("Subject")
    @Expose
    private String subject;
    @SerializedName("Body")
    @Expose
    private String body;
    @SerializedName("SendDate")
    @Expose
    private String sendDate;
    @SerializedName("Time")
    @Expose
    private String time;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("CreateOn")
    @Expose
    private String createOn;

    public String getId() {
        return id;
    }

    public String getEventname() {
        return eventname;
    }

    public String getMailTypeId() {
        return mailTypeId;
    }

    public String getMailConversationId() {
        return mailConversationId;
    }

    public String getCount() {
        return count;
    }

    public String getSendBy() {
        return sendBy;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public String getSendDate() {
        return sendDate;
    }

    public String getTime() {
        return time;
    }

    public String getStatus() {
        return status;
    }

    public String getCreateOn() {
        return createOn;
    }
}
