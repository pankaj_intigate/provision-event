package provision.com.app.apiResponse.homeBanner;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductList {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("events_title")
    @Expose
    private String eventsTitle;
    @SerializedName("eventAddress")
    @Expose
    private String eventAddress;
    @SerializedName("eventCategoryId")
    @Expose
    private String eventCategoryId;
    @SerializedName("IsRegisteration")
    @Expose
    private String isRegisteration;
    @SerializedName("event_start_date")
    @Expose
    private String eventStartDate;
    @SerializedName("event_end_date")
    @Expose
    private String eventEndDate;
    @SerializedName("eventDate")
    @Expose
    private String eventDate;
    /*   @SerializedName("description")
       @Expose
       private String description;*/
    @SerializedName("Img")
    @Expose
    private String img;
    @SerializedName("eventImage")
    @Expose
    private String eventImage;
    @SerializedName("AppImg")
    @Expose
    private String appImg;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEventsTitle() {
        return eventsTitle;
    }

    public void setEventsTitle(String eventsTitle) {
        this.eventsTitle = eventsTitle;
    }

    public String getEventAddress() {
        return eventAddress;
    }

    public void setEventAddress(String eventAddress) {
        this.eventAddress = eventAddress;
    }

    public String getEventCategoryId() {
        return eventCategoryId;
    }

    public void setEventCategoryId(String eventCategoryId) {
        this.eventCategoryId = eventCategoryId;
    }

    public String getIsRegisteration() {
        return isRegisteration;
    }

    public void setIsRegisteration(String isRegisteration) {
        this.isRegisteration = isRegisteration;
    }

    public String getEventStartDate() {
        return eventStartDate;
    }

    public void setEventStartDate(String eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    public String getEventEndDate() {
        return eventEndDate;
    }

    public void setEventEndDate(String eventEndDate) {
        this.eventEndDate = eventEndDate;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

/*    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }*/

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getEventImage() {
        return eventImage;
    }

    public void setEventImage(String eventImage) {
        this.eventImage = eventImage;
    }

    public String getAppImg() {
        return appImg;
    }

    public void setAppImg(String appImg) {
        this.appImg = appImg;
    }

}
