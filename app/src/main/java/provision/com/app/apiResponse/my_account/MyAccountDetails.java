package provision.com.app.apiResponse.my_account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import provision.com.app.apiResponse.BaseResponce;

/**
 * Created by pankajk on 7/13/2018.
 * All rights reserved by Intigate Technologies.
 */

public class MyAccountDetails extends BaseResponce {
    @SerializedName("response")
    @Expose
    private MyAccount response;

    public MyAccount getResponse() {
        return response;
    }

}
