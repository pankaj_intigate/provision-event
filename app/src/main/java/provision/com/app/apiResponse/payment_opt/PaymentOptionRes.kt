package provision.com.app.apiResponse.payment_opt


import com.google.gson.annotations.SerializedName
import provision.com.app.apiResponse.BaseResponce

data class PaymentOptionRes(
        @SerializedName("response")
        var response: List<Response?>? = listOf()
) : BaseResponce()