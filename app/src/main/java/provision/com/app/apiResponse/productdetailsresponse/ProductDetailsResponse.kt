package provision.com.app.apiResponse.productdetailsresponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import provision.com.app.apiResponse.BaseResponce


class ProductDetailsResponse:BaseResponce() {
    @SerializedName("response")
    @Expose
    var response: Response? = null
}