package provision.com.app.apiResponse.my_order_list.my_order_details


import com.google.gson.annotations.SerializedName

data class Response(
        @SerializedName("webcall")
    var webcall: String? = "",
        @SerializedName("eventIdAarryNew")
    var eventIdAarryNew: List<String?>? = listOf(),
        @SerializedName("invoiceInfo")
    var invoiceInfo: InvoiceInfo? = InvoiceInfo(),
        @SerializedName("participate_info")
    var participateInfo: ArrayList<ParticipateInfo>? = ArrayList(),
        @SerializedName("totalCoupon")
    var totalCoupon: String? = "",
        @SerializedName("bookingUserAddress")
    var bookingUserAddress: BookingUserAddress? = BookingUserAddress(),
        @SerializedName("bookingShopitems")
    var bookingShopitems: BookingShopitems? = BookingShopitems(),
        @SerializedName("IsItemData")
    var isItemData: Int? = 0,
        @SerializedName("sub_total_amount")
    var subTotalAmount: SubTotalAmount? = SubTotalAmount()
)