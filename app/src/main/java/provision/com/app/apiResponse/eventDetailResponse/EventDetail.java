package provision.com.app.apiResponse.eventDetailResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EventDetail {

    @SerializedName("i_id")
    @Expose
    private String iId;
    @SerializedName("v_title")
    @Expose
    private String vTitle;
    @SerializedName("v_address_lat")
    @Expose
    private String vAddressLat;
    @SerializedName("v_address_long")
    @Expose
    private String vAddressLong;
    @SerializedName("CancellationPolicy")
    @Expose
    private String cancellationPolicy;
    @SerializedName("v_description")
    @Expose
    private String vDescription;
    @SerializedName("v_address")
    @Expose
    private String vAddress;
    @SerializedName("i_category_id")
    @Expose
    private String iCategoryId;
    @SerializedName("event_start_date")
    @Expose
    private String eventStartDate;
    @SerializedName("event_end_date")
    @Expose
    private String eventEndDate;
    @SerializedName("eventDate")
    @Expose
    private String eventDate;
    @SerializedName("ti_registration_page")
    @Expose
    private String tiRegistrationPage;
    @SerializedName("ti_free")
    @Expose
    private String tiFree;
    @SerializedName("v_result_code")
    @Expose
    private String vResultCode;
    @SerializedName("onlyTeam")
    @Expose
    private String onlyTeam;
    @SerializedName("IsRegisterShow")
    @Expose
    private String isRegisterShow;
    @SerializedName("Hashtag")
    @Expose
    private String hashtag;
    @SerializedName("IsRegistration")
    @Expose
    private String isRegistration;
    @SerializedName("VirtualEvent")
    @Expose
    private int virtualEvent;

    public int getVirtualEvent() {
        return virtualEvent;
    }

    public String getIsRegistration() {
        return isRegistration;
    }

    public String getIId() {
        return iId;
    }

    public void setIId(String iId) {
        this.iId = iId;
    }

    public String getVTitle() {
        return vTitle;
    }

    public void setVTitle(String vTitle) {
        this.vTitle = vTitle;
    }

    public String getVAddressLat() {
        return vAddressLat;
    }


    public String getVAddressLong() {
        return vAddressLong;
    }


    public String getCancellationPolicy() {
        return cancellationPolicy;
    }

    public void setCancellationPolicy(String cancellationPolicy) {
        this.cancellationPolicy = cancellationPolicy;
    }

    public String getVDescription() {
        return vDescription;
    }

    public void setVDescription(String vDescription) {
        this.vDescription = vDescription;
    }

    public String getVAddress() {
        return vAddress;
    }

    public void setVAddress(String vAddress) {
        this.vAddress = vAddress;
    }

    public String getICategoryId() {
        return iCategoryId;
    }

    public void setICategoryId(String iCategoryId) {
        this.iCategoryId = iCategoryId;
    }

    public String getEventStartDate() {
        return eventStartDate;
    }

    public void setEventStartDate(String eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    public String getEventEndDate() {
        return eventEndDate;
    }

    public void setEventEndDate(String eventEndDate) {
        this.eventEndDate = eventEndDate;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getTiRegistrationPage() {
        return tiRegistrationPage;
    }

    public void setTiRegistrationPage(String tiRegistrationPage) {
        this.tiRegistrationPage = tiRegistrationPage;
    }

    public String getTiFree() {
        return tiFree;
    }

    public void setTiFree(String tiFree) {
        this.tiFree = tiFree;
    }

    public String getVResultCode() {
        return vResultCode;
    }

    public void setVResultCode(String vResultCode) {
        this.vResultCode = vResultCode;
    }

    public String getOnlyTeam() {
        return onlyTeam;
    }

    public void setOnlyTeam(String onlyTeam) {
        this.onlyTeam = onlyTeam;
    }

    public String getIsRegisterShow() {
        return isRegisterShow;
    }

    public void setIsRegisterShow(String isRegisterShow) {
        this.isRegisterShow = isRegisterShow;
    }

    public String getHashtag() {
        return hashtag;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }

}
