package provision.com.app.apiResponse.signInSignUpResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import provision.com.app.apiResponse.BaseResponce;

public class SignInSignUpResponse extends BaseResponce {
    @SerializedName("response")
    @Expose
    private Response response;

    public Response getResponse() {
        return response;
    }

}
