package provision.com.app.apiResponse.productdetailsresponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
class ColorAttr {
    @SerializedName("i_id")
    @Expose
    var iId: String? = null
    @SerializedName("Isselected")
    @Expose
    var isselected: Int = 0
    @SerializedName("color_id")
    @Expose
    var colorId: String? = null
    @SerializedName("color_name")
    @Expose
    var colorName: String? = null
    @SerializedName("color_code")
    @Expose
    var colorCode: String? = null
    @SerializedName("sortorder")
    @Expose
    var sortorder: String? = null
    @SerializedName("IsAvailability")
    @Expose
    var isAvailability: Int = 0

}