package provision.com.app.apiResponse.event_shop


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
@Keep
data class Response(
    @SerializedName("data")
    var data: ArrayList<Data>? = null,
    @SerializedName("totalpagecount")
    var totalpagecount: Int? = 0
)