package provision.com.app.apiResponse.my_order_list.my_order_details


import com.google.gson.annotations.SerializedName

data class SubTotalAmount(
    @SerializedName("Subtotal")
    var subtotal: Double? = 0.0,
    @SerializedName("AddonsTotal")
    var addonsTotal: Double? = 0.0,
    @SerializedName("TotalDeliveryAmount")
    var totalDeliveryAmount: Double? = 0.0,
    @SerializedName("TotalRefundAmount")
    var totalRefundAmount: Double? = 0.0,
    @SerializedName("TotalDiscount")
    var totalDiscount: Double? = 0.0,
    @SerializedName("TotalItemAmount")
    var totalItemAmount: Double? = 0.0,
    @SerializedName("GrandTotal")
    var grandTotal: Double? = 0.0,
    @SerializedName("TotalFeePrice")
    var totalFeePrice: Int? = 0
)