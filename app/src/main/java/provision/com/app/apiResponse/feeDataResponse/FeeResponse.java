package provision.com.app.apiResponse.feeDataResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeeResponse {

    @SerializedName("Version")
    @Expose
    private String version;
    @SerializedName("AppBlock")
    @Expose
    private String appBlock;
    @SerializedName("cmd")
    @Expose
    private String cmd;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("response")
    @Expose
    private Response response;
    @SerializedName("response_messege")
    @Expose
    private String responseMessege;
    @SerializedName("CurrencyValue")
    @Expose
    private double currencyValue;
    @SerializedName("Numberformat")
    @Expose
    private int numberformat;
    @SerializedName("CurrrencyCode")
    @Expose
    private String currrencyCode;

    public String getCurrrencyCode() {
        return currrencyCode;
    }

    public double getCurrencyValue() {
        return currencyValue;
    }

    public int getNumberformat() {
        return numberformat;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAppBlock() {
        return appBlock;
    }

    public void setAppBlock(String appBlock) {
        this.appBlock = appBlock;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public String getResponseMessege() {
        return responseMessege;
    }

    public void setResponseMessege(String responseMessege) {
        this.responseMessege = responseMessege;
    }

}
