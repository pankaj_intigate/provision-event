package provision.com.app.apiResponse.eventRegister;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UInfo {

    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("Gender")
    @Expose
    private String gender;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("PhoneNo")
    @Expose
    private String phoneNo;
    @SerializedName("Tsize")
    @Expose
    private String tsize;
    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("Dob")
    @Expose
    private String dob;
    @SerializedName("fireupload")
    @Expose
    private String fireupload;
    @SerializedName("ti_group_id")
    @Expose
    private String tiGroupId;
    @SerializedName("userImg")
    @Expose
    private String userImg;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getTsize() {
        return tsize;
    }

    public void setTsize(String tsize) {
        this.tsize = tsize;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getFireupload() {
        return fireupload;
    }

    public void setFireupload(String fireupload) {
        this.fireupload = fireupload;
    }

    public String getTiGroupId() {
        return tiGroupId;
    }

    public void setTiGroupId(String tiGroupId) {
        this.tiGroupId = tiGroupId;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

}
