package provision.com.app.apiResponse.eventListResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Year {

    @SerializedName("Year")
    @Expose
    private Integer yearVal;

    public Integer getYear() {
        return yearVal;
    }

    public void setYear(Integer year) {
        this.yearVal = year;

    }

    @Override
    public String toString() {
        return ""+yearVal;
    }
}
