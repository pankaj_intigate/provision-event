package provision.com.app.apiResponse.feePriceResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeeHeader {

    @SerializedName("RegistrationType")
    @Expose
    private String registrationType;
    @SerializedName("MinP_t")
    @Expose
    private String minPT;

    public String getRegistrationType() {
        return registrationType;
    }

    public void setRegistrationType(String registrationType) {
        this.registrationType = registrationType;
    }

    public String getMinPT() {
        return minPT;
    }

    public void setMinPT(String minPT) {
        this.minPT = minPT;
    }

}
