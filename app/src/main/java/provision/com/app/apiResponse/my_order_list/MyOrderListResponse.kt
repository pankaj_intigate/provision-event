package provision.com.app.apiResponse.my_order_list


import com.google.gson.annotations.SerializedName
import provision.com.app.apiResponse.BaseResponce

data class MyOrderListResponse(

    @SerializedName("response")
    var response: Response? = Response()
):BaseResponce()