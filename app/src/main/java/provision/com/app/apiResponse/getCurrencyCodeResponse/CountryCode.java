package provision.com.app.apiResponse.getCurrencyCodeResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CountryCode {

    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("code")
    @Expose
    private String code;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return title;
    }
}
