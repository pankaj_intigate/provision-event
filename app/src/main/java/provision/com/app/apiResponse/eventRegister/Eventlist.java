package provision.com.app.apiResponse.eventRegister;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Eventlist {
    @SerializedName("i_id")
    @Expose
    private String iId;
    @SerializedName("v_title")
    @Expose
    private String vTitle;
    @SerializedName("ti_as_a_team")
    @Expose
    private String tiAsATeam;
    @SerializedName("onlyTeam")
    @Expose
    private String onlyTeam;

    public String getIId() {
        return iId;
    }

    public void setIId(String iId) {
        this.iId = iId;
    }

    public String getVTitle() {
        return vTitle;
    }

    public void setVTitle(String vTitle) {
        this.vTitle = vTitle;
    }

    public String getTiAsATeam() {
        return tiAsATeam;
    }

    public void setTiAsATeam(String tiAsATeam) {
        this.tiAsATeam = tiAsATeam;
    }

    public String getOnlyTeam() {
        return onlyTeam;
    }

    public void setOnlyTeam(String onlyTeam) {
        this.onlyTeam = onlyTeam;
    }

    @Override
    public String toString() {
        return vTitle;
    }
}
