package provision.com.app.apiResponse.create_token;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pankajk on 10/4/2018.
 * All rights reserved by Intigate Technologies.
 */
public class Responce {
    @SerializedName("IsNotify")
    @Expose
    private String IsNotify;

    public String getIsNotify() {
        return IsNotify;
    }
}
