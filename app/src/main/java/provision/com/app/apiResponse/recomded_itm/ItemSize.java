package provision.com.app.apiResponse.recomded_itm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemSize {

    @SerializedName("i_id")
    @Expose
    private String iId;
    @SerializedName("v_value")
    @Expose
    private String vValue;
    private boolean selected;

    public String getIId() {
        return iId;
    }

    public void setIId(String iId) {
        this.iId = iId;
    }

    public String getVValue() {
        return vValue;
    }

    public void setVValue(String vValue) {
        this.vValue = vValue;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
