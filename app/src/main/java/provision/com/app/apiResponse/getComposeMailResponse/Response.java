package provision.com.app.apiResponse.getComposeMailResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {

    @SerializedName("evnetsList")
    @Expose
    private List<EvnetsList> evnetsList = null;
    @SerializedName("mailtype")
    @Expose
    private Mailtype mailtype;

    public List<EvnetsList> getEvnetsList() {
        return evnetsList;
    }

    public void setEvnetsList(List<EvnetsList> evnetsList) {
        this.evnetsList = evnetsList;
    }

    public Mailtype getMailtype() {
        return mailtype;
    }

    public void setMailtype(Mailtype mailtype) {
        this.mailtype = mailtype;
    }

}
