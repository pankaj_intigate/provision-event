package provision.com.app.apiResponse.myWalletResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import provision.com.app.apiResponse.BaseResponce;

/**
 * Created by gauravg on 7/19/2018.
 */

public class MyWallet extends BaseResponce {
    @SerializedName("response")
    @Expose
    private Response response;

    public Response getResponse() {
        return response;
    }
}
