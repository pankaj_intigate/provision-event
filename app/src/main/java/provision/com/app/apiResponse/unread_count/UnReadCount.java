package provision.com.app.apiResponse.unread_count;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import provision.com.app.apiResponse.BaseResponce;

/**
 * Created by pankajk on 9/24/2018.
 * All rights reserved by Intigate Technologies.
 */
public class UnReadCount extends BaseResponce {
    @SerializedName("response")
    @Expose
    private Response response;

    public Response getResponse() {
        return response;
    }
}
