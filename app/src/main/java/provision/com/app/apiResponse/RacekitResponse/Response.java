package provision.com.app.apiResponse.RacekitResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {

    @SerializedName("racekit")
    @Expose
    private List<Racekit> racekit = null;

    public List<Racekit> getRacekit() {
        return racekit;
    }

    public void setRacekit(List<Racekit> racekit) {
        this.racekit = racekit;
    }

}
