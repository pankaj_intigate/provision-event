package provision.com.app.apiResponse.eventDetailResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Feature {

    @SerializedName("TabId")
    @Expose
    private String tabId;
    @SerializedName("EventId")
    @Expose
    private String eventId;
    @SerializedName("TabName")
    @Expose
    private String tabName;
    @SerializedName("Show")
    @Expose
    private int show;

    public int getShow() {
        return show;
    }

    public String getTabId() {
        return tabId;
    }

    public void setTabId(String tabId) {
        this.tabId = tabId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getTabName() {
        return tabName;
    }

    public void setTabName(String tabName) {
        this.tabName = tabName;
    }

}
