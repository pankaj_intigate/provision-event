package provision.com.app.apiResponse.event_shop


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import provision.com.app.apiResponse.BaseResponce
@Keep
data class ShopItemEventResponse(
    @SerializedName("response")
    var response: Response? = Response()
):BaseResponce()