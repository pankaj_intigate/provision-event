package provision.com.app.apiResponse.productdetailsresponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import provision.com.app.apiResponse.event_shop.ItemImage


class Response {
    @SerializedName("i_id")
    @Expose
    var iId: String? = null
    @SerializedName("IsStock")
    @Expose
    var isStock: Int = 0
    @SerializedName("tsizeId")
    @Expose
    var tsizeId: String? = null
    @SerializedName("TypeTitle")
    @Expose
    var typeTitle: String? = null
    @SerializedName("itemImg")
    @Expose
    var itemImg: String? = null
    @SerializedName("image")
    @Expose
    var image: String? = null
    @SerializedName("itemTitle")
    @Expose
    var itemTitle: String? = null
    @SerializedName("v_desc")
    @Expose
    var vDesc: String? = null
    @SerializedName("salesPrice")
    @Expose
    var salesPrice: Double = 0.0
    @SerializedName("basePrice")
    @Expose
    var basePrice: Double = 0.0
    @SerializedName("discounttype")
    @Expose
    var discounttype: String? = null
    @SerializedName("Discount")
    @Expose
    var discount: Double = 0.0
    @SerializedName("shiping_and_returns")
    @Expose
    var shipingAndReturns: String? = null
    @SerializedName("wishlist")
    @Expose
    var wishlist: Int? = null
    @SerializedName("IsSelectedcolor")
    @Expose
    var isSelectedcolor: String? = null
    @SerializedName("IsSelectedsize")
    @Expose
    var isSelectedsize: String? = null
    @SerializedName("option_id")
    @Expose
    var optionId: String? = null
    @SerializedName("item_image")
    @Expose
    var itemImage: ArrayList<ItemImage>? = null
    @SerializedName("Discountvalue")
    @Expose
    var discountvalue: String? = null
    @SerializedName("shareurl")
    @Expose
    var shareurl: String? = null
    @SerializedName("itemSize")
    @Expose
    var itemSize: ArrayList<ItemSize>? = null
    @SerializedName("ColorAttr")
    @Expose
    var colorAttr: ArrayList<ColorAttr>? = null
}