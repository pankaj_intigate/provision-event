package provision.com.app.apiResponse.sponserResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {

    @SerializedName("sponser")
    @Expose
    private List<Sponser> sponser = null;

    public List<Sponser> getSponser() {
        return sponser;
    }

    public void setSponser(List<Sponser> sponser) {
        this.sponser = sponser;
    }

}
