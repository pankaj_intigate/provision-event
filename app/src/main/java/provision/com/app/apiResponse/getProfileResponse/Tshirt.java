package provision.com.app.apiResponse.getProfileResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Tshirt {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("Size")
    @Expose
    private String size;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return size;
    }
}
