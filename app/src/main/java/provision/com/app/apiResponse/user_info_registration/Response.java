package provision.com.app.apiResponse.user_info_registration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by pankajk on 9/20/2018.
 * All rights reserved by Intigate Technologies.
 */
public class Response {
    @SerializedName("uInfo")
    @Expose
    private UInfo uInfo;

    public UInfo getuInfo() {
        return uInfo;
    }
}
