package provision.com.app.apiResponse.myWalletResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {

    @SerializedName("wallet_amount")
    @Expose
    private double walletAmount;
    @SerializedName("wallet_detail")
    @Expose
    private List<WalletDetail> walletDetail = null;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("Paymenturl")
    @Expose
    private String paymentUrl;
    @SerializedName("Paytype")
    @Expose
    private int paytype;
    @SerializedName("bookings_id")
    @Expose
    private String bookingsId;

    public Integer getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    public String getUrl() {
        return url;
    }

    public String getPaymentUrl() {
        return paymentUrl;
    }

    public int getPaytype() {
        return paytype;
    }

    public String getBookingsId() {
        return bookingsId;
    }

    public double getWalletAmount() {
        return walletAmount;
    }

    public void setWalletAmount(double walletAmount) {
        this.walletAmount = walletAmount;
    }


    public List<WalletDetail> getWalletDetail() {
        return walletDetail;
    }


}
