package provision.com.app.apiResponse.notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import provision.com.app.apiResponse.BaseResponce;

/**
 * Created by pankajk on 9/24/2018.
 * All rights reserved by Intigate Technologies.
 */
public class NotificationEntity extends BaseResponce{
    @SerializedName("response")
    @Expose
    private Response response;

    public Response getResponse() {
        return response;
    }
}
