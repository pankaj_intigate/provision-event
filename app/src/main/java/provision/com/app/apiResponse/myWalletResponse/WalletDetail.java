package provision.com.app.apiResponse.myWalletResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WalletDetail {

    @SerializedName("Amounttype")
    @Expose
    private String amounttype;
    @SerializedName("Amount")
    @Expose
    private double amount;
    @SerializedName("Reason")
    @Expose
    private String reason;
    @SerializedName("TransactionId")
    @Expose
    private String transactionId;
    @SerializedName("dated")
    @Expose
    private String dated;

    public String getAmounttype() {
        return amounttype;
    }

    public void setAmounttype(String amounttype) {
        this.amounttype = amounttype;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }


    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getDated() {
        return dated;
    }

    public void setDated(String dated) {
        this.dated = dated;
    }

}
