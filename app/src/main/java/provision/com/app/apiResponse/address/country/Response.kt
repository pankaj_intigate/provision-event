package provision.com.app.apiResponse.address.country


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class Response(
    @SerializedName("ShopCountryList")
    var shopCountryList: List<ShopCountry> = listOf()
)