package provision.com.app.apiResponse.my_order_list.my_order_details


import com.google.gson.annotations.SerializedName

data class BookingShopitems(
        @SerializedName("deliveryInfo")
    var deliveryInfo: ArrayList<DeliveryInfo>? = ArrayList(),
        @SerializedName("pickupInfo")
    var pickupInfo: List<Any?>? = listOf()
)