package provision.com.app.apiResponse.shop_product_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetails {
    @SerializedName("i_id")
    @Expose
    private String iId;
    @SerializedName("tsizeId")
    @Expose
    private String tsizeId;
    @SerializedName("TypeTitle")
    @Expose
    private String typeTitle;
    @SerializedName("itemImg")
    @Expose
    private String itemImg;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("itemTitle")
    @Expose
    private String itemTitle;
    @SerializedName("salesPrice")
    @Expose
    private double salesPrice;
    @SerializedName("basePrice")
    @Expose
    private double basePrice;
    @SerializedName("Discount")
    @Expose
    private String discount;
    @SerializedName("DiscountType")
    @Expose
    private String discountType;
    @SerializedName("Discountvalue")
    @Expose
    private String discountvalue;
    @SerializedName("wishlist")
    @Expose
    private Object wishlist;
    @SerializedName("subid")
    @Expose
    private String subid;
    @SerializedName("color_id")
    @Expose
    private String colorId;
    @SerializedName("size_id")
    @Expose
    private String sizeId;
    @SerializedName("sortorder")
    @Expose
    private String sortorder;
    @SerializedName("SizeTitle")
    @Expose
    private String sizeTitle;
    @SerializedName("ColorTitle")
    @Expose
    private String colorTitle;
    @SerializedName("pvcolor")
    @Expose
    private String pvcolor;
    @SerializedName("pvsize")
    @Expose
    private String pvsize;

    public String getiId() {
        return iId;
    }

    public String getTsizeId() {
        return tsizeId;
    }

    public String getTypeTitle() {
        return typeTitle;
    }

    public String getItemImg() {
        return itemImg;
    }

    public String getImage() {
        return image;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public double getSalesPrice() {
        return salesPrice;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public String getDiscount() {
        return discount;
    }

    public String getDiscountType() {
        return discountType;
    }

    public String getDiscountvalue() {
        return discountvalue;
    }

    public Object getWishlist() {
        return wishlist;
    }

    public String getSubid() {
        return subid;
    }

    public String getColorId() {
        return colorId;
    }

    public String getSizeId() {
        return sizeId;
    }

    public String getSortorder() {
        return sortorder;
    }

    public String getSizeTitle() {
        return sizeTitle;
    }

    public String getColorTitle() {
        return colorTitle;
    }

    public String getPvcolor() {
        return pvcolor;
    }

    public String getPvsize() {
        return pvsize;
    }
}
