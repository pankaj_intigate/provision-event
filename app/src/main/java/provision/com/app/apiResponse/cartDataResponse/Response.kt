package provision.com.app.apiResponse.cartDataResponse


import com.google.gson.annotations.SerializedName

data class Response(
        @SerializedName("other_charge_per")
        var otherChargePer: Int? = 0,
        @SerializedName("EventsData")
        var eventsData: ArrayList<EventsData>? = null,
        @SerializedName("ItemsGrouping")
        var itemsGrouping: ItemsGrouping? = ItemsGrouping(),
        @SerializedName("participate_data")
        var participateData: ArrayList<ParticipateData>? = null,
        @SerializedName("itemEvent")
        var itemEvent: String? = "",
        @SerializedName("isItemShow")
        var isItemShow: Int? = 0,
        @SerializedName("Subtotal")
        var subtotal: Double? = 0.000,
        @SerializedName("AddonsTotal")
        var addonsTotal: Double = 0.000,
        @SerializedName("showingAdrresType")
        var showingAdrresType: Double = 0.0,
        @SerializedName("TotalDeliveryAmount")
        var totalDeliveryAmount: Double = 0.000,
        @SerializedName("TotalRefundAmount")
        var totalRefundAmount: Double = 0.000,
        @SerializedName("TotalDiscount")
        var totalDiscount: Double = 0.000,
        @SerializedName("TotalItemAmount")
        var totalItemAmount: Double = 0.000,
        @SerializedName("TotalItemQtyCount")
        var totalItemQtyCount: Int? = 0,
        @SerializedName("TotalEventItemPrice")
        var totalEventItemPrice: String? = "",
        @SerializedName("GrandTotal")
        var grandTotal: Double? = 0.0,
        @SerializedName("TotalFeePrice")
        var totalFeePrice: Int? = 0,
        @SerializedName("TotalEventDeliveryAmount")
        var totalEventDeliveryAmount: Double = 0.000,
        @SerializedName("TotalEventDiscount")
        var totalEventDiscount: Double = 0.000,
        @SerializedName("disclaimer")
        var disclaimer: Disclaimer? = Disclaimer(),
        @SerializedName("IsWallet")
        var isWallet: Int? = 0,
        @SerializedName("IsAppliedShopCoupon")
        var isAppliedShopCoupon: Int? = 0,
        @SerializedName("IsCash")
        var isCash: Int? = 0,
        @SerializedName("IsCashOndelivery")
        var isCashOndelivery: Int? = 0,
        @SerializedName("IsItemData")
        var isItemData: Int? = 0,
        @SerializedName("itemDeliveryType")
        var itemDeliveryType: String? = "",
        @SerializedName("itemDeliveryAmount")
        var itemDeliveryAmount: ItemDeliveryAmount? = null,
        @SerializedName("IsEventData")
        var isEventData: Int? = 0,
        @SerializedName("shippingcharge")
        var shippingcharge: Double = 0.0,
        @SerializedName("userAddInfo")
        var userAddInfo: UserAddInfo? = null,
        @SerializedName("IsAddress")
        var isAddress: Int? = 0,
        @SerializedName("deliveryType")
        var deliveryType: String? = "",
        @SerializedName("IsProceed")
        var isProceed: Int? = 0,
        @SerializedName("IsappliedShipping")
        var isappliedShipping: Int? = 0,
        @SerializedName("shopCouponDetail")
        var shopCouponDetail: ShopCouponDetails? = null,
        @SerializedName("totalItemCount")
        var totalItemCount: Int? = 0,
        @SerializedName("totalEventItemCount")
        var totalEventItemCount: Int? = 0,
        @SerializedName("totalParticipentCount")
        var totalParticipentCount: Int? = 0,
        @SerializedName("EventTotal")
        var eventTotal: Int? = 0,
        @SerializedName("ContactSetting")
        var contactSetting: ContactSetting? = ContactSetting()
)