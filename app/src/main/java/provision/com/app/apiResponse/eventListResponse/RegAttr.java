package provision.com.app.apiResponse.eventListResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RegAttr {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("typeval")
    @Expose
    private String typeval;
    @SerializedName("defaultValue")
    @Expose
    private String defaultValue;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("IsMandetory")
    @Expose
    private String isMandetory;
    @SerializedName("Class")
    @Expose
    private String _class;
    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("Value")
    @Expose
    private List<Value> value = null;
    @SerializedName("Value_array")
    @Expose
    private ValueArray valueArray;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeval() {
        return typeval;
    }

    public void setTypeval(String typeval) {
        this.typeval = typeval;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getIsMandetory() {
        return isMandetory;
    }

    public void setIsMandetory(String isMandetory) {
        this.isMandetory = isMandetory;
    }

    public String getClass_() {
        return _class;
    }

    public void setClass_(String _class) {
        this._class = _class;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Value> getValue() {
        return value;
    }

    public void setValue(List<Value> value) {
        this.value = value;
    }

    public ValueArray getValueArray() {
        return valueArray;
    }

    public void setValueArray(ValueArray valueArray) {
        this.valueArray = valueArray;
    }

}
