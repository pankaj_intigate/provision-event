package provision.com.app.apiResponse.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pankajk on 7/13/2018.
 * All rights reserved by Intigate Technologies.
 */

public class UserResponce {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("userType")
    @Expose
    private String userType;
    @SerializedName("is_logged_in")
    @Expose
    private int isLoggedIn;
    @SerializedName("social_id")
    @Expose
    private String social_id;

    public String getSocial_id() {
        return social_id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getUserType() {
        return userType;
    }

    public int getIsLoggedIn() {
        return isLoggedIn;
    }
}
