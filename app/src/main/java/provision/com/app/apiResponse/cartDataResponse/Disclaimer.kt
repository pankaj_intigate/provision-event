package provision.com.app.apiResponse.cartDataResponse


import com.google.gson.annotations.SerializedName

data class Disclaimer(
    @SerializedName("data")
    var `data`: String? = ""
)