package provision.com.app.apiResponse.filterContentResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {

    @SerializedName("category")
    @Expose
    private List<Category> category = null;
    @SerializedName("Year")
    @Expose
    private List<Integer> year = null;
    @SerializedName("Month")
    @Expose
    private List<String> month = null;
    @SerializedName("status")
    @Expose
    private List<Status> status = null;

    public List<Category> getCategory() {
        return category;
    }

    public void setCategory(List<Category> category) {
        this.category = category;
    }

    public List<Integer> getYear() {
        return year;
    }

    public void setYear(List<Integer> year) {
        this.year = year;
    }

    public List<String> getMonth() {
        return month;
    }

    public void setMonth(List<String> month) {
        this.month = month;
    }

    public List<Status> getStatus() {
        return status;
    }

    public void setStatus(List<Status> status) {
        this.status = status;
    }

}
