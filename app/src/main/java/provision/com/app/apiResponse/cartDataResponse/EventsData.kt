package provision.com.app.apiResponse.cartDataResponse


import com.google.gson.annotations.SerializedName

data class EventsData(
    @SerializedName("BookingId")
    var bookingId: String? = "",
    @SerializedName("Name")
    var name: String? = "",
    @SerializedName("Email")
    var email: String? = "",
    @SerializedName("TShirtSize")
    var tShirtSize: String? = "",
    @SerializedName("Gender")
    var gender: String? = "",
    @SerializedName("dob")
    var dob: String? = "",
    @SerializedName("FeeCategory")
    var feeCategory: String? = "",
    @SerializedName("EventName")
    var eventName: String? = "",
    @SerializedName("FeePrice")
    var feePrice: Double=0.0,
    @SerializedName("RefundableAmount")
    var refundableAmount: String? = "",
    @SerializedName("DeliveryAmount")
    var deliveryAmount: Double=0.0,
    @SerializedName("CouponCode")
    var couponCode: String? = "",
    @SerializedName("CouponValue")
    var couponValue: String? = "",
    @SerializedName("ParticipantId")
    var participantId: String? = "",
    @SerializedName("SessionId")
    var sessionId: String? = "",
    @SerializedName("PrintTshirt")
    var printTshirt: Double =0.0,
    @SerializedName("MedalEnv")
    var medalEnv: Double =0.0,
    @SerializedName("MedalEnvTitle")
    var medalEnvTitle: String? = "",
    @SerializedName("PrintTshirtTitle")
    var printTshirtTitle: String? = "",
    @SerializedName("EventId")
    var eventId: String? = "",
    @SerializedName("TeamName")
    var teamName: Any? = Any()
)