package provision.com.app.apiResponse.my_order_list


import com.google.gson.annotations.SerializedName

data class ItemsInfoData(
    @SerializedName("data")
    var `data`: ArrayList<Data>? = null,
    @SerializedName("totalpagecount")
    var totalpagecount: Int? = 0
)