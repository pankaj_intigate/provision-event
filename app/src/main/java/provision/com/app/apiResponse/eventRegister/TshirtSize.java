package provision.com.app.apiResponse.eventRegister;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TshirtSize {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("Size")
    @Expose
    private String size;
    @SerializedName("masterId")
    @Expose
    private String masterId;
    @SerializedName("eventId")
    @Expose
    private String eventId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getMasterId() {
        return masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

}
