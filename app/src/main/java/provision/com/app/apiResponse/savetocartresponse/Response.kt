package provision.com.app.apiResponse.savetocartresponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class Response {
    @SerializedName("lastItem")
    @Expose
    var lastItem: String? = null
}