package provision.com.app.apiResponse.cartDataResponse


import com.google.gson.annotations.SerializedName

data class DeliveryInfo(
    @SerializedName("item_id")
    var itemId: String? = "",
    @SerializedName("ItemAttrVal")
    var itemAttrVal: String? = "",
    @SerializedName("ItemAmount")
    var itemAmount: String? = "",
    @SerializedName("quantity")
    var quantity: String? = "",
    @SerializedName("ItemName")
    var itemName: String? = "",
    @SerializedName("BookingItemId")
    var bookingItemId: String? = "",
    @SerializedName("SessionId")
    var sessionId: String? = "",
    @SerializedName("itemImg")
    var itemImg: String? = "",
    @SerializedName("is_delivery")
    var isDelivery: String? = "",
    @SerializedName("address_note")
    var addressNote: String? = "",
    @SerializedName("color_code")
    var colorCode: String? = "",
    @SerializedName("color_id")
    var colorId: String? = "",
    @SerializedName("size_id")
    var sizeId: String? = "",
    @SerializedName("wishlist")
    var wishlist: Int? = 0,
    @SerializedName("CartItemType")
    var cartItemType: String? = "",
    @SerializedName("basePrice")
    var basePrice: String? = "",
    @SerializedName("msg")
    var msg: String? = "",
    @SerializedName("maxQtyAvaliable")
    var maxQtyAvaliable: Int= 0
)