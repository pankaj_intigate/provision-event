package provision.com.app.apiResponse.shop_product_list.short_product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShortingProductlist {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("Id")
    @Expose
    private String id;

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public String getId() {
        return id;
    }
}
