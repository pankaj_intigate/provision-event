package provision.com.app.apiResponse.productdetailsresponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class ItemSize {
    @SerializedName("i_id")
    @Expose
    var iId: String? = null
    @SerializedName("v_value")
    @Expose
    var vValue: String? = null
    @SerializedName("Isselected")
    @Expose
    var isselected: Int = 0
    @SerializedName("IsAvailability")
    @Expose
    var isAvailability: Int = 0
    @SerializedName("size_id")
    @Expose
    var sizeId: String? = null
    @SerializedName("sortorder")
    @Expose
    var sortorder: String? = null
}