package provision.com.app.apiResponse.check_participate.save_booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pankajk on 8/23/2018.
 * All rights reserved by Intigate Technologies.
 */

public class SaveBookingResponse {
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("Paytype")
    @Expose
    private String payType;

    public int getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    public String getUrl() {
        return url;
    }

    public String getPayType() {
        return payType;
    }
}
