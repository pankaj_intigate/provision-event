package provision.com.app.apiResponse.cartDataResponse


import com.google.gson.annotations.SerializedName

data class CartDataResponse(
    @SerializedName("Version")
    var version: String? = "",
    @SerializedName("versionName")
    var versionName: String? = "",
    @SerializedName("AndroidVersion")
    var androidVersion: String? = "",
    @SerializedName("ForceUpdate")
    var forceUpdate: Boolean? = false,
    @SerializedName("IosVersion")
    var iosVersion: String? = "",
    @SerializedName("AppBlock")
    var appBlock: String? = "",
    @SerializedName("CurrrencyCode")
    var currrencyCode: String? = "",
    @SerializedName("CurrencyValue")
    var currencyValue: Double=0.0,
    @SerializedName("Numberformat")
    var numberformat: String? = "",
    @SerializedName("cmd")
    var cmd: String? = "",
    @SerializedName("status")
    var status: String? = "",
    @SerializedName("response")
    var response: Response? = Response(),
    @SerializedName("response_messege")
    var responseMessege: String? = ""
)