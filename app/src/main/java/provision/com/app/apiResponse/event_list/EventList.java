package provision.com.app.apiResponse.event_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pankajk on 7/18/2018.
 * All rights reserved by Intigate Technologies.
 */

public class EventList {

    @SerializedName("bookingId")
    @Expose
    private String bookingId;
    @SerializedName("participateId")
    @Expose
    private String participateId;
    @SerializedName("eventName")
    @Expose
    private String eventName;
    @SerializedName("event_date")
    @Expose
    private String eventDate;
    @SerializedName("event_start_date")
    @Expose
    private String eventStartDate;
    @SerializedName("event_end_date")
    @Expose
    private String eventEndDate;
    @SerializedName("VirtualEvent")
    @Expose
    private int virtualEvent=0;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("qrCode")
    @Expose
    private String qrCode;
    @SerializedName("v_reg_no")
    @Expose
    private String vRegNo;
    @SerializedName("participateName")
    @Expose
    private String participateName;


    public int getVirtualEvent() {
        return virtualEvent;
    }

    public String getEventStartDate() {
        return eventStartDate;
    }

    public String getEventEndDate() {
        return eventEndDate;
    }

    public String getBookingId() {
        return bookingId;
    }

    public String getParticipateId() {
        return participateId;
    }

    public String getEventName() {
        return eventName;
    }

    public String getEventDate() {
        return eventDate;
    }

    public String getAddress() {
        return address;
    }

    public String getQrCode() {
        return qrCode;
    }

    public String getvRegNo() {
        return vRegNo;
    }

    public String getParticipateName() {
        return participateName;
    }
}
