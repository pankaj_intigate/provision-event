package provision.com.app.apiResponse.shop_product_list.short_product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import provision.com.app.apiResponse.BaseResponce;

public class ShortingResponse extends BaseResponce {
    @SerializedName("response")
    @Expose
    private ArrayList<ShortingProductlist> shortingResponses;

    public ArrayList<ShortingProductlist> getShortingResponses() {
        return shortingResponses;
    }
}
