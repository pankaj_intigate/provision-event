package provision.com.app.apiResponse.receipt_details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pankajk on 7/18/2018.
 * All rights reserved by Intigate Technologies.
 */

public class Response {
    @SerializedName("invoiceInfo")
    @Expose
    private InvoiceInfo invoiceInfo;
    @SerializedName("participate_info")
    @Expose
    private List<ParticipateInfo> participateInfo = null;
    @SerializedName("totalCoupon")
    @Expose
    private String totalCoupon;
    @SerializedName("bookingitems")
    @Expose
    private List<Object> bookingitems = null;

    public InvoiceInfo getInvoiceInfo() {
        return invoiceInfo;
    }

    public List<ParticipateInfo> getParticipateInfo() {
        return participateInfo;
    }

    public String getTotalCoupon() {
        return totalCoupon;
    }

    public List<Object> getBookingitems() {
        return bookingitems;
    }
}
