package provision.com.app.apiResponse.eventListResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Value {
    @SerializedName("social")
    @Expose
    private String social;
    @SerializedName("genderStr")
    @Expose
    private String genderStr;
    @SerializedName("Id")
    @Expose
    private String Id;
    @SerializedName("Size")
    @Expose
    private String Size;
    @SerializedName("masterId")
    @Expose
    private String masterId;
    @SerializedName("Price")
    @Expose
    private double price;
    @SerializedName("eventId")
    @Expose
    private String eventId;
    @SerializedName("haveyou")
    @Expose
    private String haveyou;

    public String getSocial() {
        return social;
    }

    public void setSocial(String social) {
        this.social = social;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getSize() {
        return Size;
    }

    public void setSize(String size) {
        Size = size;
    }

    public String getMasterId() {
        return masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getHaveyou() {
        return haveyou;
    }

    public void setHaveyou(String haveyou) {
        this.haveyou = haveyou;
    }

    public double getPrice() {
        return price;
    }

    public String getGenderStr() {
        return genderStr;
    }

    @Override
    public String toString() {
        return Size;
    }
}
