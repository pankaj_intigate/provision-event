package provision.com.app.apiResponse.wishlist


import com.google.gson.annotations.SerializedName
import provision.com.app.apiResponse.BaseResponce

data class WishlistResponse(
    @SerializedName("response")
    var response: Response? = Response()
):BaseResponce()