package provision.com.app.apiResponse.wishlist


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("i_id")
    var iId: String? = "",
    @SerializedName("tsizeId")
    var tsizeId: String? = "",
    @SerializedName("TypeTitle")
    var typeTitle: String? = "",
    @SerializedName("itemImg")
    var itemImg: String? = "",
    @SerializedName("image")
    var image: String? = "",
    @SerializedName("itemTitle")
    var itemTitle: String? = "",
    @SerializedName("salesPrice")
    var salesPrice:Double=0.0,
    @SerializedName("basePrice")
    var basePrice:Double=0.0,
    @SerializedName("Discount")
    var discount: Double=0.0,
    @SerializedName("option_id")
    var optionId: String? = "",
    @SerializedName("Discountvalue")
    var discountvalue: String? = ""
)