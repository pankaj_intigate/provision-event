package provision.com.app.apiResponse.getProfileResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CountryCode {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("dial_code")
    @Expose
    private String dialCode;
    @SerializedName("code")
    @Expose
    private String code;
    private boolean countryCodeEnable;

    public boolean isCountryCodeEnable() {

        return countryCodeEnable;
    }

    public void setCountryCodeEnable(boolean countryCodeEnable) {
        this.countryCodeEnable = countryCodeEnable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getDialCode() {
        return dialCode;
    }

    public void setDialCode(String dialCode) {
        this.dialCode = dialCode;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    @Override
    public String toString() {
        if (countryCodeEnable)
            return dialCode;
        else
            return name;
    }
}
