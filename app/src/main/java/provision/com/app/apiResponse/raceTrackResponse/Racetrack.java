package provision.com.app.apiResponse.raceTrackResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Racetrack {

    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("TrackImg")
    @Expose
    private String trackImg;
    @SerializedName("ThumbImg")
    @Expose
    private String thumbImg;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTrackImg() {
        return trackImg;
    }

    public void setTrackImg(String trackImg) {
        this.trackImg = trackImg;
    }

    public String getThumbImg() {
        return thumbImg;
    }

    public void setThumbImg(String thumbImg) {
        this.thumbImg = thumbImg;
    }

}
