package provision.com.app.apiResponse.cartDataResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EventsDatum {

    @SerializedName("BookingId")
    @Expose
    private String bookingId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("TShirtSize")
    @Expose
    private String tShirtSize;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("FeeCategory")
    @Expose
    private String feeCategory;
    @SerializedName("EventName")
    @Expose
    private String eventName;
    @SerializedName("FeePrice")
    @Expose
    private double feePrice;
    @SerializedName("MedalEnv")
    @Expose
    private double medalEnv;
    @SerializedName("PrintTshirt")
    @Expose
    private double printTshirt;
    @SerializedName("RefundableAmount")
    @Expose
    private double refundableAmount;
    @SerializedName("DeliveryAmount")
    @Expose
    private String deliveryAmount;
    @SerializedName("CouponCode")
    @Expose
    private String couponCode;
    @SerializedName("CouponValue")
    @Expose
    private String couponValue;
    @SerializedName("TeamName")
    @Expose
    private String teamName;
    @SerializedName("ParticipantId")
    @Expose
    private String participantId;
    @SerializedName("SessionId")
    @Expose
    private String sessionId;
    @SerializedName("Gender")
    @Expose
    private String gender;

    public double getMedalEnv() {
        return medalEnv;
    }

    public double getPrintTshirt() {
        return printTshirt;
    }

    public String gettShirtSize() {
        return tShirtSize;
    }

    public String getGender() {
        return gender;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public void setTShirtSize(String tShirtSize) {
        this.tShirtSize = tShirtSize;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getFeeCategory() {
        return feeCategory;
    }

    public void setFeeCategory(String feeCategory) {
        this.feeCategory = feeCategory;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public double getFeePrice() {
        return feePrice;
    }

    public void setFeePrice(double feePrice) {
        this.feePrice = feePrice;
    }

    public double getRefundableAmount() {
        return refundableAmount;
    }

    public void setRefundableAmount(double refundableAmount) {
        this.refundableAmount = refundableAmount;
    }

    public String getDeliveryAmount() {
        return deliveryAmount;
    }

    public void setDeliveryAmount(String deliveryAmount) {
        this.deliveryAmount = deliveryAmount;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getCouponValue() {
        return couponValue;
    }

    public void setCouponValue(String couponValue) {
        this.couponValue = couponValue;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

}
