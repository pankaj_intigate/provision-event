package provision.com.app.apiResponse.my_order_list.my_order_details


import com.google.gson.annotations.SerializedName

data class ParticipateInfo(
    @SerializedName("participateId")
    var participateId: String? = "",
    @SerializedName("firstName")
    var firstName: String? = "",
    @SerializedName("lastName")
    var lastName: String? = "",
    @SerializedName("eventName")
    var eventName: String? = "",
    @SerializedName("RegistrationNo")
    var registrationNo: String? = "",
    @SerializedName("Price")
    var price: Double? = 0.00,
    @SerializedName("Gender")
    var gender: String? = "",
    @SerializedName("Email")
    var email: String? = "",
    @SerializedName("Age")
    var age: String? = "",
    @SerializedName("Tshirt")
    var tshirt: String? = "",
    @SerializedName("TeamName")
    var teamName: String? = "",
    @SerializedName("feename")
    var feename: String? = "",
    @SerializedName("discountAmount")
    var discountAmount: String? = "",
    @SerializedName("i_amount")
    var iAmount: String? = "",
    @SerializedName("is_delivarable")
    var isDelivarable: String? = "",
    @SerializedName("i_has_refunded")
    var iHasRefunded: String? = "",
    @SerializedName("eventImg")
    var eventImg: String? = "",
    @SerializedName("Cancelled")
    var cancelled: String? = "",
    @SerializedName("Iscancelled")
    var iscancelled: String? = "",
    @SerializedName("PrintTshirt")
    var printTshirt: Double = 0.000,
    @SerializedName("MedalEnv")
    var medalEnv: Double =0.000,
    @SerializedName("MedalEnvTitle")
    var medalEnvTitle: String? = "",
    @SerializedName("PrintTshirtTitle")
    var printTshirtTitle: String? = ""
)