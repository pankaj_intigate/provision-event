package provision.com.app.apiResponse.my_order_list


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("i_invoice_id")
    var iInvoiceId: String? = "",
    @SerializedName("invoice")
    var invoice: String? = "",
    @SerializedName("price")
    var price: String? = "",
    @SerializedName("size")
    var size: String? = "",
    @SerializedName("quantity")
    var quantity: String? = "",
    @SerializedName("Title")
    var title: String? = "",
    @SerializedName("orderdate")
    var orderdate: String? = "",
    @SerializedName("IsItemData")
    var isItemData: String? = "",
    @SerializedName("ItemDelivery")
    var itemDelivery: String? = "",
    @SerializedName("Address")
    var address: String? = "",
    @SerializedName("bookingId")
    var bookingId: String? = "",
    @SerializedName("totalPrice")
    var totalPrice: Double =0.0 ,
    var type: Int =0
)