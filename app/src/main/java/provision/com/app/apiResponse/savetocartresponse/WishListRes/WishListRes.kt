package provision.com.app.apiResponse.savetocartresponse.WishListRes


import com.google.gson.annotations.SerializedName
import provision.com.app.apiResponse.BaseResponce

data class WishListRes(
        @SerializedName("response")
        var response: Response? = Response()
) : BaseResponce()