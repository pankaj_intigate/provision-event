package provision.com.app.apiResponse.my_order_list


import com.google.gson.annotations.SerializedName

data class UserInfo(
    @SerializedName("userId")
    var userId: String? = "",
    @SerializedName("FullName")
    var fullName: String? = "",
    @SerializedName("Gender")
    var gender: String? = "",
    @SerializedName("Email")
    var email: String? = "",
    @SerializedName("PhoneNo")
    var phoneNo: String? = "",
    @SerializedName("Tsize")
    var tsize: String? = "",
    @SerializedName("Country")
    var country: String? = "",
    @SerializedName("Residence")
    var residence: String? = "",
    @SerializedName("Dob")
    var dob: String? = "",
    @SerializedName("country_code")
    var countryCode: String? = "",
    @SerializedName("userImg")
    var userImg: String? = "",
    @SerializedName("Address")
    var address: String? = "",
    @SerializedName("IsNotify")
    var isNotify: String? = ""
)