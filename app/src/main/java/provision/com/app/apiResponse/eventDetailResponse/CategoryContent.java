package provision.com.app.apiResponse.eventDetailResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryContent {

    @SerializedName("Image")
    @Expose
    private String image;
    @SerializedName("v_title")
    @Expose
    private String vTitle;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getVTitle() {
        return vTitle;
    }

    public void setVTitle(String vTitle) {
        this.vTitle = vTitle;
    }

}
