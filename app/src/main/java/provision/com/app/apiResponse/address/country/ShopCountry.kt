package provision.com.app.apiResponse.address.country


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class ShopCountry(
    @SerializedName("api_code")
    var apiCode: Any = Any(),
    @SerializedName("i_id")
    var iId: String = "",
    @SerializedName("Name")
    var name: String = ""
){
    override fun toString(): String {
        return name
    }
}