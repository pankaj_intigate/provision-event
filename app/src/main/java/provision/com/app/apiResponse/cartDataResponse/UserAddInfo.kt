package provision.com.app.apiResponse.cartDataResponse


import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class UserAddInfo(
    @SerializedName("userId")
    var userId: String? = "",
    @SerializedName("FullName")
    var fullName: String? = "",
    @SerializedName("PhoneNo")
    var phoneNo: String? = "",
    @SerializedName("Country")
    var country: String? = "",
    @SerializedName("CId")
    var cId: String? = "",
    @SerializedName("C_Id")
    var c_Id: String? = "",
    @SerializedName("address_apartment_no")
    var addressApartmentNo: String? = "",
    @SerializedName("CityName")
    var cityName: String? = "",
    @SerializedName("emailAddr")
    var emailAddr: String? = "",
    @SerializedName("Statename")
    var statename: String? = "",
    @SerializedName("Sid")
    var sid: String? = "",
    @SerializedName("address_street")
    var addressStreet: String? = "",
    @SerializedName("block")
    var block: String? = "",
    @SerializedName("avenue")
    var avenue: String? = ""
):Parcelable{
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(userId)
        parcel.writeString(fullName)
        parcel.writeString(phoneNo)
        parcel.writeString(country)
        parcel.writeString(cId)
        parcel.writeString(c_Id)
        parcel.writeString(addressApartmentNo)
        parcel.writeString(cityName)
        parcel.writeString(emailAddr)
        parcel.writeString(statename)
        parcel.writeString(sid)
        parcel.writeString(addressStreet)
        parcel.writeString(block)
        parcel.writeString(avenue)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UserAddInfo> {
        override fun createFromParcel(parcel: Parcel): UserAddInfo {
            return UserAddInfo(parcel)
        }

        override fun newArray(size: Int): Array<UserAddInfo?> {
            return arrayOfNulls(size)
        }
    }

}