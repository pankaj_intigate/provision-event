package provision.com.app.apiResponse.cartDataResponse


import com.google.gson.annotations.SerializedName

data class ContactSetting(
    @SerializedName("Id")
    var id: String? = "",
    @SerializedName("Phone")
    var phone: String? = "",
    @SerializedName("Email")
    var email: String? = "",
    @SerializedName("Available")
    var available: String? = "",
    @SerializedName("status")
    var status: String? = ""
)