package provision.com.app.apiResponse.faqResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {

    @SerializedName("event_faq")
    @Expose
    private List<EventFaq> eventFaq = null;

    public List<EventFaq> getEventFaq() {
        return eventFaq;
    }

    public void setEventFaq(List<EventFaq> eventFaq) {
        this.eventFaq = eventFaq;
    }

}
