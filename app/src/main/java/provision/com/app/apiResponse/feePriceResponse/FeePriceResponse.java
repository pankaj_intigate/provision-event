package provision.com.app.apiResponse.feePriceResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeePriceResponse {
    @SerializedName("response")
    @Expose
    private Response response;
    @SerializedName("CurrrencyCode")
    @Expose
    private String currrencyCode;
    @SerializedName("CurrencyValue")
    @Expose
    private double currencyValue;
    @SerializedName("Numberformat")
    @Expose
    private int numberformat;

    public String getCurrrencyCode() {
        return currrencyCode;
    }

    public double getCurrencyValue() {
        return currencyValue;
    }

    public int getNumberformat() {
        return numberformat;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

}
