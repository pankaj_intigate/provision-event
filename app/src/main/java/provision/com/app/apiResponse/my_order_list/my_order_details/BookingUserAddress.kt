package provision.com.app.apiResponse.my_order_list.my_order_details


import com.google.gson.annotations.SerializedName

data class BookingUserAddress(
    @SerializedName("i_id")
    var iId: String? = "",
    @SerializedName("user_id")
    var userId: String? = "",
    @SerializedName("bookingId")
    var bookingId: String? = "",
    @SerializedName("sessionId")
    var sessionId: String? = "",
    @SerializedName("reg_fullname")
    var regFullname: String? = "",
    @SerializedName("reg_firstName")
    var regFirstName: String? = "",
    @SerializedName("reg_lastName")
    var regLastName: String? = "",
    @SerializedName("v_gender")
    var vGender: Any? = Any(),
    @SerializedName("reg_phoneNumber")
    var regPhoneNumber: String? = "",
    @SerializedName("emailAddr")
    var emailAddr: String? = "",
    @SerializedName("reg_addr")
    var regAddr: String? = "",
    @SerializedName("CId")
    var cId: String? = "",
    @SerializedName("CountryName")
    var countryName: String? = "",
    @SerializedName("C_Id")
    var c_Id: String? = "",
    @SerializedName("CityName")
    var cityName: String? = "",
    @SerializedName("SId")
    var sId: String? = "",
    @SerializedName("Statename")
    var statename: String? = "",
    @SerializedName("DeliveryType")
    var deliveryType: String? = "",
    @SerializedName("address_street")
    var addressStreet: String? = "",
    @SerializedName("address_apartment_no")
    var addressApartmentNo: String? = "",
    @SerializedName("avenue")
    var avenue: String? = "",
    @SerializedName("block")
    var block: String? = "",
    @SerializedName("status")
    var status: String? = ""
)