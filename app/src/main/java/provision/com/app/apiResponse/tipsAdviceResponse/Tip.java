package provision.com.app.apiResponse.tipsAdviceResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Tip {

    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("VolImg")
    @Expose
    private String volImg;
    @SerializedName("ThumbImg")
    @Expose
    private String thumbImg;
    @SerializedName("FileToDownload")
    @Expose
    private String fileToDownload;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVolImg() {
        return volImg;
    }

    public void setVolImg(String volImg) {
        this.volImg = volImg;
    }

    public String getThumbImg() {
        return thumbImg;
    }

    public void setThumbImg(String thumbImg) {
        this.thumbImg = thumbImg;
    }

    public String getFileToDownload() {
        return fileToDownload;
    }

    public void setFileToDownload(String fileToDownload) {
        this.fileToDownload = fileToDownload;
    }

}
