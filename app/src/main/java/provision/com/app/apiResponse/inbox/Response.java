package provision.com.app.apiResponse.inbox;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pankajk on 7/18/2018.
 * All rights reserved by Intigate Technologies.
 */

public class Response {

    @SerializedName("userInbox")
    @Expose
    private UserInbox userInbox;

    public UserInbox getUserInbox() {
        return userInbox;
    }
}
