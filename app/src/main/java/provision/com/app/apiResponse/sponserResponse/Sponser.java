package provision.com.app.apiResponse.sponserResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sponser {

    @SerializedName("SponserImg")
    @Expose
    private String sponserImg;
    @SerializedName("ThumbImg")
    @Expose
    private String thumbImg;

    public String getSponserImg() {
        return sponserImg;
    }

    public void setSponserImg(String sponserImg) {
        this.sponserImg = sponserImg;
    }

    public String getThumbImg() {
        return thumbImg;
    }

    public void setThumbImg(String thumbImg) {
        this.thumbImg = thumbImg;
    }

}
