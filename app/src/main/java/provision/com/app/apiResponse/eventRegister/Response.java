package provision.com.app.apiResponse.eventRegister;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {

    @SerializedName("uInfo")
    @Expose
    private UInfo uInfo;
    @SerializedName("eventlist")
    @Expose
    private List<Eventlist> eventlist = null;
    @SerializedName("itemlist")
    @Expose
    private List<Itemlist> itemlist = null;
    @SerializedName("eventId")
    @Expose
    private String eventId;
    @SerializedName("disclaimer")
    @Expose
    private Disclaimer disclaimer;
    @SerializedName("tshirt_size")
    @Expose
    private List<TshirtSize> tshirtSize = null;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("meta_desc")
    @Expose
    private String metaDesc;
    @SerializedName("keyword")
    @Expose
    private String keyword;
    @SerializedName("Event_Lock")
    @Expose
    private String eventLock;
    @SerializedName("Country")
    @Expose
    private List<Country> country = null;

    public UInfo getUInfo() {
        return uInfo;
    }
//
//    public void setUInfo(UInfo uInfo) {
//        this.uInfo = uInfo;
//    }

    public List<Eventlist> getEventlist() {
        return eventlist;
    }

    public void setEventlist(List<Eventlist> eventlist) {
        this.eventlist = eventlist;
    }

    public List<Itemlist> getItemlist() {
        return itemlist;
    }

    public void setItemlist(List<Itemlist> itemlist) {
        this.itemlist = itemlist;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public Disclaimer getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(Disclaimer disclaimer) {
        this.disclaimer = disclaimer;
    }

    public List<TshirtSize> getTshirtSize() {
        return tshirtSize;
    }

    public void setTshirtSize(List<TshirtSize> tshirtSize) {
        this.tshirtSize = tshirtSize;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMetaDesc() {
        return metaDesc;
    }

    public void setMetaDesc(String metaDesc) {
        this.metaDesc = metaDesc;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getEventLock() {
        return eventLock;
    }

    public void setEventLock(String eventLock) {
        this.eventLock = eventLock;
    }

    public List<Country> getCountry() {
        return country;
    }

    public void setCountry(List<Country> country) {
        this.country = country;
    }

}
