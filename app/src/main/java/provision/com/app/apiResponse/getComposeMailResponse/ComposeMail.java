package provision.com.app.apiResponse.getComposeMailResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import provision.com.app.apiResponse.BaseResponce;

/**
 * Created by gauravg on 7/19/2018.
 */

public class ComposeMail extends BaseResponce {
    @SerializedName("response")
    @Expose
    private Response response;

    public Response getResponse() {
        return response;
    }
}
