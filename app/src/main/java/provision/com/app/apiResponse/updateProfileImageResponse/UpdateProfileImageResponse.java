package provision.com.app.apiResponse.updateProfileImageResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateProfileImageResponse {

    @SerializedName("Version")
    @Expose
    private String version;
    @SerializedName("AppBlock")
    @Expose
    private String appBlock;
    @SerializedName("CurrrencyCode")
    @Expose
    private String currrencyCode;
    @SerializedName("CurrencyValue")
    @Expose
    private String currencyValue;
    @SerializedName("Numberformat")
    @Expose
    private String numberformat;
    @SerializedName("cmd")
    @Expose
    private String cmd;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("response")
    @Expose
    private Response response;
    @SerializedName("response_messege")
    @Expose
    private String responseMessege;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAppBlock() {
        return appBlock;
    }

    public void setAppBlock(String appBlock) {
        this.appBlock = appBlock;
    }

    public String getCurrrencyCode() {
        return currrencyCode;
    }

    public void setCurrrencyCode(String currrencyCode) {
        this.currrencyCode = currrencyCode;
    }

    public String getCurrencyValue() {
        return currencyValue;
    }

    public void setCurrencyValue(String currencyValue) {
        this.currencyValue = currencyValue;
    }

    public String getNumberformat() {
        return numberformat;
    }

    public void setNumberformat(String numberformat) {
        this.numberformat = numberformat;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public String getResponseMessege() {
        return responseMessege;
    }

    public void setResponseMessege(String responseMessege) {
        this.responseMessege = responseMessege;
    }

}
