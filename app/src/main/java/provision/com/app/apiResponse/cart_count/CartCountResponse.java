package provision.com.app.apiResponse.cart_count;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pankajk on 8/28/2018.
 * All rights reserved by Intigate Technologies.
 */

public class CartCountResponse {
    @SerializedName("cart_count")
    @Expose
    private int cart_count;

    public int getCart_count() {
        return cart_count;
    }
}
