package provision.com.app.apiResponse.inboxDetailsResponse;

import androidx.annotation.Keep;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
@Keep

public class List {

    @SerializedName("cmd")
    @Expose
    private String cmd;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("event_name")
    @Expose
    private String eventName;
    @SerializedName("response")
    @Expose
    private java.util.List<Response_> response = null;
    @SerializedName("response_messege")
    @Expose
    private String responseMessege;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public java.util.List<Response_> getResponse() {
        return response;
    }

    public void setResponse(java.util.List<Response_> response) {
        this.response = response;
    }

    public String getResponseMessege() {
        return responseMessege;
    }

    public void setResponseMessege(String responseMessege) {
        this.responseMessege = responseMessege;
    }

}
