package provision.com.app.apiResponse.event_shop


import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class ItemImage(
    @SerializedName("Id")
    var id: String? = "",
    @SerializedName("ItemId")
    var itemId: String? = "",
    @SerializedName("Image")
    var image: String? = "",
    @SerializedName("color_id")
    var colorId: String? = "",
    @SerializedName("Featured")
    var featured: String? = "",
    @SerializedName("imageUrl")
    var imageUrl: String? = ""
):Parcelable{
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(itemId)
        parcel.writeString(image)
        parcel.writeString(colorId)
        parcel.writeString(featured)
        parcel.writeString(imageUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ItemImage> {
        override fun createFromParcel(parcel: Parcel): ItemImage {
            return ItemImage(parcel)
        }

        override fun newArray(size: Int): Array<ItemImage?> {
            return arrayOfNulls(size)
        }
    }

}