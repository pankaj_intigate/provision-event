package provision.com.app.apiResponse.event_shop


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("i_id")
    var iId: String? = "",
    @SerializedName("tsizeId")
    var tsizeId: String = "",
    @SerializedName("TypeTitle")
    var typeTitle: String = "",
    @SerializedName("itemImg")
    var itemImg: String? = "",
    @SerializedName("image")
    var image: String = "",
    @SerializedName("itemTitle")
    var itemTitle: String ?= null,
    @SerializedName("salesPrice")
    var salesPrice: Double=0.0,
    @SerializedName("basePrice")
    var basePrice: String = "",
    @SerializedName("discounttype")
    var discounttype: String = "",
    @SerializedName("Discount")
    var discount: String = "",
    @SerializedName("wishlist")
    var wishlist: Int? = 0,
    @SerializedName("IsSelectedcolor")
    var isSelectedcolor: String = "",
    @SerializedName("IsSelectedsize")
    var isSelectedsize: String = "",
    @SerializedName("option_id")
    var optionId: String = "",
    @SerializedName("IsStock")
    var isStock: Int = 0,
    @SerializedName("item_image")
    var itemImage: ArrayList<ItemImage>? = null,
    @SerializedName("Discountvalue")
    var discountvalue: String? = "",
    @SerializedName("shareurl")
    var shareurl: String? = "",
    @SerializedName("itemSize")
    var itemSize: ArrayList<ItemSize>? = null,
    @SerializedName("ColorAttr")
    var colorAttr: ArrayList<ColorAttr>? =null
)