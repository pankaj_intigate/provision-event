package provision.com.app.apiResponse.volunteerResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {

    @SerializedName("volunteer")
    @Expose
    private List<Volunteer> volunteer = null;

    public List<Volunteer> getVolunteer() {
        return volunteer;
    }

    public void setVolunteer(List<Volunteer> volunteer) {
        this.volunteer = volunteer;
    }

}
