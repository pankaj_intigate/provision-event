package provision.com.app.apiResponse.city_response


import com.google.gson.annotations.SerializedName
import provision.com.app.apiResponse.BaseResponce

data class CityResponse(
        @SerializedName("response")
        var response: ArrayList<Response>? = null

        ) : BaseResponce() {

}