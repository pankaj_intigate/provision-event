package provision.com.app.apiResponse.shop_product_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class Response {
    @SerializedName("data")
    @Expose
    ArrayList<ProductDetails> arrayList;

    public ArrayList<ProductDetails> getArrayList() {
        return arrayList;
    }
}
