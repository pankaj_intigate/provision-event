package provision.com.app.apiResponse.wishlist


import com.google.gson.annotations.SerializedName

data class Response(
    @SerializedName("data")
    var data: ArrayList<Data>? = null,
    @SerializedName("totalpagecount")
    var totalpagecount: Int? = 0
)