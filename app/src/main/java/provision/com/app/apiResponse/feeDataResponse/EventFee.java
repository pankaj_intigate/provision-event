package provision.com.app.apiResponse.feeDataResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EventFee {

    @SerializedName("Category")
    @Expose
    private String category;
    @SerializedName("ShowIndivisualPrice")
    @Expose
    private double showIndivisualPrice;
    @SerializedName("ShowTeamPrice")
    @Expose
    private double showTeamPrice;
    @SerializedName("IndivisualPrice")
    @Expose
    private double indivisualPrice;
    @SerializedName("TeamPrice")
    @Expose
    private double teamPrice;

    public String getCategory() {
        return category;
    }

    public double getShowIndivisualPrice() {
        return showIndivisualPrice;
    }

    public double getShowTeamPrice() {
        return showTeamPrice;
    }

    public double getIndivisualPrice() {
        return indivisualPrice;
    }

    public double getTeamPrice() {
        return teamPrice;
    }
}
