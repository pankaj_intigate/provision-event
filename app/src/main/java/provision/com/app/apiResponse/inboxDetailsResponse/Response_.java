package provision.com.app.apiResponse.inboxDetailsResponse;

import androidx.annotation.Keep;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
@Keep

public class Response_ {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("MailConversationId")
    @Expose
    private String mailConversationId;
    @SerializedName("UserId")
    @Expose
    private String userId;
    @SerializedName("guestEmail")
    @Expose
    private Object guestEmail;
    @SerializedName("userEmail")
    @Expose
    private String userEmail;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("Subject")
    @Expose
    private String subject;
    @SerializedName("Body")
    @Expose
    private String body;
    @SerializedName("SendBy")
    @Expose
    private String sendBy;
    @SerializedName("CreateOn")
    @Expose
    private String createOn;
    @SerializedName("CreateTime")
    @Expose
    private String createTime;
    @SerializedName("SenderName")
    @Expose
    private String senderName;
    @SerializedName("Image")
    @Expose
    private String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMailConversationId() {
        return mailConversationId;
    }

    public void setMailConversationId(String mailConversationId) {
        this.mailConversationId = mailConversationId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Object getGuestEmail() {
        return guestEmail;
    }

    public void setGuestEmail(Object guestEmail) {
        this.guestEmail = guestEmail;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSendBy() {
        return sendBy;
    }

    public void setSendBy(String sendBy) {
        this.sendBy = sendBy;
    }

    public String getCreateOn() {
        return createOn;
    }

    public void setCreateOn(String createOn) {
        this.createOn = createOn;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
