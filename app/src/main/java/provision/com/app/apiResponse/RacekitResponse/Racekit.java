package provision.com.app.apiResponse.RacekitResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Racekit {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("KitName")
    @Expose
    private String kitName;
    @SerializedName("KitImg")
    @Expose
    private String kitImg;
    @SerializedName("ThumbImg")
    @Expose
    private String thumbImg;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKitName() {
        return kitName;
    }

    public void setKitName(String kitName) {
        this.kitName = kitName;
    }

    public String getKitImg() {
        return kitImg;
    }

    public void setKitImg(String kitImg) {
        this.kitImg = kitImg;
    }

    public String getThumbImg() {
        return thumbImg;
    }

    public void setThumbImg(String thumbImg) {
        this.thumbImg = thumbImg;
    }

}
