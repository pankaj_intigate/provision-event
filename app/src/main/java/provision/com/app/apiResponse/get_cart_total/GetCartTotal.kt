package provision.com.app.apiResponse.get_cart_total

import com.google.gson.annotations.SerializedName
import provision.com.app.apiResponse.BaseResponce

class GetCartTotal : BaseResponce() {
    @SerializedName("response")
    var response: Response? = Response()
}