package provision.com.app.apiResponse.get_cart_total


import com.google.gson.annotations.SerializedName

data class Response(
        @SerializedName("GrandTotal")
        var grandTotal: Double? = 0.0
)