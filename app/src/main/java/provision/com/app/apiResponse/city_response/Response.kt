package provision.com.app.apiResponse.city_response


import com.google.gson.annotations.SerializedName

data class Response(
    @SerializedName("Id")
    var id: String? = "",
    @SerializedName("CityName")
    var cityName: String = ""
){
    override fun toString(): String {
        return cityName
    }
}