package provision.com.app.apiResponse.savetocartresponse.WishListRes


import com.google.gson.annotations.SerializedName

data class Response(
    @SerializedName("msg")
    var msg: String? = "",
    @SerializedName("status")
    var status: Int= 0
)