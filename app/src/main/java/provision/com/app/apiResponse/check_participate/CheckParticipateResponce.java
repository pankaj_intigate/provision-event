package provision.com.app.apiResponse.check_participate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pankajk on 8/23/2018.
 * All rights reserved by Intigate Technologies.
 */

public class CheckParticipateResponce {
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("msg")
    @Expose
    private String msg;

    public int getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }
}
