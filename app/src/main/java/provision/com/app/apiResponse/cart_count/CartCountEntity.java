package provision.com.app.apiResponse.cart_count;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import provision.com.app.apiResponse.BaseResponce;

/**
 * Created by pankajk on 8/28/2018.
 * All rights reserved by Intigate Technologies.
 */

public class CartCountEntity extends BaseResponce {
    @SerializedName("response")
    @Expose
    private CartCountResponse cartCountResponse;

    public CartCountResponse getCartCountResponse() {
        return cartCountResponse;
    }
}
