package provision.com.app.apiResponse.getComposeMailResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Mailtype {

    @SerializedName("cmd")
    @Expose
    private String cmd;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("suppordata")
    @Expose
    private List<Suppordatum> suppordata = null;
    @SerializedName("response_messege")
    @Expose
    private String responseMessege;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Suppordatum> getSuppordata() {
        return suppordata;
    }

    public void setSuppordata(List<Suppordatum> suppordata) {
        this.suppordata = suppordata;
    }

    public String getResponseMessege() {
        return responseMessege;
    }

    public void setResponseMessege(String responseMessege) {
        this.responseMessege = responseMessege;
    }

}
