package provision.com.app.apiResponse.feeDataResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {

    @SerializedName("event_fee")
    @Expose
    private List<EventFee> eventFee = null;

    public List<EventFee> getEventFee() {
        return eventFee;
    }

    public void setEventFee(List<EventFee> eventFee) {
        this.eventFee = eventFee;
    }

}
