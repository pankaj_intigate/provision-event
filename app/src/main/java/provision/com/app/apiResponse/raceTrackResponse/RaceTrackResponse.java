package provision.com.app.apiResponse.raceTrackResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import provision.com.app.apiResponse.BaseResponce;

public class RaceTrackResponse extends BaseResponce {


    @SerializedName("response")
    @Expose
    private Response response;


    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

}
