package provision.com.app.apiResponse.feePriceResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("EventFee")
    @Expose
    private EventFee eventFee;

    public EventFee getEventFee() {
        return eventFee;
    }

    public void setEventFee(EventFee eventFee) {
        this.eventFee = eventFee;
    }

}
