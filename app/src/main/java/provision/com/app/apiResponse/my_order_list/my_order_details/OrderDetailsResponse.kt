package provision.com.app.apiResponse.my_order_list.my_order_details


import com.google.gson.annotations.SerializedName
import provision.com.app.apiResponse.BaseResponce

data class OrderDetailsResponse(
    @SerializedName("response")
    var response: Response? = Response()
):BaseResponce()