package provision.com.app.apiResponse.event_shop


import com.google.gson.annotations.SerializedName

data class ItemSize(
    @SerializedName("i_id")
    var iId: String? = "",
    @SerializedName("v_value")
    var vValue: String? = "",
    @SerializedName("Isselected")
    var isselected: Int= 0,
    @SerializedName("size_id")
    var sizeId: String? = "",
    @SerializedName("sortorder")
    var sortorder: String? = "",
    @SerializedName("IsAvailability")
    var isAvailability: Int = 0
)