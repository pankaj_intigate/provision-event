package provision.com.app.apiResponse.feePriceResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Fee {

    @SerializedName("tprice")
    @Expose
    private double tprice;
    @SerializedName("price")
    @Expose
    private double price;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("is_d")
    @Expose
    private String isD;
    @SerializedName("d_amount")
    @Expose
    private String dAmount;

    public double getTprice() {
        return tprice;
    }

    public double getPrice() {
        return price;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIsD() {
        return isD;
    }

}
