package provision.com.app.apiResponse.unread_count;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pankajk on 9/24/2018.
 * All rights reserved by Intigate Technologies.
 */
public class Response {
    @SerializedName("unReadCount")
    @Expose
    private int unReadCount;

    public int getUnReadCount() {
        return unReadCount;
    }
}
