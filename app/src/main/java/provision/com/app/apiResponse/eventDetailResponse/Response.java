package provision.com.app.apiResponse.eventDetailResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {

    @SerializedName("event_detail")
    @Expose
    private EventDetail eventDetail;
    @SerializedName("event_img")
    @Expose
    private List<EventImg> eventImg = null;
    @SerializedName("share_url")
    @Expose
    private String shareUrl;
    @SerializedName("fb_url")
    @Expose
    private String fbUrl;
    @SerializedName("fb_image")
    @Expose
    private String fbImage;
    @SerializedName("analyticCode")
    @Expose
    private String analyticCode;
    @SerializedName("compaignCode")
    @Expose
    private String compaignCode;
    @SerializedName("category_content")
    @Expose
    private CategoryContent categoryContent;
    @SerializedName("feature")
    @Expose
    private List<Feature> feature = null;

    public String getShareUrl() {
        return shareUrl;
    }

    public EventDetail getEventDetail() {
        return eventDetail;
    }

    public void setEventDetail(EventDetail eventDetail) {
        this.eventDetail = eventDetail;
    }

    public List<EventImg> getEventImg() {
        return eventImg;
    }

    public void setEventImg(List<EventImg> eventImg) {
        this.eventImg = eventImg;
    }

    public String getFbUrl() {
        return fbUrl;
    }

    public void setFbUrl(String fbUrl) {
        this.fbUrl = fbUrl;
    }

    public String getFbImage() {
        return fbImage;
    }

    public void setFbImage(String fbImage) {
        this.fbImage = fbImage;
    }

    public String getAnalyticCode() {
        return analyticCode;
    }

    public void setAnalyticCode(String analyticCode) {
        this.analyticCode = analyticCode;
    }

    public String getCompaignCode() {
        return compaignCode;
    }

    public void setCompaignCode(String compaignCode) {
        this.compaignCode = compaignCode;
    }

    public CategoryContent getCategoryContent() {
        return categoryContent;
    }

    public void setCategoryContent(CategoryContent categoryContent) {
        this.categoryContent = categoryContent;
    }

    public List<Feature> getFeature() {
        return feature;
    }

    public void setFeature(List<Feature> feature) {
        this.feature = feature;
    }

}
