package provision.com.app.apiResponse.termsConditionsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("termcondition")
    @Expose
    private Termcondition termcondition;

    public Termcondition getTermcondition() {
        return termcondition;
    }

    public void setTermcondition(Termcondition termcondition) {
        this.termcondition = termcondition;
    }

}
