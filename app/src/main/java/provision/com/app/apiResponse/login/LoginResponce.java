package provision.com.app.apiResponse.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import provision.com.app.apiResponse.BaseResponce;

/**
 * Created by pankajk on 7/13/2018.
 * All rights reserved by Intigate Technologies.
 */

public class LoginResponce extends BaseResponce {
    @SerializedName("response")
    @Expose
    UserResponce responce;

    public UserResponce getResponce() {
        return responce;
    }
}
