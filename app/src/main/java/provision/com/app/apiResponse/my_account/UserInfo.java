package provision.com.app.apiResponse.my_account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pankajk on 7/13/2018.
 * All rights reserved by Intigate Technologies.
 */

public class UserInfo {
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("Gender")
    @Expose
    private String gender;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("PhoneNo")
    @Expose
    private String phoneNo;
    @SerializedName("Tsize")
    @Expose
    private String tsize;
    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("Residence")
    @Expose
    private String residence;
    @SerializedName("Dob")
    @Expose
    private String dob;
    @SerializedName("userImg")
    @Expose
    private String userImg;
    @SerializedName("country_code")
    @Expose
    private String country_code;
    @SerializedName("Address")
    @Expose
    private String adrress;
    @SerializedName("IsNotify")
    @Expose
    private String IsNotify;

    public String getIsNotify() {
        return IsNotify;
    }

    public String getAdrress() {
        return adrress;
    }

    public String getUserId() {
        return userId;
    }

    public String getFullName() {
        return fullName;
    }

    public String getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public String getTsize() {
        return tsize;
    }

    public String getCountry() {
        return country;
    }

    public String getResidence() {
        return residence;
    }

    public String getDob() {
        return dob;
    }

    public String getUserImg() {
        return userImg;
    }

    public String getCountry_code() {
        return country_code;
    }
}
