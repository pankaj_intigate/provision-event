package provision.com.app.apiResponse.receipt_details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pankajk on 7/18/2018.
 * All rights reserved by Intigate Technologies.
 */

public class ParticipateInfo {
    @SerializedName("participateId")
    @Expose
    private String participateId;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("eventName")
    @Expose
    private String eventName;
    @SerializedName("RegistrationNo")
    @Expose
    private String registrationNo;
    @SerializedName("Price")
    @Expose
    private double price;
    @SerializedName("Age")
    @Expose
    private String age;
    @SerializedName("Tshirt")
    @Expose
    private String tshirt;
    @SerializedName("TeamName")
    @Expose
    private String teamName;
    @SerializedName("feename")
    @Expose
    private String feename;
    @SerializedName("discountAmount")
    @Expose
    private String discountAmount;
    @SerializedName("i_amount")
    @Expose
    private String iAmount;
    @SerializedName("is_delivarable")
    @Expose
    private String isDelivarable;
    @SerializedName("i_has_refunded")
    @Expose
    private String iHasRefunded;
    @SerializedName("eventImg")
    @Expose
    private String eventImg;
    @SerializedName("Cancelled")
    @Expose
    private int cancelled;

    @SerializedName("Iscancelled")
    @Expose
    private int isCancelled;

    public int getIsCancelled() {
        return isCancelled;
    }

    public String getParticipateId() {
        return participateId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEventName() {
        return eventName;
    }

    public String getRegistrationNo() {
        return registrationNo;
    }

    public double getPrice() {
        return price;
    }

    public String getAge() {
        return age;
    }

    public String getTshirt() {
        return tshirt;
    }

    public String getTeamName() {
        return teamName;
    }

    public String getFeename() {
        return feename;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public String getiAmount() {
        return iAmount;
    }

    public String getIsDelivarable() {
        return isDelivarable;
    }

    public String getiHasRefunded() {
        return iHasRefunded;
    }

    public String getEventImg() {
        return eventImg;
    }

    public int getCancelled() {
        return cancelled;
    }
}
