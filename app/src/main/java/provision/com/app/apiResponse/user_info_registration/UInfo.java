package provision.com.app.apiResponse.user_info_registration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pankajk on 9/20/2018.
 * All rights reserved by Intigate Technologies.
 */
public class UInfo {
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("Gender")
    @Expose
    private String gender;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("PhoneNo")
    @Expose
    private String phoneNo;
    @SerializedName("Tsize")
    @Expose
    private String tsize;
    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("Residence")
    @Expose
    private String residence;
    @SerializedName("Dob")
    @Expose
    private String dob;
    @SerializedName("fireupload")
    @Expose
    private String fireupload;
    @SerializedName("ti_group_id")
    @Expose
    private String tiGroupId;
    @SerializedName("userImg")
    @Expose
    private String userImg;
    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("dd")
    @Expose
    private String dd;
    @SerializedName("mm")
    @Expose
    private String mm;
    @SerializedName("yy")
    @Expose
    private String yy;
    @SerializedName("country_code")
    @Expose
    private String countryCode;

    public String getCountryCode() {
        return countryCode;
    }

    public String getUserId() {
        return userId;
    }

    public String getFullName() {
        return fullName;
    }

    public String getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public String getTsize() {
        return tsize;
    }

    public String getCountry() {
        return country;
    }

    public String getResidence(){
        return residence;
    }

    public String getDob() {
        return dob;
    }

    public String getFireupload() {
        return fireupload;
    }

    public String getTiGroupId() {
        return tiGroupId;
    }

    public String getUserImg() {
        return userImg;
    }

    public String getAddress() {
        return address;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDd() {
        return dd;
    }

    public String getMm() {
        return mm;
    }

    public String getYy() {
        return yy;
    }
}
