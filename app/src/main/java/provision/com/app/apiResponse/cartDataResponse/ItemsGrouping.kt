package provision.com.app.apiResponse.cartDataResponse


import com.google.gson.annotations.SerializedName

data class ItemsGrouping(
    @SerializedName("deliveryInfo")
    var deliveryInfo: List<DeliveryInfo>? = null,
    @SerializedName("pickupInfo")
    var pickupInfo: List<Any?>? = listOf()
)