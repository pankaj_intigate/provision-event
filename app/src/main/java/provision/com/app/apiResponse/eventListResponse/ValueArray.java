package provision.com.app.apiResponse.eventListResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ValueArray {

    @SerializedName("Year")
    @Expose
    private List<Year> year = null;
    @SerializedName("Month")
    @Expose
    private List<Month> month = null;
    @SerializedName("Days")
    @Expose
    private List<Date> dates = null;

    public List<Year> getYear() {
        return year;
    }

    public List<Month> getMonth() {
        return month;
    }

    public List<Date> getDates() {
        return dates;
    }
}
