package provision.com.app.apiResponse.savetocartresponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import provision.com.app.apiResponse.BaseResponce



class SaveToCartResponse : BaseResponce() {
    @SerializedName("response")
    @Expose
    var response: Response? = Response()
}