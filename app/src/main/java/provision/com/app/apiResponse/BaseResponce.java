package provision.com.app.apiResponse;

import androidx.annotation.Keep;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pankajk on 7/13/2018.
 * All rights reserved by Intigate Technologies.
 */
@Keep
public class BaseResponce {
    @SerializedName("AndroidVersion")
    @Expose
    private int androidVersion;
    @SerializedName("Version")
    @Expose
    String version = null;
    @SerializedName("ForceUpdate")
    @Expose
    Boolean forceUpdate = false;
    @SerializedName("IosVersion")
    @Expose
    String iosVersion = null;
    @SerializedName("versionName")
    @Expose
    private String versionName;
    @SerializedName("AppBlock")
    @Expose
    private String appBlock;
    @SerializedName("CurrrencyCode")
    @Expose
    private String currrencyCode;
    @SerializedName("CurrencyValue")
    @Expose
    private double currencyValue;
    @SerializedName("Numberformat")
    @Expose
    private int numberformat;
    @SerializedName("cmd")
    @Expose
    private String cmd;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("response_messege")
    @Expose
    private String responseMessege;

    public String getVersionName() {
        return versionName;
    }

    public int getVersion() {
        return androidVersion;
    }

    public String getAppBlock() {
        return appBlock;
    }

    public String getCmd() {
        return cmd;
    }

    public String getStatus() {
        return status;
    }

    public String getResponseMessege() {
        return responseMessege;
    }

    public String getCurrrencyCode() {
        return currrencyCode;
    }

    public double getCurrencyValue() {
        return currencyValue;
    }

    public int getNumberformat() {
        return numberformat;
    }

    public int getAndroidVersion() {
        return androidVersion;
    }

    public Boolean getForceUpdate() {
        return forceUpdate;
    }

    public String getIosVersion() {
        return iosVersion;
    }
}
