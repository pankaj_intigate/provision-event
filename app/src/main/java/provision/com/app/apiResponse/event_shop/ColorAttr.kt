package provision.com.app.apiResponse.event_shop


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ColorAttr(
    @SerializedName("i_id")
    var iId: String? = "",
    @SerializedName("Isselected")
    var isselected: Int = 0,
    @SerializedName("color_id")
    var colorId: String? = "",
    @SerializedName("color_name")
    var colorName: String? = "",
    @SerializedName("color_code")
    var colorCode: String? = "",
    @SerializedName("sortorder")
    var sortorder: String? = "",
    @SerializedName("IsAvailability")
    var isAvailability: Int = 0
)