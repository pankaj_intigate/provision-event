package provision.com.app.apiResponse.address

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import provision.com.app.apiResponse.BaseResponce
import provision.com.app.apiResponse.cartDataResponse.UserAddInfo

class AddressREssponce :BaseResponce() {
    @SerializedName("response")
    @Expose
    var responce: UserAddInfo?=null
}