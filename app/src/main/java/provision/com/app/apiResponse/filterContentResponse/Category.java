package provision.com.app.apiResponse.filterContentResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {

    @SerializedName("i_id")
    @Expose
    private String iId;
    @SerializedName("v_title")
    @Expose
    private String vTitle;
    @SerializedName("Image")
    @Expose
    private String image;

    public String getIId() {
        return iId;
    }

    public void setIId(String iId) {
        this.iId = iId;
    }

    public String getVTitle() {
        return vTitle;
    }

    public void setVTitle(String vTitle) {
        this.vTitle = vTitle;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
