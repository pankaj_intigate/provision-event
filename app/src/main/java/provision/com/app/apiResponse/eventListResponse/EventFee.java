package provision.com.app.apiResponse.eventListResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EventFee {

    @SerializedName("fee")
    @Expose
    private List<Fee> fee = null;

    public List<Fee> getFee() {
        return fee;
    }

    public void setFee(List<Fee> fee) {
        this.fee = fee;
    }

}
