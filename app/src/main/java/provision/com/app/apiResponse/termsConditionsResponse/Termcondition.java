package provision.com.app.apiResponse.termsConditionsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Termcondition {

    @SerializedName("i_id")
    @Expose
    private String iId;
    @SerializedName("t_terms")
    @Expose
    private String tTerms;
    @SerializedName("v_title")
    @Expose
    private String vTitle;

    public String getIId() {
        return iId;
    }

    public void setIId(String iId) {
        this.iId = iId;
    }

    public String getTTerms() {
        return tTerms;
    }

    public void setTTerms(String tTerms) {
        this.tTerms = tTerms;
    }

    public String getVTitle() {
        return vTitle;
    }

    public void setVTitle(String vTitle) {
        this.vTitle = vTitle;
    }

}
