package provision.com.app.apiResponse.getCurrencyCodeResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import provision.com.app.apiResponse.BaseResponce;

public class GetCurrencyCodeResponse extends BaseResponce {
    @SerializedName("response")
    @Expose
    private Response response;

    public Response getResponse() {
        return response;
    }
}
