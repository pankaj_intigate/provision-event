package provision.com.app.utils;

import android.util.Log;

import com.adjust.sdk.Adjust;
import com.adjust.sdk.AdjustEvent;

public class CommonUtils {
    public static final boolean ENABLE_LOG = false;

    public static void showLog(String tag, String message) {
        if (isDebug()) {
            Log.e(tag, "" + message);
        }
    }

    public static boolean isDebug() {
        return ENABLE_LOG;
    }

    public static void onTrackEventClick(String token) {
        AdjustEvent event = new AdjustEvent(token);
        Adjust.trackEvent(event);
      //  Log.d("Adjust Analytics token",""+token);
    }

    public static void onTrackEventWithDetails(String token,String key,String value) {
        AdjustEvent adjustEvent = new AdjustEvent(token);
        adjustEvent.addCallbackParameter(key,value);
        Adjust.trackEvent(adjustEvent);
     //   Log.d("Analytics with Detail",""+token+" : "+key+" : "+value);
    }
}