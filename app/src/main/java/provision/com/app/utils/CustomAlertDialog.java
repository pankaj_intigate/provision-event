package provision.com.app.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;

/**
 * Created by gauravg on 7/16/2018.
 */

public class CustomAlertDialog {
    public CustomAlertDialog(Context context, String message, String okButton, onAlertDialogCustomListener lsitener) {
        new CustomAlertDialog(context, "", message, okButton, "", lsitener);
    }


    public CustomAlertDialog(Context context, String title, String message, String okButton, String cancekButton, final onAlertDialogCustomListener listener) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        } else {
            builder.setTitle(context.getString(R.string.app_name));
        }
        if (!TextUtils.isEmpty(message)) {
            builder.setMessage(message);
        } else {
            builder.setMessage("");
        }

        builder.setPositiveButton(TextUtils.isEmpty(okButton) ? context.getString(R.string.dialog_ok) : okButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (listener != null) {
                    listener.onSuccessListener(dialog);
                }
            }
        });
        builder.setNegativeButton(TextUtils.isEmpty(cancekButton) ? context.getString(R.string.dialog_cancel) : cancekButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (listener != null) {
                    listener.onCancelListener();
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public static void forceUpdate(BaseActivity mActivity) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
        } else {
            builder = new AlertDialog.Builder(mActivity);
        }

        builder.setTitle(mActivity.getString(R.string.new_version));

        builder.setMessage(mActivity.getString(R.string.update_app));
        builder.setCancelable(false);
        builder.setPositiveButton("UPDATE", (dialog, which) -> {
            final String appPackageName = mActivity.getPackageName();
            try {
                mActivity.startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)), 222);
            } catch (android.content.ActivityNotFoundException anfe) {
                mActivity.startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)), 222);
            }
        });
        builder.show();
    }

    public interface onAlertDialogCustomListener {
        void onSuccessListener(DialogInterface dialog);

        void onCancelListener();
    }

}
