package provision.com.app.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by pankajk on 7/12/2018.
 */

public class PrefUtils {
    public static final String USER_ID = "USER_ID";
    public static final String VERSION_CODE = "VERSION_CODE";
    public static final String VERSION_NAME = "VERSION_NAME";
    public static final String USER_NAME = "USER_NAME";
    public static final String MOBILE_NO = "MOBILE_NUMBER";
    public static final String EMAIL_ID = "EMAIL_ID";
    public static final String USER_IMAGE = "USER_IMAGE";
    public static final String COUNTRY_CODE = "COUNTRY_CODE";
    public static final String COUNTRY_NAME = "COUNTRY_NAME";
    public static final String SESSION_ID = "session_id";
    public static final String TOKEN = "TOKEN";
    public static final String CART_ITEM = "cart_item";
    public static final String SOCIAL_ID = "SOCIAL_ID";
    public static final String PASSWORD = "PASSWORD";
    public static final String NOTIFICATION_ENABLE = "NOTIFICATION_ENABLE";
    public static final String USER_TYPE = "usertype";


    public static String saveToPrefs(Context context, String key, String value) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();
        return key;
    }


    public static String getFromPrefs(Context context, String key) {
        if (context == null) {
            return "";
        }
        SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        try {
            return sharedPrefs.getString(key, "");
        } catch (Exception e) {
            return "";
        }
    }

    public static void logout(Context context) {
        SharedPreferences logoutPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = logoutPrefs.edit();
        editor.clear();
        editor.apply();
    }
}
