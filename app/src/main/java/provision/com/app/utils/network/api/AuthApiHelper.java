package provision.com.app.utils.network.api;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import provision.com.app.apiResponse.BaseResponce;
import provision.com.app.apiResponse.RacekitResponse.RacekitResponse;
import provision.com.app.apiResponse.address.AddressREssponce;
import provision.com.app.apiResponse.address.country.ShopCountryRes;
import provision.com.app.apiResponse.cartDataResponse.CartDataResponse;
import provision.com.app.apiResponse.cartDeleteResponse.CartDeleteResponse;
import provision.com.app.apiResponse.cart_count.CartCountEntity;
import provision.com.app.apiResponse.categoryEventResponse.CategoryEventResponse;
import provision.com.app.apiResponse.check_participate.CheckParticipatEntity;
import provision.com.app.apiResponse.check_participate.save_booking.SaveBookingEntity;
import provision.com.app.apiResponse.city_response.CityResponse;
import provision.com.app.apiResponse.create_token.NotificateTokenUpdate;
import provision.com.app.apiResponse.eventDetailResponse.EventDetailResponse;
import provision.com.app.apiResponse.eventListResponse.EventListResponse;
import provision.com.app.apiResponse.eventSearchResponse.EventSearchResponse;
import provision.com.app.apiResponse.event_list.EventListEntity;
import provision.com.app.apiResponse.event_shop.ShopItemEventResponse;
import provision.com.app.apiResponse.faqResponse.FAQResponse;
import provision.com.app.apiResponse.feeDataResponse.FeeResponse;
import provision.com.app.apiResponse.feePriceResponse.FeePriceResponse;
import provision.com.app.apiResponse.filterContentResponse.FilterContentResponse;
import provision.com.app.apiResponse.getComposeMailResponse.ComposeMail;
import provision.com.app.apiResponse.getCurrencyCodeResponse.GetCurrencyCodeResponse;
import provision.com.app.apiResponse.get_cart_total.GetCartTotal;
import provision.com.app.apiResponse.gmailLoginResponse.GmailSignInResponse;
import provision.com.app.apiResponse.homeBanner.HomeBanerResponse;
import provision.com.app.apiResponse.inbox.InboxListEntity;
import provision.com.app.apiResponse.inboxDetailsResponse.InboxDetails;
import provision.com.app.apiResponse.login.LoginResponce;
import provision.com.app.apiResponse.myWalletResponse.MyWallet;
import provision.com.app.apiResponse.my_account.MyAccountDetails;
import provision.com.app.apiResponse.my_order_list.MyOrderListResponse;
import provision.com.app.apiResponse.my_order_list.my_order_details.OrderDetailsResponse;
import provision.com.app.apiResponse.notification.NotificationEntity;
import provision.com.app.apiResponse.organiserResponse.OrganiserResponse;
import provision.com.app.apiResponse.outletResponse.OutLetResponse;
import provision.com.app.apiResponse.participate_info.ParticipateInfoEntity;
import provision.com.app.apiResponse.payment_opt.PaymentOptionRes;
import provision.com.app.apiResponse.productdetailsresponse.ProductDetailsResponse;
import provision.com.app.apiResponse.raceTrackResponse.RaceTrackResponse;
import provision.com.app.apiResponse.receipt_details.ReceiptDetails;
import provision.com.app.apiResponse.recomded_itm.RecomdedItem;
import provision.com.app.apiResponse.resultResponse.ResultResponse;
import provision.com.app.apiResponse.saveParticipent.SaveParticipentResponse;
import provision.com.app.apiResponse.savetocartresponse.SaveToCartResponse;
import provision.com.app.apiResponse.savetocartresponse.WishListRes.WishListRes;
import provision.com.app.apiResponse.shop_product_list.ProductListResponse;
import provision.com.app.apiResponse.shop_product_list.short_product.ShortingResponse;
import provision.com.app.apiResponse.signInSignUpResponse.SignInSignUpResponse;
import provision.com.app.apiResponse.sponserResponse.SponsorsResponse;
import provision.com.app.apiResponse.termsConditionsResponse.TermsConditionsResponse;
import provision.com.app.apiResponse.tipsAdviceResponse.TipsAdviceResponse;
import provision.com.app.apiResponse.unread_count.UnReadCount;
import provision.com.app.apiResponse.upcomingEventResponse.UpcomingEventResponse;
import provision.com.app.apiResponse.updateProfileImageResponse.UpdateProfileImageResponse;
import provision.com.app.apiResponse.user_info_registration.UserInfoReg;
import provision.com.app.apiResponse.volunteerResponse.VolunteerResponse;
import provision.com.app.apiResponse.wishlist.WishlistResponse;
import provision.com.app.shop.model.filterlistresponse.FilterListResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


public interface AuthApiHelper {

    //getHomeBanner
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<HomeBanerResponse> homeBanerProductList(@Field(ApiConstant.KEY_COMMAND) String command,
                                                 @Field(ApiConstant.KEY_FETURED) String featured,
                                                 @Field(ApiConstant.API_VERSION) String version);

    //    getEventsUpComing
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<UpcomingEventResponse> homeUpcomingEvent(@Field(ApiConstant.KEY_COMMAND) String command,
                                                  @Field(ApiConstant.API_VERSION) String version);

    //    GetEventdetail
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<EventDetailResponse> eventDetail(@Field(ApiConstant.KEY_COMMAND) String command,
                                          @Field(ApiConstant.KEY_EVENT_ID) String eventId,
                                          @Field(ApiConstant.API_VERSION) String version);

    //    GetEventTab
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<FeeResponse> feeData(@Field(ApiConstant.KEY_COMMAND) String command,
                              @Field(ApiConstant.KEY_EVENT_ID) String eventId,
                              @Field(ApiConstant.KEY_TAB_ID) String tabId,
                              @Field(ApiConstant.KEY_TAB_NAME) String tabName,
                              @Field("CountryCode") String countryCode,
                              @Field(ApiConstant.API_VERSION) String version);

    //    do_login
    @POST(ApiConstant.USER_SERVICE)
    @FormUrlEncoded
    Call<LoginResponce> getLogin(@Field(ApiConstant.KEY_COMMAND) String command,
                                 @Field("Email") String email,
                                 @Field("Password") String password,
                                 @Field("DeviceId") String deviceId,
                                 @Field("deviceToken") String deviceToken,
                                 @Field(ApiConstant.API_VERSION) String version);

    //GUEST LOGING
    @POST("signinupservice/getApiData")
    @FormUrlEncoded
    Call<LoginResponce> checkoutViaGuest(@Field(ApiConstant.KEY_COMMAND) String command,
                                         @Field(ApiConstant.API_VERSION) String version,
                                         @Field("SessionId") String sessionId,
                                         @Field("Email") String Email,
                                         @Field("DeviceId") String DeviceId,
                                         @Field("userId") String userId,
                                         @Field("country_code") String country_code,
                                         @Field("deviceToken") String deviceToken
    );

    //    GetRegister
    @POST(ApiConstant.USER_SERVICE)
    @FormUrlEncoded
    Call<LoginResponce> doRegister(@Field(ApiConstant.KEY_COMMAND) String command,
                                   @Field("Name") String name,
                                   @Field("Email") String email,
                                   @Field("Password") String password,
                                   @Field("PhoneNo") String phoneNo,
                                   @Field("DeviceId") String deviceId,
                                   @Field("country_code") String countryCode,
                                   @Field("deviceToken") String deviceToken,
                                   @Field(ApiConstant.API_VERSION) String version);

    //    forgot_password
    @POST(ApiConstant.USER_SERVICE)
    @FormUrlEncoded
    Call<LoginResponce> forgotPassword(@Field(ApiConstant.KEY_COMMAND) String command,
                                       @Field("Email") String emaild,
                                       @Field(ApiConstant.API_VERSION) String version);

    //    myaccount
    @POST(ApiConstant.MY_ACCOUNT)
    @FormUrlEncoded
    Call<MyAccountDetails> getAccountDetails(@Field(ApiConstant.KEY_COMMAND) String command,
                                             @Field("Email") String emaild,
                                             @Field("userId") String userId,
                                             @Field(ApiConstant.API_VERSION) String version);

    //    getProfileContent
    @POST(ApiConstant.MY_ACCOUNT)
    @FormUrlEncoded
    Call<MyAccountDetails> getProfileDetails(@Field(ApiConstant.KEY_COMMAND) String command,
                                             @Field("userId") String userId,
                                             @Field(ApiConstant.API_VERSION) String version);

    //    updateNotify
    @POST(ApiConstant.MY_ACCOUNT)
    @FormUrlEncoded
    Call<BaseResponce> updateNotify(@Field(ApiConstant.KEY_COMMAND) String command,
                                    @Field("deviceToken") String userId,
                                    @Field(ApiConstant.API_VERSION) String version);

    //    updateaccount
    @POST(ApiConstant.MY_ACCOUNT)
    @FormUrlEncoded
    Call<MyAccountDetails> updateProfile(@Field(ApiConstant.KEY_COMMAND) String command,
                                         @Field("userId") String userId,
                                         @Field("userImg") String image,
                                         @Field("FullName") String name,
                                         @Field("PhoneNo") String phoneNo,
                                         @Field("Tsize") String tShirtSize,
                                         @Field("Country") String country,
                                         @Field("Dob") String dob,
                                         @Field("Gender") String gender,
                                         @Field("country_code") String country_code,
                                         @Field("Address") String address,
                                         @Field("Residence") String residence,
                                         @Field(ApiConstant.API_VERSION) String version);

    //    GetEventTab
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<RacekitResponse> racekitResponse(@Field(ApiConstant.KEY_COMMAND) String command,
                                          @Field(ApiConstant.KEY_EVENT_ID) String eventId,
                                          @Field(ApiConstant.KEY_TAB_ID) String tabId,
                                          @Field(ApiConstant.KEY_TAB_NAME) String tabName,
                                          @Field(ApiConstant.API_VERSION) String version);

    //    GetEventTab
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<OutLetResponse> outLetResponse(@Field(ApiConstant.KEY_COMMAND) String command,
                                        @Field(ApiConstant.KEY_EVENT_ID) String eventId,
                                        @Field(ApiConstant.KEY_TAB_ID) String tabId,
                                        @Field(ApiConstant.KEY_TAB_NAME) String tabName,
                                        @Field(ApiConstant.API_VERSION) String version);

    //    GetEventTab
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<OrganiserResponse> organiseResponse(@Field(ApiConstant.KEY_COMMAND) String command,
                                             @Field(ApiConstant.KEY_EVENT_ID) String eventId,
                                             @Field(ApiConstant.KEY_TAB_ID) String tabId,
                                             @Field(ApiConstant.KEY_TAB_NAME) String tabName,
                                             @Field(ApiConstant.API_VERSION) String version);

    //    GetEventTab
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<SponsorsResponse> sponserResponse(@Field(ApiConstant.KEY_COMMAND) String command,
                                           @Field(ApiConstant.KEY_EVENT_ID) String eventId,
                                           @Field(ApiConstant.KEY_TAB_ID) String tabId,
                                           @Field(ApiConstant.KEY_TAB_NAME) String tabName,
                                           @Field(ApiConstant.API_VERSION) String version);

    //    GetEventTab
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<RaceTrackResponse> raceTrackResponse(@Field(ApiConstant.KEY_COMMAND) String command,
                                              @Field(ApiConstant.KEY_EVENT_ID) String eventId,
                                              @Field(ApiConstant.KEY_TAB_ID) String tabId,
                                              @Field(ApiConstant.KEY_TAB_NAME) String tabName,
                                              @Field(ApiConstant.API_VERSION) String version);

    //    GetEventTab
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<VolunteerResponse> volunteerResponse(@Field(ApiConstant.KEY_COMMAND) String command,
                                              @Field(ApiConstant.KEY_EVENT_ID) String eventId,
                                              @Field(ApiConstant.KEY_TAB_ID) String tabId,
                                              @Field(ApiConstant.KEY_TAB_NAME) String tabName,
                                              @Field(ApiConstant.API_VERSION) String version);

    //    GetEventTab
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<TipsAdviceResponse> tipsAdviceResponse(@Field(ApiConstant.KEY_COMMAND) String command,
                                                @Field(ApiConstant.KEY_EVENT_ID) String eventId,
                                                @Field(ApiConstant.KEY_TAB_ID) String tabId,
                                                @Field(ApiConstant.KEY_TAB_NAME) String tabName,
                                                @Field(ApiConstant.API_VERSION) String version);

    //    GetEventTab
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<FAQResponse> faqResponse(@Field(ApiConstant.KEY_COMMAND) String command,
                                  @Field(ApiConstant.KEY_EVENT_ID) String eventId,
                                  @Field(ApiConstant.KEY_TAB_ID) String tabId,
                                  @Field(ApiConstant.KEY_TAB_NAME) String tabName,
                                  @Field(ApiConstant.API_VERSION) String version);

    //    GetEventTab
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<TermsConditionsResponse> termsConditionsResponse(@Field(ApiConstant.KEY_COMMAND) String command,
                                                          @Field(ApiConstant.KEY_EVENT_ID) String eventId,
                                                          @Field(ApiConstant.KEY_TAB_ID) String tabId,
                                                          @Field(ApiConstant.KEY_TAB_NAME) String tabName,
                                                          @Field(ApiConstant.API_VERSION) String version);

    //  command='GetAjaxEventsListing',categoryId=dyanimic pass,year=pass,month=pass,keyword=pass,status=1 or 2
//    GetAjaxEventsListing
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<EventSearchResponse> searchEventResponse(@Field(ApiConstant.KEY_COMMAND) String command,
                                                  @Field(ApiConstant.KEY_KEYWORD) String keyword,
                                                  @Field(ApiConstant.KEY_CATEGORY_ID) String categoryId,
                                                  @Field(ApiConstant.KEY_YEAR) int year,
                                                  @Field(ApiConstant.KEY_MONTH) String month,
                                                  @Field(ApiConstant.KEY_STATUS) String status,
                                                  @Field(ApiConstant.API_VERSION) String version);

    //    getCountryCode
    @POST(ApiConstant.USER_SERVICE)
    @FormUrlEncoded
    Call<MyAccountDetails> getCountryCode(@Field(ApiConstant.KEY_COMMAND) String command,
                                          @Field(ApiConstant.API_VERSION) String version);

    //    setPassword
    @POST(ApiConstant.MY_ACCOUNT)
    @FormUrlEncoded
    Call<MyAccountDetails> changePassword(@Field(ApiConstant.KEY_COMMAND) String command,
                                          @Field("userId") String userId,
                                          @Field("currentPassword") String oldPassword,
                                          @Field("Password") String password,
                                          @Field(ApiConstant.API_VERSION) String version);

    //    GetEventFilterData
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<FilterContentResponse> getFilterContent(@Field(ApiConstant.KEY_COMMAND) String command,
                                                 @Field(ApiConstant.API_VERSION) String version);

    //    getEventsUser
    @POST(ApiConstant.MY_ACCOUNT)
    @FormUrlEncoded
    Call<EventListEntity> getEventList(@Field(ApiConstant.KEY_COMMAND) String command,
                                       @Field("userId") String userId,
                                       @Field(ApiConstant.API_VERSION) String version);

    //    getReceipt
    @POST(ApiConstant.MY_ACCOUNT)
    @FormUrlEncoded
    Call<ReceiptDetails> getReceipt(@Field(ApiConstant.KEY_COMMAND) String command,
                                    @Field("bookingId") String bookingId,
                                    @Field("CountryCode") String countryCode,
                                    @Field(ApiConstant.API_VERSION) String version);

    //    getparticipateInfo
    @POST(ApiConstant.MY_ACCOUNT)
    @FormUrlEncoded
    Call<ParticipateInfoEntity> getParticipateInfo(@Field(ApiConstant.KEY_COMMAND) String command,
                                                   @Field("participateId") String bookingId,
                                                   @Field("CountryCode") String countryCode,
                                                   @Field(ApiConstant.API_VERSION) String version);

    //    cancelparticipateInfo
    @POST(ApiConstant.MY_ACCOUNT)
    @FormUrlEncoded
    Call<ParticipateInfoEntity> cancelParticipateInfo(@Field(ApiConstant.KEY_COMMAND) String command,
                                                      @Field("participateId") String bookingId,
                                                      @Field(ApiConstant.API_VERSION) String version);

    //    otp_verified
    @POST(ApiConstant.USER_SERVICE)
    @FormUrlEncoded
    Call<LoginResponce> getOTP(@Field(ApiConstant.KEY_COMMAND) String command,
                               @Field("Email") String email,
                               @Field("otp") String otp,
                               @Field("new_password") String newPassword,
                               @Field("confirm_password") String confirmPassword,
                               @Field(ApiConstant.API_VERSION) String version);

    //    getUserInbox
    @POST(ApiConstant.MY_ACCOUNT)
    @FormUrlEncoded
    Call<InboxListEntity> getInboxList(@Field(ApiConstant.KEY_COMMAND) String command,
                                       @Field("userId") String userId,
                                       @Field(ApiConstant.API_VERSION) String version);

    //    compose_mail
    @POST(ApiConstant.MY_ACCOUNT)
    @FormUrlEncoded
    Call<ComposeMail> composeMail(@Field(ApiConstant.KEY_COMMAND) String command,
                                  @Field("userId") String userId,
                                  @Field(ApiConstant.API_VERSION) String version);

    //    send_compose_mail
    @POST(ApiConstant.MY_ACCOUNT)
    @FormUrlEncoded
    Call<InboxListEntity> composeMail(@Field(ApiConstant.KEY_COMMAND) String command,
                                      @Field("userId") String userId,
                                      @Field("MailTypeId") String mailTypeId,
                                      @Field("EventId") String eventId,
                                      @Field("Subject") String subject,
                                      @Field("Body") String body,
                                      @Field(ApiConstant.API_VERSION) String version);

    //    GetRegisteration
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<EventListResponse> getRegistration(@Field(ApiConstant.KEY_COMMAND) String command,
                                            @Field("userId") String userId,
                                            @Field("is_logged_in") String isLogin,
                                            @Field("event_id") String eventId,
                                            @Field("log") String log,
                                            @Field("oiv") String oiv,
                                            @Field("CountryCode") String countryCode,
                                            @Field("IsAndroid") String isAndroid,
                                            @Field(ApiConstant.API_VERSION) String version);

    //    getWalletTransactions
    @POST(ApiConstant.MY_ACCOUNT)
    @FormUrlEncoded
    Call<MyWallet> myWallet(@Field(ApiConstant.KEY_COMMAND) String command,
                            @Field("emailId") String emailId,
                            @Field("userId") String userId,
                            @Field("CountryCode") String countryCode,
                            @Field(ApiConstant.API_VERSION) String version);

    //    getSupportInfo
    @POST(ApiConstant.MY_ACCOUNT)
    @FormUrlEncoded
    Call<InboxDetails> inboxDetails(@Field(ApiConstant.KEY_COMMAND) String command,
                                    @Field("infoId") String infoId,
                                    @Field(ApiConstant.API_VERSION) String version);

    //    replySupport
    @POST(ApiConstant.MY_ACCOUNT)
    @FormUrlEncoded
    Call<InboxListEntity> replyMail(@Field(ApiConstant.KEY_COMMAND) String command,
                                    @Field("userId") String userId,
                                    @Field("SId") String sId,
                                    @Field("Body") String body,
                                    @Field(ApiConstant.API_VERSION) String version);

    @POST(ApiConstant.MY_ACCOUNT)
    @FormUrlEncoded
    Call<CategoryEventResponse> getCategoryEventResponse(@Field(ApiConstant.KEY_COMMAND) String command,
                                                         @Field(ApiConstant.KEY_EVENT_ID) String eventId,
                                                         @Field(ApiConstant.API_VERSION) String version);

    //    GetFeesPriceAndregistrationType
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<FeePriceResponse> getFeePrice(@Field(ApiConstant.KEY_COMMAND) String command,
                                       @Field(ApiConstant.KEY_EVENT_ID) String eventId,
                                       @Field(ApiConstant.KEY_FEE_ID) String feeId,
                                       @Field(ApiConstant.KEY_CATEGORY_TYPE) String categoryType,
                                       @Field("CountryCode") String currencyCode,
                                       @Field(ApiConstant.API_VERSION) String version);

    //    GetEventsResult
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<ResultResponse> getResultResponse(@Field(ApiConstant.KEY_COMMAND) String command,
                                           @Field(ApiConstant.KEY_KEYWORD) String keyword,
                                           @Field(ApiConstant.KEY_CATEGORY_ID) String categoryId,
                                           @Field(ApiConstant.KEY_YEAR) String year,
                                           @Field(ApiConstant.KEY_MONTH) String month,
                                           @Field(ApiConstant.KEY_STATUS) String status,
                                           @Field(ApiConstant.API_VERSION) String version);

    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<ResultResponse> getUploadResultEventList(@Field(ApiConstant.KEY_COMMAND) String command,
                                                  @Field(ApiConstant.KEY_KEYWORD) String keyword,
                                                  @Field(ApiConstant.KEY_CATEGORY_ID) String categoryId,
                                                  @Field(ApiConstant.KEY_YEAR) String year,
                                                  @Field(ApiConstant.KEY_MONTH) String month,
                                                  @Field(ApiConstant.KEY_STATUS) String status,
                                                  @Field(ApiConstant.API_VERSION) String version);

    //    userInfo
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<UserInfoReg> getUserInfoForRegistration(@Field(ApiConstant.KEY_COMMAND) String command,
                                                 @Field("is_logged_in") String isLoggedIn,
                                                 @Field("userId") String userId,
                                                 @Field(ApiConstant.API_VERSION) String version);

    //    SaveParticipant
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<SaveParticipentResponse> saveParticipent(@Field("command") String command,
                                                  @Field("analyticActive") String analyticActive,
                                                  @Field("event_fee") String categoryId,
                                                  @Field("invoice_id") String ivoiceId,
                                                  @Field("team_id") String teamId,
                                                  @Field("ptake") String ptake,
                                                  @Field("team_name") String teamName,
                                                  @Field("amount") String amount,
                                                  @Field("amount_fee_id") String amountFeeId,
                                                  @Field("e_id") String eventId,
                                                  @Field("v_fname") String firstName,
                                                  @Field("v_lname") String lastName,
                                                  @Field("v_dob") String dataOfbirth,
                                                  @Field("dd") String date,
                                                  @Field("mm") String month,
                                                  @Field("yy") String year,
                                                  @Field("v_gender") String gender,
                                                  @Field("v_phone_1") String mobileNumber,
                                                  @Field("v_email") String email,
                                                  @Field("v_country") String country,
                                                  @Field("is_d") String isId,
                                                  @Field("v_size") String vSize,
                                                  @Field("v_dis") String distance,
                                                  @Field("v_hw") String howWear,
                                                  @Field("v_add") String address,
                                                  @Field("v_hv") String hv,
                                                  @Field("v_exp") String exp,
                                                  @Field("v_university") String university,
                                                  @Field("v_universityid") String universityId,
                                                  @Field("v_fit") String vfit,
                                                  @Field("v_bib") String bibb,
                                                  @Field("v_coupon") String coupon,
                                                  @Field("SessionId") String sessionId,
                                                  @Field("is_logged_in") String loggedInId,
                                                  @Field("UserId") String userid,
                                                  @Field("Printing_tshirts") String printing_tshirts,
                                                  @Field("Medal_engraving") String medal_engraving,
                                                  @Field("reg_civil") String reg_civil,
                                                  @Field("is_holder") String isHolder,
                                                  @Field("v_residence") String v_residence,
                                                  @Field("car-make-mode") String carMakeMode,
                                                  @Field(ApiConstant.API_VERSION) String version);

    //    GetCartDetails
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<CartDataResponse> cartDataResponse(@Field("command") String command,
                                            @Field("SessionId") String sessionId,
                                            @Field("paymentOption") String paymentOption,
                                            @Field("is_logged_in") int isLoggedIn,
                                            @Field("CountryCode") String countryCode,
                                            @Field("Email") String email,
                                            @Field("androidcall") String webCall,
                                            @Field("userId") String userId,
                                            @Field("userType") String userType,
                                            @Field("page_type") String pageType,
                                            @Field(ApiConstant.API_VERSION) String version);


    //    addcartItem
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<RecomdedItem> getRecomdedItem(@Field("command") String command,
                                       @Field("cartItem") String cartItem,
                                       @Field("CountryCode") String countryCode,
                                       @Field(ApiConstant.API_VERSION) String version);

    //    addcartItem
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<BaseResponce> removeRecodedItemFromCart(@Field("command") String command,
                                                 @Field("id") String cartItem,
                                                 @Field("sessionId") String sessionId,
                                                 @Field(ApiConstant.API_VERSION) String version);

    //    savecartItem
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<SaveToCartResponse> saveCartItem(@Field("command") String command,
                                          @Field("id") String id,
                                          @Field("SessionId") String sessionId,
                                          @Field("quantity") String quantity,
                                          @Field("color_id") String colorId,
                                          @Field("size_id") String tsizeId,
                                          @Field("type") String type,
                                          @Field("eventId") String eventId,
                                          @Field(ApiConstant.API_VERSION) String version);


    //    DeleteParticipantFromCart
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<CartDeleteResponse> cartDeleteResponse(@Field("command") String command,
                                                @Field("Booking_d_Id") String sessionId,
                                                @Field("ParticipantId") String participantId,
                                                @Field(ApiConstant.API_VERSION) String version);

    //    ChangeUserProfile
    @Multipart
    @POST(ApiConstant.MY_ACCOUNT)
    Call<UpdateProfileImageResponse> updateProfileImage(@Part("command") RequestBody command,
                                                        @Part MultipartBody.Part image,
                                                        @Part("userId") RequestBody userId,
                                                        @Part(ApiConstant.API_VERSION) RequestBody version);

    //    getCurrencyCode
    @POST(ApiConstant.USER_SERVICE)
    @FormUrlEncoded
    Call<GetCurrencyCodeResponse> getCurrencyCode(@Field(ApiConstant.KEY_COMMAND) String command,
                                                  @Field(ApiConstant.API_VERSION) String version);

    //    Addwallet
    @POST(ApiConstant.PAYMENT_SERVICES)
    @FormUrlEncoded
    Call<MyWallet> addWallet(@Field(ApiConstant.KEY_COMMAND) String command,
                             @Field("Email") String emailId,
                             @Field("UserId") String userId,
                             @Field("Amount") String amount,
                             @Field("is_logged_in") String isLoggedIn,
                             @Field("Password") String password,
                             @Field("social_id") String SocialId,
                             @Field("Paytype") String Paytype,
                             @Field(ApiConstant.API_VERSION) String version);

    //getapplogin
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<MyWallet> addMoney(@Field(ApiConstant.KEY_COMMAND) String command,
                            @Field(ApiConstant.API_VERSION) String version);

    @POST(ApiConstant.PAYMENT_SERVICES)
    @FormUrlEncoded
    Call<MyWallet> saveBookingData(@Field(ApiConstant.KEY_COMMAND) String command,
                                   @Field("session") String sessionId,
                                   @Field("userId") String userId,
                                   @Field("webcall") int webCall,
                                   @Field("paymentType") String paymentOption,
                                   @Field(ApiConstant.API_VERSION) String version);



    //    SaveBooking
    @POST(ApiConstant.PAYMENT_SERVICES)
    @FormUrlEncoded

    Call<SaveBookingEntity> doPayment(@Field("command") String command,
                                      @Field("SessionId") String sessionId,
                                      @Field("Paytype") String paymentOption,
                                      @Field("UserId") String userId,
                                      @Field("Email") String emailId,
                                      @Field("Name") String name,
                                      @Field("TotalAmount") String totalAmount,
                                      @Field("is_logged_in") int isLoggedIn,
                                      @Field("webcall") int webCall,
                                      @Field(ApiConstant.API_VERSION) String version);

    @POST(ApiConstant.PAYMENT_SERVICES)
    @FormUrlEncoded
    Call<BaseResponce> cancelOrderForResetCart(@Field("command") String command,
                                               @Field("SessionId") String sessionId,
                                               @Field(ApiConstant.API_VERSION) String version);

    //    checkParticipate
    @POST(ApiConstant.PAYMENT_SERVICES)
    @FormUrlEncoded
    Call<CheckParticipatEntity> checkPaymentProceed(@Field("command") String command,
                                                    @Field("SessionId") String sessionId,
                                                    @Field(ApiConstant.API_VERSION) String version);

    //    reg_with_gmail
    @POST(ApiConstant.USER_SERVICE)
    @FormUrlEncoded
    Call<GmailSignInResponse> gmailLogin(@Field(ApiConstant.KEY_COMMAND) String command,
                                         @Field("id") String id,
                                         @Field("email") String email,
                                         @Field("name") String name,
                                         @Field("gender") String gender,
                                         @Field("picture") String image,
                                         @Field("locale") String locale,
                                         @Field("DeviceId") String deviceId,
                                         @Field("deviceToken") String deviceToken,
                                         @Field(ApiConstant.API_VERSION) String version);

    //    fbLogin
    @POST(ApiConstant.USER_SERVICE)
    @FormUrlEncoded
    Call<GmailSignInResponse> facebookLogin(@Field(ApiConstant.KEY_COMMAND) String command,
                                            @Field("id") String id,
                                            @Field("email") String email,
                                            @Field("first_name") String firstName,
                                            @Field("last_name") String lastName,
                                            @Field("gender") String gender,
                                            @Field("image") String image,
                                            @Field("DeviceId") String deviceId,
                                            @Field("deviceToken") String deviceToken,
                                            @Field(ApiConstant.API_VERSION) String version);

    //    check_signin_signup
    @POST(ApiConstant.USER_SERVICE)
    @FormUrlEncoded
    Call<SignInSignUpResponse> getSignInSignUpResponse(@Field(ApiConstant.KEY_COMMAND) String command,
                                                       @Field("Email") String email,
                                                       @Field(ApiConstant.API_VERSION) String version);

    //    GetCartCount
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<CartCountEntity> getCartCount(@Field(ApiConstant.KEY_COMMAND) String command,
                                       @Field("SessionId") String sessionId,
                                       @Field("userId") String userId,
                                       @Field(ApiConstant.API_VERSION) String version);

    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<NotificationEntity> getNotificationList(@Field(ApiConstant.KEY_COMMAND) String command,
                                                 @Field("Device") String deviceId,
                                                 @Field("deviceToken") String deviceToken,
                                                 @Field(ApiConstant.API_VERSION) String version);

    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<NotificateTokenUpdate> createToken(@Field(ApiConstant.KEY_COMMAND) String command,
                                            @Field("DeviceId") String deviceId,
                                            @Field("deviceToken") String deviceToken,
                                            @Field(ApiConstant.API_VERSION) String version);


    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<UnReadCount> getNotificationCount(@Field(ApiConstant.KEY_COMMAND) String command,
                                           @Field("Device") String deviceId,
                                           @Field("deviceToken") String deviceToken,
                                           @Field(ApiConstant.API_VERSION) String version);

    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<BaseResponce> readNotification(@Field(ApiConstant.KEY_COMMAND) String command,
                                        @Field("Id") String sessionId,
                                        @Field("deviceToken") String deviceToken,
                                        @Field(ApiConstant.API_VERSION) String version);

    //shop_item
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<ProductListResponse> shopProductList(@Field(ApiConstant.KEY_COMMAND) String command,
                                              @Field(ApiConstant.API_VERSION) String version,
                                              @Field("Device") String device,
                                              @Field("Sorting") String Sorting,
                                              @Field("searchData") String searchData,
                                              @Field("category") String category,
                                              @Field("seacrh") String seacrh,
                                              @Field("size_id") String size_id,
                                              @Field("color_id") String color_id,
                                              @Field("offers") String offers,
                                              @Field("Price") String Price,
                                              @Field("resetAll") String resetAll,
                                              @Field("p_page") int p_page,
                                              @Field("userId") String userId
    );

    //shop_item
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<ShortingResponse> shortData(@Field(ApiConstant.KEY_COMMAND) String command,
                                     @Field(ApiConstant.API_VERSION) String version

    );


    //shop_item_details
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<ProductDetailsResponse> shopProductDetails(@Field(ApiConstant.KEY_COMMAND) String command,
                                                    @Field(ApiConstant.API_VERSION) String version,
                                                    @Field("Device") String device,
                                                    @Field("Id") String id,
                                                    @Field("color_id") String color_id,
                                                    @Field("IsAvailabilityColor") String isAvailabilityColor,
                                                    @Field("size_id") String size_id,
                                                    @Field("IsAvailabilitySize") String isAvailabilitySize,
                                                    @Field("userId") String userId
    );

    //getEventItemData
    @POST("apiservice/getApiData")
    @FormUrlEncoded
    Call<ShopItemEventResponse> shopItemReg(@Field(ApiConstant.KEY_COMMAND) String command,
                                            @Field(ApiConstant.API_VERSION) String version,
                                            @Field("Device") String device,
                                            @Field("item_id") String itemId,
                                            @Field("event_id") String eventId,
                                            @Field("color_id") String color_id,
                                            @Field("IsAvailabilityColor") String isAvailabilityColor,
                                            @Field("size_id") String size_id,
                                            @Field("IsAvailabilitySize") String isAvailabilitySize,
                                            @Field("p_page") int page,
                                            @Field("userId") String userId
    );

    //adduserWishList
    @POST(ApiConstant.HOOME_BANER_URL)
    @FormUrlEncoded
    Call<WishListRes> addToWishList(@Field("command") String command,
                                    @Field("userId") String userId,
                                    @Field("productId") String productId,
                                    @Field("pagetype") String pageType,
                                    @Field("SessionId") String sessionId,
                                    @Field(ApiConstant.API_VERSION) String version);

    //citylist
    @POST("apiservice/getApiData")
    @FormUrlEncoded
    Call<CityResponse> getCity(@Field(ApiConstant.KEY_COMMAND) String command,
                               @Field(ApiConstant.API_VERSION) String version,
                               @Field("CId") String userId
    );


    //adduseraddress
    @POST("apiservice/getApiData")
    @FormUrlEncoded
    Call<BaseResponce> addAccountAddress(@Field(ApiConstant.KEY_COMMAND) String command,
                                         @Field("SessionId") String sessionId,
                                         @Field(ApiConstant.API_VERSION) String version,
                                         @Field("userId") String userId,
                                         @Field("address_country") String countryId,
                                         @Field("first_name") String name,
                                         @Field("mobile_number") String mobileNumber,
                                         @Field("address_city") String addressCity,
                                         @Field("street") String street,
                                         @Field("apartment_no") String apartment_no,
                                         @Field("avenue") String avenue,
                                         @Field("block") String block,
                                         @Field("emailAddr") String email
    );

    //getuserWishList
    @POST("apiservice/getApiData")
    @FormUrlEncoded
    Call<WishlistResponse> getuserWishList(@Field(ApiConstant.KEY_COMMAND) String command,
                                           @Field(ApiConstant.API_VERSION) String version,
                                           @Field("userId") String userId
    );

    //filterList
    @POST("apiservice/getApiData")
    @FormUrlEncoded
    Call<FilterListResponse> filterList(@Field(ApiConstant.KEY_COMMAND) String command,
                                        @Field(ApiConstant.API_VERSION) String version,
                                        @Field("userId") String userId,
                                        @Field("Sorting") String sorting,
                                        @Field("searchData") String searchData,
                                        @Field("category") String category,
                                        @Field("seacrh") String search,
                                        @Field("size_id") String sizeId,
                                        @Field("color_id") String colorId,
                                        @Field("offers") String offers,
                                        @Field("Price") String price,
                                        @Field("resetAll") String resetAll);

    //getuserWishList
    @POST("apiservice/getApiData")
    @FormUrlEncoded
    Call<PaymentOptionRes> getDeliveryOption(@Field(ApiConstant.KEY_COMMAND) String command,
                                             @Field(ApiConstant.API_VERSION) String version,
                                             @Field("IsWallet") int isWallet,
                                             @Field("IsCash") int isCash,
                                             @Field("IsCashOndelivery") int isCashOndelivery,
                                             @Field("GrandTotal") String grandTotal
    );

    //appliedShopCoupon
    @POST("apiservice/getApiData")
    @FormUrlEncoded
    Call<BaseResponce> appliedShopCoupon(@Field(ApiConstant.KEY_COMMAND) String command,
                                         @Field(ApiConstant.API_VERSION) String version,
                                         @Field("CouponCode") String CouponCode,
                                         @Field("SessionId") String sessionId,
                                         @Field("amount") String amount,
                                         @Field("userId") String userId
    );

    //DeleteItemCouponFromCart
    @POST("apiservice/getApiData")
    @FormUrlEncoded
    Call<BaseResponce> deleteItemCouponFromCart(@Field(ApiConstant.KEY_COMMAND) String command,
                                                @Field(ApiConstant.API_VERSION) String version,
                                                @Field("CouponCode") String CouponCode,
                                                @Field("SessionId") String sessionId,
                                                @Field("CouponId") String couponId,
                                                @Field("userId") String userId
    );

    //update Cart qty
    @POST("apiservice/getApiData")
    @FormUrlEncoded
    Call<BaseResponce> updateCartQty(@Field(ApiConstant.KEY_COMMAND) String command,
                                     @Field(ApiConstant.API_VERSION) String version,
                                     @Field("SessionId") String sessionId,
                                     @Field("bookingId") String bookingId,
                                     @Field("quantity") String quantity
    );


    //getItemUser
    @POST("myaccount/getApiData")
    @FormUrlEncoded
    Call<MyOrderListResponse> getItemUser(@Field(ApiConstant.KEY_COMMAND) String command,
                                          @Field(ApiConstant.API_VERSION) String version,
                                          @Field("userId") String userId,
                                          @Field("p_page") int pageNo
    );

    //getItemUser
    @POST("myaccount/getApiData")
    @FormUrlEncoded
    Call<OrderDetailsResponse> getOrderDerails(@Field(ApiConstant.KEY_COMMAND) String command,
                                               @Field(ApiConstant.API_VERSION) String version,
                                               @Field("bookingId") String bookingId,
                                               @Field("CountryCode") String countryCode
    );

    //getItemUser
    @POST("apiservice/getApiData")
    @FormUrlEncoded
    Call<AddressREssponce> getAddressInfo(@Field(ApiConstant.KEY_COMMAND) String command,
                                          @Field(ApiConstant.API_VERSION) String version,
                                          @Field("is_logged_in") String isLoggedIn,
                                          @Field("userId") String userId
    );

    //getCartTotal
    @POST("apiservice/getApiData")
    @FormUrlEncoded
    Call<GetCartTotal> getCartTotal(@Field(ApiConstant.KEY_COMMAND) String command,
                                    @Field(ApiConstant.API_VERSION) String version,
                                    @Field("SessionId") String sessionId,
                                    @Field("page_type") String pageType
    );

    @FormUrlEncoded
    @POST("apiservice/getApiData")
    Call<ShopCountryRes> getShopCountyList(@Field("command") String command, @Field("version") String version, @Field("CountryType") String CountryType);


}



