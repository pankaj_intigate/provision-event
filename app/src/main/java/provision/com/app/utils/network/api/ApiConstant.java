package provision.com.app.utils.network.api;

/**
 * Created by amitk on 12-07-2018.
 */

public class ApiConstant {
    public static final String HOOME_BANER_URL = "apiservice/getApiData";

    //signinupservice
    public static final String USER_SERVICE = "signinupservice/getApiData";
    //signinupservice
    public static final String MY_ACCOUNT = "myaccount/getApiData";
    //PAYMENT
    public static final String PAYMENT_SERVICES = "paymentService/getApiData";

    ///Api key
    public static final String KEY_COMMAND = "command";
    public static final String KEY_FETURED = "featured";
    public static final String API_VERSION = "version";
    public static final String KEY_EVENT_ID = "event_id";
    public static final String KEY_TAB_ID = "TabId";
    public static final String KEY_TAB_NAME = "TabName";
    public static final String KEY_EVENT_NAME = "event_name";
    public static final String KEY_FEE_ID = "fee_id";
    public static final String KEY_CATEGORY_TYPE = "CategoryType";


    public static final String KEY_CATEGORY_ID = "categoryId";
    public static final String KEY_YEAR = "year";
    public static final String KEY_MONTH = "month";
    public static final String KEY_KEYWORD = "keyword";
    public static final String KEY_STATUS = "status";

    public static final String KEY_WEB_CALL = "2";
}
