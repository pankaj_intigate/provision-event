package provision.com.app.utils.network.api;

/**
 * Created by ratnad on 8/31/2017.
 */

public class RetroError {
    private final int httpErrorCode;
    private final RetroError.Kind kind;
    private String errorMessage;

    public RetroError(RetroError.Kind kind, String errorMessage, int httpErrorCode) {
        this.httpErrorCode = httpErrorCode;
        this.kind = kind;
        this.errorMessage = errorMessage;
    }

    public int getHttpErrorCode() {
        return this.httpErrorCode;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public RetroError.Kind getKind() {
        return this.kind;
    }

    public static enum Kind {
        NETWORK,
        HTTP,
        UNEXPECTED;

        private Kind() {
        }
    }
}
