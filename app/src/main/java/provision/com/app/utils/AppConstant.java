package provision.com.app.utils;

/**
 * Created by gauravg on 7/9/2018.
 */

public class AppConstant {
    public static final String imageUrl = "https://images.indianexpress.com/2018/07/vida1.jpg?w=610";


    public static final String KEY_EVENT_ID = "event_id";
    public static final String KEY_SELECTED_TAB_NAME = "selected_tab_name";

    public static final String KEY_SELECTED_TAB_ID = "selected_tab_id";
    public static final String KEY_SELECTED_TAB_EVENT_ID = "selected_tab_event_id";


    public static final String KEY_CATEGORY = "category";
    public static final String KEY_YEAR = "year";
    public static final String KEY_MONTH = "month";
    public static final String KEY_STATUS = "status";
    public static final String KEY_SEARCH_TEXT = "searchtext";
}
