package provision.com.app.utils.network.api;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import provision.com.app.utils.CommonUtils;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {
    //live url
    //private static final String BASE_URL = "https://events.pro-vision.com/webapi/";
    //Live Pro Vision
    //public static final String PRIVACY_POLICY_URL = "https://events.pro-vision.com/cms/privacy/0";
    //public static final String TERMS_N_CONDITIONS_URL = "https://events.pro-vision.com/cms/tnc/0";

    //---------------------------------------------------------------------------------------


    //live
//    private static final String BASE_URL = "https://suffix.events/webapi/";
    //development url
    private static final String BASE_URL = "https://service.suffix.events/";
//    ---------------------------------------------------------------------------------------

    public static final String ABOUT_US_URL = "https://wesuffix.com/";
    public static final String PRIVACY_POLICY_URL = "https://suffix.events/cms/privacy/0";
    public static final String TERMS_N_CONDITIONS_URL = "https://suffix.events/cms/tnc/0";

    //provision
    //public static final String HELP_URL = "https://service.pro-vision.com/cms/Help";
    //qa suffix
    public static final String HELP_URL = "https://service.suffix.events/cms/Help";


    public static final String YOUTUBE = "https://www.youtube.com/channel/UC4DvB45pC62KrcLVQbykSjQ";
    public static final String INSTAGRAM = "https://www.instagram.com/pv.events/";
    public static final String FACEBOOK = "https://www.facebook.com/provision.sport?ref=hl";
    //--------------------------------------------------------------------------------------
    //old version
    //public static String API_VERSION = "3";
    //new version
    public static String API_VERSION = "5";
    private static Retrofit retrofit = null;
    private static AuthApiHelper authApiHelper;

    public static Retrofit getClient(final Context context) {
        if (retrofit == null) {
            OkHttpClient.Builder httpClient;
            httpClient = new OkHttpClient.Builder();
            if (CommonUtils.isDebug()) {
                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                httpClient.addInterceptor(logging);
            }
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            httpClient.connectTimeout(100, TimeUnit.SECONDS);
            httpClient.readTimeout(100, TimeUnit.SECONDS);
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(httpClient.build())
                    .build();
        }
        return retrofit;
    }

    public static AuthApiHelper getAPiAuthHelper(Context context) {
        if (retrofit == null) {
            getClient(context);
        }
        if (authApiHelper == null) {
            authApiHelper = retrofit.create(AuthApiHelper.class);
        }
        return authApiHelper;
    }


}
