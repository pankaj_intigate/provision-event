package provision.com.app.utils.network.api;

import androidx.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.UnknownHostException;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Amit Pal on 12/07/2018.
 */

public abstract class CallbackManager<T> implements Callback<T> {

    private HashMap<String, String> mRequestHeaderMap = new HashMap<>();

    public CallbackManager() {
    }

    @Override
    public void onResponse(@Nullable Call call,@Nullable  Response response) {
        Log.d("onResponse", new Gson().toJson(response.body()));
        if (response.isSuccessful()) {
            String jsonString = new Gson().toJson(response.body());
            try {
                JSONObject jsonObject = new JSONObject(jsonString);
                if (jsonObject == JSONObject.NULL) {
                    onError(new RetroError(RetroError.Kind.HTTP, "Some thing went wrong.", -111));
                    return;
                }
                jsonObject.isNull("response");
                JSONObject jsonObject1 = jsonObject.getJSONObject("response");
                if (jsonObject1 == JSONObject.NULL) {
                    onError(new RetroError(RetroError.Kind.HTTP, "Some thing went wrong.", -111));
                    return;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                onError(new RetroError(RetroError.Kind.HTTP, "Some thing went wrong.", -111));
            }




//            try {
                onSuccess(response.body(), "");
//            } catch (Exception exception) {
//                Log.e("exception ", "exceptionexception " + exception.getMessage());
//            }
        } else {

            onError(new RetroError(RetroError.Kind.HTTP, "Unable to connect to server. Try after sometime.", -999));
        }
    }


    @Override
    public void onFailure(@Nullable Call call,@Nullable  Throwable throwable) {
        Log.d("OkHttp",throwable.getLocalizedMessage());
        if (throwable instanceof UnknownHostException) {
            onError(new RetroError(RetroError.Kind.NETWORK, throwable.getMessage(), -999));
        } else {
            onError(new RetroError(RetroError.Kind.UNEXPECTED, throwable.getMessage(), -999));
        }
    }

    protected abstract void onSuccess(Object object, String message);

    protected abstract void onError(RetroError retroError);

    protected abstract void onFailure(String retroError);

}
