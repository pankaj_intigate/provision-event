package provision.com.app.shop.filter.category

import android.content.Context
import android.os.SystemClock
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_category_layout.view.*
import provision.com.app.R
import provision.com.app.interfaces.OnItemFilterSelectListener
import provision.com.app.shop.model.filterlistresponse.Category

class CategoriesAdapter(var context: Context, var categoryList: ArrayList<Category>, var onItemFilterSelectListener: OnItemFilterSelectListener) : RecyclerView.Adapter<CategoriesAdapter.ViewHolder>() {
    private var mLastClickTime: Long = 0

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        var view: View = LayoutInflater.from(context).inflate(R.layout.item_category_layout, p0, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return categoryList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvCategory.text = categoryList.get(position).title

        holder.itemView.cbCategory.isChecked = categoryList.get(position).isselected == 1

        holder.itemView.cbCategory.setOnCheckedChangeListener(null)
        holder.itemView.cbCategory.setOnCheckedChangeListener { buttonView, isChecked ->
            if (SystemClock.elapsedRealtime() - mLastClickTime < 300){
                return@setOnCheckedChangeListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()
            if (isChecked) {
                categoryList.get(position).isselected = 1
                onItemFilterSelectListener.onItemSelected(position, categoryList.get(position).id!!,"", categoryList.get(position).title!!,"Category")
            } else {
                categoryList.get(position).isselected = 0
                onItemFilterSelectListener.onItemUnSelected(position, categoryList.get(position).id!!,"",  categoryList.get(position).title!!,"Category")
            }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}
}