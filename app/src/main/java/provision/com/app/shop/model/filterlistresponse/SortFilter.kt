package provision.com.app.shop.model.filterlistresponse

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class SortFilter() :Parcelable{
    @SerializedName("Key")
    @Expose
    var key: String? = null
    @SerializedName("value")
    @Expose
    var value: String? = null
    @SerializedName("IsselectedSorting")
    @Expose
    var isselectedSorting: String? = null

    constructor(parcel: Parcel) : this() {
        key = parcel.readString()
        value = parcel.readString()
        isselectedSorting = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(key)
        parcel.writeString(value)
        parcel.writeString(isselectedSorting)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SortFilter> {
        override fun createFromParcel(parcel: Parcel): SortFilter {
            return SortFilter(parcel)
        }

        override fun newArray(size: Int): Array<SortFilter?> {
            return arrayOfNulls(size)
        }
    }

}