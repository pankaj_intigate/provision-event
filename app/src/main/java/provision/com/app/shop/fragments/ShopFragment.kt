package provision.com.app.shop.fragments


import android.app.Activity.RESULT_OK
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import com.google.android.material.bottomsheet.BottomSheetDialog
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.TextUtils
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_shop.*
import provision.com.app.R
import provision.com.app.adapter.ShopItemProductListAdapter
import provision.com.app.apiResponse.shop_product_list.ProductDetails
import provision.com.app.apiResponse.shop_product_list.ProductListResponse
import provision.com.app.apiResponse.shop_product_list.short_product.ShortingResponse
import provision.com.app.databinding.FragmentShopBinding
import provision.com.app.fragment.BaseFragment
import provision.com.app.item_decorater.SpacesItemDecoration
import provision.com.app.shop.filter.FiltersActivity
import provision.com.app.shop.model.filterlistresponse.Category
import provision.com.app.shop.model.filterlistresponse.Color
import provision.com.app.shop.model.filterlistresponse.Offers
import provision.com.app.shop.model.filterlistresponse.Size
import provision.com.app.utils.PrefUtils
import provision.com.app.utils.network.api.ApiClient
import provision.com.app.utils.network.api.CallbackManager
import provision.com.app.utils.network.api.RetroError
import java.util.*

open class ShopFragment : BaseFragment() {
    var binding: FragmentShopBinding? = null
    private var productListAdapter: ShopItemProductListAdapter? = null
    private var productList: ArrayList<ProductDetails>? = null
    private var bottomSheetDialog: BottomSheetDialog? = null
    private var shortingBy = ""
    private var pageNo = 1
    private var selectedCategory = ArrayList<Category>()
    private var selectedSize = ArrayList<Size>()
    private var selectedColor = ArrayList<Color>()
    private var isSelectedFilterData = false
    private var selectedOffers = ArrayList<Offers>()
    var minPrice: String = ""
    var maxPrice: String = ""
    private var isLoading = false
    private var isDataAvailable = true
    var categoryId: String = ""
    var colorId: String = ""
    var sizeId: String = ""
    var price: String = ""
    var searchData: String = ""
    var offers: String = ""
    var searchTxt: String = ""
    var searchImg: AppCompatImageView? = null

    companion object {
        var shopFragment: ShopFragment? = null
        @JvmStatic
        fun newInstance(): ShopFragment {
            if (shopFragment == null) {
                shopFragment = ShopFragment()
            }
            return shopFragment!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_shop, container, false)
            binding!!.searchEdt.setOnEditorActionListener(object : TextView.OnEditorActionListener {
                override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        searchTxt = searchEdt.text.toString()
                        searchData = "shop_item"
                        pageNo = 1
                        isDataAvailable = true
                        getProductList(categoryId, colorId, sizeId, price, searchData)
                        return true
                    }
                    return false
                }

            })
            binding!!.cancelTV.setOnClickListener { v ->
                binding!!.searchEdt.setText("")
                searchTxt = ""
                searchData = ""
                pageNo = 1
                searchLL.visibility = View.GONE
                searchImg!!.visibility = View.VISIBLE
                isDataAvailable = true
                getProductList(categoryId, colorId, sizeId, price, searchData)
            }
            setAdapter()
            binding!!.shotRL.setOnClickListener {
                if (bottomSheetDialog != null) {
                    bottomSheetDialog!!.show()
                } else {
                    //  getShortData()
                }
            }
            getShortData()
            getProductList(categoryId, colorId, sizeId, price, searchData)
            binding!!.rlFilter.setOnClickListener {
                startActivityForResult(Intent(activity, FiltersActivity::class.java)
                        .putExtra("isSelectedFilterData", isSelectedFilterData)
                        .putExtra("MinPrice", minPrice)
                        .putExtra("MaxPrice", maxPrice)
                        .putParcelableArrayListExtra("SelectedCategoryList", selectedCategory)
                        .putParcelableArrayListExtra("SelectedSizeList", selectedSize)
                        .putParcelableArrayListExtra("SelectedColorList", selectedColor)
                        .putParcelableArrayListExtra("SelectedOffersList", selectedOffers), 601)
            }
            binding!!.productRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                val mLayoutManager = binding!!.productRecyclerView.layoutManager as GridLayoutManager
                val visibleItemCount = mLayoutManager.getChildCount();
                val totalItemCount = mLayoutManager.getItemCount();
                val pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)

                    if ((pastVisiblesItems + 5) >= totalItemCount && !isLoading && isDataAvailable) {
                        getProductList(categoryId, colorId, sizeId, price, searchData)
                    }
                }
            })
        }

        return binding!!.root
    }

    fun onSearchClick(searchImg: AppCompatImageView) {
        this.searchImg = searchImg

        this.searchImg!!.setOnClickListener { v ->
            this.searchImg!!.visibility = View.GONE
            binding!!.searchLL.visibility = View.VISIBLE
        }


    }

    private fun setAdapter() {
        productList = ArrayList()
        val spacingInPixels = resources.getDimensionPixelOffset(R.dimen.dim_1dp)
        val spacesItemDecoration = SpacesItemDecoration(spacingInPixels)
        binding!!.productRecyclerView.addItemDecoration(spacesItemDecoration)
//        productListAdapter = ShopItemProductListAdapter(activity, productList, 1.0, "")
        binding!!.productRecyclerView.layoutManager = GridLayoutManager(activity, 2)
//        binding.productRecyclerView.adapter = productListAdapter
    }

    private fun getProductList(categoryId: String, colorId: String, sizeId: String, price: String, searchData: String) {
        if (isOnline()) {
            var progressBar: View? = null
            if (pageNo == 1) {
                progressBar = showProgressBar()

            }
            isLoading = true
            ApiClient.getAPiAuthHelper(activity).shopProductList("shop_item", "2", "1", shortingBy, searchData, categoryId, searchTxt, sizeId, colorId, offers, price, "", pageNo, PrefUtils.getFromPrefs(activity, PrefUtils.USER_ID)).enqueue(object : CallbackManager<ProductListResponse>() {
                override fun onSuccess(`object`: Any, message: String) {


                    val productListResponse = `object` as ProductListResponse
                    if (pageNo == 1) {
                        cancelProgressBar(progressBar)
                    }
                    if (productListResponse.response.arrayList.size > 0) {
                        if (productListResponse.response.arrayList.size < 10) {
                            isDataAvailable = false
                        } else {
                            isDataAvailable = true
                        }

                        if (pageNo == 1) {
                            binding!!.tvNoDataFound.visibility = View.GONE
                            binding!!.productRecyclerView.visibility = View.VISIBLE
                            productList!!.clear()
                            productList!!.addAll(productListResponse.response.arrayList)
                            productListAdapter = ShopItemProductListAdapter(activity, productList, productListResponse.currencyValue, productListResponse.currrencyCode)
                            binding!!.productRecyclerView.adapter = productListAdapter
                        } else {
                            productList!!.addAll(productListResponse.response.arrayList)
                            productListAdapter!!.notifyDataSetChanged()
                        }
                        isLoading = false
                        pageNo = pageNo + 1
                    } else {
                        if (pageNo == 1) {
                            binding!!.productRecyclerView.visibility = View.GONE
                            binding!!.tvNoDataFound.visibility = View.VISIBLE
                        }
                    }

                }

                override fun onError(retroError: RetroError) {}

                override fun onFailure(retroError: String) {}


            })
        }
    }

    fun convertSpToPixels(sp: Float): Float {
        val aa = sp / getResources().getDisplayMetrics().scaledDensity;
        return aa
    }

    private fun getShortData() {
        ApiClient.getAPiAuthHelper(activity).shortData("sort_filter", ApiClient.API_VERSION).enqueue(object : CallbackManager<ShortingResponse>() {
            override fun onSuccess(`object`: Any, message: String) {
                val shortingResponse = `object` as ShortingResponse
                bottomSheetDialog = BottomSheetDialog(activity!!)
                bottomSheetDialog!!.setContentView(R.layout.short_shop_item)
                val radioGroup = bottomSheetDialog!!.findViewById<RadioGroup>(R.id.shortRG)
                if (shortingResponse.shortingResponses != null && shortingResponse.shortingResponses.size > 0) {
                    for (i in 0 until shortingResponse.shortingResponses.size) {

                        val rdbtn = RadioButton(activity!!)
                        rdbtn.id = i
                        rdbtn.text = shortingResponse.shortingResponses[i].value
                        rdbtn.setPadding(0, 0, 0, 8)
                        rdbtn.setTextSize(convertSpToPixels(getResources().getDimension(R.dimen.dim_14sp)))
                        radioGroup!!.addView(rdbtn)
                        if (i == 0) {
                            shortingBy = shortingResponse.shortingResponses[i].name
                            rdbtn.isChecked = true
                        }
                    }
                }
                radioGroup!!.setOnCheckedChangeListener { group, checkedId ->
                    shortingBy = shortingResponse.shortingResponses[checkedId].name
                    pageNo = 1
                    productList!!.clear()
                    productListAdapter!!.notifyDataSetChanged()
                    searchData = "shop_item"
                    getProductList(categoryId, colorId, sizeId, price, searchData)
                    bottomSheetDialog!!.dismiss()
                }
                // pageNo = 1
                // getProductList()
            }

            override fun onError(retroError: RetroError) {

            }

            override fun onFailure(retroError: String) {

            }
        })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 601 && resultCode == RESULT_OK) {
            if (data != null) {
                selectedCategory.clear()
                selectedCategory.addAll(data.getParcelableArrayListExtra("SelectedCategoryList"))
                //   Log.d("selectedCategoryComma", android.text.TextUtils.join(",", selectedCategory))
                selectedSize.clear()
                selectedSize.addAll(data.getParcelableArrayListExtra("SelectedSizeList"))
                selectedColor.clear()
                selectedColor.addAll(data.getParcelableArrayListExtra("SelectedColorList"))
                //     Log.d("selectedCategoryCommaCo", android.text.TextUtils.join(",", selectedColor))
                selectedOffers.clear()
                selectedOffers.addAll(data.getParcelableArrayListExtra("SelectedOffersList"))
                isSelectedFilterData = data.getBooleanExtra("IsSelectedFilterData", false)
                categoryId = TextUtils.join(",", selectedCategory)
                sizeId = TextUtils.join(",", selectedSize)
                colorId = TextUtils.join(",", selectedColor)
                offers = TextUtils.join(",", selectedOffers)
                minPrice = data.getStringExtra("MinPrice")
                maxPrice = data.getStringExtra("MaxPrice")
                if (!TextUtils.isEmpty(minPrice) && !TextUtils.isEmpty(maxPrice)) {
                    price = "$minPrice,$maxPrice"
                } else {
                    price = ""
                }
                searchData = "shop_item"
                isDataAvailable = true;
                pageNo = 1
                getProductList(categoryId, colorId, sizeId, price, searchData)
            }
        }
    }
}
