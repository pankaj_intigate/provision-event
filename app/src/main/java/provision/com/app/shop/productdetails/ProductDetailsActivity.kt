package provision.com.app.shop.productdetails

import android.app.Activity
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import kotlinx.android.synthetic.main.activity_product_details.*
import kotlinx.android.synthetic.main.add_to_cart_dialog.view.*
import kotlinx.android.synthetic.main.header_layout.*
import provision.com.app.R
import provision.com.app.activity.BaseActivity
import provision.com.app.activity.CartActivity
import provision.com.app.activity.LoginActivity
import provision.com.app.apiResponse.cart_count.CartCountEntity
import provision.com.app.apiResponse.event_shop.ItemImage
import provision.com.app.apiResponse.productdetailsresponse.ColorAttr
import provision.com.app.apiResponse.productdetailsresponse.ItemSize
import provision.com.app.apiResponse.productdetailsresponse.ProductDetailsResponse
import provision.com.app.apiResponse.savetocartresponse.SaveToCartResponse
import provision.com.app.apiResponse.savetocartresponse.WishListRes.WishListRes
import provision.com.app.shop.event_shop_item.RefreshItemOnChanage
import provision.com.app.utils.PrefUtils
import provision.com.app.utils.Utils
import provision.com.app.utils.network.api.ApiClient
import provision.com.app.utils.network.api.AuthApiHelper
import provision.com.app.utils.network.api.CallbackManager
import provision.com.app.utils.network.api.RetroError
import retrofit2.Call
import retrofit2.Callback

class ProductDetailsActivity : BaseActivity(), RefreshItemOnChanage {
    lateinit var productViewPagerAdapter: ProductViewPagerAdapter
    lateinit var productColorAdapter: ProductColorAdapter
    lateinit var productSizeAdapter: ProductSizeAdapter
    private var isOpen: Boolean = false
    private var productSubId: String? = null
    private var colorList = ArrayList<ColorAttr>()
    private var itemSize = ArrayList<ItemSize>()
    var colorID = ""
    var shareurl = ""
    var sizeID = ""
    var itemID = ""
    var isAvailabilityColor = ""
    var isAvailabilitySize = ""
    var qty: Int = 1
    var isOutOfStock = 1
    override fun onRefresh(position: Int, colorid: String, sizeId: String, isColorChanged: Boolean, itemPosition: Int) {


//        Log.d("hello","collor $colorID    size $sizeID  isAvailability  ${colorList[position].isAvailability}")

        if (isColorChanged) {
            if (colorList[itemPosition].isAvailability == 2) {
                isAvailabilityColor = ""
                isAvailabilitySize = ""
                sizeID = ""
                colorID = colorid
//                Log.d("hello","collor $colorID    size $sizeID")
                getProductDetails(colorid, "2", "", "")
                return
            }
        } else {
            if (itemSize[itemPosition].isAvailability == 2) {
                isAvailabilityColor = ""
                isAvailabilitySize = ""
                isAvailabilitySize = ""
                sizeID = sizeId
                colorID = ""
//                Log.d("hello","collor $colorID    size $sizeID")
                getProductDetails("", "", sizeId, "2")

                return
            }
        }

//
        if (!colorid.isEmpty()) {
            for (color in colorList) {
                if (color.isselected == 1) {
                    color.isselected = 0
                    break
                }
            }
            colorList[position].isselected = 1
            colorID = colorid
            isAvailabilityColor =  colorList[itemPosition].isAvailability.toString()
        } else {
            for (color in colorList) {
                if (color.isselected == 1) {
                    colorID = color.colorId!!
                    isAvailabilityColor = color.isAvailability.toString()
                    break
                }
            }
        }


        if (!sizeId.isEmpty()) {
            for (itemSize in itemSize) {
                if (itemSize.isselected == 1) {
                    itemSize.isselected = 0
                    break
                }
            }
            itemSize[position].isselected = 1
            sizeID = sizeId
            isAvailabilitySize =  itemSize[itemPosition].isAvailability.toString()
        } else {
            for (itemSize in itemSize) {
                if (itemSize.isselected == 1) {
                    sizeID = itemSize.sizeId!!
                    isAvailabilitySize = itemSize.isAvailability.toString()
                    if (itemSize.isAvailability == 2) {
                        colorID = ""
                        isAvailabilitySize = ""
                    }
                    break
                }
            }
        }
        if (isAvailabilityColor.equals("3")) {
            isAvailabilityColor = ""
        }
        if (isAvailabilitySize.equals("3")) {
            isAvailabilitySize = ""
        }
//        Log.d("hello","collor $colorID    size $sizeID")
        getProductDetails(colorID, isAvailabilityColor, sizeID, isAvailabilitySize)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_details)

        if (intent != null) {
            productSubId = intent.getStringExtra("ProductSubId")
        }

        back.visibility = View.VISIBLE
        back.setOnClickListener { finish() }
        cartRL.visibility = View.VISIBLE

        cartRL.setOnClickListener {
            val intent = Intent(this, CartActivity::class.java)
            startActivityForResult(intent, 101)
            overridePendingTransition(R.anim.bottom_up, R.anim.stay)
        }

//        rlDescription.setOnClickListener {
//            if (isOpen) {
//                isOpen = false
//                ivDescription.rotation = 270f
//                tvDescription.visibility = View.GONE
//            } else {
//                isOpen = true
//                ivDescription.rotation = 0f
//                tvDescription.visibility = View.VISIBLE
//            }
//        }

//        rlShippingAndReturn.setOnClickListener {
//            if (isOpen) {
//                isOpen = false
//                ivShippingAndReturn.rotation = 270f
//                tvShippingAndReturn.visibility = View.GONE
//            } else {
//                isOpen = true
//                ivShippingAndReturn.rotation = 0f
//                tvShippingAndReturn.visibility = View.VISIBLE
//            }
//        }
        searchImg.visibility = View.VISIBLE
        searchImg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.share))
        searchImg.setOnClickListener {
            val share = Intent(Intent.ACTION_SEND)
            share.type = "text/plain"
            share.putExtra(Intent.EXTRA_TEXT, shareurl)

            startActivity(Intent.createChooser(share, "Share link!"))
        }
        btn_add_cart.setOnClickListener {
            if (isOutOfStock == 0) {
                return@setOnClickListener
            }
            val sessionId = PrefUtils.getFromPrefs(this, PrefUtils.SESSION_ID)




            for (sizeDetails in itemSize){
                if (sizeDetails.isselected==1){
                    sizeID = sizeDetails.sizeId!!
                    break
                }
            }
            for (colorAttr in colorList){
                if (colorAttr.isselected==1){
                    colorID = colorAttr.colorId!!
                    break
                }
            }

//            if (TextUtils.isEmpty(sizeID)) {
//                showToast(this, "Please select size")
//                return@setOnClickListener
//            }

            if (TextUtils.isEmpty(colorID)) {
                showToast(this, "Please select color")
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(sizeID)) {
                showToast(this, "Please select size")
                return@setOnClickListener
            }
            saveItemCart(itemID, sessionId, "" + qty, colorID, sizeID, "2", "")
        }

        ivWishList.setOnClickListener {
            if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(this, PrefUtils.USER_ID))) {
                addToWishList(PrefUtils.getFromPrefs(this, PrefUtils.USER_ID), itemID, "CartItem", PrefUtils.getFromPrefs(this, PrefUtils.SESSION_ID))

            } else {
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }

        if (qty < 10) {
            tvQuantity.text = "0$qty"
        } else {
            tvQuantity.text = "" + qty
        }
        ivMinus.setOnClickListener {
            if (qty > 1) {
                qty--
            }
            if (qty < 10) {
                tvQuantity.text = "0" + qty
            } else {
                tvQuantity.text = "" + qty
            }
        }

        ivPlus.setOnClickListener {
            if (qty in 1..998) {
                qty++
            }
            if (qty < 10) {
                tvQuantity.text = "0$qty"
            } else {
                tvQuantity.text = "" + qty
            }
        }

        getProductDetails("", "", "", "")
        setProductColorAdapter()
        setProductSizeAdapter()
    }

    private fun openCartDialog() {
        val inflater: LayoutInflater = layoutInflater
        val layout = inflater.inflate(R.layout.add_to_cart_dialog, null)
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setView(layout)
        alertDialog.setCancelable(false)
        val dialog: AlertDialog = alertDialog.create()

        layout.tvContinueShopping.setOnClickListener {
            dialog.dismiss()
        }

        layout.tvViewCart.setOnClickListener {
            val intent = Intent(this, CartActivity::class.java)
            startActivityForResult(intent, 101)
            overridePendingTransition(R.anim.bottom_up, R.anim.stay)
            dialog.dismiss()
        }

        dialog.show()

    }

    private fun getProductDetails(colorId: String, isAvailabilityColor: String, sizeId: String, isAvailabilitySize: String) {
        Log.d("hello", "collor $colorID  isAvailabilityS  $isAvailabilityColor  size $sizeID  isAvailabilityS  $isAvailabilitySize")
        if (isNetworkAvailable) {
            val progressBar = showProgressBar()

            ApiClient.getAPiAuthHelper(this).shopProductDetails("shop_item_details", ApiClient.API_VERSION, "1", productSubId,
                    colorId, isAvailabilityColor, sizeId, isAvailabilitySize, PrefUtils.getFromPrefs(this, PrefUtils.USER_ID))
                    .enqueue(object : CallbackManager<ProductDetailsResponse>() {
                        override fun onSuccess(`object`: Any?, message: String?) {
                            cancelProgressBar(progressBar)
                            val productDetailsResponse: ProductDetailsResponse = `object` as ProductDetailsResponse
                            itemID = productDetailsResponse.response!!.iId!!
                            tvTitle.text = productDetailsResponse.response!!.typeTitle
                            tvProductName.text = productDetailsResponse.response!!.itemTitle

                            tvProductPrice.text = Utils.convertPrice(productDetailsResponse.response!!.salesPrice, productDetailsResponse.currencyValue, 3, "") + " " + productDetailsResponse.currrencyCode
                            if (productDetailsResponse.response!!.basePrice > 0) {
                                tvProductDiscountPrice.visibility = View.VISIBLE
                                tvProductDiscountPrice.text = Utils.convertPrice(productDetailsResponse.response!!.basePrice, productDetailsResponse.currencyValue, 3, "") + " " + productDetailsResponse.currrencyCode
                                tvProductDiscountPrice.paintFlags = tvProductDiscountPrice.getPaintFlags() or Paint.STRIKE_THRU_TEXT_FLAG
                            } else {
                                tvProductDiscountPrice.visibility = View.GONE
                            }
                            if (productDetailsResponse.response!!.wishlist == 1) {
                                ivWishList.setImageResource(R.drawable.wishlistactive)
                            }
                            shareurl = productDetailsResponse.response!!.shareurl!!
                            colorList.clear()
                            colorList.addAll(productDetailsResponse.response!!.colorAttr!!)
                            if (colorList.isEmpty()) {
                                colorID = "1"
                                colorTV.visibility = View.GONE
                                rcvHorizontalColorList.visibility = View.GONE
                            }

                            productColorAdapter.notifyDataSetChanged()

                            setImagesInViewPager(productDetailsResponse.response!!.itemImage)
                            if (productDetailsResponse.response!!.discount <= 0) {
                                tvProductPercentageOff.visibility = View.GONE
                            }
                            tvProductPercentageOff.text = productDetailsResponse.response!!.discountvalue
                            if (productDetailsResponse.response!!.itemSize!!.size > 0) {
                                llSize.visibility = View.VISIBLE
                                rcvHorizontalSizeList.visibility = View.VISIBLE
                                itemSize.clear()
                                itemSize.addAll(productDetailsResponse.response!!.itemSize!!)
                                productSizeAdapter.notifyDataSetChanged()
                            } else {
                                llSize.visibility = View.GONE
                                rcvHorizontalSizeList.visibility = View.GONE
                                sizeID = "1"
                            }

                            if (productDetailsResponse.response!!.vDesc!=null &&!TextUtils.isEmpty(productDetailsResponse.response!!.vDesc!!)) {
                                tvDescription.loadDataWithBaseURL(null, productDetailsResponse.response!!.vDesc!!, "text/html", "UTF-8", null)
                                rlDescription.visibility = View.VISIBLE
                            } else {
                                rlDescription.visibility = View.GONE
                            }

                            if (!TextUtils.isEmpty(productDetailsResponse.response!!.shipingAndReturns)) {
                                tvShippingAndReturn.loadDataWithBaseURL(null, productDetailsResponse.response!!.shipingAndReturns!!, "text/html", "UTF-8", null)
                                rlShippingAndReturn.visibility = View.VISIBLE
                            } else {
                                rlShippingAndReturn.visibility = View.GONE
                            }
                            if (productDetailsResponse.response!!.isStock > 0) {
                                btn_add_cart.text = "Add to cart"
          btn_add_cart.isEnabled=true
                            } else {
                                btn_add_cart.text = "OUT OF STOCK"
                                btn_add_cart.isEnabled=false
                            }
                            isOutOfStock = productDetailsResponse.response!!.isStock
                            if (productDetailsResponse.response!!.basePrice == productDetailsResponse.response!!.salesPrice) {
                                tvProductDiscountPrice.visibility = View.GONE
                            }
                        }

                        override fun onError(retroError: RetroError?) {
                            showToast(this@ProductDetailsActivity, getString(R.string.response_msg))
                        }

                        override fun onFailure(retroError: String?) {
                            showToast(this@ProductDetailsActivity, getString(R.string.response_msg))
                        }
                    })
        }
    }

    private fun setImagesInViewPager(itemImage: ArrayList<ItemImage>?) {
        productViewPagerAdapter = ProductViewPagerAdapter(this, itemImage!!)
        vpProduct.adapter = productViewPagerAdapter
        if (itemImage.size > 1) {
            tbProductViewPager.setupWithViewPager(vpProduct, true)
        }
    }

    private fun setProductColorAdapter() {
        rcvHorizontalColorList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rcvHorizontalColorList.setHasFixedSize(true)
        rcvHorizontalColorList.itemAnimator = DefaultItemAnimator()
        productColorAdapter = ProductColorAdapter(this, colorList, this)
        rcvHorizontalColorList.adapter = productColorAdapter
    }

    private fun setProductSizeAdapter() {
        rcvHorizontalSizeList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rcvHorizontalSizeList.setHasFixedSize(true)
        rcvHorizontalSizeList.itemAnimator = DefaultItemAnimator()
        productSizeAdapter = ProductSizeAdapter(this, itemSize, this)
        rcvHorizontalSizeList.adapter = productSizeAdapter
    }

    private fun saveItemCart(id: String, sessionId: String, quantity: String, colorid: String, tshirt: String, type: String, eventId: String) {
        if (isOnline()) {
            val view = showProgressBar()

            ApiClient.getAPiAuthHelper(this).saveCartItem("savecartItem", id, sessionId, quantity, colorid, tshirt, type, eventId, ApiClient.API_VERSION).enqueue(object : Callback<SaveToCartResponse> {
                override fun onResponse(call: Call<SaveToCartResponse>, response: retrofit2.Response<SaveToCartResponse>) {
                    val res = response.body()
                    if (res!!.response!!.lastItem.equals("addtocart", true)) {
                        openCartDialog()
                        cartItemCount(sessionId)
                    } else {
                        showToast(this@ProductDetailsActivity, "" + res.responseMessege)
                    }
                    cancelProgressBar(view)
                }

                override fun onFailure(call: Call<SaveToCartResponse>, t: Throwable) {
                    cancelProgressBar(view)
                }
            })
        } else {
            showToast(this, getResources().getString(R.string.check_internet))
        }
    }


    private fun addToWishList(userId: String, productId: String, type: String, sessionId: String) {
        if (isOnline()) {
            val view = showProgressBar()

            ApiClient.getAPiAuthHelper(this).addToWishList("adduserWishList", userId, productId, type, sessionId, ApiClient.API_VERSION).enqueue(object : Callback<WishListRes> {
                override fun onResponse(call: Call<WishListRes>, response: retrofit2.Response<WishListRes>) {
                    val wishListRes = response.body()
                    if (wishListRes!!.response!!.status == 1) {
                        ivWishList.setImageResource(R.drawable.wishlistactive)
                    } else {
                        ivWishList.setImageResource(R.drawable.wishlist)
                    }
                    cancelProgressBar(view)
                }

                override fun onFailure(call: Call<WishListRes>, t: Throwable) {
                    cancelProgressBar(view)
                }
            })
        } else {
            showToast(this, getResources().getString(R.string.check_internet))
        }
    }

    override fun onResume() {
        super.onResume()
//        if(!TextUtils.isEmpty(PrefUtils.getFromPrefs(this,PrefUtils.CART_ITEM))){
//            cartCountTextView.visibility = View.VISIBLE
//            cartCountTextView.text = ""+PrefUtils.getFromPrefs(this,PrefUtils.CART_ITEM)
//        }

        if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM)) && PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM) != "0") {
            cartCountTextView.setVisibility(View.VISIBLE)
            cartCountTextView.setText(PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM))
        } else {
            cartCountTextView.setVisibility(View.GONE)
        }
    }

    private fun cartItemCount(sessionId: String) {
        val authApiHelper = ApiClient.getClient(this).create(AuthApiHelper::class.java)
        authApiHelper.getCartCount("GetCartCount", sessionId, PrefUtils.getFromPrefs(this, PrefUtils.USER_ID), ApiClient.API_VERSION).enqueue(object : CallbackManager<CartCountEntity>() {
            override fun onSuccess(`object`: Any, message: String) {
                val baseResponce = `object` as CartCountEntity
                if (baseResponce.cartCountResponse != null) {
                    cartCountTextView.visibility = View.VISIBLE
                    PrefUtils.saveToPrefs(this@ProductDetailsActivity, PrefUtils.CART_ITEM, "" + baseResponce.cartCountResponse.cart_count.toString())
                    cartCountTextView.text = "" + baseResponce.cartCountResponse.cart_count
                }
            }

            override fun onError(retroError: RetroError) {
                showToast(this@ProductDetailsActivity, getString(R.string.response_msg))
            }

            override fun onFailure(retroError: String) {
                showToast(this@ProductDetailsActivity, getString(R.string.response_msg))
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }
}
