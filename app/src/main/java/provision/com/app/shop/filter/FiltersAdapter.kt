package provision.com.app.shop.filter

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_categories.view.*
import provision.com.app.R
import provision.com.app.interfaces.MinMaxPrice
import provision.com.app.interfaces.OnItemFilterSelectListener
import provision.com.app.shop.filter.category.CategoriesActivity
import provision.com.app.shop.model.filterlistresponse.Color
import provision.com.app.shop.model.filterlistresponse.Datum
import provision.com.app.shop.model.filterlistresponse.Offers
import provision.com.app.shop.model.filterlistresponse.Size

class FiltersAdapter(val context: Context, var filterArrayList: ArrayList<Datum>, var onItemFilterSelectListener: OnItemFilterSelectListener,var minMaxPrice: MinMaxPrice) : RecyclerView.Adapter<FiltersAdapter.ViewHolder>() {
    private lateinit var sizeAdapter: SizeAdapter
    private var sizeArrayList = ArrayList<Size>()
    private lateinit var colorAdapter: ColorAdapter
    private lateinit var offersAdapter:OffersAdapter
    private var colorArrayList = ArrayList<Color>()
    private var offersArrayList = ArrayList<Offers>()
    private var isSizeOpen = true
    private var isColorOpen = true
    private var isPriceOpen = true
    private var isOffersOpen = true
    private var minPrice = ""
    private var maxPrice = ""

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_categories, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return filterArrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvCategory.text = filterArrayList.get(position).name


        if (filterArrayList.get(position).name.equals("Category", true) && !TextUtils.isEmpty((context as FiltersActivity).showCategory())) {
            holder.itemView.tvSelectItem.visibility = View.VISIBLE
            holder.itemView.tvSelectItem.text = (context as FiltersActivity).showCategory().substring(0, (context as FiltersActivity).showCategory().length - 2)
        } else {
            holder.itemView.tvSelectItem.visibility = View.GONE
        }


        if (filterArrayList.get(position).name.equals("sort_filter", true)) {
            holder.itemView.dividerView.visibility = View.GONE
            holder.itemView.rlCategory.visibility = View.GONE
        } else {
            holder.itemView.dividerView.visibility = View.VISIBLE
            holder.itemView.rlCategory.visibility = View.VISIBLE
        }

        holder.itemView.edtMin.setText("" + filterArrayList.get(position).price!!.minimumPrice)
        holder.itemView.edtMax.setText("" + filterArrayList.get(position).price!!.maximumPrice)

        holder.itemView.edtMin.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
               // filterArrayList.get(position).price!!.minimumPrice = s.toString().trim()
                minMaxPrice.onMinMaxPrice(s.toString().trim(),holder.itemView.edtMax.text.toString().trim())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        holder.itemView.edtMax.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
              //  filterArrayList.get(position).price!!.maximumPrice = s.toString().trim()
                minMaxPrice.onMinMaxPrice(holder.itemView.edtMin.text.toString().trim(),s.toString().trim())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
       // minPrice(holder)
      //  maxPrice(holder)
        when (filterArrayList.get(position).name) {
            "Category" -> {
                holder.itemView.llParent.setOnClickListener {
                    (context as Activity).startActivityForResult(Intent(context, CategoriesActivity::class.java).putParcelableArrayListExtra("CategoryList", filterArrayList.get(position).category), 501)
                }
            }

            "Size" -> {
                holder.itemView.llParent.setOnClickListener {
                    setSizeAdapter(holder)
                    if (isSizeOpen) {
                        isSizeOpen = false
                        holder.itemView.rcvColorSizeList.visibility = View.VISIBLE
                        holder.itemView.ivArrow.setImageResource(R.drawable.downarrow)
                        if (filterArrayList.get(position).size!!.size > 0) {
                            sizeArrayList.clear()
                            sizeArrayList.addAll(filterArrayList.get(position).size!!)
                            sizeAdapter.notifyDataSetChanged()
                        }
                    } else {
                        isSizeOpen = true
                        holder.itemView.ivArrow.setImageResource(R.drawable.rightarrow)
                        holder.itemView.rcvColorSizeList.visibility = View.GONE
                    }
                }
            }

            "Color" -> {
                holder.itemView.llParent.setOnClickListener {
                    setColorAdapter(holder)
                    if (isColorOpen) {
                        isColorOpen = false
                        holder.itemView.rcvColorSizeList.visibility = View.VISIBLE
                        holder.itemView.ivArrow.setImageResource(R.drawable.downarrow)
                        if (filterArrayList.get(position).color!!.size > 0) {
                            colorArrayList.clear()
                            colorArrayList.addAll(filterArrayList.get(position).color!!)
                            colorAdapter.notifyDataSetChanged()
                        }
                    } else {
                        isColorOpen = true
                        holder.itemView.ivArrow.setImageResource(R.drawable.rightarrow)
                        holder.itemView.rcvColorSizeList.visibility = View.GONE
                    }
                }
            }

            "Price" -> {
                holder.itemView.llParent.setOnClickListener {
                    if (isPriceOpen) {
                        isPriceOpen = false
                        holder.itemView.ivArrow.setImageResource(R.drawable.downarrow)
                        holder.itemView.llPrice.visibility = View.VISIBLE
                    } else {
                        isPriceOpen = true
                        holder.itemView.ivArrow.setImageResource(R.drawable.rightarrow)
                        holder.itemView.llPrice.visibility = View.GONE
                    }
                }
            }

            "Offers" -> {
                holder.itemView.llParent.setOnClickListener {
                    setOffersAdapter(holder)
                    if (filterArrayList.get(position).offers!!.size > 0) {
                        offersArrayList.clear()
                        offersArrayList.addAll(filterArrayList.get(position).offers!!)
                        offersAdapter.notifyDataSetChanged()
                    }else{
                        return@setOnClickListener
                    }

                    if (isOffersOpen) {
                        isOffersOpen = false
                        holder.itemView.rcvColorSizeList.visibility = View.VISIBLE
                        holder.itemView.ivArrow.setImageResource(R.drawable.downarrow)
                    } else {
                        isOffersOpen = true
                        holder.itemView.ivArrow.setImageResource(R.drawable.rightarrow)
                        holder.itemView.rcvColorSizeList.visibility = View.GONE
                    }
                }
            }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    private fun setSizeAdapter(holder: ViewHolder) {
        holder.itemView.rcvColorSizeList.layoutManager = GridLayoutManager(context, 4)
        holder.itemView.rcvColorSizeList.setHasFixedSize(true)
        holder.itemView.rcvColorSizeList.itemAnimator = DefaultItemAnimator()
        sizeAdapter = SizeAdapter(context, sizeArrayList, onItemFilterSelectListener)
        holder.itemView.rcvColorSizeList.adapter = sizeAdapter
    }

    private fun setColorAdapter(holder: ViewHolder) {
        holder.itemView.rcvColorSizeList.layoutManager = GridLayoutManager(context, 2)
        holder.itemView.rcvColorSizeList.setHasFixedSize(true)
        holder.itemView.rcvColorSizeList.itemAnimator = DefaultItemAnimator()
        colorAdapter = ColorAdapter(context, colorArrayList, onItemFilterSelectListener)
        holder.itemView.rcvColorSizeList.adapter = colorAdapter
    }

    private fun setOffersAdapter(holder: ViewHolder) {
        holder.itemView.rcvColorSizeList.layoutManager = GridLayoutManager(context, 2)
        holder.itemView.rcvColorSizeList.setHasFixedSize(true)
        holder.itemView.rcvColorSizeList.itemAnimator = DefaultItemAnimator()
        offersAdapter = OffersAdapter(context, offersArrayList, onItemFilterSelectListener)
        holder.itemView.rcvColorSizeList.adapter = offersAdapter
    }


  /*  fun minPrice(holder: ViewHolderItem): String {
        minPrice = holder.itemView.edtMin.text.toString().trim()
        return minPrice
    }

    fun maxPrice(holder: ViewHolderItem): String {
        maxPrice = holder.itemView.edtMax.text.toString().trim()
        return maxPrice
    }

    fun getMinPrice():String{
        return minPrice
    }

    fun getMaxPrice():String{
        return maxPrice
    }*/
}