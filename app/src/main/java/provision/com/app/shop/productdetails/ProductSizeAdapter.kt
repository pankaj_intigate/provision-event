package provision.com.app.shop.productdetails

import android.content.Context
import android.graphics.Paint
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_product_size_layout.view.*
import provision.com.app.R
import provision.com.app.apiResponse.productdetailsresponse.ItemSize
import provision.com.app.shop.event_shop_item.RefreshItemOnChanage

class ProductSizeAdapter(var context: Context, var sizeList: ArrayList<ItemSize>, var refreshItemOnChanage: RefreshItemOnChanage) : RecyclerView.Adapter<ProductSizeAdapter.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_product_size_layout, p0, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return sizeList.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        if (sizeList[position].isselected == 1 ) {
            viewHolder.itemView.tvProductItemSize.background = ContextCompat.getDrawable(context, R.drawable.size_selected)
            viewHolder.itemView.tvProductItemSize.setPaintFlags(0)
            viewHolder.itemView.tvProductItemSize.setTextColor(ContextCompat.getColor(context,R.color.black))
        } else if (sizeList[position].isAvailability == 1) {
            viewHolder.itemView.tvProductItemSize.background = ContextCompat.getDrawable(context, R.drawable.size_unselected)
            viewHolder.itemView.tvProductItemSize.setPaintFlags(0)
            viewHolder.itemView.tvProductItemSize.setTextColor(ContextCompat.getColor(context,R.color.black))
        } else if (sizeList[position].isAvailability == 2) {
            viewHolder.itemView.tvProductItemSize.background = ContextCompat.getDrawable(context, R.drawable.item_not_available)
            viewHolder.itemView.tvProductItemSize.setPaintFlags(viewHolder.itemView.tvProductItemSize.getPaintFlags() or  Paint.STRIKE_THRU_TEXT_FLAG)
            viewHolder.itemView.tvProductItemSize.setTextColor(ContextCompat.getColor(context,R.color.colorLightGray))
        } else if (sizeList[position].isAvailability == 3) {
            viewHolder.itemView.tvProductItemSize.background = ContextCompat.getDrawable(context, R.drawable.size_unselected)
            viewHolder.itemView.tvProductItemSize.setPaintFlags(0)
            viewHolder.itemView.tvProductItemSize.setTextColor(ContextCompat.getColor(context,R.color.black))
        }
        viewHolder.itemView.tvProductItemSize.text = sizeList.get(position).vValue

        viewHolder.itemView.tvProductItemSize.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
//                if (sizeList[position].isAvailability == 3) {
//                    return
//                }
                refreshItemOnChanage.onRefresh(0, "", sizeList[position].sizeId.toString(),false,position)
            }

        })
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}
}