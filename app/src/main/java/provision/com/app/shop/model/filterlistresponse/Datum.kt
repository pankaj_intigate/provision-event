package provision.com.app.shop.model.filterlistresponse

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Datum() : Parcelable {
    @SerializedName("Name")
    @Expose
    var name: String? = null
    @SerializedName("Category")
    @Expose
     var category: ArrayList<Category>?=ArrayList()
    @SerializedName("Size")
    @Expose
    var size: ArrayList<Size>?=ArrayList()
    @SerializedName("Color")
    @Expose
    var color: ArrayList<Color>? = null
    @SerializedName("Offers")
    @Expose
    var offers: ArrayList<Offers>? = null
    @SerializedName("sort_filter")
    @Expose
    var sortFilter: ArrayList<SortFilter>? = null
    @SerializedName("Price")
    @Expose
    var price: Price? = Price()

    constructor(parcel: Parcel) : this() {
        name = parcel.readString()
        price = parcel.readParcelable(Price::class.java.classLoader)
//        parcel.readTypedList(size, Size.CREATOR)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeParcelable(price, flags)

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Datum> {
        override fun createFromParcel(parcel: Parcel): Datum {
            return Datum(parcel)
        }

        override fun newArray(size: Int): Array<Datum?> {
            return arrayOfNulls(size)
        }
    }


}