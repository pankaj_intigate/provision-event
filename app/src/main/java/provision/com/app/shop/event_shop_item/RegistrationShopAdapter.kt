package provision.com.app.shop.event_shop_item

import android.content.Context
import android.os.Build
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.item_shop_reg.view.*
import provision.com.app.R
import provision.com.app.adapter.EventProductImageAdapter
import provision.com.app.adapter.EventProductSizeAdapter
import provision.com.app.apiResponse.event_shop.Data
import provision.com.app.utils.Utils


class RegistrationShopAdapter(var context: Context, var itemList: ArrayList<Data>, var refreshItemOnChanage: RefreshItemOnChanage, var currencyValue: Double, var currencyCode: String) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    var isLoading = false
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        if (p1 == itemList.size) {
            val view = LayoutInflater.from(context).inflate(R.layout.load_more_progress, p0, false)
            return LoadMoreViewHolder(view)
        }
        val view = LayoutInflater.from(context).inflate(R.layout.item_shop_reg, p0, false)
        return ViewHolder(view)


    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        if (isLoading) {
            return itemList.size + 1
        }
        return itemList.size
    }

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        if (p0 is ViewHolder) {
            p0.itemView.product_name_tv.text = itemList[p1].itemTitle
            p0.itemView.productPriceTV.text = Utils.convertPrice(itemList[p1].salesPrice, currencyValue, 3, "") + " " + currencyCode
            p0.itemView.colorRV.layoutManager = LinearLayoutManager(context)
            p0.itemView.colorRV.isNestedScrollingEnabled = false
            if (itemList[p1].itemSize != null && itemList[p1].itemSize!!.size > 0) {
                p0.itemView.productSizeRV.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                p0.itemView.productSizeRV.isNestedScrollingEnabled = false
                p0.itemView.sizeTV.visibility = View.VISIBLE
                p0.itemView.productSizeRV.adapter = EventProductSizeAdapter(context, itemList[p1].itemSize!!, p1, refreshItemOnChanage)
            } else {
                p0.itemView.sizeTV.visibility = View.GONE
            }
            p0.itemView.colorRV.adapter = ColorEventShopAdapter(context, itemList[p1].colorAttr!!, p1, refreshItemOnChanage)

            if (itemList[p1].itemImage != null && itemList[p1].itemImage!!.size > 0) {
                p0.itemView.vpProduct.adapter = EventProductImageAdapter(context, itemList[p1].itemImage!!)
                p0.itemView.tbProductViewPager.setupWithViewPager(p0.itemView.vpProduct, true)
                if (itemList[p1].itemImage!!.size <= 1) {
                    p0.itemView.tbProductViewPager.visibility = View.GONE
                } else {
                    p0.itemView.tbProductViewPager.visibility = View.VISIBLE
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    p0.itemView.vpProduct.isNestedScrollingEnabled = true
                }
            }else{
                p0.itemView.vpProduct.adapter =null
            }
            p0.itemView.qtyMinsTV.setOnClickListener {
                val qty = p0.itemView.qtyTV.text.toString().toInt()
                if (qty == 1) {
                    return@setOnClickListener
                }
                if (qty == 0) {
                    Toast.makeText(context, "No item added", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
                p0.itemView.qtyTV.text = (qty - 1).toString()
            }
            p0.itemView.qtyPluseTV.setOnClickListener {
                val qty = p0.itemView.qtyTV.text.toString().toInt()
                p0.itemView.qtyTV.text = (qty + 1).toString()
            }
            if (itemList[p1].isStock >0) {
                p0.itemView.btn_sendToCart.text = "SEND TO CART"
                p0.itemView.btn_sendToCart.isEnabled=true
            } else {
                p0.itemView.btn_sendToCart.text = "OUT OF STOCK"
                p0.itemView.btn_sendToCart.isEnabled=false
            }
            p0.itemView.btn_sendToCart.setOnClickListener {
                if (itemList[p1].isStock == 0) {
                    return@setOnClickListener
                }
                val qty = p0.itemView.qtyTV.text.toString().toInt()
                if (qty == 0) {
                    Toast.makeText(context, "No item added", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
                refreshItemOnChanage.addToCart(p1, p0.itemView.qtyTV.text.toString())
            }

        }

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    class LoadMoreViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}