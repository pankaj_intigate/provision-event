package provision.com.app.shop.filter

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.TextUtils
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_filters.*
import kotlinx.android.synthetic.main.filter_header_layout.*
import provision.com.app.R
import provision.com.app.activity.BaseActivity
import provision.com.app.interfaces.MinMaxPrice
import provision.com.app.interfaces.OnItemFilterSelectListener
import provision.com.app.shop.model.filterlistresponse.*
import provision.com.app.utils.PrefUtils
import provision.com.app.utils.network.api.ApiClient
import provision.com.app.utils.network.api.CallbackManager
import provision.com.app.utils.network.api.RetroError


class FiltersActivity : BaseActivity(), OnItemFilterSelectListener, MinMaxPrice {
    lateinit var filtersAdapter: FiltersAdapter
    var filterArrayList = ArrayList<Datum>()
    var selectedCategoryList = ArrayList<Category>()
    var selectedSize = ArrayList<Size>()
    var selectedColor = ArrayList<Color>()
    var selectedOffers = ArrayList<Offers>()
    private var minPrice = ""
    private var maxPrice = ""
    private var categoryId: String = ""

    private var isSelectedFilter = false

    override fun onMinMaxPrice(minPrice: String, maxPrice: String) {
        this.minPrice = minPrice
        this.maxPrice = maxPrice
    }

    override fun onItemSelected(position: Int, itemId: String, filterType: String, itemName: String, flag: String) {
        if (flag.equals("Size", true)) {
            val size = Size()
            size.id = itemId
            size.title = itemName
            size.isselected = 1
            selectedSize.add(size)
        } else if (flag.equals("Color", true)) {
            val color = Color()
            color.id = itemId
            color.title = itemName
            color.isselected = 1
            selectedColor.add(color)
        } else if (flag.equals("Offers", true)) {
            val offers = Offers()
            offers.type = filterType
            offers.value = itemId
            offers.title = itemName
            offers.isselected = 1
            selectedOffers.add(offers)
        }
    }

    override fun onItemUnSelected(position: Int, itemId: String, filterType: String, itemName: String, flag: String) {
        if (flag.equals("Size", true)) {
            if (selectedSize.size > 0) {
                for (size in selectedSize) {
                    if (size.title.equals(itemName)) {
                        selectedSize.remove(size)
                        break
                    }
                }
            }
        } else if (flag.equals("Color", true)) {
            if (selectedColor.size > 0) {
                for (color in selectedColor) {
                    if (color.title.equals(itemName)) {
                        selectedColor.remove(color)
                        break
                    }
                }
            }
        } else if (flag.equals("Offers", true)) {
            if (selectedOffers.size > 0) {
                for (offer in selectedOffers) {
                    if (offer.title.equals(itemName)) {
                        selectedOffers.remove(offer)
                        break
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filters)
        ivBack.setOnClickListener { finish() }
        tvTitle.text = "Filters"

        if (intent != null) {
            isSelectedFilter = intent.getBooleanExtra("isSelectedFilterData", false)
        }

        if (isSelectedFilter) {
            selectedCategoryList.addAll(intent.getParcelableArrayListExtra("SelectedCategoryList"))
            selectedSize.addAll(intent.getParcelableArrayListExtra("SelectedSizeList"))
            selectedColor.addAll(intent.getParcelableArrayListExtra("SelectedColorList"))
            selectedOffers.addAll(intent.getParcelableArrayListExtra("SelectedOffersList"))
            minPrice = intent.getStringExtra("MinPrice")
            maxPrice = intent.getStringExtra("MaxPrice")
        }
        categoryId = TextUtils.join(",", selectedCategoryList)
        filterList(categoryId)


        setFilterListAdapter()

        tvReset.setOnClickListener {
            val intent = Intent()
            selectedCategoryList.clear()
            intent.putParcelableArrayListExtra("SelectedCategoryList", selectedCategoryList)
            selectedSize.clear()
            intent.putParcelableArrayListExtra("SelectedSizeList", selectedSize)
            selectedColor.clear()
            intent.putParcelableArrayListExtra("SelectedColorList", selectedColor)
            selectedOffers.clear()
            intent.putParcelableArrayListExtra("SelectedOffersList", selectedOffers)
            intent.putExtra("MinPrice", "")
            intent.putExtra("MaxPrice", "")
            isSelectedFilter = false
            intent.putExtra("IsSelectedFilterData", isSelectedFilter)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }

        btnApply.setOnClickListener {
            val intent = Intent()
            intent.putParcelableArrayListExtra("SelectedCategoryList", selectedCategoryList)
            intent.putParcelableArrayListExtra("SelectedSizeList", selectedSize)
            intent.putParcelableArrayListExtra("SelectedColorList", selectedColor)
            intent.putParcelableArrayListExtra("SelectedOffersList", selectedOffers)
            intent.putExtra("MinPrice", minPrice)
            intent.putExtra("MaxPrice", maxPrice)
            intent.putExtra("IsSelectedFilterData", true)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    private fun setFilterListAdapter() {
        rcvFiltersList.layoutManager = LinearLayoutManager(this)
        rcvFiltersList.setHasFixedSize(true)
        rcvFiltersList.isNestedScrollingEnabled = false
        rcvFiltersList.itemAnimator = DefaultItemAnimator()
        filtersAdapter = FiltersAdapter(this, filterArrayList, this, this)
        rcvFiltersList.adapter = filtersAdapter
    }


    private fun filterList(categoryId: String) {
        if (isNetworkAvailable) {
            val progressBar = showProgressBar()
            val sizeId = TextUtils.join(",", selectedSize)
            val colorId = TextUtils.join(",", selectedColor)
            val offerId = TextUtils.join(",", selectedOffers)
            ApiClient.getAPiAuthHelper(this).filterList("filterList", ApiClient.API_VERSION, PrefUtils.getFromPrefs(this, PrefUtils.USER_ID), "", "shop_item", categoryId, "", sizeId, colorId, offerId, "", "")
                    .enqueue(object : CallbackManager<FilterListResponse>() {
                        override fun onSuccess(`object`: Any?, message: String?) {
                            cancelProgressBar(progressBar)
                            val filterListResponse: FilterListResponse = `object` as FilterListResponse

                            if (filterListResponse.response!!.data!!.size > 0) {
                                btnApply.visibility = View.VISIBLE
                                filterArrayList.clear()
                                filterArrayList.addAll(filterListResponse.response!!.data!!)

                                // filtersAdapter.notifyDataSetChanged()
                                for (data in filterArrayList) {
                                    when (data.name) {
                                        "Category" -> {
                                            for (category in selectedCategoryList) {
                                                for (categoryData in data.category!!) {
                                                    if (category.id.equals(categoryData.id)) {
                                                        categoryData.isselected = 1
                                                    }
                                                }
                                            }
                                        }
                                        "Size" -> {
                                            for (size in selectedSize) {
                                                for (sizeData in data.size!!) {
                                                    if (size.id.equals(sizeData.id)) {
                                                        sizeData.isselected = 1
                                                    }
                                                }
                                            }
                                        }
                                        "Color" -> {
                                            for (color in selectedColor) {
                                                for (colorData in data.color!!) {
                                                    if (color.id.equals(colorData.id)) {
                                                        colorData.isselected = 1
                                                    }
                                                }
                                            }
                                        }
                                        "Offers" -> {
                                            for (offer in selectedOffers) {
                                                for (offerData in data.offers!!) {
                                                    if (offer.title.equals(offerData.title)) {
                                                        offerData.isselected = 1
                                                    }
                                                }
                                            }
                                        }

                                        "Price" -> {
                                            if (!TextUtils.isEmpty(minPrice) && !TextUtils.isEmpty(maxPrice)) {
                                                data.price!!.minimumPrice = minPrice
                                                data.price!!.maximumPrice = maxPrice
                                            }
                                        }
                                    }
                                }
                                setFilterListAdapter()
                            } else {
                                btnApply.visibility = View.GONE
                            }

                        }

                        override fun onError(retroError: RetroError?) {

                        }

                        override fun onFailure(retroError: String?) {

                        }
                    })
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 501 && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                selectedCategoryList.clear()
                selectedCategoryList = data.getParcelableArrayListExtra<Category>("SelectedCategory")
                filterArrayList[0].category!!.clear()
                filterArrayList[0].category!!.addAll(selectedCategoryList)
                categoryId = TextUtils.join(",", selectedCategoryList)
                Log.d("categoryId:", categoryId)
                filterList(categoryId)
            }
        }
    }

    fun showCategory(): String {
        var showCategory: String = ""
        if (selectedCategoryList.size > 0) {
            for (i in selectedCategoryList.indices) {
                if (selectedCategoryList.get(i).isselected == 1) {
                    showCategory = selectedCategoryList.get(i).title!! + ", " + showCategory
                }
            }
        }
        return showCategory
    }

}
