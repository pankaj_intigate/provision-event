package provision.com.app.shop.event_shop_item

import android.content.Intent
import android.os.Bundle
import androidx.core.widget.NestedScrollView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import kotlinx.android.synthetic.main.activity_shop_item_from_registration.*
import kotlinx.android.synthetic.main.add_to_cart_dialog.view.*
import kotlinx.android.synthetic.main.new_header_layout.*
import provision.com.app.R
import provision.com.app.activity.BaseActivity
import provision.com.app.activity.CartActivity
import provision.com.app.apiResponse.cart_count.CartCountEntity
import provision.com.app.apiResponse.event_shop.Data
import provision.com.app.apiResponse.event_shop.ShopItemEventResponse
import provision.com.app.apiResponse.savetocartresponse.SaveToCartResponse
import provision.com.app.utils.PrefUtils
import provision.com.app.utils.network.api.ApiClient
import provision.com.app.utils.network.api.AuthApiHelper
import provision.com.app.utils.network.api.CallbackManager
import provision.com.app.utils.network.api.RetroError
import retrofit2.Call
import retrofit2.Callback

class ShopItemFromRegistration : BaseActivity(), RefreshItemOnChanage {
    override fun addToCart(position: Int, qty: String) {
        val sessionId = PrefUtils.getFromPrefs(this, PrefUtils.SESSION_ID)
        var colorID = ""
        var sizeId = ""
        if (itemList[position].colorAttr!!.isEmpty()) {
            colorID = "1"
        } else {
            for (color in itemList[position].colorAttr!!) {
                if (color.isselected == 1) {
                    colorID = color.colorId!!
                }
            }
        }
        if (itemList[position].itemSize!!.isEmpty()) {
            sizeId = "1"
        } else {
            for (color in itemList[position].itemSize!!) {
                if (color.isselected == 1) {
                    sizeId = color.sizeId!!
                }
            }
        }

        if (TextUtils.isEmpty(colorID)) {
            showToast(this, "Please select color")
            return
        }
        if (TextUtils.isEmpty(sizeId)) {
            showToast(this, "Please select size")
            return
        }
        saveItemCart(itemList[position].iId!!, sessionId, qty, colorID, sizeId, "2", eventId)
    }

    override fun onRefresh(position: Int, colorid: String, sizeId: String, isColorChanged: Boolean, itemPosition: Int) {

        if (isColorChanged) {
            if (itemList[position].colorAttr!![itemPosition].isAvailability == 2) {
                isAvailabilityColor = ""
                isAvailabilitySize = ""
                sizeID = ""
                colorID = colorid
//                Log.d("hello","collor $colorID    size $sizeID")
                getShortData(position, colorid, "2", "", "", itemList[position].optionId)
                return
            }
        } else {
            if (itemList[position].itemSize!![itemPosition].isAvailability == 2) {
                isAvailabilityColor = ""
                isAvailabilitySize = ""
                isAvailabilitySize = ""
                sizeID = sizeId
                colorID = ""
//                Log.d("hello","collor $colorID    size $sizeID")
                getShortData(position, "", "", sizeId, "2", itemList[position].optionId)

                return
            }
        }

//
        if (!colorid.isEmpty()) {

            for (color in itemList[position].colorAttr!!) {
                if (color.isselected == 1) {
                    color.isselected = 0
                    break
                }
            }
            itemList[position].colorAttr!![itemPosition].isselected = 1
            colorID = colorid
            isAvailabilityColor = itemList[position].colorAttr!![itemPosition].isAvailability.toString()
        } else {
            colorID=""
            for (color in itemList[position].colorAttr!!) {
                if (color.isselected == 1) {
                    colorID = color.colorId!!
                    isAvailabilityColor = color.isAvailability.toString()
                    break
                }
            }
        }


        if (!sizeId.isEmpty()) {
            for (itemSize in itemList[position].itemSize!!) {
                if (itemSize.isselected == 1) {
                    itemSize.isselected = 0
                    break
                }
            }
            itemList[position].itemSize!![itemPosition].isselected = 1
            sizeID = sizeId
            isAvailabilitySize = itemList[position].itemSize!![itemPosition].isAvailability.toString()
        } else {
            for (itemSize in itemList[position].itemSize!!) {
                if (itemSize.isselected == 1) {
                    sizeID = itemSize.sizeId!!
                    isAvailabilitySize = itemSize.isAvailability.toString()
                    if (itemSize.isAvailability == 2) {
                        colorID = ""
                        isAvailabilityColor = ""
                    }
                    break
                }
            }
        }
        if (isAvailabilityColor.equals("3")) {
            isAvailabilityColor = ""
        }
        if (isAvailabilitySize.equals("3")) {
            isAvailabilitySize = ""
        }
        getShortData(position, colorID, isAvailabilityColor, sizeId, isAvailabilitySize, itemList[position].optionId)
    }

    var sizeID = ""
    var colorID = ""
    var isAvailabilityColor = ""
    var isAvailabilitySize = ""
    private var isLoading = false
    private var isDataAvailable = true
    private var pageNo = 1
    lateinit var registrationShopAdapter: RegistrationShopAdapter
    lateinit var itemList: ArrayList<Data>
    private lateinit var eventId: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shop_item_from_registration)
        itemList = ArrayList()
        tv_header.visibility = View.VISIBLE
        tv_header.text = "Shop Item"
        back.setOnClickListener { finish() }
        eventId = intent.getStringExtra("eventID")
        cartRL.visibility = View.VISIBLE
        if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM)) && PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM) != "0") {
            cartCountTextView.setVisibility(View.VISIBLE)
            cartCountTextView.setText(PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM))
        } else {
            cartCountTextView.setVisibility(View.GONE)
        }
        cartRL.setOnClickListener {
            val cartIntent = Intent(this, CartActivity::class.java)
            startActivityForResult(cartIntent, 101)

            overridePendingTransition(R.anim.bottom_up, R.anim.stay)
        }
        registrationShopAdapter = RegistrationShopAdapter(this, itemList, this, 1.0, "")

        shopItemRv.layoutManager = LinearLayoutManager(this)
        shopItemRv.adapter = registrationShopAdapter
        shopItemRv.isNestedScrollingEnabled = true
        cartRL.visibility = View.VISIBLE
        nestedScrollview.setOnScrollChangeListener(object : NestedScrollView.OnScrollChangeListener {
            override fun onScrollChange(p0: NestedScrollView?, p1: Int, p2: Int, p3: Int, p4: Int) {
                if (p2 == (p0!!.getChildAt(0).getMeasuredHeight() - p0.getMeasuredHeight())) {
                    Log.i("TAG", "BOTTOM SCROLL")
                    if (!isLoading && isDataAvailable) {
                        Log.i("TAG", "BOTTOM SCROLL  Loading")

                        isLoading = true
                        getShortData()

                        registrationShopAdapter.isLoading = true
                        registrationShopAdapter.notifyDataSetChanged()
                    }
                }
            }

        })
//        shopItemRv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//            val mLayoutManager = shopItemRv.layoutManager as LinearLayoutManager
//            var totalItemCount = mLayoutManager.getItemCount()
//            var pastVisiblesItems = mLayoutManager.findLastVisibleItemPosition()
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                super.onScrolled(recyclerView, dx, dy)
//                pastVisiblesItems = mLayoutManager.findLastVisibleItemPosition()
//                totalItemCount = mLayoutManager.getItemCount()
//                Log.d("findFirstVisible","$pastVisiblesItems   $totalItemCount")
//
//
//                if ((pastVisiblesItems + 5) >= totalItemCount && !isLoading && isDataAvailable) {
//                    getShortData()
//                    isLoading=true
//                    registrationShopAdapter.isLoading = true
//                    registrationShopAdapter.notifyDataSetChanged()
//                }
//
//            }
//        })
        btnDone.setOnClickListener { finish() }
        getShortData()
//        cartItemCount()
    }

    override fun onResume() {
        super.onResume()
        cartItemCount()
        if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(this@ShopItemFromRegistration, PrefUtils.CART_ITEM)) && PrefUtils.getFromPrefs(this@ShopItemFromRegistration, PrefUtils.CART_ITEM) != "0") {
            cartCountTextView.setVisibility(View.VISIBLE)
            cartCountTextView.setText(PrefUtils.getFromPrefs(this@ShopItemFromRegistration, PrefUtils.CART_ITEM))
        } else {
            cartCountTextView.setVisibility(View.GONE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

    }

    private fun getShortData() {
        var progressBar: View? = null
        if (pageNo == 1) {
            progressBar = showProgressBar()
        }
        ApiClient.getAPiAuthHelper(this).shopItemReg("getEventItemData", ApiClient.API_VERSION, "2", "", eventId, "", "", "", "", pageNo, "").enqueue(object : CallbackManager<ShopItemEventResponse>() {
            override fun onSuccess(`object`: Any, message: String) {
                val shortingResponse = `object` as ShopItemEventResponse
                if (shortingResponse.status.equals("1")) {
                    if (pageNo > 1) {
                        registrationShopAdapter.isLoading = false
                        registrationShopAdapter.notifyDataSetChanged()

                    }
                    btnDone.visibility = View.VISIBLE
                    itemList.addAll(shortingResponse.response!!.data!!)
                    registrationShopAdapter.isLoading = false
                    registrationShopAdapter.currencyCode = shortingResponse.currrencyCode
                    registrationShopAdapter.currencyValue = shortingResponse.currencyValue
                    registrationShopAdapter.notifyDataSetChanged()
                    isLoading = false
                    isDataAvailable = shortingResponse.response!!.data!!.size >= 10
                    if (pageNo == 1) {
                        cancelProgressBar(progressBar)
                    }
                    if (shortingResponse.response!!.totalpagecount == itemList.size) {
                        isDataAvailable = false
                    }
                    pageNo += 1
                }


            }

            override fun onError(retroError: RetroError) {
                showToast(this@ShopItemFromRegistration, getString(R.string.response_msg))
                cancelProgressBar(progressBar)
                isLoading = false
                if (pageNo > 1) {
                    registrationShopAdapter.isLoading = false
                    registrationShopAdapter.notifyDataSetChanged()
                }

            }

            override fun onFailure(retroError: String) {
                showToast(this@ShopItemFromRegistration, getString(R.string.response_msg))
                cancelProgressBar(progressBar)
                isLoading = false
                if (pageNo > 1) {
                    registrationShopAdapter.isLoading = false
                    registrationShopAdapter.notifyDataSetChanged()
                }

            }
        })
    }

    private fun getShortData(position: Int, colorId: String, isAvailabilityColor: String, sizeId: String, isAvailabilitySize: String, itemId: String) {
        val progressBar = showProgressBar()
        ApiClient.getAPiAuthHelper(this).shopItemReg("getEventItemData", ApiClient.API_VERSION, "2", itemId, eventId, colorId, isAvailabilityColor, sizeId, isAvailabilitySize, 1, "").enqueue(object : CallbackManager<ShopItemEventResponse>() {
            override fun onSuccess(`object`: Any, message: String) {
                val shortingResponse = `object` as ShopItemEventResponse
                itemList.set(position, shortingResponse.response!!.data!![0])
                registrationShopAdapter.notifyItemChanged(position)
                btnDone.visibility = View.VISIBLE
                cancelProgressBar(progressBar)

            }

            override fun onError(retroError: RetroError) {
                cancelProgressBar(progressBar)
                showToast(this@ShopItemFromRegistration, getString(R.string.response_msg))

            }

            override fun onFailure(retroError: String) {
                cancelProgressBar(progressBar)
                showToast(this@ShopItemFromRegistration, getString(R.string.response_msg))

            }
        })
    }

    private fun saveItemCart(id: String, sessionId: String, quantity: String, colorid: String, tshirt: String, type: String, eventId: String) {
        if (isOnline()) {
            val view = showProgressBar()

            ApiClient.getAPiAuthHelper(this).saveCartItem("savecartItem", id, sessionId, quantity, colorid, tshirt, type, eventId, ApiClient.API_VERSION).enqueue(object : Callback<SaveToCartResponse> {
                override fun onResponse(call: Call<SaveToCartResponse>, response: retrofit2.Response<SaveToCartResponse>) {
                    val res = response.body()
                    if (res!!.response!!.lastItem.equals("addtocart", true)) {
//                        openCartDialog()
                        showToast(this@ShopItemFromRegistration, res.responseMessege)
//                        cartItemCount(sessionId)
                    } else {
                        showToast(this@ShopItemFromRegistration, "" + res.responseMessege)
                    }
                    cancelProgressBar(view)
                    cartItemCount()
                }

                override fun onFailure(call: Call<SaveToCartResponse>, t: Throwable) {
                    showToast(this@ShopItemFromRegistration, getString(R.string.response_msg))
                    cancelProgressBar(view)
                }
            })
        } else {
            showToast(this, getString(R.string.check_internet))
        }
    }

    private fun cartItemCount() {
        val progressBar = showProgressBar()
        val sessionId = PrefUtils.getFromPrefs(this, PrefUtils.SESSION_ID)
        val authApiHelper = ApiClient.getClient(this).create(AuthApiHelper::class.java)
        authApiHelper.getCartCount("GetCartCount", sessionId, PrefUtils.getFromPrefs(this, PrefUtils.USER_ID), ApiClient.API_VERSION).enqueue(object : CallbackManager<CartCountEntity>() {
            override fun onSuccess(`object`: Any, message: String) {
                val baseResponce = `object` as CartCountEntity
                if (baseResponce.cartCountResponse != null) {
                    PrefUtils.saveToPrefs(this@ShopItemFromRegistration, PrefUtils.CART_ITEM, baseResponce.cartCountResponse.cart_count.toString())
                    if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(this@ShopItemFromRegistration, PrefUtils.CART_ITEM)) && PrefUtils.getFromPrefs(this@ShopItemFromRegistration, PrefUtils.CART_ITEM) != "0") {
                        cartCountTextView.setVisibility(View.VISIBLE)
                        cartCountTextView.setText(PrefUtils.getFromPrefs(this@ShopItemFromRegistration, PrefUtils.CART_ITEM))
                    } else {
                        cartCountTextView.setVisibility(View.GONE)
                    }
                }
                cancelProgressBar(progressBar)
            }

            override fun onError(retroError: RetroError) {
                showToast(this@ShopItemFromRegistration, getString(R.string.response_msg))
                cancelProgressBar(progressBar)
            }

            override fun onFailure(retroError: String) {
                showToast(this@ShopItemFromRegistration, getString(R.string.response_msg))
                cancelProgressBar(progressBar)

            }
        })
    }


    private fun openCartDialog() {
        val inflater: LayoutInflater = layoutInflater
        val layout = inflater.inflate(R.layout.add_to_cart_dialog, null)
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setView(layout)
        alertDialog.setCancelable(false)
        val dialog: AlertDialog = alertDialog.create()

        layout.tvContinueShopping.setOnClickListener {
            dialog.dismiss()
        }

        layout.tvViewCart.setOnClickListener {
            val intent = Intent(this, CartActivity::class.java)
            startActivityForResult(intent, 101)
            overridePendingTransition(R.anim.bottom_up, R.anim.stay)
            dialog.dismiss()
        }

        dialog.show()

    }
}
