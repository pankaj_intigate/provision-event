package provision.com.app.shop.checkout

import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_checkout.*
import kotlinx.android.synthetic.main.new_header_layout.*
import provision.com.app.R
import provision.com.app.activity.BaseActivity
import provision.com.app.activity.WebViewActivity
import provision.com.app.apiResponse.BaseResponce
import provision.com.app.apiResponse.cartDataResponse.*
import provision.com.app.apiResponse.cart_count.CartCountEntity
import provision.com.app.apiResponse.check_participate.CheckParticipatEntity
import provision.com.app.apiResponse.myWalletResponse.MyWallet
import provision.com.app.apiResponse.payment_opt.PaymentOptionRes
import provision.com.app.app.AppConstant
import provision.com.app.shop.AddAddressActivity
import provision.com.app.shop.cart.CartParticipateDataAdapter
import provision.com.app.utils.CommonUtils
import provision.com.app.utils.PrefUtils
import provision.com.app.utils.Utils
import provision.com.app.utils.network.api.ApiClient
import provision.com.app.utils.network.api.AuthApiHelper
import provision.com.app.utils.network.api.CallbackManager
import provision.com.app.utils.network.api.RetroError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class CheckoutActivity : BaseActivity() {
    private var cartDataList: ArrayList<EventsData>? = null
    private var shopDataList: ArrayList<DeliveryInfo>? = null
    private var participateData: ArrayList<ParticipateData>? = null
    private var cartParticipateDataAdapter: CartParticipateDataAdapter? = null
    private var isAddress = 0
    private var isAddressRequired = 0
    private var totalItemAmount = ""
    private var couponCodeValue = ""
    private var couponId = ""
    private var mPaymentType = ""
    private var userAddInfo: UserAddInfo? = null
    private lateinit var checkedRadioButton: RadioButton
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checkout)
        cartDataList = ArrayList()
        shopDataList = ArrayList()
        participateData = ArrayList()
        back.setOnClickListener { finish() }
        tv_header.text = getString(R.string.checkout)
        participateDataList.setLayoutManager(LinearLayoutManager(this))
        participateDataList.setNestedScrollingEnabled(false)
        cartParticipateDataAdapter = CartParticipateDataAdapter(this, participateData!!, 1.0, "")
        participateDataList.setAdapter(cartParticipateDataAdapter)
        addAddress.setOnClickListener {
            if (isAddress == 0) {
                startActivity(Intent(this, AddAddressActivity::class.java))
                return@setOnClickListener
            }
            startActivity(Intent(this, AddAddressActivity::class.java).putExtra("data", userAddInfo))
        }
        applyCouponCodeTv.setOnClickListener {
            appliedCoupanCode()
        }
        couponCodeTV.setOnClickListener {
            removeCouponCode()
        }
        paymentOptionRG.setOnCheckedChangeListener { group, checkedId ->
            mPaymentType = checkedId.toString()

        }
        setSpannable("I have read and accepted the race disclaimer. |قد قمت بقراءة لائحة القوانين المنصوصة", 34, 45)
        checkOut.setOnClickListener {
            if (isAddressRequired == 1 && isAddress == 0) {
                showToast(this, getString(R.string.please_add_address))
                return@setOnClickListener
            }
            if (mPaymentType.isEmpty()) {
                showToast(this, getString(R.string.select_payment_type))
                return@setOnClickListener
            }
            if (!checkbox.isChecked && disclaimerLayout.visibility == View.VISIBLE) {
                showToast(this, "Please accept the disclaimer.")
                return@setOnClickListener
            }
            checkParticipate()
        }
    }

    private fun removeCouponCode() {
        val view = showProgressBar()
        ApiClient.getAPiAuthHelper(this).deleteItemCouponFromCart("DeleteItemCouponFromCart",
                ApiClient.API_VERSION, couponCodeValue, PrefUtils.getFromPrefs(this, PrefUtils.SESSION_ID), couponId,
                PrefUtils.getFromPrefs(this, PrefUtils.USER_ID)).enqueue(object : CallbackManager<BaseResponce>() {
            override fun onSuccess(`object`: Any?, message: String?) {
                val baseResponce: BaseResponce = `object` as BaseResponce
                if (baseResponce.status.equals("1")) {
                    getCartData()
                }
                cancelProgressBar(view)
            }

            override fun onError(retroError: RetroError?) {
                cancelProgressBar(view)
            }

            override fun onFailure(retroError: String?) {
                cancelProgressBar(view)
            }

        })
    }

    override fun onResume() {
        super.onResume()
        getCartData()
    }

    private fun getDeliveryOption(grandTotal: String, isWallet: Int,
                                  isCash: Int,
                                  isCashOndelivery: Int) {
        val view = showProgressBar()
        val authApiHelper = ApiClient.getClient(this@CheckoutActivity).create(AuthApiHelper::class.java)
        authApiHelper.getDeliveryOption("getPaymentMethod", ApiClient.API_VERSION, isWallet, isCash, isCashOndelivery, grandTotal).enqueue(object : CallbackManager<PaymentOptionRes>() {
            override fun onSuccess(`object`: Any?, message: String?) {
                val res: PaymentOptionRes = `object` as PaymentOptionRes
                paymentOptionRG.removeAllViews()
                paymentOptionRG.clearCheck()
                paymentOptionRG.invalidate()
                for (i in 0..res.response!!.size - 1) {
                    val lp = RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT)
                    lp.setMargins(0, 12, 0, 12)
                    val rdbtnp = RadioButton(this@CheckoutActivity)
                    rdbtnp.layoutParams = lp
                    rdbtnp.setText(res.response!![i]!!.title)
                    rdbtnp.id = res.response!![i]!!.paymentvalue
                    rdbtnp.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
                    rdbtnp.buttonDrawable = ContextCompat.getDrawable(this@CheckoutActivity, R.drawable.radio_button)
                    rdbtnp.gravity = Gravity.CENTER_VERTICAL
                    rdbtnp.setPadding(29, 0, 0, 0)
                    if (res.response!![i]!!.isSelected == 1) {
                        checkedRadioButton = rdbtnp
                    }
                    paymentOptionRG.addView(rdbtnp)
                    paymentOptionRG.invalidate()
                }
                if (::checkedRadioButton.isInitialized) {
                    checkedRadioButton.isChecked = true
                }
                cancelProgressBar(view)
            }

            override fun onError(retroError: RetroError?) {
                cancelProgressBar(view)
            }

            override fun onFailure(retroError: String?) {
                cancelProgressBar(view)
            }

        })

    }

    private fun cartItemCount() {
        val progressBar = showProgressBar()
        val sessionId = PrefUtils.getFromPrefs(this, PrefUtils.SESSION_ID)
        val authApiHelper = ApiClient.getClient(this).create(AuthApiHelper::class.java)
        authApiHelper.getCartCount("GetCartCount", sessionId, PrefUtils.getFromPrefs(this, PrefUtils.USER_ID), ApiClient.API_VERSION).enqueue(object : CallbackManager<CartCountEntity>() {
            override fun onSuccess(`object`: Any, message: String) {
                val baseResponce = `object` as CartCountEntity
                if (baseResponce.cartCountResponse != null) {
                    PrefUtils.saveToPrefs(this@CheckoutActivity, PrefUtils.CART_ITEM, baseResponce.cartCountResponse.cart_count.toString())
                    if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(this@CheckoutActivity, PrefUtils.CART_ITEM)) && PrefUtils.getFromPrefs(this@CheckoutActivity, PrefUtils.CART_ITEM) != "0") {
                        cartCountTextView.setVisibility(View.VISIBLE)
                        cartCountTextView.setText(PrefUtils.getFromPrefs(this@CheckoutActivity, PrefUtils.CART_ITEM))
                    } else {
                        cartCountTextView.setVisibility(View.GONE)
                    }
                }
                cancelProgressBar(progressBar)
            }

            override fun onError(retroError: RetroError) {
                showToast(this@CheckoutActivity, getString(R.string.response_msg))
                cancelProgressBar(progressBar)
            }

            override fun onFailure(retroError: String) {
                showToast(this@CheckoutActivity, getString(R.string.response_msg))
                cancelProgressBar(progressBar)

            }
        })
    }

    private fun getCartData() {
        if (isOnline()) {

            val sessionId = PrefUtils.getFromPrefs(this, PrefUtils.SESSION_ID)
            //            sessionId = "1539773516668-7677c37b-c0a4-4752-af45-e4f686b5cda5";
            var isLoggedIn = 0
            if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(this, PrefUtils.USER_ID))) {
                isLoggedIn = 1
            }
            val emailId = if (TextUtils.isEmpty(PrefUtils.getFromPrefs(this, PrefUtils.EMAIL_ID))) "" else PrefUtils.getFromPrefs(this, PrefUtils.EMAIL_ID)

            val view = showProgressBar()
            val authApiHelper = ApiClient.getClient(this@CheckoutActivity).create(AuthApiHelper::class.java)
            val call = authApiHelper.cartDataResponse("GetCartDetails", sessionId, "", isLoggedIn, if (TextUtils.isEmpty(PrefUtils.getFromPrefs(this, PrefUtils.COUNTRY_CODE))) "KWD" else PrefUtils.getFromPrefs(this, PrefUtils.COUNTRY_CODE), emailId, "", PrefUtils.getFromPrefs(this, PrefUtils.USER_ID), PrefUtils.getFromPrefs(this, PrefUtils.USER_TYPE), "Cart/paynow", ApiClient.API_VERSION)//KEY_WEB_CALL
            call.enqueue(object : CallbackManager<CartDataResponse>() {
                override fun onSuccess(`object`: Any, message: String) {
                    cancelProgressBar(view)
                    val cartDataResponse = `object` as CartDataResponse
                    if (cartDataResponse == null || cartDataResponse.response == null) {
                        Toast.makeText(this@CheckoutActivity, getString(R.string.response_msg), Toast.LENGTH_SHORT).show()
//                        showViewGroup(RetryListener { this@CartFragment.onRetryCall() })
//                        isError0nCart = true
//                        return
                    }
//                    isError0nCart = false
                    if (cartDataResponse.status == "1") {
                        shopDataList!!.clear()
                        cartDataList!!.clear()
                        participateData!!.clear()
                        participateData!!.addAll(cartDataResponse.response!!.participateData!!)
                        cartDataList!!.addAll(cartDataResponse.response!!.eventsData!!)
                        shopDataList!!.addAll(cartDataResponse.response!!.itemsGrouping!!.deliveryInfo!!)
                        if (cartDataResponse.response!!.totalItemCount!! > 0) {
                            isAddressRequired = 1
                            addAddressLayout.visibility = View.VISIBLE
                        } else {
                            isAddressRequired = 0
                            addAddressLayout.visibility = View.GONE
                        }
                        cartParticipateDataAdapter = CartParticipateDataAdapter(this@CheckoutActivity, participateData!!, cartDataResponse.currencyValue, cartDataResponse.currrencyCode)
                        participateDataList.setAdapter(cartParticipateDataAdapter)
                        if (participateData!!.isEmpty()) {
                            llRegistration.setVisibility(View.GONE)
                            disclaimerLayout.setVisibility(View.GONE)
                        } else {
                            llRegistration.setVisibility(View.VISIBLE)
                            disclaimerLayout.setVisibility(View.VISIBLE)
                        }
                        if (cartDataResponse.response!!.addonsTotal > 0) {
                            addOnTR.visibility = View.VISIBLE
                            addOnsPriceTv.setText(Utils.convertPrice(cartDataResponse.response!!.addonsTotal, cartDataResponse.currencyValue, 3, "") + " " + cartDataResponse.currrencyCode)
                        } else {
                            addOnTR.setVisibility(View.GONE)
                        }

                        if (cartDataResponse.response!!.totalEventDeliveryAmount > 0) {
                            raceKitDeliveryChargeTV.setText(Utils.convertPrice(cartDataResponse.response!!.totalEventDeliveryAmount, cartDataResponse.currencyValue, 3, "") + " " + cartDataResponse.currrencyCode)
                        } else {
                            raceKitDeliveryChargeTr.setVisibility(View.GONE)
                        }

                        if (cartDataResponse.response!!.totalEventDiscount > 0) {
                            dicountChargeTV.setText(Utils.convertPrice(cartDataResponse.response!!.totalEventDiscount, cartDataResponse.currencyValue, 3, "") + " " + cartDataResponse.currrencyCode)
                        } else {
                            dicountChargeTR.setVisibility(View.GONE)
                        }

                        if (cartDataResponse.response!!.totalRefundAmount > 0) {
                            eventRefundableAmountTV.setText(Utils.convertPrice(cartDataResponse.response!!.totalRefundAmount, cartDataResponse.currencyValue, 3, "") + " " + cartDataResponse.currrencyCode)
                        } else {
                            eventRefundableAmountTR.setVisibility(View.GONE)
                        }

                        if (cartDataResponse.response!!.totalItemAmount > 0) {
                            totalItemAmount = cartDataResponse.response!!.totalItemAmount.toString()
                            shopItemCountTV.setText("Item (" + cartDataResponse.response!!.totalItemCount + ")")
                            shopItemPriceTV.setText(Utils.convertPrice(cartDataResponse.response!!.totalItemAmount, cartDataResponse.currencyValue, 3, "") + " " + cartDataResponse.currrencyCode)
                        } else {
                            shopItemDetails.setVisibility(View.GONE)
                        }
                        totalPriceTv.setText(Utils.convertPrice(cartDataResponse.response!!.subtotal!!, cartDataResponse.currencyValue, 3, "") + " " + cartDataResponse.currrencyCode)
                        if (cartDataResponse.response!!.totalDiscount > 0) {
                            totalDeliveryChargeTV.setText(Utils.convertPrice(cartDataResponse.response!!.totalDeliveryAmount, cartDataResponse.currencyValue, 3, "") + " " + cartDataResponse.currrencyCode)
                            totalDeliveryChargeTR.visibility = View.VISIBLE
                        } else {
                            totalDeliveryChargeTR.visibility = View.GONE
                        }

                        if (cartDataResponse.response!!.totalDiscount > 0) {
                            totalDiscountTV.setText(Utils.convertPrice(cartDataResponse.response!!.totalDiscount, cartDataResponse.currencyValue, 3, "") + " " + cartDataResponse.currrencyCode)
                            totalDiscountTR.visibility = View.VISIBLE
                        } else {
                            totalDiscountTR.visibility = View.GONE
                        }
                        grandTotalTextView.setText(Utils.convertPrice(cartDataResponse.response!!.grandTotal!!, cartDataResponse.currencyValue, 3, "") + " " + cartDataResponse.currrencyCode)
                        isAddress = cartDataResponse.response!!.isAddress!!
                        userAddInfo = cartDataResponse.response!!.userAddInfo
                        if (userAddInfo != null) {
                            first_name.text = cartDataResponse.response!!.userAddInfo!!.fullName
                            address.text = "${cartDataResponse.response!!.userAddInfo!!.phoneNo} \n${cartDataResponse.response!!.userAddInfo!!.addressStreet} ${cartDataResponse.response!!.userAddInfo!!.addressApartmentNo} ${cartDataResponse.response!!.userAddInfo!!.block} ${cartDataResponse.response!!.userAddInfo!!.avenue} ${cartDataResponse.response!!.userAddInfo!!.cityName} ${cartDataResponse.response!!.userAddInfo!!.country}"
                        }
                        if (cartDataResponse.response!!.itemDeliveryAmount != null) {
                            deliveryRB.text = "${(cartDataResponse.response!!.itemDeliveryAmount!!.deliveryCharges!! * cartDataResponse.currencyValue)} ${cartDataResponse.currrencyCode} Standard Delivery"
                            deliveryText.text = cartDataResponse.response!!.itemDeliveryAmount!!.deliveryText!!
                        }
                        getDeliveryOption(cartDataResponse.response!!.grandTotal.toString(), cartDataResponse.response!!.isWallet!!, cartDataResponse.response!!.isCash!!, cartDataResponse.response!!.isCashOndelivery!!)
                        shopDeliveryChargesTV.text = "${Utils.convertPrice(cartDataResponse.response!!.shippingcharge, cartDataResponse.currencyValue, 3, "")}  ${cartDataResponse.currrencyCode}"
                        if (isAddressRequired == 1) {
                            if (isAddress == 0) {
                                first_name.visibility = View.GONE
                                address.visibility = View.GONE
                                llDeliveryOpt.visibility = View.GONE
                                addAddress.text = getString(R.string.add_address)
                            } else {
                                addAddress.text = getString(R.string.edit_address)
                                first_name.visibility = View.VISIBLE
                                address.visibility = View.VISIBLE
                                llDeliveryOpt.visibility = View.VISIBLE
                            }
                        } else {
                            llAddress.visibility = View.GONE
                            llDeliveryOpt.visibility = View.GONE
                        }
                        if (cartDataResponse.response!!.isAppliedShopCoupon == 0) {
                            llApplyCouponCode.visibility = View.VISIBLE
                            llRemoveCoupon.visibility = View.GONE
                            shopItemDiscountTR.visibility = View.GONE
                        } else {
                            llRemoveCoupon.visibility = View.VISIBLE
                            shopItemDiscountTR.visibility = View.VISIBLE
                            llApplyCouponCode.visibility = View.GONE
                            couponCodeValue = cartDataResponse.response!!.shopCouponDetail!!.couponCode!!
                            couponId = cartDataResponse.response!!.shopCouponDetail!!.id!!
                            couponCodeTV.text = couponCodeValue
//                            shopItemDiscountPricesTV.text = cartDataResponse.response!!.shopCouponDetail!!.couponValue!!
                            shopItemDiscountPricesTV.text = Utils.convertPrice(cartDataResponse.response!!.shopCouponDetail!!.couponValue!!, cartDataResponse.currencyValue, 3, "") + " " + cartDataResponse.currrencyCode
                        }
                    } else {
//                      cartMainLayout.setVisibility(View.GONE)
//                      emptyCart.rlParent.setVisibility(View.VISIBLE)
//                      emptyCart.btnRegisterEvent.setOnClickListener({ v ->
//                            //                            Intent intent1 = new Intent(this, RegisterEventActivity.class);
//                            //                            intent1.putExtra("eventId", "0");
//                            //                            startActivityForResult(intent1, 101);
//                            this!!.setResult(Activity.RESULT_OK)
//                            this!!.finish()
//                        })
                    }
                }

                override fun onError(retroError: RetroError) {
                    cancelProgressBar(view)
//                    showViewGroup(RetryListener { this@CartFragment.onRetryCall() })
//                    isError0nCart = true
                    //                    PrefUtils.saveToPrefs(this, PrefUtils.CART_ITEM, cartDataList.size() + "");
                    //                  cartMainLayout.setVisibility(View.GONE);
                    //                  emptyCart.rlParent.setVisibility(View.VISIBLE);
                    //                  emptyCart.btnRegisterEvent.setOnClickListener(v -> {
                    ////                            Intent intent1 = new Intent(this, RegisterEventActivity.class);
                    ////                            intent1.putExtra("eventId", "0");
                    ////                            startActivityForResult(intent1, 101);
                    //                        this.setResult(Activity.RESULT_OK);
                    //                        this.finish();
                    //                    });

                }

                override fun onFailure(retroError: String) {
                    cancelProgressBar(view)
//                    showViewGroup(RetryListener { this@CartFragment.onRetryCall() })
//                    isError0nCart = true
                }
            })
        } else {
//            showViewGroup(RetryListener { this@CartFragment.onRetryCall() })
//            isError0nCart = true
            showToast(this@CheckoutActivity, getResources().getString(R.string.check_internet))
        }
    }

    private fun setSpannable(text: String, start: Int, end: Int) {
        val ss = SpannableString(text)
        val clickableSpan: ClickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
                startActivity(Intent(this@CheckoutActivity, WebViewActivity::class.java).putExtra("URL", ApiClient.TERMS_N_CONDITIONS_URL).putExtra("TitleName", "Disclaimer"))
                CommonUtils.onTrackEventClick(AppConstant.DISCLAIMER_EVENT_TOKEN)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
                ds.color = ContextCompat.getColor(this@CheckoutActivity, R.color.colorAccent)
            }
        }
        ss.setSpan(clickableSpan, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        textViewDisclamer.movementMethod = LinkMovementMethod.getInstance()
        textViewDisclamer.setText(ss, TextView.BufferType.SPANNABLE)
    }

    private fun appliedCoupanCode() {
        val couponCode = edtCouponCode.text.toString()
        if (couponCode.isEmpty()) {
            showToast(this, "Enter coupan code")
        }
        val view = showProgressBar()
        ApiClient.getAPiAuthHelper(this).appliedShopCoupon("appliedShopCoupon",
                ApiClient.API_VERSION, couponCode, PrefUtils.getFromPrefs(this, PrefUtils.SESSION_ID), totalItemAmount,
                PrefUtils.getFromPrefs(this, PrefUtils.USER_ID)).enqueue(object : CallbackManager<BaseResponce>() {
            override fun onSuccess(`object`: Any?, message: String?) {
                val baseResponce: BaseResponce = `object` as BaseResponce
                if (baseResponce.status.equals("1")) {
                    getCartData()
                } else {
                    invalidCouponDialog(baseResponce.responseMessege)
                }
                cancelProgressBar(view)
            }

            override fun onError(retroError: RetroError?) {
                cancelProgressBar(view)
            }

            override fun onFailure(retroError: String?) {
                cancelProgressBar(view)
            }

        })

    }

    private fun invalidCouponDialog(message: String) {
        val inflater = layoutInflater
        val alertLayout = inflater.inflate(R.layout.invalid_copon_dialog, null)
        val tvMessage = alertLayout.findViewById<TextView>(R.id.tvMessage)
        val btnOkay = alertLayout.findViewById<TextView>(R.id.btnOkay)
        tvMessage.text = message
        val alert = AlertDialog.Builder(this)
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout)
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false)
        val dialog = alert.create()
        btnOkay.setOnClickListener { view: View? -> dialog.cancel() }
        dialog.show()
    }

    private fun saveBookingData() {
        if (isOnline) {
            val view = showProgressBar()
            val sessionId = PrefUtils.getFromPrefs(this, PrefUtils.SESSION_ID)
            var isLoggedIn = 0
            var userId = PrefUtils.getFromPrefs(this, PrefUtils.USER_ID)
            if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(this, PrefUtils.USER_ID))) {
                isLoggedIn = 1
            } else {
                userId = "0"
            }

            ApiClient.getAPiAuthHelper(this).saveBookingData("SaveBooking_Data", sessionId, userId, 0, mPaymentType, ApiClient.API_VERSION).enqueue(object : CallbackManager<MyWallet?>() {
                override fun onSuccess(`object`: Any, message: String) {
                    val myWallet = `object` as MyWallet
                    if (myWallet.status == "1") {
                        startActivity(Intent(this@CheckoutActivity, WebViewActivity::class.java).putExtra("URL", myWallet.response.paymentUrl).putExtra("TitleName", "").putExtra("isCartReset", true))
                        finish()
                    }
                    cancelProgressBar(view)
                }

                override fun onError(retroError: RetroError) {
                    cancelProgressBar(view)
                }

                override fun onFailure(retroError: String) {
                    cancelProgressBar(view)
                }
            })
        } else {
            showToast(this, resources.getString(R.string.check_internet))
        }
    }

    private fun hitAddWalletService() {
        if (isOnline()) {
            val view = showProgressBar()
            val sessionId = PrefUtils.getFromPrefs(this, PrefUtils.SESSION_ID)
            var isLoggedIn = 0
            var userId = PrefUtils.getFromPrefs(this, PrefUtils.USER_ID)
            if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(this, PrefUtils.USER_ID))) {
                isLoggedIn = 1
            } else {
                userId = "0"
            }

            val currencyCode = PrefUtils.getFromPrefs(this, PrefUtils.COUNTRY_CODE)
            val apiVersion = ApiClient.API_VERSION
            val parms = "$currencyCode/$sessionId/$mPaymentType/$isLoggedIn/$userId/$apiVersion/2"


            ApiClient.getAPiAuthHelper(this).addMoney("getapplogin", ApiClient.API_VERSION).enqueue(object : Callback<MyWallet> {
                override fun onResponse(call: Call<MyWallet>, response: Response<MyWallet>) {
                    if (response.body() == null || response.body()!!.response == null) {
                        showToast(this@CheckoutActivity, getString(R.string.response_msg))
                        return
                    }
                    if (response.body()!!.status == "1") {
                        startActivity(Intent(this@CheckoutActivity, WebViewActivity::class.java).putExtra("URL", response.body()!!.response.paymentUrl + parms).putExtra("TitleName", "").putExtra("isCartReset", true))
                        finish()
                    } else {
                        showToast(this@CheckoutActivity, response.body()!!.responseMessege)
                    }
                    cancelProgressBar(view)
                }

                override fun onFailure(call: Call<MyWallet>, t: Throwable) {
                    cancelProgressBar(view)
                }
            })
        } else {
            showToast(this@CheckoutActivity, getResources().getString(R.string.check_internet))
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (PrefUtils.getFromPrefs(this, PrefUtils.USER_TYPE).equals("5")) {
            PrefUtils.saveToPrefs(this, PrefUtils.USER_ID, "");
            PrefUtils.saveToPrefs(this, PrefUtils.EMAIL_ID, "");
            PrefUtils.saveToPrefs(this, PrefUtils.USER_TYPE, "");
        }

    }


    private fun checkParticipate() {
        if (isOnline) {
            val sessionId = PrefUtils.getFromPrefs(this, PrefUtils.SESSION_ID)
            val view: View = showProgressBar()
            val authApiHelper = ApiClient.getClient(this).create(AuthApiHelper::class.java)
            val call = authApiHelper.checkPaymentProceed("checkParticipate", sessionId, ApiClient.API_VERSION)
            call.enqueue(object : CallbackManager<CheckParticipatEntity?>() {
                override fun onSuccess(`object`: Any, message: String) {
                    val checkParticipatEntity = `object` as CheckParticipatEntity
                    if (checkParticipatEntity == null) {
                        Toast.makeText(this@CheckoutActivity, getString(R.string.response_msg), Toast.LENGTH_SHORT).show()
                        return
                    }
                    if (checkParticipatEntity.checkParticipateResponce.status == 1) { //                        doPayment();
                        saveBookingData()
                    } else {
                        Toast.makeText(this@CheckoutActivity, checkParticipatEntity.checkParticipateResponce.msg, Toast.LENGTH_SHORT).show()
                    }
//                    showToast(this@CheckoutActivity,"api hit check participate")
                    cancelProgressBar(view)
                }

                override fun onError(retroError: RetroError) {
                    cancelProgressBar(view)
                }

                override fun onFailure(retroError: String) {
                    cancelProgressBar(view)
                }
            })
        } else {
            showToast(this@CheckoutActivity, getResources().getString(R.string.check_internet))
        }
    }
}
