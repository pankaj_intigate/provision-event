package provision.com.app.shop.model.filterlistresponse

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class Price() :Parcelable{
    @SerializedName("MaximumPrice")
    @Expose
    var maximumPrice: String = ""
    @SerializedName("MinimumPrice")
    @Expose
    var minimumPrice: String = ""

    constructor(parcel: Parcel) : this() {
        maximumPrice = parcel.readString()!!
        minimumPrice = parcel.readString()!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(maximumPrice)
        parcel.writeString(minimumPrice)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Price> {
        override fun createFromParcel(parcel: Parcel): Price {
            return Price(parcel)
        }

        override fun newArray(size: Int): Array<Price?> {
            return arrayOfNulls(size)
        }
    }
}