package provision.com.app.shop.filter

import android.content.Context
import android.os.SystemClock
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.sub_item_size.view.*
import provision.com.app.R
import provision.com.app.interfaces.OnItemFilterSelectListener
import provision.com.app.shop.model.filterlistresponse.Offers

class OffersAdapter(val context: Context, var offersArrayList: ArrayList<Offers>, var onItemFilterSelectListener: OnItemFilterSelectListener) : RecyclerView.Adapter<OffersAdapter.ViewHolder>() {
    private var mLastClickTime: Long = 0

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.sub_item_size, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return offersArrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.cbSizeText.text = offersArrayList.get(position).title

        holder.itemView.cbSizeText.isChecked = offersArrayList.get(position).isselected == 1

        holder.itemView.cbSizeText.setOnCheckedChangeListener { buttonView, isChecked ->
            if (SystemClock.elapsedRealtime() - mLastClickTime < 300){
                return@setOnCheckedChangeListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()
            if(isChecked){
                offersArrayList.get(position).isselected = 1
                onItemFilterSelectListener.onItemSelected(position,offersArrayList.get(position).value!!,offersArrayList.get(position).type!!,offersArrayList.get(position).title!!,"Offers")
            }else{
                offersArrayList.get(position).isselected = 0
                onItemFilterSelectListener.onItemUnSelected(position,offersArrayList.get(position).value!!,offersArrayList.get(position).type!!,offersArrayList.get(position).title!!,"Offers")
            }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}
}