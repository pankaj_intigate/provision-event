package provision.com.app.shop

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.facebook.appevents.AppEventsConstants
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner
import kotlinx.android.synthetic.main.activity_add_address.*
import kotlinx.android.synthetic.main.new_header_layout.*
import provision.com.app.R
import provision.com.app.activity.BaseActivity
import provision.com.app.apiResponse.BaseResponce
import provision.com.app.apiResponse.address.AddressREssponce
import provision.com.app.apiResponse.address.country.ShopCountry
import provision.com.app.apiResponse.address.country.ShopCountryRes
import provision.com.app.apiResponse.cartDataResponse.UserAddInfo
import provision.com.app.apiResponse.city_response.CityResponse
import provision.com.app.apiResponse.city_response.Response
import provision.com.app.shop.checkout.CheckoutActivity
import provision.com.app.utils.PrefUtils
import provision.com.app.utils.Utils
import provision.com.app.utils.network.api.ApiClient
import provision.com.app.utils.network.api.CallbackManager
import provision.com.app.utils.network.api.RetroError

class AddAddressActivity : BaseActivity() {
    private var cityName: String? = null
    lateinit var cityList: ArrayList<Response>
    var command = "adduseraddress"
    var isAddress = 0
    private var countryId = ""
    private val countryList = ArrayList<ShopCountry>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_address)
        tv_header.text = getString(R.string.add_address)
        back.setOnClickListener { finish() }

        val countryCodeArrayAdapter = ArrayAdapter(this@AddAddressActivity,
                android.R.layout.simple_dropdown_item_1line, countryList)
        spn_country.setAdapter(countryCodeArrayAdapter)
        spn_country.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                countryId = countryList[position].iId
                getCityList(countryId)
                spnCity.setText("")

            }

        })
        getCountry()
        cityList = ArrayList()
        val cityAdapter = ArrayAdapter(this@AddAddressActivity,
                android.R.layout.simple_dropdown_item_1line, cityList)
        spnCity.setAdapter(cityAdapter)
        spnCity.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                cityName = cityList[position].id
            }

        })
        btn_add_address.setOnClickListener {
            addAccountAddress()
        }
        emailEditText.setText(PrefUtils.getFromPrefs(this, PrefUtils.EMAIL_ID))
        first_name.setText(PrefUtils.getFromPrefs(this, PrefUtils.USER_NAME))
        if (intent.hasExtra("data")) {
            val userAddInfo = intent.getParcelableExtra<UserAddInfo>("data")
            spn_country.setText(userAddInfo!!.country)
            countryId = userAddInfo.cId.toString()
            spnCity.setText(userAddInfo.cityName)
            first_name.setText(userAddInfo.fullName)
            blockEdt.setText(userAddInfo.block)
            streetEdt.setText(userAddInfo.addressStreet)
            avenueEdt.setText(userAddInfo.avenue)
            house_appartmentEdt.setText(userAddInfo.addressApartmentNo)
            mobileEditText.setText(userAddInfo.phoneNo)
            emailEditText.setText(userAddInfo.emailAddr)
            command = "addshopaddress"
        }

        if (intent.hasExtra("isMore")) {
            command = "adduseraddress"
            isAddress = 1
//            getAddresss()
        } else {
            command = "addshopaddress"
            getAddresss()
            isAddress = 0

        }
    }

    private fun getAddresss() {
        val progressBar = showProgressBar()
        ApiClient.getAPiAuthHelper(this).getAddressInfo("myAccountAddInfo", ApiClient.API_VERSION, "1", PrefUtils.getFromPrefs(this, PrefUtils.USER_ID)).enqueue(object : CallbackManager<AddressREssponce>() {
            override fun onSuccess(`object`: Any?, message: String?) {
                val addressREssponce: AddressREssponce = `object` as AddressREssponce
                if (addressREssponce.status.equals("1")) {
                    val userAddInfo = addressREssponce.responce
                    spn_country.setText(userAddInfo!!.country)
                    countryId = userAddInfo.cId!!
                    getCityList(countryId)
                    first_name.setText(userAddInfo.fullName)
                    blockEdt.setText(userAddInfo.block)
                    streetEdt.setText(userAddInfo.addressStreet)
                    avenueEdt.setText(userAddInfo.avenue)
                    house_appartmentEdt.setText(userAddInfo.addressApartmentNo)
                    mobileEditText.setText(userAddInfo.phoneNo)
                    if (TextUtils.isEmpty(userAddInfo.c_Id)) {
                        cityName = cityList[0].id
                        spnCity.setText(cityList[0].cityName)
                    } else {
                        cityName = userAddInfo.c_Id
                        spnCity.setText(userAddInfo.cityName)
                    }
                }
                cancelProgressBar(progressBar)
            }

            override fun onError(retroError: RetroError?) {
                cancelProgressBar(progressBar)

            }

            override fun onFailure(retroError: String?) {
                cancelProgressBar(progressBar)

            }
        })
    }

    private fun getCityList(countryId: String) {
        val progressBar = showProgressBar()
        ApiClient.getAPiAuthHelper(this).getCity("citylist", ApiClient.API_VERSION, countryId).enqueue(object : CallbackManager<CityResponse>() {
            override fun onSuccess(`object`: Any?, message: String?) {
                val cityResponse: CityResponse = `object` as CityResponse
                if (cityResponse.status.equals("1")) {
                    cityList.clear()
                    cityList.addAll(cityResponse.response!!)
                    val countryCodeArrayAdapter = ArrayAdapter(this@AddAddressActivity,
                            android.R.layout.simple_dropdown_item_1line, cityList)
                    spnCity.setAdapter(countryCodeArrayAdapter)
//                    spnCity.setText(cityList[0].cityName)
//                    cityName = (cityList[0].cityName)

//                    if (isAddress == 1) {
//
//                        getAddresss()
//                    }
                }
                cancelProgressBar(progressBar)

            }

            override fun onError(retroError: RetroError?) {
                cancelProgressBar(progressBar)

            }

            override fun onFailure(retroError: String?) {
                cancelProgressBar(progressBar)
            }

        })
    }

    private fun getCountry() {
        val str = "getShopCountryList"
        val progressBar = showProgressBar()
        ApiClient.getAPiAuthHelper(this).getShopCountyList(str, ApiClient.API_VERSION, "ShopCountry")
                .enqueue(object : CallbackManager<ShopCountryRes>() {
                    override fun onSuccess(`object`: Any?, message: String?) {
                        if (`object` != null) {
                            val cityResponse: ShopCountryRes = `object` as ShopCountryRes
                            if (cityResponse.getStatus().equals(AppEventsConstants.EVENT_PARAM_VALUE_YES)) {
                                countryList.clear()

                                countryList.addAll(cityResponse.response.shopCountryList)
                                spn_country.setAdapter<ArrayAdapter<ShopCountry>>(ArrayAdapter<ShopCountry>(this@AddAddressActivity, android.R.layout.simple_dropdown_item_1line, countryList))
                                if (isAddress == 1) {
                                    getAddresss()
                                }
                            }
                            cancelProgressBar(progressBar)
                            return
                        }
                    }

                    override fun onError(retroError: RetroError?) {
                    }

                    override fun onFailure(retroError: String?) {
                    }

                })
    }

    private fun addAccountAddress() {
        val firstName = first_name.text.toString()
        if (TextUtils.isEmpty(firstName)) {
            showToast(this, "Please enter name")
            return
        }
        val email = emailEditText.text.toString()
        if (!Utils.isValidEmail(email)) {
            showToast(this, "Please enter valid email")
            return
        }

        if (TextUtils.isEmpty(cityName)) {

            showToast(this, "Please select Governorate")
            return
        }
        val block = blockEdt.text.toString()
        if (TextUtils.isEmpty(block)) {
            showToast(this, "Please enter block")
            return
        }
        val street = streetEdt.text.toString()
        if (TextUtils.isEmpty(street)) {
            showToast(this, "Please enter street")
            return
        }

        val mobileNumber = mobileEditText.text.toString()
        if (TextUtils.isEmpty(mobileNumber)) {
            showToast(this, "Please enter mobile number")
            return
        }
        val houseAppartment = house_appartmentEdt.text.toString()
        if (TextUtils.isEmpty(houseAppartment)) {
            showToast(this, "Please enter House/Apartment")
            return
        }


        val avenue = avenueEdt.text.toString()

        val progressBar = showProgressBar()
        ApiClient.getAPiAuthHelper(this).addAccountAddress(command, PrefUtils.getFromPrefs(this, PrefUtils.SESSION_ID), ApiClient.API_VERSION,
                PrefUtils.getFromPrefs(this, PrefUtils.USER_ID), countryId, firstName, mobileNumber, cityName, street, houseAppartment, avenue, block, email)
                .enqueue(object : CallbackManager<BaseResponce>() {
                    override fun onSuccess(`object`: Any?, message: String?) {
                        cancelProgressBar(progressBar)
                        if (intent.hasExtra("isCheckOut")) {
                            startActivity(Intent(this@AddAddressActivity, CheckoutActivity::class.java))
                        }
                        finish()
                    }

                    override fun onError(retroError: RetroError?) {
                        cancelProgressBar(progressBar)
                    }

                    override fun onFailure(retroError: String?) {
                        cancelProgressBar(progressBar)
                    }

                })
    }
}
