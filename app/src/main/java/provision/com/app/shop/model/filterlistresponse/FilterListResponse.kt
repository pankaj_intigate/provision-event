package provision.com.app.shop.model.filterlistresponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import provision.com.app.apiResponse.BaseResponce


class FilterListResponse : BaseResponce() {
    @SerializedName("response")
    @Expose
    var response: Response? = null
}