package provision.com.app.shop.model.filterlistresponse

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Category() : Parcelable {
    @SerializedName("Id")
    @Expose
    var id: String? = null
    @SerializedName("Title")
    @Expose
    var title: String? = null
    @SerializedName("Isselected")
    @Expose
    var isselected: Int? = 0

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        title = parcel.readString()
        isselected = parcel.readValue(Int::class.java.classLoader) as? Int
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(title)
        parcel.writeValue(isselected)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Category> {
        override fun createFromParcel(parcel: Parcel): Category {
            return Category(parcel)
        }

        override fun newArray(size: Int): Array<Category?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return id!!
    }
}