package provision.com.app.shop.productdetails

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.ShapeDrawable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_product_color_layout.view.*
import provision.com.app.R
import provision.com.app.apiResponse.productdetailsresponse.ColorAttr
import provision.com.app.shop.event_shop_item.RefreshItemOnChanage

class ProductColorAdapter(var context: Context, var colorList: ArrayList<ColorAttr>,var refreshItemOnChanage: RefreshItemOnChanage) : RecyclerView.Adapter<ProductColorAdapter.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.item_product_color_layout, p0, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return colorList.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, p1: Int) {
//        try {
//            val myDrawable1 = ContextCompat.getDrawable(context, R.drawable.product_color_rounded_corner)!!.mutate()
////            myDrawable.setColorFilter(Color.parseColor(colorList.get(p1).colorCode!!.trim()), PorterDuff.Mode.SRC_IN)
//            viewHolder.itemView.ivColor.setImageDrawable(myDrawable1)
//        }catch (e:Exception){
//
//        }


        if (colorList[p1].isselected == 1  ) {
            viewHolder.itemView.ivColor.setImageDrawable(null)
            val myDrawable = ContextCompat.getDrawable(context, R.drawable.size_selected)!!.mutate()
            ( myDrawable as GradientDrawable).setColor(Color.parseColor(colorList.get(p1).colorCode!!.trim()))
            viewHolder.itemView.rlParent.background = myDrawable
            viewHolder.itemView.rlParent.alpha=1f
        } else if (colorList[p1].isselected == 0 && colorList[p1].isAvailability==1 ){
            val myDrawable = ContextCompat.getDrawable(context, R.drawable.size_unselected)!!.mutate()
            ( myDrawable as GradientDrawable).setColor(Color.parseColor(colorList.get(p1).colorCode!!.trim()))
            viewHolder.itemView.rlParent.background = myDrawable
            viewHolder.itemView.ivColor.setImageDrawable(null)
            viewHolder.itemView.rlParent.alpha=1f
//            viewHolder.itemView.rlParent.background = ContextCompat.getDrawable(context, R.drawable.size_unselected)
        } else if ( colorList[p1].isAvailability==2){
            val myDrawable = ContextCompat.getDrawable(context, R.drawable.size_unselected)!!.mutate()
            ( myDrawable as GradientDrawable).setColor(Color.parseColor(colorList.get(p1).colorCode!!.trim()))
            viewHolder.itemView.rlParent.background = myDrawable
            viewHolder.itemView.rlParent.alpha=0.3f
            val myDrawable1 = ContextCompat.getDrawable(context, R.drawable.product_color_rounded_corner)!!.mutate()
//            myDrawable.setColorFilter(Color.parseColor(colorList.get(p1).colorCode!!.trim()), PorterDuff.Mode.SRC_IN)
            viewHolder.itemView.ivColor.setImageDrawable(myDrawable1)
        }else{
            val myDrawable = ContextCompat.getDrawable(context, R.drawable.size_unselected)!!.mutate()
            ( myDrawable as GradientDrawable).setColor(Color.parseColor(colorList.get(p1).colorCode!!.trim()))
            viewHolder.itemView.rlParent.background = myDrawable
            viewHolder.itemView.rlParent.alpha=0.3f
            viewHolder.itemView.ivColor.setImageDrawable(null)
        }
        viewHolder.itemView.ivColor.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
//                if (colorList[p1].isAvailability == 3) {
//                    return
//                }
                refreshItemOnChanage.onRefresh(p1, colorList[p1].colorId.toString(), "",true,p1)
            }

        })
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}
}