package provision.com.app.shop.my_order

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.activity_order_details.*
import kotlinx.android.synthetic.main.new_header_layout.*
import provision.com.app.R
import provision.com.app.activity.BaseActivity
import provision.com.app.apiResponse.my_order_list.my_order_details.OrderDetailsResponse
import provision.com.app.utils.PrefUtils
import provision.com.app.utils.Utils
import provision.com.app.utils.network.api.ApiClient
import provision.com.app.utils.network.api.CallbackManager
import provision.com.app.utils.network.api.RetroError

class OrderDetailsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_details)
        tv_header.text = getString(R.string.order_detils)
        back.setOnClickListener { finish() }
        shopItemRv.setLayoutManager(LinearLayoutManager(this))
        shopItemRv.setNestedScrollingEnabled(false)
        getMyOrder()
    }

    private fun getMyOrder() {
        if (!isNetworkAvailable) {
            showToast(this, getResources().getString(R.string.check_internet))
            return
        }
        val progressBar = showProgressBar()
        ApiClient.getAPiAuthHelper(this).getOrderDerails("getReceipt", ApiClient.API_VERSION,
                intent.getStringExtra("bookingId"), PrefUtils.getFromPrefs(this, PrefUtils.COUNTRY_CODE)).enqueue(object : CallbackManager<OrderDetailsResponse>() {
            override fun onSuccess(`object`: Any?, message: String?) {
                val myOrderListResponse = `object` as OrderDetailsResponse
                if (myOrderListResponse.status.equals("1")) {
                    val adapter = MyOrderDetailsAdapter(this@OrderDetailsActivity, myOrderListResponse.response!!.participateInfo!!, myOrderListResponse.response!!.bookingShopitems!!.deliveryInfo!!, 1.00, "KD", 4)
                    shopItemRv.adapter = adapter
                    tvInvoice.text = myOrderListResponse.response!!.invoiceInfo!!.registrationNo
                    tvDate.text = myOrderListResponse.response!!.invoiceInfo!!.bookingDate
                    tvPaymentMethod.text = myOrderListResponse.response!!.invoiceInfo!!.paymentType
                    tvPaymentStatus.text = myOrderListResponse.response!!.invoiceInfo!!.status
                    tvDeliveryStatus.text = myOrderListResponse.response!!.invoiceInfo!!.deliveryStatus
                    if (myOrderListResponse.response!!.bookingUserAddress != null) {
                        addressTV.text = "${myOrderListResponse.response!!.bookingUserAddress!!.regFullname} ${myOrderListResponse.response!!.bookingUserAddress!!.regPhoneNumber} \n" +
                                "${myOrderListResponse.response!!.bookingUserAddress!!.addressStreet} ${myOrderListResponse.response!!.bookingUserAddress!!.addressApartmentNo} " +
                                " ${myOrderListResponse.response!!.bookingUserAddress!!.block} ${myOrderListResponse.response!!.bookingUserAddress!!.cityName} " +
                                "${myOrderListResponse.response!!.bookingUserAddress!!.countryName} "
                    } else {
                        addressTV.visibility = View.GONE
                    }
                    tvName.text = myOrderListResponse.response!!.invoiceInfo!!.name
                    tvEmail.text = myOrderListResponse.response!!.invoiceInfo!!.email
                    tvMobileNo.text = myOrderListResponse.response!!.invoiceInfo!!.phoneNo
                    tvResults.text = myOrderListResponse.response!!.invoiceInfo!!.r
                    tvStauts.text = myOrderListResponse.response!!.invoiceInfo!!.status

                    val subTotalAmount = myOrderListResponse.response!!.subTotalAmount!!
//                    totalDeliveryChargeTV.text = "${myOrderListResponse.response!!.subTotalAmount!!.totalDeliveryAmount!!} ${myOrderListResponse.currrencyCode}"
//                    totalDiscountTV.text = "${myOrderListResponse.response!!.subTotalAmount!!.totalDiscount!!} ${myOrderListResponse.currrencyCode}"
//                    totalPriceTv.text = "${myOrderListResponse.response!!.subTotalAmount!!.subtotal!!} ${myOrderListResponse.currrencyCode}"
//                    grandTotalTextView.text = "${myOrderListResponse.response!!.subTotalAmount!!.grandTotal!!} ${myOrderListResponse.currrencyCode}"

                    if (subTotalAmount.totalRefundAmount!! <= 0) {
                        totalRefundableAmoutTR.visibility = View.GONE
                    }
                    if (subTotalAmount.totalDeliveryAmount!! <= 0) {
                        totalDeliveryChargeTR.visibility = View.GONE
                    }
                    if (subTotalAmount.totalDiscount!! <= 0) {
                        totalDiscountTR.visibility = View.GONE
                    }
                    eventRefundableAmountTV.text = Utils.convertPrice(subTotalAmount.totalRefundAmount!!, myOrderListResponse.currencyValue, 3, "") + "${myOrderListResponse.currrencyCode}"
                    totalDeliveryChargeTV.text = Utils.convertPrice(subTotalAmount.totalDeliveryAmount!!, myOrderListResponse.currencyValue, 3, "") + "${myOrderListResponse.currrencyCode}"
                    totalDiscountTV.text = Utils.convertPrice(subTotalAmount.totalDiscount!!, myOrderListResponse.currencyValue, 3, "") + "${myOrderListResponse.currrencyCode}"
                    totalPriceTv.text = Utils.convertPrice(subTotalAmount.subtotal!!, myOrderListResponse.currencyValue, 3, "") + "${myOrderListResponse.currrencyCode}"
                    grandTotalTextView.text = Utils.convertPrice(subTotalAmount.grandTotal!!, myOrderListResponse.currencyValue, 3, "") + "${myOrderListResponse.currrencyCode}"
//                    orderList.addAll(myOrderListResponse.response!!.itemsInfoData!!.data!!)
//                    adapter.notifyDataSetChanged()
                }
                cancelProgressBar(progressBar)
            }

            override fun onError(retroError: RetroError?) {
                cancelProgressBar(progressBar)
            }

            override fun onFailure(retroError: String?) {
                cancelProgressBar(progressBar)
            }

        })
    }
}
