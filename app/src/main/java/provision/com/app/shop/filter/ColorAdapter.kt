package provision.com.app.shop.filter

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.drawable.GradientDrawable
import android.os.SystemClock
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.sub_item_color.view.*
import provision.com.app.R
import provision.com.app.interfaces.OnItemFilterSelectListener
import provision.com.app.shop.model.filterlistresponse.Color

class ColorAdapter(val context:Context,var colorList:ArrayList<Color>,var onItemFilterSelectListener: OnItemFilterSelectListener): RecyclerView.Adapter<ColorAdapter.ViewHolder>() {
    private var mLastClickTime: Long = 0

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){}

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ColorAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.sub_item_color, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return colorList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val myDrawable: GradientDrawable = ContextCompat.getDrawable(context, R.drawable.circle_color_shape)!!.mutate() as GradientDrawable
        myDrawable.setColor(android.graphics.Color.parseColor(colorList.get(position).colorCode!!.trim()))
        myDrawable.setStroke(1,android.graphics.Color.DKGRAY)
        holder.itemView.cbColorText.setCompoundDrawablesWithIntrinsicBounds(myDrawable,null,null,null)
        holder.itemView.cbColorText.text = colorList.get(position).title

        holder.itemView.cbColorText.isChecked = colorList.get(position).isselected == 1

        holder.itemView.cbColorText.setOnCheckedChangeListener { buttonView, isChecked ->
            if (SystemClock.elapsedRealtime() - mLastClickTime < 300){
                return@setOnCheckedChangeListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()
            if(isChecked){
                colorList.get(position).isselected = 1
                onItemFilterSelectListener.onItemSelected(position,colorList.get(position).id!!,"", colorList.get(position).title!!,"Color")
            }else{
                colorList.get(position).isselected = 0
                onItemFilterSelectListener.onItemUnSelected(position,colorList.get(position).id!!,"", colorList.get(position).title!!,"Color")
            }
        }
    }
}