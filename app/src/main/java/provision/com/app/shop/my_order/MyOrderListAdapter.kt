package provision.com.app.shop.my_order

import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.my_order_item.view.*
import provision.com.app.R
import provision.com.app.apiResponse.my_order_list.Data
import provision.com.app.utils.Utils

class MyOrderListAdapter(var context: Context, var list: ArrayList<Data>, var currencyValue: Double, var courncyCode: String) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var isLoading = false
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        Log.d("onCreateViewHolder", "$p1")
        if (p1 == 1) {
            val view = LayoutInflater.from(context).inflate(R.layout.load_more_progress, p0, false)
            return LoadMoreViewHolder(view)
        } else {
            val itemView = LayoutInflater.from(context).inflate(R.layout.my_order_item, p0, false)
            return ViewHolderItem(itemView)
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {
        if (1 == list[position].type) {
            return 1
        }
        return 0
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, p1: Int) {
        if (viewHolder is LoadMoreViewHolder) {
            return
        }
        if (viewHolder is ViewHolderItem) {
            val data = list[p1]
            viewHolder.itemView.parentLayout.setOnClickListener {
                context.startActivity(Intent(context, OrderDetailsActivity::class.java).putExtra("bookingId", data.bookingId))
            }
            viewHolder.itemView.tvInvoice.text = "Invoice Id : ${data.invoice}"
            viewHolder.itemView.tvDate.text = data.orderdate
            viewHolder.itemView.tvTotal.text = Utils.convertPrice(currencyValue, data.totalPrice, 3, "").toString() + " " + courncyCode
        }

    }

    class ViewHolderItem(itemView: View) : RecyclerView.ViewHolder(itemView)
    class LoadMoreViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}