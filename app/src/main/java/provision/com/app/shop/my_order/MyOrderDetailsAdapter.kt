package provision.com.app.shop.my_order

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_event_detail.view.*
import kotlinx.android.synthetic.main.shop_item_details.view.*
import provision.com.app.R
import provision.com.app.apiResponse.my_order_list.my_order_details.DeliveryInfo
import provision.com.app.apiResponse.my_order_list.my_order_details.ParticipateInfo
import provision.com.app.utils.Utils

class MyOrderDetailsAdapter(var context: Context, var eventlist: ArrayList<ParticipateInfo>, var shopitems: ArrayList<DeliveryInfo>, var currencyValue: Double, var currencyCode: String, var numberFormat: Int) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val VIEW_TYPE_EVENT_DATA = 111
    private val VIEW_TYPE_SHOP_HEADER = 112
    private val VIEW_TYPE_SHOP_DATA = 113

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_EVENT_DATA) {
            val view = LayoutInflater.from(context).inflate(R.layout.item_event_detail, parent, false)
            return EventViewHolder(view)
        } else if (viewType == VIEW_TYPE_SHOP_HEADER) {
            val view = LayoutInflater.from(context).inflate(R.layout.shop_header, parent, false)
            return ShopHeaderViewHolder(view)
        } else if (viewType == VIEW_TYPE_SHOP_DATA) {
            val view = LayoutInflater.from(context).inflate(R.layout.shop_item_details, parent, false)
            return ShopViewHolder(view)
        }
        val view = LayoutInflater.from(context).inflate(R.layout.item_cart, parent, false)
        return EventViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (viewHolder is EventViewHolder) {
            val eventsDatum = eventlist.get(position)
            viewHolder.itemView.tv_name.setText(eventsDatum.firstName + " " + eventsDatum.firstName)
            viewHolder.itemView.eventCategoryName.setText(eventsDatum.feename)
            viewHolder.itemView.eventNameText.setText(eventsDatum.eventName)
            viewHolder.itemView.all_details_textview.setText("")
            viewHolder.itemView.tv_amount.setText(Utils.convertPrice(currencyValue, eventsDatum.price!!, numberFormat, "") + " " + currencyCode)
            if (eventsDatum.printTshirt > 0) {
                viewHolder.itemView.tshirt_printing_layout.setVisibility(View.VISIBLE)
                viewHolder.itemView.tv_amount_tshirt_printing.setText(Utils.convertPrice(currencyValue, eventsDatum.printTshirt, numberFormat, "") + " " + currencyCode)

            } else {
                viewHolder.itemView.tshirt_printing_layout.setVisibility(View.GONE)
            }
            if (eventsDatum.medalEnv > 0) {
                viewHolder.itemView.medal_engraving_layout.setVisibility(View.VISIBLE)
                viewHolder.itemView.tv_amount_medal_engraving.setText(Utils.convertPrice(currencyValue, eventsDatum.medalEnv, numberFormat, "") + " " + currencyCode)
            } else {
                viewHolder.itemView.medal_engraving_layout.setVisibility(View.GONE)
            }

            if (eventsDatum.age != null && eventsDatum.age != "null" && eventsDatum.age != "0") {
                viewHolder.itemView.all_details_textview.append(eventsDatum.age!! + " yrs , ")
            }
            if (eventsDatum.gender != null && eventsDatum.gender != "null") {
                var gender = ""
                if (eventsDatum.gender!!.equals("male", ignoreCase = true)) {
                    gender = "Male"
                }
                if (eventsDatum.gender!!.equals("female", ignoreCase = true)) {
                    gender = "Female"

                }
                viewHolder.itemView.all_details_textview.append("$gender , ")
            }
            //        if (eventsDatum.gettShirtSize() != null && !eventsDatum.gettShirtSize().equals("null") && !TextUtils.isEmpty(eventsDatum.gettShirtSize()) && !eventsDatum.gettShirtSize().equals("Select")) {
            //            holder.all_details_textview.append("T-shirt Size : " + eventsDatum.gettShirtSize() + " , ");
            //        }
            viewHolder.itemView.all_details_textview.append(eventsDatum.email)
            if (eventsDatum.teamName != null && eventsDatum.teamName != "null") {
                viewHolder.itemView.teamNameText.setText("Team : " + eventsDatum.teamName!!)
                viewHolder.itemView.teamNameText.setVisibility(View.VISIBLE)
            } else {
                viewHolder.itemView.teamNameText.setVisibility(View.GONE)
            }
            //        holder.refundAmountText.setText(Utils.convertPrice(currencyValue, eventsDatum.getRefundableAmount(), numberFormat) + " " + currencyCode);


            return
        }

        if (viewHolder is ShopViewHolder) {
            val deliveryInfo = shopitems.get(position - (eventlist.size + 1))
            viewHolder.itemView.totalPriceTv.text = Utils.convertPrice(deliveryInfo.price!!, currencyValue, 3, "") + " " + currencyCode
            viewHolder.itemView.product_name_tv.setText(deliveryInfo.itemName)
            if ((!!TextUtils.isEmpty(deliveryInfo.size) || !deliveryInfo.size.equals("NA", true)) && (TextUtils.isEmpty(deliveryInfo.colorCode!!) || deliveryInfo.colorCode!!.equals("NA"))) {
                viewHolder.itemView.sellPriceTv.visibility = View.GONE
            } else {
                viewHolder.itemView.sellPriceTv.visibility = View.VISIBLE
                if (!TextUtils.isEmpty(deliveryInfo.size) && !deliveryInfo.size.equals("NA", true)) {
                    viewHolder.itemView.sellPriceTv.setText("Item Size : " + deliveryInfo.size)
                }
                if (TextUtils.isEmpty(deliveryInfo.colorCode!!) || deliveryInfo.colorCode!!.equals("NA")) {
                    viewHolder.itemView.imageColor.visibility = View.GONE
                } else {
                    if (viewHolder.itemView.sellPriceTv.text.toString().length > 0) {
                        viewHolder.itemView.sellPriceTv.text = viewHolder.itemView.sellPriceTv.text.toString() + " |"
                    }
                    viewHolder.itemView.sellPriceTv.text = viewHolder.itemView.sellPriceTv.text.toString() + " Item Color : "
                    val myDrawable = ContextCompat.getDrawable(context, R.drawable.product_color_rounded_corner)!!.mutate()
                    myDrawable.setColorFilter(Color.parseColor(deliveryInfo.colorCode!!.trim()), PorterDuff.Mode.SRC_IN)
                    viewHolder.itemView.imageColor.setImageDrawable(myDrawable)
                    viewHolder.itemView.imageColor.visibility = View.VISIBLE
                }
            }


            viewHolder.itemView.qtyTV.setText("Qty : " + deliveryInfo.quantity)
            Glide.with(context)
                    .load(deliveryInfo.itemImg)
                    .fitCenter()
                    .into(viewHolder.itemView.item_image)

        }

    }

    override fun getItemCount(): Int {
        return if (shopitems.size == 0) {
            eventlist.size
        } else {
            eventlist.size + shopitems.size + 1
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (position < eventlist.size) {
            return VIEW_TYPE_EVENT_DATA
        }
        if (position == eventlist.size) {
            return VIEW_TYPE_SHOP_HEADER
        }
        return if (position > eventlist.size) {
            VIEW_TYPE_SHOP_DATA
        } else VIEW_TYPE_EVENT_DATA
    }

    class EventViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    class ShopViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    class ShopHeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}