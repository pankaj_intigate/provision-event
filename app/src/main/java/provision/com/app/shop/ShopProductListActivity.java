package provision.com.app.shop;

import android.app.Activity;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import android.util.TypedValue;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.adapter.ShopItemProductListAdapter;
import provision.com.app.apiResponse.shop_product_list.ProductDetails;
import provision.com.app.apiResponse.shop_product_list.ProductListResponse;
import provision.com.app.apiResponse.shop_product_list.short_product.ShortingResponse;
import provision.com.app.databinding.ActivityShopProductListBinding;
import provision.com.app.item_decorater.SpacesItemDecoration;
import provision.com.app.shop.filter.FilterFragmentDialog;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;

public class ShopProductListActivity extends BaseActivity implements View.OnClickListener {
    private ActivityShopProductListBinding binding;
    private ShopItemProductListAdapter productListAdapter;
    private ArrayList<ProductDetails> productList;
    private BottomSheetDialog bottomSheetDialog;
    private String shortingBy = "";
    private int pageNo = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_shop_product_list);
        binding.headerLayout.tvHeader.setText(R.string.shop);
        binding.headerLayout.cartRL.setVisibility(View.VISIBLE);
        binding.headerLayout.back.setOnClickListener(v -> {
            finish();
        });
        setAdapter();
        binding.shotRL.setOnClickListener(this);
        getProductList();
        //  getShortData();
        //  showEditDialog();
        binding.rlFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(getApplicationContext(), FiltersActivity.class));
            }
        });
    }

    private void setAdapter() {
        productList = new ArrayList<>();
        int spacingInPixels = getResources().getDimensionPixelOffset(R.dimen.dim_1dp);
        SpacesItemDecoration spacesItemDecoration = new SpacesItemDecoration(spacingInPixels);
        binding.productRecyclerView.addItemDecoration(spacesItemDecoration);
        productListAdapter = new ShopItemProductListAdapter(this, productList, 1.0, "");
        binding.productRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        binding.productRecyclerView.setAdapter(productListAdapter);
    }

    private void getProductList() {
        if (isNetworkAvailable()) {
            View progressBar = showProgressBar();
            ApiClient.getAPiAuthHelper(this).shopProductList("shop_item", "2", "1", shortingBy, "", "", "", "", "", "", "", "", pageNo, "").enqueue(new CallbackManager<ProductListResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    cancelProgressBar(progressBar);
                    ProductListResponse productListResponse = (ProductListResponse) object;
                    productList.addAll(productListResponse.getResponse().getArrayList());
                    productListAdapter.notifyDataSetChanged();

                }

                @Override
                protected void onError(RetroError retroError) {
                }

                @Override
                protected void onFailure(String retroError) {
                }


            });
        }
    }

    private void getShortData() {
        ApiClient.getAPiAuthHelper(this).shortData("sort_filter", "2").enqueue(new CallbackManager<ShortingResponse>() {
            @Override
            protected void onSuccess(Object object, String message) {
                ShortingResponse shortingResponse = (ShortingResponse) object;
                bottomSheetDialog = new BottomSheetDialog(ShopProductListActivity.this);
                bottomSheetDialog.setContentView(R.layout.short_shop_item);
                RadioGroup radioGroup = bottomSheetDialog.findViewById(R.id.shortRG);
                if (shortingResponse.getShortingResponses() != null && shortingResponse.getShortingResponses().size() > 0) {
                    for (int i = 0; i < shortingResponse.getShortingResponses().size(); i++) {

                        RadioButton rdbtn = new RadioButton(ShopProductListActivity.this);
                        rdbtn.setId(i);
                        rdbtn.setText(shortingResponse.getShortingResponses().get(i).getValue());
                        rdbtn.setPadding(0, 0, 0, 8);
                        rdbtn.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getDimension(R.dimen.dim_8sp));
                        radioGroup.addView(rdbtn);
                        if (i == 0) {
                            shortingBy = shortingResponse.getShortingResponses().get(i).getName();
                            rdbtn.setChecked(true);
                        }
                    }
                }
                radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
                    shortingBy = shortingResponse.getShortingResponses().get(checkedId).getName();
                    pageNo = 1;
                    productList.clear();
                    productListAdapter.notifyDataSetChanged();
                    getProductList();
                    bottomSheetDialog.dismiss();
                });
                pageNo = 1;
                getProductList();
            }

            @Override
            protected void onError(RetroError retroError) {

            }

            @Override
            protected void onFailure(String retroError) {

            }
        });
    }

    private void showEditDialog() {
        FragmentManager fm = getSupportFragmentManager();
        FilterFragmentDialog editNameDialogFragment = new FilterFragmentDialog();
        editNameDialogFragment.show(fm, "fragment_edit_name");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.shotRL) {
            if (bottomSheetDialog != null) {
                bottomSheetDialog.show();
            } else {
                getShortData();
            }
        }
    }
}
