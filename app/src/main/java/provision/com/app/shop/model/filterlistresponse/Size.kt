package provision.com.app.shop.model.filterlistresponse

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class Size() : Parcelable{
    @SerializedName("Id")
    @Expose
    var id: String? = null
    @SerializedName("Title")
    @Expose
    var title: String? = null
    @SerializedName("Isselected")
    @Expose
    var isselected: Int = 0

    override fun toString(): String {
        return id!!
    }
    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        title = parcel.readString()
        isselected = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(title)
        parcel.writeInt(isselected)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Size> {
        override fun createFromParcel(parcel: Parcel): Size {
            return Size(parcel)
        }

        override fun newArray(size: Int): Array<Size?> {
            return arrayOfNulls(size)
        }
    }
}