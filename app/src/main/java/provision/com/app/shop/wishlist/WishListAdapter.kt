package provision.com.app.shop.wishlist

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.wishlist_item.view.*
import provision.com.app.R
import provision.com.app.apiResponse.wishlist.Data
import provision.com.app.shop.productdetails.ProductDetailsActivity
import provision.com.app.utils.Utils

class WishListAdapter(var context: Context, var list: ArrayList<Data>, var removeWishlistItemListener: RemoveWishlistItemListener, var currencyValue: Double, var currencyCode: String) : RecyclerView.Adapter<WishListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.wishlist_item, p0, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        Glide.with(context)
                .load(list[position].itemImg)
                .fitCenter()
                .placeholder(R.drawable.placeholder_logo)
                .into(viewHolder.itemView.item_image)
        viewHolder.itemView.product_name_tv.text = list[position].itemTitle
        viewHolder.itemView.sellPriceTv.text = Utils.convertPrice(list[position].salesPrice, currencyValue, 3, "") + " " + currencyCode
        if (list[position].salesPrice == list[position].basePrice) {
            viewHolder.itemView.basePriceTv.visibility = View.GONE
        } else {
            viewHolder.itemView.basePriceTv.text = Utils.convertPrice(list[position].basePrice, currencyValue, 3, "") + " " + currencyCode
            viewHolder.itemView.basePriceTv.visibility = View.VISIBLE
            viewHolder.itemView.basePriceTv.setPaintFlags(viewHolder.itemView.basePriceTv.getPaintFlags() or Paint.STRIKE_THRU_TEXT_FLAG)

        }
        viewHolder.itemView.deleteImg.setOnClickListener {
            removeWishlistItemListener.removeWishlistItem(position)
        }
        if (list[position].discount > 0) {
            viewHolder.itemView.discountPriceTV.text = list[position].discountvalue
            viewHolder.itemView.discountPriceTV.visibility = View.VISIBLE
        } else {
            viewHolder.itemView.discountPriceTV.visibility = View.GONE
        }
        viewHolder.itemView.setOnClickListener {
            (context as Activity).startActivityForResult(Intent(context, ProductDetailsActivity::class.java).putExtra("ProductSubId", list[position].optionId), 101)

        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}