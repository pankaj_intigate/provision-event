package provision.com.app.shop.filter.category


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_categories.*
import kotlinx.android.synthetic.main.filter_header_layout.*
import provision.com.app.R
import provision.com.app.activity.BaseActivity
import provision.com.app.interfaces.OnItemFilterSelectListener
import provision.com.app.shop.model.filterlistresponse.Category

class CategoriesActivity : BaseActivity(), OnItemFilterSelectListener {


    lateinit var categoriesAdapter: CategoriesAdapter
    var categoryList = ArrayList<Category>()
    var selectedCategory = ArrayList<Category>()

    override fun onItemSelected(position: Int, itemId: String, filterType: String, itemName: String, flag: String) {
        val category = Category()
        category.id = itemId
        category.title = itemName
        category.isselected = 1
        selectedCategory.add(category)
    }

    override fun onItemUnSelected(position: Int, itemId: String, filterType: String, itemName: String, flag: String) {
        if (selectedCategory.size > 0) {
            for (category in selectedCategory) {
                if (category.title.equals(itemName)) {
                    selectedCategory.remove(category)
                    break
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_categories)
        tvTitle.text = "Categories"
        tvReset.isAllCaps = false
        tvReset.text = "Done"
        tvReset.setTextColor(ContextCompat.getColor(this, R.color.blue))
        ivBack.setOnClickListener { finish() }

        if (intent != null) {
            categoryList.addAll(intent.getParcelableArrayListExtra("CategoryList"))
        }

        tvReset.setOnClickListener {
            val intent = Intent()
            intent.putParcelableArrayListExtra("SelectedCategory", selectedCategory)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }

        setCategoriesAdapter()
    }

    private fun setCategoriesAdapter() {
        rcvCategoryList.layoutManager = LinearLayoutManager(this)
        rcvCategoryList.setHasFixedSize(true)
        rcvCategoryList.itemAnimator = DefaultItemAnimator()
        categoriesAdapter = CategoriesAdapter(this, categoryList, this)
        rcvCategoryList.adapter = categoriesAdapter
        categoriesAdapter.notifyDataSetChanged()
    }


}
