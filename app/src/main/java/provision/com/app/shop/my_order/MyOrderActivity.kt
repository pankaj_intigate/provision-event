package provision.com.app.shop.my_order

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.activity_my_order.*
import kotlinx.android.synthetic.main.new_header_layout.*
import provision.com.app.R
import provision.com.app.activity.BaseActivity
import provision.com.app.apiResponse.my_order_list.Data
import provision.com.app.apiResponse.my_order_list.MyOrderListResponse
import provision.com.app.utils.PrefUtils
import provision.com.app.utils.network.api.ApiClient
import provision.com.app.utils.network.api.CallbackManager
import provision.com.app.utils.network.api.RetroError

class MyOrderActivity : BaseActivity() {
    private lateinit var adapter: MyOrderListAdapter
    private lateinit var orderList: ArrayList<Data>
    private var pageNo = 1
    private var isLoading = false
    private var isDataAvailable = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_order)
        tv_header.text = getString(R.string.my_order)
        back.setOnClickListener { finish() }
        myOrderList.layoutManager = LinearLayoutManager(this)
        orderList = ArrayList()
        adapter = MyOrderListAdapter(this, orderList, 0.0, "")
        myOrderList.adapter = adapter
        getMyOrder()
        myOrderList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            val mLayoutManager = myOrderList.layoutManager as LinearLayoutManager
            val totalItemCount = mLayoutManager.getItemCount()
            val pastVisiblesItems = mLayoutManager.findLastVisibleItemPosition()
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

                if ((pastVisiblesItems + 5) >= totalItemCount && !isLoading && isDataAvailable) {
                    val data = Data()
                    data.type = 1
                    orderList.add(data)
                    isLoading = true
                    getMyOrder()
                    adapter.isLoading = true
                    adapter.notifyDataSetChanged()
                }
                super.onScrolled(recyclerView, dx, dy)
            }
        })
    }

    private fun getMyOrder() {
        if (!isNetworkAvailable) {
            showToast(this, getResources().getString(R.string.check_internet))
            return
        }
        var progressBar: View? = null
        if (pageNo == 1) {
            progressBar = showProgressBar()
        }

        ApiClient.getAPiAuthHelper(this).getItemUser("getItemUser", ApiClient.API_VERSION,
                PrefUtils.getFromPrefs(this, PrefUtils.USER_ID), pageNo).enqueue(object : CallbackManager<MyOrderListResponse>() {
            override fun onSuccess(`object`: Any?, message: String?) {
                val myOrderListResponse = `object` as MyOrderListResponse
                if (myOrderListResponse.status.equals("1")) {
                    if (myOrderListResponse.response!!.itemsInfoData!!.data!!.size < 10) {
                        isDataAvailable = false
                    } else {
                        isDataAvailable = true
                    }
                    if (pageNo == 1) {
                        orderList.addAll(myOrderListResponse.response!!.itemsInfoData!!.data!!)
                        adapter = MyOrderListAdapter(this@MyOrderActivity, orderList, myOrderListResponse.currencyValue, myOrderListResponse.currrencyCode!!)
                        myOrderList.adapter = adapter
                    } else {
                        orderList.removeAt(orderList.size - 1)
                        orderList.addAll(myOrderListResponse.response!!.itemsInfoData!!.data!!)
                        adapter.isLoading = false
                        adapter.notifyDataSetChanged()

                    }
                }
                if (pageNo == 1) {
                    cancelProgressBar(progressBar)
                }
                if (orderList.size == 0) {
                    tvNoDataFound.visibility = View.VISIBLE
                    myOrderList.visibility = View.GONE
                } else {
                    tvNoDataFound.visibility = View.GONE
                    myOrderList.visibility = View.VISIBLE
                }
                pageNo = pageNo + 1
                isLoading = false
            }

            override fun onError(retroError: RetroError?) {
                if (pageNo == 1) {
                    cancelProgressBar(progressBar)
                } else {
                    orderList.removeAt(orderList.size - 1)
                    adapter.notifyDataSetChanged()
                    adapter.isLoading = false
                }
                isLoading = false
            }

            override fun onFailure(retroError: String?) {
                if (pageNo == 1) {
                    cancelProgressBar(progressBar)
                } else {
                    orderList.removeAt(orderList.size - 1)
                    adapter.notifyDataSetChanged()
                    adapter.isLoading = false
                }
                isLoading = false
            }

        })
    }
}
