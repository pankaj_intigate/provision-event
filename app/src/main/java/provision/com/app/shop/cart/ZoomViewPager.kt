package provision.com.app.shop.cart

import android.content.Context
import androidx.viewpager.widget.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.zoom_view_pager.view.*
import provision.com.app.R
import provision.com.app.apiResponse.event_shop.ItemImage

class ZoomViewPager(var context: Context, var productImageList: ArrayList<ItemImage>) : PagerAdapter() {
    override fun isViewFromObject(p0: View, p1: Any): Boolean {
        return p0 == p1
    }

    override fun getCount(): Int {
        return productImageList.size
    }


    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val layout: ViewGroup = LayoutInflater.from(context).inflate(R.layout.zoom_view_pager, container, false) as ViewGroup
        container.addView(layout)

        Glide.with(context)
                .load(productImageList.get(position).imageUrl)
                .placeholder(R.drawable.placeholder_logo)
                .fitCenter()
                .into(layout.product_image)
        return layout
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}