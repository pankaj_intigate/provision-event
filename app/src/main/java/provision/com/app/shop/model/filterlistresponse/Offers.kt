package provision.com.app.shop.model.filterlistresponse

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Offers() : Parcelable {
    @SerializedName("Id")
    @Expose
    var id: String? = null
    @SerializedName("type")
    @Expose
    var type: String? = null
    @SerializedName("value")
    @Expose
    var value: String? = null
    @SerializedName("Title")
    @Expose
    var title: String? = null
    @SerializedName("Isselected")
    @Expose
    var isselected: Int = 0

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        type = parcel.readString()
        value = parcel.readString()
        title = parcel.readString()
        isselected = parcel.readInt()
    }

    override fun toString(): String {
        return value + "_" + type
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(type)
        parcel.writeString(value)
        parcel.writeString(title)
        parcel.writeInt(isselected)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Offers> {
        override fun createFromParcel(parcel: Parcel): Offers {
            return Offers(parcel)
        }

        override fun newArray(size: Int): Array<Offers?> {
            return arrayOfNulls(size)
        }
    }


}