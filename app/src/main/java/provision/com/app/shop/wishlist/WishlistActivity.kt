package provision.com.app.shop.wishlist

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.activity_wishlist.*
import kotlinx.android.synthetic.main.new_header_layout.*
import provision.com.app.R
import provision.com.app.activity.BaseActivity
import provision.com.app.apiResponse.savetocartresponse.WishListRes.WishListRes
import provision.com.app.apiResponse.wishlist.Data
import provision.com.app.apiResponse.wishlist.WishlistResponse
import provision.com.app.utils.PrefUtils
import provision.com.app.utils.network.api.ApiClient
import provision.com.app.utils.network.api.CallbackManager
import provision.com.app.utils.network.api.RetroError
import retrofit2.Call
import retrofit2.Callback

class WishlistActivity : BaseActivity(), RemoveWishlistItemListener {
    override fun removeWishlistItem(posttion: Int) {
        addToWishList(posttion, PrefUtils.getFromPrefs(this, PrefUtils.USER_ID), list[posttion].iId!!, "CartItem", PrefUtils.getFromPrefs(this, PrefUtils.SESSION_ID))

    }

    lateinit var wishListAdapter: WishListAdapter
    lateinit var list: ArrayList<Data>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wishlist)
        wishListRV.layoutManager = LinearLayoutManager(this)
        list = ArrayList()
        wishListAdapter = WishListAdapter(this, list, this, 1.0, "")
        wishListRV.adapter = wishListAdapter
        getWislist()
        tv_header.text = getString(R.string.wishlist)
        back.setOnClickListener { finish() }
    }

    private fun addToWishList(posttion: Int, userId: String, productId: String, type: String, sessionId: String) {
        if (isOnline()) {
            val view = showProgressBar()

            ApiClient.getAPiAuthHelper(this).addToWishList("adduserWishList", userId, productId, type, sessionId, ApiClient.API_VERSION).enqueue(object : Callback<WishListRes> {
                override fun onResponse(call: Call<WishListRes>, response: retrofit2.Response<WishListRes>) {
                    list.removeAt(posttion)
                    if (list.size == 0) {
                        tvNoDataFound.visibility = View.VISIBLE
                        wishListRV.visibility = View.GONE
                    } else {
                        tvNoDataFound.visibility = View.GONE
                        wishListRV.visibility = View.VISIBLE
                    }
                    wishListAdapter.notifyDataSetChanged()
                    cancelProgressBar(view)
                }

                override fun onFailure(call: Call<WishListRes>, t: Throwable) {
                    cancelProgressBar(view)
                }
            })
        } else {
            showToast(this, getResources().getString(R.string.check_internet))
        }
    }

    private fun getWislist() {
        val progressBar = showProgressBar()
        ApiClient.getAPiAuthHelper(this).getuserWishList("getuserWishList", ApiClient.API_VERSION,
                PrefUtils.getFromPrefs(this, PrefUtils.USER_ID))
                .enqueue(object : CallbackManager<WishlistResponse>() {
                    override fun onSuccess(`object`: Any?, message: String?) {

                        val wishlistResponse: WishlistResponse = `object` as WishlistResponse
                        if (wishlistResponse.status.equals("1")) {
                            list.addAll(wishlistResponse.response!!.data!!)
                            wishListAdapter = WishListAdapter(this@WishlistActivity, list, this@WishlistActivity, wishlistResponse.currencyValue, wishlistResponse.currrencyCode)
                            wishListRV.adapter = wishListAdapter
                            if (list.size == 0) {
                                tvNoDataFound.visibility = View.VISIBLE
                                wishListRV.visibility = View.GONE
                            } else {
                                tvNoDataFound.visibility = View.GONE
                                wishListRV.visibility = View.VISIBLE
                            }
                        }

                        cancelProgressBar(progressBar)
                    }

                    override fun onError(retroError: RetroError?) {
                        cancelProgressBar(progressBar)
                    }

                    override fun onFailure(retroError: String?) {
                        cancelProgressBar(progressBar)
                    }

                })
    }
}
