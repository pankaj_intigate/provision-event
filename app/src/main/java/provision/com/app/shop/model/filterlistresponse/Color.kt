package provision.com.app.shop.model.filterlistresponse

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class Color() : Parcelable{
    @SerializedName("Id")
    @Expose
    var id: String? = null
    @SerializedName("Title")
    @Expose
    var title: String? = null
    @SerializedName("Isselected")
    @Expose
    var isselected: Int = 0
    @SerializedName("ColorCode")
    @Expose
    var colorCode: String? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        title = parcel.readString()
        isselected = parcel.readInt()
        colorCode = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(title)
        parcel.writeInt(isselected)
        parcel.writeString(colorCode)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Color> {
        override fun createFromParcel(parcel: Parcel): Color {
            return Color(parcel)
        }

        override fun newArray(size: Int): Array<Color?> {
            return arrayOfNulls(size)
        }
    }
    override fun toString(): String {
        return id!!
    }
}