package provision.com.app.shop.fragments

import android.widget.ImageView

interface SearchShopListener {
    fun onSearchButtonClick(searchImg:ImageView)

}