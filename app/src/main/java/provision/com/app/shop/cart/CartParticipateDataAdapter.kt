package provision.com.app.shop.cart

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.paricipate_data.view.*
import provision.com.app.R
import provision.com.app.apiResponse.cartDataResponse.ParticipateData
import provision.com.app.utils.Utils

class CartParticipateDataAdapter(var context: Context, var participateData: ArrayList<ParticipateData>, var currencyValue: Double, var currrencyCode: String?) : RecyclerView.Adapter<CartParticipateDataAdapter.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.paricipate_data, p0, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return participateData.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.titleTv.text = participateData[position].eventName + " - Participate (" + participateData[position].participateCount + ")"
        viewHolder.itemView.tv_price.text = Utils.convertPrice(participateData[position].totalFeePrice, currencyValue, 3, currrencyCode)+" "+currrencyCode
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}