package provision.com.app.shop.event_shop_item

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.GradientDrawable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_product_color_layout.view.*
import provision.com.app.R
import provision.com.app.apiResponse.event_shop.ColorAttr

class ColorEventShopAdapter(var context: Context, var list: ArrayList<ColorAttr>, var parrentPosition: Int, var refreshItemOnChanage: RefreshItemOnChanage) : RecyclerView.Adapter<ColorEventShopAdapter.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.item_product_color_layout, p0, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return list.size
    }


    override fun onBindViewHolder(viewHolder: ViewHolder, p1: Int) {
//        val myDrawable = ContextCompat.getDrawable(context, R.drawable.product_color_rounded_corner)!!.mutate()
//        myDrawable.setColorFilter(Color.parseColor(list[p1].colorCode!!.trim()), PorterDuff.Mode.SRC_IN)
//        viewHolder.itemView.ivColor.setImageDrawable(myDrawable)
//        if (list[p1].isselected == 1) {
//            viewHolder.itemView.rlParent.background = ContextCompat.getDrawable(context, R.drawable.size_selected)
//        } else {
//            viewHolder.itemView.rlParent.background = ContextCompat.getDrawable(context, R.drawable.size_unselected)
//        }
//        viewHolder.itemView.ivColor.setOnClickListener(object : View.OnClickListener {
//            override fun onClick(v: View?) {
//                refreshItemOnChanage.onRefresh(parrentPosition, list[p1].colorId.toString(), "",true,p1)
//            }
//
//        })



        if (list[p1].isselected == 1 ) {
            viewHolder.itemView.ivColor.setImageDrawable(null)
            val myDrawable = ContextCompat.getDrawable(context, R.drawable.size_selected)!!.mutate()
            ( myDrawable as GradientDrawable).setColor(Color.parseColor(list.get(p1).colorCode!!.trim()))
            viewHolder.itemView.rlParent.background = myDrawable
            viewHolder.itemView.rlParent.alpha=1f
        } else if (list[p1].isselected == 0 && list[p1].isAvailability==1 ){
            val myDrawable = ContextCompat.getDrawable(context, R.drawable.size_unselected)!!.mutate()
            ( myDrawable as GradientDrawable).setColor(Color.parseColor(list.get(p1).colorCode!!.trim()))
            viewHolder.itemView.rlParent.background = myDrawable
            viewHolder.itemView.ivColor.setImageDrawable(null)
            viewHolder.itemView.rlParent.alpha=1f
//            viewHolder.itemView.rlParent.background = ContextCompat.getDrawable(context, R.drawable.size_unselected)
        } else if ( list[p1].isAvailability==2){
            val myDrawable = ContextCompat.getDrawable(context, R.drawable.size_unselected)!!.mutate()
            ( myDrawable as GradientDrawable).setColor(Color.parseColor(list.get(p1).colorCode!!.trim()))
            viewHolder.itemView.rlParent.background = myDrawable
            viewHolder.itemView.rlParent.alpha=0.3f
            val myDrawable1 = ContextCompat.getDrawable(context, R.drawable.product_color_rounded_corner)!!.mutate()
//            myDrawable.setColorFilter(Color.parseColor(colorList.get(p1).colorCode!!.trim()), PorterDuff.Mode.SRC_IN)
            viewHolder.itemView.ivColor.setImageDrawable(myDrawable1)
        }else{
            val myDrawable = ContextCompat.getDrawable(context, R.drawable.size_unselected)!!.mutate()
            ( myDrawable as GradientDrawable).setColor(Color.parseColor(list.get(p1).colorCode!!.trim()))
            viewHolder.itemView.rlParent.background = myDrawable
            viewHolder.itemView.rlParent.alpha=0.3f
            viewHolder.itemView.ivColor.setImageDrawable(null)
        }
        viewHolder.itemView.ivColor.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
//                if (list[p1].isAvailability == 3) {
//                    return
//                }
                refreshItemOnChanage.onRefresh(parrentPosition, list[p1].colorId.toString(), "",true,p1)
            }

        })
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}