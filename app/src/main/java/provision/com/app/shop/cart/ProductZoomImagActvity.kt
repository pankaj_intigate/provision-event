package provision.com.app.shop.cart

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_product_zoom_imag_actvity.*
import kotlinx.android.synthetic.main.header_layout.*
import provision.com.app.R
import provision.com.app.apiResponse.event_shop.ItemImage

class ProductZoomImagActvity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_zoom_imag_actvity)
        val itemImage = intent.getParcelableArrayListExtra<ItemImage>("dataList")
        val productViewPagerAdapter = ZoomViewPager(this, itemImage)
        vpProduct.adapter = productViewPagerAdapter
        vpProduct.currentItem = intent.getIntExtra("position", 0)
        back.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.close_cart))
        back.visibility = View.VISIBLE

        back.setOnClickListener({ v -> finish() })
        if (itemImage.size > 1) {
            tbProductViewPager.setupWithViewPager(vpProduct, true)
        }
    }
}
