package provision.com.app.binder;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.activity.CartActivity;
import provision.com.app.activity.MyEventsActivity;
import provision.com.app.adapter.MyEventsAdapter;
import provision.com.app.apiResponse.event_list.EventList;
import provision.com.app.apiResponse.event_list.EventListEntity;
import provision.com.app.databinding.ActivityMyEventsBinding;
import provision.com.app.interfaces.RetryListener;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.network.api.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gauravg on 7/10/2018.
 */

public class MyEventsBinder implements RetryListener {
    BaseActivity context;
    ActivityMyEventsBinding binding;
    private List<EventList> dataModelList;
    private MyEventsAdapter adapter;

    public MyEventsBinder(BaseActivity context, ActivityMyEventsBinding binding) {
        this.context = context;
        this.binding = binding;
        context.setRetryListener(this);
        onCreate();
    }

    private void onCreate() {
        dataModelList = new ArrayList<>();
        binding.rcvMyEvents.setLayoutManager(new LinearLayoutManager(context));
        adapter = new MyEventsAdapter(context, dataModelList);
        binding.rcvMyEvents.setAdapter(adapter);
        binding.headerLayout.back.setVisibility(View.VISIBLE);
        binding.headerLayout.imgHeader.setVisibility(View.GONE);
        binding.headerLayout.tvTitle.setText("My Booking");
        binding.headerLayout.cartRL.setVisibility(View.VISIBLE);
        binding.headerLayout.cartRL.setOnClickListener(v -> {
            Intent cartIntent = new Intent(context, CartActivity.class);
            ((Activity) context).startActivityForResult(cartIntent, 101);
            ((Activity) context).overridePendingTransition(R.anim.bottom_up, R.anim.stay);
        });
        binding.headerLayout.back.setOnClickListener(v ->
        {
            ((MyEventsActivity) context).finish();
        });
        getEventList();
    }

    private void getEventList() {
        if (context.isOnline()) {
            View view = (context).showProgressBar();
            ApiClient.getAPiAuthHelper(context).getEventList("getEventsUser", PrefUtils.getFromPrefs(context, PrefUtils.USER_ID),ApiClient.API_VERSION).enqueue(new Callback<EventListEntity>() {
                @Override
                public void onResponse(Call<EventListEntity> call, Response<EventListEntity> response) {
                    ((BaseActivity) context).cancelProgressBar(view);
                    dataModelList.clear();
                    if (response.body() == null || response.body().getResponse() == null) {
                        context.showNetworkLayout();
                        Toast.makeText(context, context.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (response.body().getStatus().equals("1")) {
                        if (response.body().getResponse().getEventsInfo() != null) {
                            dataModelList.addAll(response.body().getResponse().getEventsInfo());
                        }
                        if (dataModelList == null || dataModelList.size() == 0) {
                            binding.tvMyEvents.setVisibility(View.GONE);
                            binding.errorTextView.setVisibility(View.VISIBLE);
                            binding.rcvMyEvents.setVisibility(View.GONE);
                        } else {
                            adapter.notifyDataSetChanged();
                            binding.tvMyEvents.setVisibility(View.GONE);
                            binding.errorTextView.setVisibility(View.GONE);
                            binding.rcvMyEvents.setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                public void onFailure(Call<EventListEntity> call, Throwable t) {
                    ((BaseActivity) context).cancelProgressBar(view);
                    context.showNetworkLayout();
                }
            });
        } else {
            context.showNetworkLayout();
            Toast.makeText(context, context.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRetry() {
        getEventList();
    }
}
