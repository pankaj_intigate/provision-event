package provision.com.app.binder;


import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.activity.MyProfileActivity;
import provision.com.app.apiResponse.getProfileResponse.Country;
import provision.com.app.apiResponse.getProfileResponse.CountryCode;
import provision.com.app.apiResponse.getProfileResponse.Gender;
import provision.com.app.apiResponse.getProfileResponse.Tshirt;
import provision.com.app.apiResponse.my_account.MyAccountDetails;
import provision.com.app.databinding.ActivityMyProfileBinding;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.network.api.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static provision.com.app.app.AppConstant.EDIT_PROFILE_EVENT_TOKEN;
import static provision.com.app.utils.CommonUtils.onTrackEventClick;

/**
 * Created by gauravg on 7/10/2018.
 */

public class MyProfileBinder {
    BaseActivity context;
    ActivityMyProfileBinding binding;
    private List<Gender> genderList;
    private List<CountryCode> countryCodeList;
    private List<Tshirt> tshirtList;
    private List<Country> countryList;
    private String selectGender, selectCountryCode, selectCountry, selectResidence, selectTShirtSize;

    public MyProfileBinder(BaseActivity context, ActivityMyProfileBinding binding) {
        this.context = context;
        this.binding = binding;
        onCreate();
    }

    private void onCreate() {
        // binding.headerLayout.back.setVisibility(View.VISIBLE);
        //  binding.headerLayout.imgHeader.setVisibility(View.VISIBLE);
        binding.back.setOnClickListener(v -> {
            ((MyProfileActivity) context).finish();
        });

        binding.etEmail.setText(PrefUtils.getFromPrefs(context, PrefUtils.EMAIL_ID));
      /*  binding.etName.setText(PrefUtils.getFromPrefs(context, PrefUtils.USER_NAME));
        binding.etMobile.setText(PrefUtils.getFromPrefs(context, PrefUtils.MOBILE_NO));*/
        if (!binding.etEmail.getText().toString().isEmpty()) {
            binding.etEmail.setEnabled(false);
        }
        getAccountDetails(PrefUtils.getFromPrefs(context, PrefUtils.USER_ID));

        binding.btnSave.setOnClickListener(v -> {
            if (binding.etName.getText().toString().isEmpty() || binding.etName.getText().toString().length() < 3) {
                Toast.makeText(context, "Please enter name", Toast.LENGTH_SHORT).show();
                return;
            }
            if (binding.etDob.getText().toString().isEmpty()) {
                Toast.makeText(context, "Please select dob of birth", Toast.LENGTH_SHORT).show();
                return;
            }
            selectGender = binding.spnGender.getText().toString().toLowerCase();
            if (TextUtils.isEmpty(selectGender)) {
                Toast.makeText(context, "Please select gender", Toast.LENGTH_SHORT).show();
                return;
            }
//            if (TextUtils.isEmpty(selectCountryCode)) {
//                Toast.makeText(context, "Please select code", Toast.LENGTH_SHORT).show();
//                return;
//            }

            if (binding.etMobile.getText().toString().isEmpty() || binding.etMobile.getText().toString().length() < 8) {
                Toast.makeText(context, "Please enter 8 to 12 digit mobile number.", Toast.LENGTH_SHORT).show();
                return;
            }
            selectTShirtSize = binding.spnTShirtSize.getText().toString();
            if (TextUtils.isEmpty(selectTShirtSize)) {
                Toast.makeText(context, "Please select T-shirt size", Toast.LENGTH_SHORT).show();
                return;
            }
            selectCountry = binding.spnNationality.getText().toString();
            if (TextUtils.isEmpty(selectCountry)) {
                Toast.makeText(context, "Please select nationality", Toast.LENGTH_SHORT).show();
                return;
            }
            selectResidence = binding.spnResidence.getText().toString();
            if (TextUtils.isEmpty(selectResidence)) {
                Toast.makeText(context, "Please select country of residence", Toast.LENGTH_SHORT).show();
                return;
            }

            if (TextUtils.isEmpty(binding.addressEditText.getText())) {
                Toast.makeText(context, "Please enter Address", Toast.LENGTH_SHORT).show();
                return;
            }

            updateProfile();
        });

    }


    private void updateProfile() {
        View progressView = (context).showProgressBar();
        ApiClient.getAPiAuthHelper(context).updateProfile("updateaccount",
                PrefUtils.getFromPrefs(context, PrefUtils.USER_ID),
                null,
                binding.etName.getText().toString(),
                binding.etMobile.getText().toString(),
                selectTShirtSize, selectCountry,
                binding.etDob.getText().toString(),
                selectGender, selectCountryCode.replace("+", ""), binding.addressEditText.getText().toString(), selectResidence, ApiClient.API_VERSION)
                .enqueue(new Callback<MyAccountDetails>() {
                    @Override
                    public void onResponse(Call<MyAccountDetails> call, Response<MyAccountDetails> response) {
                        ((MyProfileActivity) context).cancelProgressBar(progressView);
                        if (response.body() == null || response.body().getResponse() == null) {
                            Toast.makeText(context, context.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        Toast.makeText(context, response.body().getResponseMessege(), Toast.LENGTH_SHORT).show();
                        if (response.body().getStatus().equals("1")) {
                            PrefUtils.saveToPrefs(context, PrefUtils.USER_NAME, response.body().getResponse().getUserInfoForProfile().getFullName());
                            PrefUtils.saveToPrefs(context, PrefUtils.MOBILE_NO, response.body().getResponse().getUserInfoForProfile().getPhoneNo());
                            ((MyProfileActivity) context).finish();
                            onTrackEventClick(EDIT_PROFILE_EVENT_TOKEN);
                        }
                    }

                    @Override
                    public void onFailure(Call<MyAccountDetails> call, Throwable t) {
                        ((MyProfileActivity) context).cancelProgressBar(progressView);
                    }
                });
    }


    private void getAccountDetails(String userId) {
        View progressView = (context).showProgressBar();
        ApiClient.getAPiAuthHelper(context).getProfileDetails("getProfileContent", userId, ApiClient.API_VERSION).enqueue(new Callback<MyAccountDetails>() {
            @Override
            public void onResponse(Call<MyAccountDetails> call, Response<MyAccountDetails> response) {
                if (response.body().getStatus().equals("1")) {

                    binding.etName.setText(TextUtils.isEmpty(response.body().getResponse().getUserInfo().getFullName()) ? "" : response.body().getResponse().getUserInfo().getFullName());
                    binding.etDob.setText(TextUtils.isEmpty(response.body().getResponse().getUserInfo().getDob()) ? "" : response.body().getResponse().getUserInfo().getDob());
                    binding.etMobile.setText(TextUtils.isEmpty(response.body().getResponse().getUserInfo().getPhoneNo()) ? "" : response.body().getResponse().getUserInfo().getPhoneNo());
                    if (!TextUtils.isEmpty(response.body().getResponse().getUserInfo().getGender())) {
                        if (response.body().getResponse().getUserInfo().getGender().equalsIgnoreCase("male")) {
                            binding.spnGender.setText("Male");
                        }
                        if (response.body().getResponse().getUserInfo().getGender().equalsIgnoreCase("female")) {
                            binding.spnGender.setText("Female");
                        }
                    }

//                    PrefUtils.saveToPrefs(context, PrefUtils.NOTIFICATION_ENABLE, response.body().getResponse().getUserInfo().getIsNotify());
                    selectCountryCode = response.body().getResponse().getUserInfo().getCountry_code();
                    if (!TextUtils.isEmpty(selectCountryCode)) {
                        if (selectCountryCode.equals("+")) {
                            selectCountryCode = "+965";
                            binding.spnCountryCode.setText(selectCountryCode);
                        } else {
                            binding.spnCountryCode.setText(response.body().getResponse().getUserInfo().getCountry_code());
                        }
                    } else {
                        selectCountryCode = "+965";
                        binding.spnCountryCode.setText(selectCountryCode);
                    }
                    if (!TextUtils.isEmpty(response.body().getResponse().getUserInfo().getTsize())) {
                        binding.spnTShirtSize.setText(response.body().getResponse().getUserInfo().getTsize());
                    }
                    if (!TextUtils.isEmpty(response.body().getResponse().getUserInfo().getCountry())) {
                        binding.spnNationality.setText(response.body().getResponse().getUserInfo().getCountry());
                    } else {
                        selectCountry = "Kuwait";
                        binding.spnNationality.setText(selectCountry);
                    }
                    if (!TextUtils.isEmpty(response.body().getResponse().getUserInfo().getResidence())) {
                        binding.spnResidence.setText(response.body().getResponse().getUserInfo().getResidence());
                    } else {
                        selectResidence = "Kuwait";
                        binding.spnResidence.setText(selectResidence);
                    }
                    if (!TextUtils.isEmpty(response.body().getResponse().getUserInfo().getAdrress())) {
                        binding.addressEditText.setText(response.body().getResponse().getUserInfo().getAdrress());

                    }
                    if (!TextUtils.isEmpty(response.body().getResponse().getUserInfo().getUserImg())) {
                        PrefUtils.saveToPrefs(context, PrefUtils.USER_IMAGE, response.body().getResponse().getUserInfo().getUserImg());
                    }

                    //Set Gender'Data.
                    genderList = new ArrayList<>();
                    for (int i = 0; i < response.body().getResponse().getGender().size(); i++) {
                        Gender gender = new Gender();
                        gender.setGender(response.body().getResponse().getGender().get(i).getGender());
                        genderList.add(gender);

                    }
                    ArrayAdapter<Gender> arrayAdapter = new ArrayAdapter<Gender>(context,
                            android.R.layout.simple_dropdown_item_1line, genderList);
                    binding.spnGender.setAdapter(arrayAdapter);

                    binding.spnGender.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            selectGender = genderList.get(position).getGender();
                        }
                    });


                    //Set Country-Code'Data.
                    countryCodeList = new ArrayList<>();
                    CountryCode countryCode;
                    for (int i = 0; i < response.body().getResponse().getCountryCode().size(); i++) {
                        countryCode = new CountryCode();
                        countryCode.setDialCode(response.body().getResponse().getCountryCode().get(i).getDialCode());
                        countryCode.setCode(response.body().getResponse().getCountryCode().get(i).getCode());
                        countryCode.setName(response.body().getResponse().getCountryCode().get(i).getName());
                        countryCode.setCountryCodeEnable(true);
                        countryCodeList.add(countryCode);
                    }
                    ArrayAdapter<CountryCode> countryCodeArrayAdapter = new ArrayAdapter<CountryCode>(context,
                            android.R.layout.simple_dropdown_item_1line, countryCodeList);
                    binding.spnCountryCode.setAdapter(countryCodeArrayAdapter);

                    binding.spnCountryCode.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            selectCountryCode = countryCodeList.get(position).getDialCode();
                        }
                    });

                    //Set T-shirt size'Data.
                    tshirtList = new ArrayList<>();
                    for (int i = 0; i < response.body().getResponse().getTshirts().size(); i++) {
                        Tshirt tshirt = new Tshirt();
                        tshirt.setSize(response.body().getResponse().getTshirts().get(i).getSize());
                        tshirtList.add(tshirt);
                    }
                    ArrayAdapter<Tshirt> tshirtArrayAdapter = new ArrayAdapter<Tshirt>(context,
                            android.R.layout.simple_dropdown_item_1line, tshirtList);
                    binding.spnTShirtSize.setAdapter(tshirtArrayAdapter);

                    binding.spnTShirtSize.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            selectTShirtSize = tshirtList.get(position).getSize();
                        }
                    });

                    //Set Country'Data.
                    countryList = new ArrayList<>();
                    for (int i = 0; i < response.body().getResponse().getCountry().size(); i++) {
                        Country country = new Country();
                        country.setName(response.body().getResponse().getCountry().get(i).getName());
                        countryList.add(country);
                    }
                    ArrayAdapter<Country> countryArrayAdapter = new ArrayAdapter<Country>(context,
                            android.R.layout.simple_dropdown_item_1line, countryList);
                    binding.spnNationality.setAdapter(countryArrayAdapter);

                    binding.spnNationality.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            selectCountry = countryList.get(position).getName();
                          /*  for (int i = 0; i < countryList.size(); i++) {
                                if (countryCodeList.get(i).getName().equalsIgnoreCase(selectCountry)) {
                                    selectCountryCode = countryCodeList.get(i).getDialCode();
                                    binding.spnCountryCode.setText(selectCountryCode);
                                    break;
                                }
                            }*/
                        }
                    });

                    ArrayAdapter<Country> residenceArrayAdapter = new ArrayAdapter<Country>(context,
                            android.R.layout.simple_dropdown_item_1line, countryList);
                    binding.spnResidence.setAdapter(residenceArrayAdapter);

                    binding.spnResidence.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            selectResidence = countryList.get(position).getName();
                            for (int i = 0; i < countryList.size(); i++) {
                                if (countryCodeList.get(i).getName().equalsIgnoreCase(selectResidence)) {
                                    selectCountryCode = countryCodeList.get(i).getDialCode();
                                    binding.spnCountryCode.setText(selectCountryCode);
                                    break;
                                }
                            }
                        }
                    });
                }
                context.cancelProgressBar(progressView);
            }

            @Override
            public void onFailure(Call<MyAccountDetails> call, Throwable t) {
                context.cancelProgressBar(progressView);
            }
        });
    }

}
