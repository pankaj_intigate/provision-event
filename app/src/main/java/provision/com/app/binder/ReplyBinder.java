package provision.com.app.binder;

import android.view.View;
import android.widget.Toast;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.apiResponse.inbox.InboxListEntity;
import provision.com.app.databinding.ActivityReplyBinding;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.network.api.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gauravg on 7/18/2018.
 */

public class ReplyBinder {
    private BaseActivity activity;
    private ActivityReplyBinding binding;
    private String getId;

    public ReplyBinder(BaseActivity activity, ActivityReplyBinding binding, String getId) {
        this.activity = activity;
        this.binding = binding;
        this.getId = getId;


        onCreate();
    }

    private void onCreate() {
        binding.headerLayout.imgHeader.setVisibility(View.VISIBLE);
        binding.headerLayout.tvSend.setVisibility(View.VISIBLE);
        binding.headerLayout.back.setVisibility(View.VISIBLE);

        binding.headerLayout.back.setOnClickListener(v -> {
            activity.finish();
        });

        binding.headerLayout.tvSend.setOnClickListener(v -> {
            if (binding.etReply.getText().toString().isEmpty()) {
                activity.showToast(activity, "Please enter reply");
                return;
            }

            replyService(PrefUtils.getFromPrefs(activity, PrefUtils.USER_ID), getId, binding.etReply.getText().toString().trim());
        });
    }

    private void replyService(String userId, String mailId, String body) {
        View progress = activity.showProgressBar();
        ApiClient.getAPiAuthHelper(activity).replyMail("replySupport", userId, mailId, body,ApiClient.API_VERSION).enqueue(new Callback<InboxListEntity>() {
            @Override
            public void onResponse(Call<InboxListEntity> call, Response<InboxListEntity> response) {
                activity.cancelProgressBar(progress);
                if (response.body() == null || response.body().getResponse() == null) {
                    Toast.makeText(activity, activity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (response.body().getStatus().equals("1")) {
                    activity.showToast(activity, response.body().getResponseMessege());
                    activity.finish();
                }
            }

            @Override
            public void onFailure(Call<InboxListEntity> call, Throwable t) {

            }
        });
    }
}
