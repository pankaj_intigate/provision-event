package provision.com.app.binder;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.activity.ForgotPasswordActivity;
import provision.com.app.activity.ResetPasswordActivity;
import provision.com.app.apiResponse.login.LoginResponce;
import provision.com.app.databinding.ActivityForgotPasswordBinding;
import provision.com.app.utils.Utils;
import provision.com.app.utils.network.api.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static provision.com.app.app.AppConstant.FORGOT_PASSWORD_EVENT_TOKEN;
import static provision.com.app.utils.CommonUtils.onTrackEventClick;

/**
 * Created by gauravg on 7/9/2018.
 */

public class ForgotPasswordBinder {
    Context context;
    ActivityForgotPasswordBinding binding;
    private BaseActivity baseActivity;

    public ForgotPasswordBinder(Context context, ActivityForgotPasswordBinding binding) {
        this.context = context;
        this.binding = binding;
        this.baseActivity = (BaseActivity) context;
        baseActivity.hideKeyboard();
        onCreate();
    }

    private void forgotPassword(String email) {
        View progressView = baseActivity.showProgressBar();
        ApiClient.getAPiAuthHelper(baseActivity).forgotPassword("forgot_password", email,ApiClient.API_VERSION).enqueue(new Callback<LoginResponce>() {


            @Override
            public void onResponse(Call<LoginResponce> call, Response<LoginResponce> response) {
                baseActivity.cancelProgressBar(progressView);
                if (response.body() == null || response.body().getResponce() == null) {
                    Toast.makeText(context, context.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (response.body().getStatus().equals("1")) {

                    baseActivity.startActivity(new Intent(baseActivity, ResetPasswordActivity.class).putExtra("EmailId", binding.emailEditText.getText().toString()));
                    onTrackEventClick(FORGOT_PASSWORD_EVENT_TOKEN);
                }
                Toast.makeText(context, response.body().getResponseMessege(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<LoginResponce> call, Throwable t) {
                baseActivity.cancelProgressBar(progressView);
            }
        });
    }

    private void onCreate() {
        binding.headerLayout.headerDividerLine.setVisibility(View.GONE);
        binding.btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = binding.emailEditText.getText().toString();
                if (email.isEmpty()) {
                    Toast.makeText(context, "Enter email address", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!Utils.isValidEmail(email)) {
                    Toast.makeText(context, "Enter valid email address", Toast.LENGTH_SHORT).show();
                    return;
                }
                forgotPassword(email);
            }
        });
        binding.headerLayout.back.setVisibility(View.VISIBLE);
        binding.headerLayout.back.setOnClickListener(v -> {
            ((ForgotPasswordActivity) context).finish();
        });
    }
}
