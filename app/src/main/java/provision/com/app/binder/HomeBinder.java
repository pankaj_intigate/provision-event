package provision.com.app.binder;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import provision.com.app.R;
import provision.com.app.apiResponse.getCurrencyCodeResponse.CountryCode;
import provision.com.app.apiResponse.getCurrencyCodeResponse.GetCurrencyCodeResponse;
import provision.com.app.databinding.ActivityHomeBinding;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.network.api.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gauravg on 7/5/2018.
 */

public class HomeBinder {
    ActivityHomeBinding binding;
    Context context;
    CountryCode countryCode;
    private List<CountryCode> countryCodeList;
    private String[] countryArray;
    private int categorySelectedPosition = 0;

    public HomeBinder(Context context, ActivityHomeBinding binding) {
        this.context = context;
        this.binding = binding;

        onCreate();
    }

    private void onCreate() {
        binding.headerLayout.imgHeader.setVisibility(View.VISIBLE);
        //  binding.headerLayout.spnCountry.setVisibility(View.VISIBLE);
//        getCountryCode();
        binding.headerLayout.tvCountry.setVisibility(View.GONE);
        binding.headerLayout.searchImg.setVisibility(View.GONE);

      /*  if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(context, PrefUtils.COUNTRY_CODE))) {
            binding.headerLayout.tvCountry.setText(PrefUtils.getFromPrefs(context, PrefUtils.COUNTRY_CODE));
        }*/
        setCountryCode();
        binding.headerLayout.cartRL.setVisibility(View.GONE);
        binding.headerLayout.tvCountry.setOnClickListener(v -> {
            visiblePickerAnimation();
        });

        binding.cancelPicker.setOnClickListener(v -> {
            inVisiblePickerAnimation();
        });

        binding.confirmPicker.setOnClickListener(v -> {
            inVisiblePickerAnimation();
            categorySelectedPosition = binding.numberPicker.getValue();
            String selectCountryCode = countryCodeList.get(binding.numberPicker.getValue()).getCode();
            binding.headerLayout.tvCountry.setText(selectCountryCode);
            PrefUtils.saveToPrefs(context, PrefUtils.COUNTRY_CODE, selectCountryCode);
            // Toast.makeText(context,countryCodeList.get(binding.numberPicker.getValue()).getCode(),Toast.LENGTH_SHORT).show();
        });

    }

    private void getCountryCode() {
        ApiClient.getAPiAuthHelper(context).getCurrencyCode("getCurrencyCode",ApiClient.API_VERSION).enqueue(new Callback<GetCurrencyCodeResponse>() {
            @Override
            public void onResponse(Call<GetCurrencyCodeResponse> call, Response<GetCurrencyCodeResponse> response) {
                if (response.body().getStatus().equals("1")) {
                    if (response.body().getResponse().getCountryCode() != null || response.body().getResponse().getCountryCode().size() > 0) {
                        //Set Country-Code'Data.
                        countryArray = new String[response.body().getResponse().getCountryCode().size()];
                        countryCodeList = new ArrayList<>();
                        for (int i = 0; i < response.body().getResponse().getCountryCode().size(); i++) {
                            countryArray[i] = response.body().getResponse().getCountryCode().get(i).getTitle();
                            countryCode = new CountryCode();
                            countryCode.setCode(response.body().getResponse().getCountryCode().get(i).getCode());
                            countryCode.setTitle(response.body().getResponse().getCountryCode().get(i).getTitle());
                            countryCodeList.add(countryCode);
                        }
                    }

                    binding.numberPicker.setMinValue(0);
                    binding.numberPicker.setMaxValue(countryArray.length - 1);
                    binding.numberPicker.setDisplayedValues(countryArray);
                    binding.numberPicker.setValue(categorySelectedPosition);

                }
            }

            @Override
            public void onFailure(Call<GetCurrencyCodeResponse> call, Throwable t) {

            }
        });
    }

    public void setCountryCode() {
        if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(context, PrefUtils.COUNTRY_CODE))) {
            binding.headerLayout.tvCountry.setText(PrefUtils.getFromPrefs(context, PrefUtils.COUNTRY_CODE));
        } else {
            PrefUtils.saveToPrefs(context, PrefUtils.COUNTRY_CODE, "KWD");
            PrefUtils.saveToPrefs(context, PrefUtils.COUNTRY_NAME, "Kuwait");
            binding.headerLayout.tvCountry.setText(PrefUtils.getFromPrefs(context, PrefUtils.COUNTRY_CODE));
        }
    }


    private void visiblePickerAnimation() {
        Animation bottomUp = AnimationUtils.loadAnimation(context, R.anim.bottom_up);
        binding.selectedLayout.startAnimation(bottomUp);
        binding.selectedLayout.setVisibility(View.VISIBLE);
        binding.bottomView.setVisibility(View.GONE);
    }

    private void inVisiblePickerAnimation() {
        if (binding.selectedLayout.getVisibility() == View.VISIBLE) {
            Animation bottomUp = AnimationUtils.loadAnimation(context,
                    R.anim.bottom_down);
            binding.selectedLayout.startAnimation(bottomUp);
            binding.selectedLayout.setVisibility(View.GONE);
            binding.bottomView.setVisibility(View.VISIBLE);
        }
    }

    private int getIndex(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                return i;
            }
        }

        return 0;
    }


}
