package provision.com.app.binder;

import android.content.Intent;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.activity.ComposeMailActivity;
import provision.com.app.adapter.InboxAdapter;
import provision.com.app.apiResponse.inbox.InboxList;
import provision.com.app.apiResponse.inbox.InboxListEntity;
import provision.com.app.databinding.ActivityInboxBinding;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.network.api.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gauravg on 7/18/2018.
 */

public class InboxBinder {
    private BaseActivity activity;
    private ActivityInboxBinding binding;
    private InboxAdapter mAdapter;
    private List<InboxList> inboxLists = new ArrayList<>();

    public InboxBinder(BaseActivity activity, ActivityInboxBinding binding) {
        this.activity = activity;
        this.binding = binding;

        onCreate();
    }

    private void onCreate() {
        binding.headerLayout.imgHeader.setVisibility(View.GONE);
        binding.headerLayout.back.setVisibility(View.VISIBLE);
        binding.headerLayout.ivCompose.setVisibility(View.VISIBLE);
        binding.headerLayout.tvTitle.setText("Inbox");

        binding.headerLayout.ivCompose.setOnClickListener(v -> {
            activity.startActivity(new Intent(activity, ComposeMailActivity.class));
        });

        binding.headerLayout.back.setOnClickListener(v -> {
            activity.finish();
        });

        binding.rcvInboxList.setLayoutManager(new LinearLayoutManager(activity));
        mAdapter = new InboxAdapter(activity, inboxLists);
        binding.rcvInboxList.setAdapter(mAdapter);
        //getInboxList();
    }

    public void getInboxList() {
        View view = activity.showProgressBar();
        ApiClient.getAPiAuthHelper(activity).getInboxList("getUserInbox", PrefUtils.getFromPrefs(activity, PrefUtils.USER_ID),ApiClient.API_VERSION).enqueue(new Callback<InboxListEntity>() {
            @Override
            public void onResponse(Call<InboxListEntity> call, Response<InboxListEntity> response) {
                if (response.body() == null || response.body().getResponse() == null) {
                    Toast.makeText(activity, activity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (response.body().getStatus().equals("1")) {
                    inboxLists.clear();
                    InboxListEntity inboxListEntity = response.body();
                    if (inboxListEntity.getResponse().getUserInbox().getUserData() != null && inboxListEntity.getResponse().getUserInbox().getUserData().size() > 0) {
                        inboxLists.addAll(inboxListEntity.getResponse().getUserInbox().getUserData());
                        mAdapter.notifyDataSetChanged();
                        binding.tvNoData.setVisibility(View.GONE);
                        binding.rcvInboxList.setVisibility(View.VISIBLE);
                    } else {
                        binding.tvNoData.setVisibility(View.VISIBLE);
                        binding.tvNoData.setText(TextUtils.isEmpty(response.body().getResponseMessege()) ? activity.getResources().getString(R.string.no_data_found) : response.body().getResponseMessege());
                        binding.rcvInboxList.setVisibility(View.GONE);
                    }
                } else {
                    binding.tvNoData.setVisibility(View.VISIBLE);
                    binding.tvNoData.setText(TextUtils.isEmpty(response.body().getResponseMessege()) ? activity.getResources().getString(R.string.no_data_found) : response.body().getResponseMessege());
                    binding.rcvInboxList.setVisibility(View.GONE);
                }
                activity.cancelProgressBar(view);
            }

            @Override
            public void onFailure(Call<InboxListEntity> call, Throwable t) {
                activity.cancelProgressBar(view);
            }
        });
    }
}
