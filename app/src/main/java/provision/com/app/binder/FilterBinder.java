package provision.com.app.binder;

import android.content.Intent;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.FilterActivity;
import provision.com.app.adapter.FilterAdapter;
import provision.com.app.apiResponse.filterContentResponse.Category;
import provision.com.app.apiResponse.filterContentResponse.FilterContentResponse;
import provision.com.app.apiResponse.filterContentResponse.Status;
import provision.com.app.databinding.ActivityFilterBinding;
import provision.com.app.model.FilterModel;
import provision.com.app.utils.AppConstant;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;

/**
 * Created by gauravg on 7/13/2018.
 */

public class FilterBinder implements FilterAdapter.ClickHelper {
    private final int REQUEST_CODE = 103;
    private FilterActivity mActivity;
    private ActivityFilterBinding binding;
    private FilterAdapter adapter;
    private List<FilterModel> filterModelList;
    private FilterContentResponse filterContentResponse;
    private String[] categoryArray, yearArray, monthArray, statusArray;
    private int categorySelectedPosition = -1, yearSelectedPosition = -1, monthSelectedPosition = -1, statusSelectedPosition = -1;
    private int selectedId;
    private String categoryId = "", yearId = "", monthId = "", statusId = "";

    public FilterBinder(FilterActivity context, ActivityFilterBinding binding) {
        this.mActivity = context;
        this.binding = binding;
        yearId = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        onCreate();
    }

    private void onCreate() {
        clickMethod();
        setAdapterFilterItem();
        getFilterContentData();

    }

    private void setCategory() {
        FilterModel filterModel = new FilterModel();
        filterModel.setCategoryId(1);
        filterModel.setCategoryName("Category");
        filterModelList.add(filterModel);

        filterModel = new FilterModel();
        filterModel.setCategoryId(2);
        filterModel.setCategoryName("Select Year");
        filterModelList.add(filterModel);

        filterModel = new FilterModel();
        filterModel.setCategoryId(3);
        filterModel.setCategoryName("Select Month");
        filterModelList.add(filterModel);

//        filterModel = new FilterModel();
//        filterModel.setCategoryId(4);
//        filterModel.setCategoryName("Select Status");
//        filterModelList.add(filterModel);
    }


    private void setNumberPicker() {
        if (selectedId == ClickHelper.CLICK_CATEGORY) {
            getCategoryArray();
            binding.numberPicker.setMinValue(0);
            binding.numberPicker.setMaxValue(categoryArray.length - 1);
            binding.numberPicker.setDisplayedValues(categoryArray);
            binding.numberPicker.setValue(categorySelectedPosition);

        } else if (selectedId == ClickHelper.CLICK_YEAR) {
            getYearArray();
            binding.numberPicker.setMinValue(0);
            binding.numberPicker.setMaxValue(yearArray.length - 1);
            binding.numberPicker.setDisplayedValues(yearArray);
            binding.numberPicker.setValue(yearSelectedPosition);
        } else if (selectedId == ClickHelper.CLICK_MONTH) {
            getMonthArray();
            binding.numberPicker.setMinValue(0);
            binding.numberPicker.setMaxValue(monthArray.length - 1);
            binding.numberPicker.setDisplayedValues(monthArray);
            binding.numberPicker.setValue(monthSelectedPosition);
        }
//        else if (selectedId == ClickHelper.CLICK_STATUS) {
//            getStatusArray();
//            binding.numberPicker.setMinValue(0);
//            binding.numberPicker.setMaxValue(statusArray.length - 1);
//            binding.numberPicker.setDisplayedValues(statusArray);
//            binding.numberPicker.setValue(statusSelectedPosition);
//        }


        visiblePickerAnimation();

        binding.cancelPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inVisiblePickerAnimation();


            }
        });

        binding.confirmPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inVisiblePickerAnimation();

                if (selectedId == ClickHelper.CLICK_CATEGORY) {
                    categorySelectedPosition = binding.numberPicker.getValue();
                    FilterModel filterModel = new FilterModel();
                    filterModel.setCategoryId(1);
                    filterModel.setCategoryName("Category");
                    filterModel.setCategorySelectedValue(filterContentResponse.getResponse().getCategory().get(categorySelectedPosition).getVTitle());
                    filterModelList.set(0, filterModel);
                } else if (selectedId == ClickHelper.CLICK_YEAR) {
                    yearSelectedPosition = binding.numberPicker.getValue();
                    FilterModel filterModel = new FilterModel();
                    filterModel.setCategoryId(2);
                    filterModel.setCategoryName("Select Year");
                    filterModel.setCategorySelectedValue("" + filterContentResponse.getResponse().getYear().get(yearSelectedPosition));
                    filterModelList.set(1, filterModel);

                } else if (selectedId == ClickHelper.CLICK_MONTH) {
                    monthSelectedPosition = binding.numberPicker.getValue();
                    FilterModel filterModel = new FilterModel();
                    filterModel.setCategoryId(3);
                    filterModel.setCategoryName("Select Month");
                    filterModel.setCategorySelectedValue("" + filterContentResponse.getResponse().getMonth().get(monthSelectedPosition));
                    filterModelList.set(2, filterModel);

                }
//                else if (selectedId == ClickHelper.CLICK_STATUS) {
//                    statusSelectedPosition = binding.numberPicker.getValue();
//                    FilterModel filterModel = new FilterModel();
//                    filterModel.setCategoryId(4);
//                    filterModel.setCategoryName("Select Status");
//                    filterModel.setCategorySelectedValue("" + filterContentResponse.getResponse().getStatus().get(statusSelectedPosition).getType());
//                    filterModelList.set(3, filterModel);
//                }
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }


    private void clickMethod() {
        binding.back.setOnClickListener(v ->
        {
            mActivity.finish();
        });

        binding.tvReset.setOnClickListener(v -> {
            inVisiblePickerAnimation();
            categorySelectedPosition = -1;
            yearSelectedPosition = -1;
            monthSelectedPosition = -1;
            statusSelectedPosition = -1;
            setAdapterFilterItem();
            binding.numberPicker.setDisplayedValues(null);
        });


        binding.tvApply.setOnClickListener(v -> {
            if (filterContentResponse != null) {
                List<Category> categories = filterContentResponse.getResponse().getCategory();
                List<Integer> yearList = filterContentResponse.getResponse().getYear();
                List<String> monthList = filterContentResponse.getResponse().getMonth();
                List<Status> statusList = filterContentResponse.getResponse().getStatus();

                if (categorySelectedPosition >= 0) {
                    categoryId = (categories != null && categories.size() > 0) ? categories.get(categorySelectedPosition).getIId() : "";
                }
                if (yearSelectedPosition >= 0) {
                    yearId = (yearList != null && yearList.size() > 0) ? "" + yearList.get(yearSelectedPosition) : "";

                }
                if (monthSelectedPosition >= 0) {
                    monthId = (monthList != null && monthList.size() > 0) ? monthList.get(monthSelectedPosition) : "";
                }
                if (statusSelectedPosition >= 0) {
                    statusId = (statusList != null && statusList.size() > 0) ? statusList.get(statusSelectedPosition).getId() : "";
                }

            }

            Intent intent = new Intent();
            intent.putExtra(AppConstant.KEY_CATEGORY, categoryId);
            intent.putExtra(AppConstant.KEY_YEAR, yearId);
            intent.putExtra(AppConstant.KEY_MONTH, monthId);
            intent.putExtra(AppConstant.KEY_STATUS, statusId);
            mActivity.setResult(REQUEST_CODE, intent);
            mActivity.finish();
        });
    }

    @Override
    public void callBack(int selectedId) {
        this.selectedId = selectedId;
        binding.numberPicker.setDisplayedValues(null);
        setNumberPicker();
    }


    private void getFilterContentData() {
        if (mActivity.isOnline()) {
            View view = mActivity.showProgressBar();
            AuthApiHelper authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper.class);
            retrofit2.Call<FilterContentResponse> call = authApiHelper.getFilterContent("GetEventFilterData",ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<FilterContentResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    mActivity.cancelProgressBar(view);
                    filterContentResponse = (FilterContentResponse) object;
                    if (filterContentResponse == null && filterContentResponse.getResponse() == null) {
                        Toast.makeText(mActivity, mActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                @Override
                protected void onError(RetroError retroError) {
                    mActivity.cancelProgressBar(view);
                }

                @Override
                protected void onFailure(String retroError) {
                    mActivity.cancelProgressBar(view);
                }
            });
        } else {
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
        }
    }


    private void setAdapterFilterItem() {
        filterModelList = new ArrayList<>();
        setCategory();
        binding.rcvFilterList.setLayoutManager(new LinearLayoutManager(mActivity));
        adapter = new FilterAdapter(mActivity, filterModelList, this);
        binding.rcvFilterList.setAdapter(adapter);

    }

    private void getCategoryArray() {
        List<Category> categories = filterContentResponse.getResponse().getCategory();
        categoryArray = new String[categories.size()];
        for (int i = 0; i < categories.size(); i++) {
            categoryArray[i] = categories.get(i).getVTitle();
        }
    }

    private void getYearArray() {
        List<Integer> yearList = filterContentResponse.getResponse().getYear();
        yearArray = new String[yearList.size()];
        for (int i = 0; i < yearList.size(); i++) {
            yearArray[i] = "" + yearList.get(i);
        }
    }

    private void getMonthArray() {
        List<String> monthList = filterContentResponse.getResponse().getMonth();
        monthArray = new String[monthList.size()];
        for (int i = 0; i < monthList.size(); i++) {
            monthArray[i] = "" + monthList.get(i);
        }
    }

    private void getStatusArray() {
        List<Status> statusList = filterContentResponse.getResponse().getStatus();
        statusArray = new String[statusList.size()];
        for (int i = 0; i < statusList.size(); i++) {
            statusArray[i] = "" + statusList.get(i).getType();
        }
    }

    private void visiblePickerAnimation() {
        Animation bottomUp = AnimationUtils.loadAnimation(mActivity,
                R.anim.bottom_up);
        binding.selectedLayout.startAnimation(bottomUp);
        binding.selectedLayout.setVisibility(View.VISIBLE);
    }

    private void inVisiblePickerAnimation() {
        if (binding.selectedLayout.getVisibility() == View.VISIBLE) {
            Animation bottomUp = AnimationUtils.loadAnimation(mActivity,
                    R.anim.bottom_down);
            binding.selectedLayout.startAnimation(bottomUp);
            binding.selectedLayout.setVisibility(View.GONE);
        }
    }

    private static class ClickHelper {
        public static final int CLICK_CATEGORY = 1;
        public static final int CLICK_YEAR = 2;
        public static final int CLICK_MONTH = 3;
        public static final int CLICK_STATUS = 4;
    }


}
