package provision.com.app.binder;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.Scope;

import org.json.JSONException;
import org.json.JSONObject;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.activity.ForgotPasswordActivity;
import provision.com.app.activity.LoginActivity;
import provision.com.app.activity.SignUpActivity;
import provision.com.app.apiResponse.gmailLoginResponse.GmailSignInResponse;
import provision.com.app.apiResponse.login.LoginResponce;
import provision.com.app.apiResponse.login.UserResponce;
import provision.com.app.apiResponse.my_account.MyAccountDetails;
import provision.com.app.apiResponse.signInSignUpResponse.SignInSignUpResponse;
import provision.com.app.databinding.ActivityLoginBinding;
import provision.com.app.shop.AddAddressActivity;
import provision.com.app.shop.checkout.CheckoutActivity;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.Utils;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.RetroError;

import static provision.com.app.app.AppConstant.LOGIN_EVENT_TOKEN;
import static provision.com.app.utils.CommonUtils.onTrackEventClick;

/**
 * Created by gauravg on 7/6/2018.
 */

public class LoginBinder {
    public static int RC_SIGN_IN = 101;
    private ActivityLoginBinding binding;
    private Context context;
    private BaseActivity baseActivity;
    private GoogleSignInClient mGoogleSignInClient;
    private boolean isUserLogin = false;
    private CallbackManager callbackManager;
    boolean isCheckOut;
    int isAddressRequred;

    public LoginBinder(Context context, ActivityLoginBinding binding, boolean isCheckOut, int isAddressRequred) {
        this.isCheckOut = isCheckOut;
        this.binding = binding;
        this.context = context;
        this.isAddressRequred = isAddressRequred;
        baseActivity = (BaseActivity) context;
        baseActivity.hideKeyboard();
        onCreate();
        googleSignInOptions();
        facebookSignIn();
        binding.loginRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.guestRb) {
                    binding.tvOr.setVisibility(View.GONE);
                    binding.llSocialLogin.setVisibility(View.GONE);
                    binding.emailEditText.setVisibility(View.GONE);
                    guestLogin("");
                } else {
                    binding.tvOr.setVisibility(View.VISIBLE);
                    binding.llSocialLogin.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public CallbackManager getCallbackManager() {
        return callbackManager;
    }

    private void facebookSignIn() {
//        binding..setReadPermissions("email");

        binding.loginButton.setReadPermissions("email");
//        LoginManager.getInstance().logInWithReadPermissions((Activity) context, Arrays.asList("public_profile"));
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        setFacebookData(loginResult);
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.d("facebook", exception.getLocalizedMessage());
                    }
                });
        binding.ivFacebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.loginButton.performClick();
            }
        });
    }

    private void setFacebookData(final LoginResult loginResult) {
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        // Application code
                        try {
                            Log.i("facebook", response.toString());
                            JSONObject jsonObject = response.getJSONObject();
                            String emailId = "";
                            if (jsonObject.has("email")) {
                                emailId = response.getJSONObject().getString("email");
                            }

                            String firstName = response.getJSONObject().getString("first_name");
                            String lastName = response.getJSONObject().getString("last_name");
                            String socialKey = response.getJSONObject().getString("id");
                            String userImage = "https://graph.facebook.com/" + socialKey + "/picture?type=large";
                            PrefUtils.saveToPrefs(baseActivity, PrefUtils.USER_IMAGE, userImage);
                            LoginManager.getInstance().logOut();
//                                doSignUp(emailId, firstName + " " + lastName, socialKey);
                            loginWithFacebook(socialKey, emailId, firstName, lastName, "", userImage);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,first_name,last_name,gender");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void googleSignInOptions() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .requestScopes(new Scope(Scopes.PLUS_ME))
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(baseActivity, gso);
    }


    private void guestLogin(String email) {
        View progress = baseActivity.showProgressBar();


        String newToken = PrefUtils.getFromPrefs(context, PrefUtils.TOKEN);
        ApiClient.getAPiAuthHelper(baseActivity).checkoutViaGuest("checkout_via_guest", ApiClient.API_VERSION, PrefUtils.getFromPrefs(context, PrefUtils.SESSION_ID),
                email, "android", "", PrefUtils.getFromPrefs(context, PrefUtils.COUNTRY_CODE), newToken).enqueue(new provision.com.app.utils.network.api.CallbackManager<LoginResponce>() {
            @Override
            protected void onSuccess(Object object, String message) {
                baseActivity.cancelProgressBar(progress);

                LoginResponce loginResponce = (LoginResponce) object;
                if (loginResponce == null || loginResponce.getResponce() == null) {
                    Toast.makeText(context, context.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (loginResponce.getStatus().equals("1")) {
                    UserResponce userResponce = loginResponce.getResponce();
                    PrefUtils.saveToPrefs(baseActivity, PrefUtils.USER_ID, userResponce.getId());
                    PrefUtils.saveToPrefs(baseActivity, PrefUtils.EMAIL_ID, userResponce.getEmail());
                    PrefUtils.saveToPrefs(baseActivity, PrefUtils.USER_TYPE, userResponce.getUserType());
                    if (isCheckOut) {
                        if (isAddressRequred > 0) {
                            baseActivity.startActivity(new Intent(baseActivity, AddAddressActivity.class).putExtra("isCheckOut", true));
                            baseActivity.finish();
                            onTrackEventClick(LOGIN_EVENT_TOKEN);
                            return;
                        }
                        baseActivity.startActivity(new Intent(baseActivity, CheckoutActivity.class));
                        baseActivity.finish();
                        onTrackEventClick(LOGIN_EVENT_TOKEN);
                    }
                    Intent backIntent = new Intent();
                    baseActivity.setResult(Activity.RESULT_OK, backIntent);
                    baseActivity.finish();
                    onTrackEventClick(LOGIN_EVENT_TOKEN);
                }
            }

            @Override
            protected void onError(RetroError retroError) {
                baseActivity.cancelProgressBar(progress);
            }

            @Override
            protected void onFailure(String retroError) {
                baseActivity.cancelProgressBar(progress);
            }
        });

    }

    private void checkSignInSignUp(String email) {
        View progress = baseActivity.showProgressBar();
        ApiClient.getAPiAuthHelper(baseActivity).getSignInSignUpResponse("check_signin_signup", email, ApiClient.API_VERSION).enqueue(new provision.com.app.utils.network.api.CallbackManager<SignInSignUpResponse>() {
            @Override
            protected void onSuccess(Object object, String message) {
                baseActivity.cancelProgressBar(progress);
                SignInSignUpResponse signInSignUpResponse = (SignInSignUpResponse) object;
                if (signInSignUpResponse == null || signInSignUpResponse.getResponse() == null) {
                    Toast.makeText(baseActivity, baseActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (signInSignUpResponse.getStatus().equals("1")) {
                    if (signInSignUpResponse.getResponse().getCmd().equalsIgnoreCase("singin")) {
                        isUserLogin = true;
                        binding.rlPassword.setVisibility(View.VISIBLE);
                    } else {
                        baseActivity.startActivityForResult(new Intent(baseActivity, SignUpActivity.class).putExtra("email", email), 555);
                    }
                } else {
                    baseActivity.showToast(baseActivity, signInSignUpResponse.getResponseMessege());
                }
            }

            @Override
            protected void onError(RetroError retroError) {
                baseActivity.cancelProgressBar(progress);
            }

            @Override
            protected void onFailure(String retroError) {
                baseActivity.cancelProgressBar(progress);
            }


        });
    }

    private void doLogin(String email, String password) {
        View progressView = baseActivity.showProgressBar();
        String newToken = PrefUtils.getFromPrefs(context, PrefUtils.TOKEN);
        Log.d("MyFirebaseMsgService", "Refreshed token: " + newToken);


        ApiClient.getAPiAuthHelper(baseActivity).getLogin("do_login", email, password, "android", newToken, ApiClient.API_VERSION).enqueue(new provision.com.app.utils.network.api.CallbackManager<LoginResponce>() {
            @Override
            protected void onSuccess(Object object, String message) {
                baseActivity.cancelProgressBar(progressView);
                LoginResponce loginResponce = (LoginResponce) object;
                if (loginResponce == null || loginResponce.getResponce() == null) {
                    Toast.makeText(context, context.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (loginResponce.getStatus().equals("1")) {
                    UserResponce userResponce = loginResponce.getResponce();
                    getAccountDetails(userResponce.getId());
                    PrefUtils.saveToPrefs(baseActivity, PrefUtils.USER_ID, userResponce.getId());
                    PrefUtils.saveToPrefs(baseActivity, PrefUtils.USER_NAME, userResponce.getName());
                    PrefUtils.saveToPrefs(baseActivity, PrefUtils.EMAIL_ID, userResponce.getEmail());
                    PrefUtils.saveToPrefs(baseActivity, PrefUtils.PASSWORD, password);
                    PrefUtils.saveToPrefs(baseActivity, PrefUtils.SOCIAL_ID, userResponce.getSocial_id());
                    PrefUtils.saveToPrefs(baseActivity, PrefUtils.USER_TYPE, userResponce.getUserType());
//                    PrefUtils.saveToPrefs(baseActivity, PrefUtils.USER_IMAGE, userResponce.get);
                    if (isCheckOut) {
                        if (isAddressRequred > 0) {
                            baseActivity.startActivity(new Intent(baseActivity, AddAddressActivity.class).putExtra("isCheckOut", true));
                            baseActivity.finish();
                            onTrackEventClick(LOGIN_EVENT_TOKEN);
                            return;
                        }
                    }
                    Intent backIntent = new Intent();
                    baseActivity.setResult(Activity.RESULT_OK, backIntent);
                    baseActivity.finish();
                    Toast.makeText(context, loginResponce.getResponseMessege(), Toast.LENGTH_SHORT).show();
                    onTrackEventClick(LOGIN_EVENT_TOKEN);
                } else {
                    Toast.makeText(context, "Either Email or Password is incorrect ! ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            protected void onError(RetroError retroError) {
                baseActivity.cancelProgressBar(progressView);
            }

            @Override
            protected void onFailure(String retroError) {
                baseActivity.cancelProgressBar(progressView);
            }


        });
    }

    private void getAccountDetails(String userId) {
        ApiClient.getAPiAuthHelper(context).getProfileDetails("getProfileContent", userId, ApiClient.API_VERSION).enqueue(new provision.com.app.utils.network.api.CallbackManager<MyAccountDetails>() {
            @Override
            protected void onSuccess(Object object, String message) {
                MyAccountDetails myAccountDetails = (MyAccountDetails) object;
                if (myAccountDetails == null || myAccountDetails.getResponse() == null) {
                    Toast.makeText(context, context.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (myAccountDetails.getStatus().equals("1")) {
//                    PrefUtils.saveToPrefs(context, PrefUtils.NOTIFICATION_ENABLE, response.body().getResponse().getUserInfo().getIsNotify());
                    if (!TextUtils.isEmpty(myAccountDetails.getResponse().getUserInfo().getUserImg())) {
                        PrefUtils.saveToPrefs(context, PrefUtils.USER_IMAGE, myAccountDetails.getResponse().getUserInfo().getUserImg());
                    }
                    PrefUtils.saveToPrefs(baseActivity, PrefUtils.MOBILE_NO, myAccountDetails.getResponse().getUserInfo().getPhoneNo());
                }
            }

            @Override
            protected void onError(RetroError retroError) {

            }

            @Override
            protected void onFailure(String retroError) {

            }


        });
    }

    private void onCreate() {
        binding.headerLayout.headerDividerLine.setVisibility(View.GONE);
        binding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = binding.emailEditText.getText().toString();
                if (email.isEmpty()) {
                    Toast.makeText(context, "Enter email address", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!Utils.isValidEmail(email)) {
                    Toast.makeText(context, "Enter valid email address", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!isUserLogin) {
                    checkSignInSignUp(email);
                    return;
                }
                String password = binding.passwordEditText.getText().toString();
                if (password.isEmpty()) {
                    Toast.makeText(context, "Enter password", Toast.LENGTH_SHORT).show();
                    return;
                }
                doLogin(email, password);
            }
        });
        binding.headerLayout.back.setVisibility(View.VISIBLE);
        binding.headerLayout.back.setOnClickListener(v -> {
            ((LoginActivity) context).finish();
        });

        binding.tvForgotPassword.setOnClickListener(V -> {
            context.startActivity(new Intent(context, ForgotPasswordActivity.class).putExtra("EmailId", binding.emailEditText.getText().toString()));
        });

        binding.ivGoogleSignIn.setOnClickListener(v -> {
            signIn();
        });


        setSpannable(context, binding.tvSignUp, context.getResources().getString(R.string.don_t_have_an_account_signup_now), 23, 34, SignUpActivity.class);

    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        baseActivity.startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void setSpannable(Context context, TextView tv, String text, int start, int end, final Class<? extends Activity> ActivityToOpen) {
        SpannableString ss = new SpannableString(text);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                ((Activity) context).startActivityForResult(new Intent(context, ActivityToOpen), 555);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(ContextCompat.getColor(context, R.color.colorAccent));
            }
        };

        ss.setSpan(clickableSpan, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv.setMovementMethod(LinkMovementMethod.getInstance());
        tv.setText(ss, TextView.BufferType.SPANNABLE);
    }

    public void updateUI(GoogleSignInAccount account) {
        String locale = context.getResources().getConfiguration().locale.getCountry();
        Log.d("getDetailsbyGmail : ", "" + account.getEmail() + " : " + account.getId() + " : " + account.getDisplayName() + " : " + account.getPhotoUrl() + " : " + account.getAccount());
        String photoUrl = "";
        if (account.getPhotoUrl() != null) {
            photoUrl = account.getPhotoUrl().toString();
        }
        hitGmailLoginService(account.getId(), account.getEmail(), account.getDisplayName(), "", photoUrl, locale);
    }

    private void hitGmailLoginService(String id, String emailId, String name, String gender, String image, String locale) {
        View view = baseActivity.showProgressBar();

        String newToken = PrefUtils.getFromPrefs(context, PrefUtils.TOKEN);
        ;
        ApiClient.getAPiAuthHelper(baseActivity).gmailLogin("reg_with_gmail", id, emailId, name, "", image, "", "android", newToken, ApiClient.API_VERSION).enqueue(new provision.com.app.utils.network.api.CallbackManager<GmailSignInResponse>() {
            @Override
            protected void onSuccess(Object object, String message) {
                baseActivity.cancelProgressBar(view);
                GmailSignInResponse gmailSignInResponse = (GmailSignInResponse) object;
                if (gmailSignInResponse == null || gmailSignInResponse.getResponse() == null) {
                    Toast.makeText(baseActivity, baseActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (gmailSignInResponse.getStatus().equals("1")) {
                    getAccountDetails(gmailSignInResponse.getResponse().getUserInfo().getId());
                    PrefUtils.saveToPrefs(baseActivity, PrefUtils.USER_ID, gmailSignInResponse.getResponse().getUserInfo().getId());
                    PrefUtils.saveToPrefs(baseActivity, PrefUtils.USER_NAME, gmailSignInResponse.getResponse().getUserInfo().getName());
                    PrefUtils.saveToPrefs(baseActivity, PrefUtils.EMAIL_ID, gmailSignInResponse.getResponse().getUserInfo().getEmail());
                    PrefUtils.saveToPrefs(baseActivity, PrefUtils.SOCIAL_ID, id);
                    if (isCheckOut) {
                        if (isAddressRequred > 0) {
                            baseActivity.startActivity(new Intent(baseActivity, AddAddressActivity.class).putExtra("isCheckOut", true));
                            baseActivity.finish();
                            onTrackEventClick(LOGIN_EVENT_TOKEN);
                            return;
                        }
                    }
                    Intent backIntent = new Intent();
                    baseActivity.setResult(Activity.RESULT_OK, backIntent);
                    baseActivity.finish();
                    Toast.makeText(context, gmailSignInResponse.getResponseMessege(), Toast.LENGTH_SHORT).show();
                    mGoogleSignInClient.signOut();
                    onTrackEventClick(LOGIN_EVENT_TOKEN);
                } else {
                    Toast.makeText(context, "Either Email or Password is incorrect ! ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            protected void onError(RetroError retroError) {
                baseActivity.cancelProgressBar(view);
            }

            @Override
            protected void onFailure(String retroError) {
                baseActivity.cancelProgressBar(view);
            }


        });

    }

    private void loginWithFacebook(String id, String email, String firstName, String lastName, String gander, String userImage) {

        String newToken = PrefUtils.getFromPrefs(context, PrefUtils.TOKEN);
        ;
        View view = baseActivity.showProgressBar();
        Log.d("MyFirebaseMsgService", "Refreshed token: " + newToken);
        ApiClient.getAPiAuthHelper(baseActivity).facebookLogin("fbLogin", id, email, firstName, lastName, gander, userImage, "android", newToken, ApiClient.API_VERSION).enqueue(new provision.com.app.utils.network.api.CallbackManager<GmailSignInResponse>() {
            @Override
            protected void onSuccess(Object object, String message) {
                baseActivity.cancelProgressBar(view);
                GmailSignInResponse gmailSignInResponse = (GmailSignInResponse) object;
                if (gmailSignInResponse == null || gmailSignInResponse.getResponse() == null) {
                    Toast.makeText(baseActivity, baseActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (gmailSignInResponse.getStatus().equals("1")) {
                    getAccountDetails(gmailSignInResponse.getResponse().getUserInfo().getId());
                    getAccountDetails(gmailSignInResponse.getResponse().getUserInfo().getId());
                    PrefUtils.saveToPrefs(baseActivity, PrefUtils.USER_ID, gmailSignInResponse.getResponse().getUserInfo().getId());
                    PrefUtils.saveToPrefs(baseActivity, PrefUtils.USER_NAME, gmailSignInResponse.getResponse().getUserInfo().getName());
                    PrefUtils.saveToPrefs(baseActivity, PrefUtils.EMAIL_ID, gmailSignInResponse.getResponse().getUserInfo().getEmail());
                    PrefUtils.saveToPrefs(baseActivity, PrefUtils.SOCIAL_ID, id);
                    if (isCheckOut) {
                        if (isAddressRequred > 0) {
                            baseActivity.startActivity(new Intent(baseActivity, AddAddressActivity.class).putExtra("isCheckOut", true));
                            baseActivity.finish();
                            onTrackEventClick(LOGIN_EVENT_TOKEN);
                            return;
                        }
                    }
                    Intent backIntent = new Intent();
                    baseActivity.setResult(Activity.RESULT_OK, backIntent);
                    baseActivity.finish();
                    Toast.makeText(context, gmailSignInResponse.getResponseMessege(), Toast.LENGTH_SHORT).show();
                    mGoogleSignInClient.signOut();
                    onTrackEventClick(LOGIN_EVENT_TOKEN);
                } else {
                    Toast.makeText(context, "Either Email or Password is incorrect ! ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            protected void onError(RetroError retroError) {
                baseActivity.cancelProgressBar(view);
            }

            @Override
            protected void onFailure(String retroError) {
                baseActivity.cancelProgressBar(view);
            }

        });

    }

}
