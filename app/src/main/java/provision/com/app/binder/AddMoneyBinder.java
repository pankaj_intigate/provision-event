package provision.com.app.binder;

import android.content.Intent;
import android.util.Base64;
import android.view.View;
import android.widget.ArrayAdapter;

import java.nio.charset.StandardCharsets;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.activity.WebViewActivity;
import provision.com.app.apiResponse.myWalletResponse.MyWallet;
import provision.com.app.databinding.ActivityAddMoneyBinding;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.network.api.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gauravg on 7/18/2018.
 */

public class AddMoneyBinder {
    private static final String[] currencyType = new String[]{"KWD", "INR", "USD"};
    private BaseActivity activity;
    private ActivityAddMoneyBinding binding;
    private String payType = "2";

    public AddMoneyBinder(BaseActivity activity, ActivityAddMoneyBinding binding) {
        this.activity = activity;
        this.binding = binding;

        onCreate();
    }

    private void onCreate() {
        binding.headerLayout.headerDividerLine.setVisibility(View.GONE);
        binding.headerLayout.back.setVisibility(View.VISIBLE);
        binding.headerLayout.back.setOnClickListener(v -> {
            activity.finish();
        });
        binding.radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.rb_kNet) {
                payType = "2";
            } else {
                payType = "3";
            }
        });
        ArrayAdapter<String> currencyAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_dropdown_item_1line, currencyType);
        binding.spnCurrencyType.setAdapter(currencyAdapter);
        binding.btnAddMoney.setOnClickListener(v -> {
            if (isValidate()) {
                hitAddWalletService();
            }
        });

    }

    private void hitAddWalletService() {
        String userId = PrefUtils.getFromPrefs(activity, PrefUtils.USER_ID);
        String password = PrefUtils.getFromPrefs(activity, PrefUtils.PASSWORD);
        String socialId = PrefUtils.getFromPrefs(activity, PrefUtils.SOCIAL_ID);
        if (socialId.equals("")) {
            socialId = "0";
        }
        if (password.equals("")) {
            password = "0";
        }
//        String currencyCode=PrefUtils.getFromPrefs(activity,PrefUtils.COUNTRY_CODE);
//        String sessionId=PrefUtils.getFromPrefs(activity,PrefUtils.SESSION_ID);
//        String parms = currencyCode + "/" + sessionId + "/" + socialId;
        String base64Password = Base64.encodeToString(password.getBytes(StandardCharsets.UTF_8), Base64.DEFAULT);
        String parms = userId + "/" + base64Password + "/" + socialId;


        ApiClient.getAPiAuthHelper(activity).addMoney("getapplogin",ApiClient.API_VERSION).enqueue(new Callback<MyWallet>() {
            @Override
            public void onResponse(Call<MyWallet> call, Response<MyWallet> response) {

                if (response.body().getStatus().equals("1")) {
                    activity.startActivity(new Intent(activity, WebViewActivity.class).putExtra("URL", response.body().getResponse().getUrl() + parms).putExtra("TitleName", "Disclaimer"));
                } else {
                    activity.showToast(activity, response.body().getResponseMessege());
                }
            }

            @Override
            public void onFailure(Call<MyWallet> call, Throwable t) {

            }
        });
//        ApiClient.getAPiAuthHelper(activity).addWallet("addwallet", PrefUtils.getFromPrefs(activity, PrefUtils.EMAIL_ID), PrefUtils.getFromPrefs(activity, PrefUtils.USER_ID), binding.etAmount.getText().toString().trim(), "1", password, socialId, payType).enqueue(new Callback<MyWallet>() {
//            @Override
//            public void onResponse(Call<MyWallet> call, ShopItemEventResponse<MyWallet> response) {
//                if (response.body().getStatus().equals("1")) {
//                    activity.startActivity(new Intent(activity, WebViewActivity.class).putExtra("URL", response.body().getResponse().getUrl()).putExtra("TitleName", "Disclaimer"));
//
//                } else {
//                    activity.showToast(activity, response.body().getResponseMessege());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<MyWallet> call, Throwable t) {
//
//            }
//        });
    }

    private boolean isValidate() {
        if (binding.etAmount.getText().toString().trim().isEmpty()) {
            activity.showToast(activity, "Please enter amount.");
            return false;
        }
        if (Integer.parseInt(binding.etAmount.getText().toString().trim()) <= 0) {
            activity.showToast(activity, "Please enter valid amount.");
            return false;
        }
        if (!(binding.rbKNet.isChecked() || binding.rbVisa.isChecked())) {
            activity.showToast(activity, "Please select payment mode.");
            return false;
        }
        return true;
    }

}
