package provision.com.app.binder;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.apiResponse.getComposeMailResponse.ComposeMail;
import provision.com.app.apiResponse.getComposeMailResponse.EvnetsList;
import provision.com.app.apiResponse.getComposeMailResponse.Suppordatum;
import provision.com.app.apiResponse.inbox.InboxListEntity;
import provision.com.app.databinding.ActivityComposeMailBinding;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.network.api.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gauravg on 7/18/2018.
 */

public class ComposeMailBinder {
    private static final String[] type = new String[]{"", ""};
    private BaseActivity activity;
    private ActivityComposeMailBinding binding;
    private String getMailTypeId, getEventTypeId;

    public ComposeMailBinder(BaseActivity activity, ActivityComposeMailBinding binding) {
        this.activity = activity;
        this.binding = binding;

        onCreate();
    }

    private void onCreate() {
        binding.headerLayout.imgHeader.setVisibility(View.GONE);
        binding.headerLayout.back.setVisibility(View.VISIBLE);
        binding.headerLayout.tvTitle.setText("Compose mail");

        binding.headerLayout.back.setOnClickListener(v -> {
            activity.finish();
        });

        if (activity.isOnline()) {
            getMailTypeService();
        } else {
            activity.showToast(activity, activity.getResources().getString(R.string.check_internet));
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, android.R.layout.simple_dropdown_item_1line, type);
        binding.spnEventType.setAdapter(adapter);
        binding.spnMailType.setAdapter(adapter);

        binding.btnSend.setOnClickListener(v -> {
            if (isValidate()) {
                if (activity.isOnline()) {
                    sendComposeMail(PrefUtils.getFromPrefs(activity, PrefUtils.USER_ID), getMailTypeId, getEventTypeId, binding.etSubject.getText().toString().trim(), binding.etMessage.getText().toString().trim());
                } else {
                    activity.showToast(activity, activity.getResources().getString(R.string.check_internet));
                }
            }
        });
    }

    private void sendComposeMail(String userId, String mailTypeId, String eventId, String subject, String body) {
        View view = activity.showProgressBar();
        ApiClient.getAPiAuthHelper(activity).composeMail("send_compose_mail", userId, mailTypeId, eventId, subject, body,ApiClient.API_VERSION).enqueue(new Callback<InboxListEntity>() {
            @Override
            public void onResponse(Call<InboxListEntity> call, Response<InboxListEntity> response) {
                activity.cancelProgressBar(view);
                if (response.body() == null || response.body().getResponse() == null) {
                    Toast.makeText(activity, activity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (response.body().getStatus().equals("1")) {
                    activity.showToast(activity, response.body().getResponseMessege());
                    activity.finish();
                }
            }

            @Override
            public void onFailure(Call<InboxListEntity> call, Throwable t) {
                activity.cancelProgressBar(view);
            }
        });

    }

    private void getMailTypeService() {
        View progress = activity.showProgressBar();
        ApiClient.getAPiAuthHelper(activity).composeMail("compose_mail", PrefUtils.getFromPrefs(activity, PrefUtils.USER_ID),ApiClient.API_VERSION).enqueue(new Callback<ComposeMail>() {
            @Override
            public void onResponse(Call<ComposeMail> call, Response<ComposeMail> response) {
                activity.cancelProgressBar(progress);
                if (response.body() == null || response.body().getResponse() == null) {
                    Toast.makeText(activity, activity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (response.body().getStatus().equals("1")) {

                    ArrayAdapter<EvnetsList> evnetsListArrayAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_dropdown_item_1line, response.body().getResponse().getEvnetsList());
                    binding.spnEventType.setAdapter(evnetsListArrayAdapter);

                    binding.spnEventType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            EvnetsList evnetsList = (EvnetsList) parent.getItemAtPosition(position);
                            getEventTypeId = evnetsList.getEventId();
                        }
                    });

                    ArrayAdapter<Suppordatum> suppordatumArrayAdapter = new ArrayAdapter<Suppordatum>(activity, android.R.layout.simple_dropdown_item_1line, response.body().getResponse().getMailtype().getSuppordata());
                    binding.spnMailType.setAdapter(suppordatumArrayAdapter);

                    binding.spnMailType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Suppordatum suppordatum = (Suppordatum) parent.getItemAtPosition(position);
                            getMailTypeId = suppordatum.getId();
                        }
                    });

                }
            }

            @Override
            public void onFailure(Call<ComposeMail> call, Throwable t) {
                activity.cancelProgressBar(progress);
            }
        });

    }

    private boolean isValidate() {
        if (binding.spnEventType.getText().toString().isEmpty()) {
            activity.showToast(activity, "Please select event");
            return false;
        }
        if (binding.spnMailType.getText().toString().isEmpty()) {
            activity.showToast(activity, "Please select mail type");
            return false;
        }
        if (binding.etSubject.getText().toString().isEmpty()) {
            activity.showToast(activity, "Please enter subject");
            return false;
        }
        if (binding.etMessage.getText().toString().isEmpty()) {
            activity.showToast(activity, "Please enter message");
            return false;
        }
        return true;
    }
}
