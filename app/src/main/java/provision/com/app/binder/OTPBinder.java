package provision.com.app.binder;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.activity.EventDialogActivity;
import provision.com.app.activity.LoginActivity;
import provision.com.app.apiResponse.login.LoginResponce;
import provision.com.app.databinding.ActivityOtpBinding;
import provision.com.app.utils.network.api.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gauravg on 7/17/2018.
 */

public class OTPBinder {
    private BaseActivity activity;
    private ActivityOtpBinding binding;
    private String mGetEmail = "";

    public OTPBinder(BaseActivity activity, ActivityOtpBinding binding, String mGetEmail) {
        this.activity = activity;
        this.binding = binding;
        this.mGetEmail = mGetEmail;
        activity.hideKeyboard();
        onCreate();
    }

    private void onCreate() {
        binding.tvEmailAddress.setText(mGetEmail);

        binding.headerLayout.imgHeader.setVisibility(View.VISIBLE);
        binding.headerLayout.back.setVisibility(View.VISIBLE);
        binding.headerLayout.back.setOnClickListener(v -> {
            activity.finish();
        });

        binding.tvResendOTP.setOnClickListener(v -> {
            forgotPassword(mGetEmail);
        });

        binding.btnSubmit.setOnClickListener(v -> {
            if (isValidate()) {
                getOTP(mGetEmail, binding.etOTP.getText().toString().trim(), binding.etNewPassword.getText().toString().trim(), binding.etConfirmPassword.getText().toString().trim());
            }
        });
    }

    private boolean isValidate() {
        if (TextUtils.isEmpty(binding.etOTP.getText().toString())) {
            Toast.makeText(activity, "Please enter OTP", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(binding.etNewPassword.getText().toString())) {
            Toast.makeText(activity, "Please enter new password", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(binding.etConfirmPassword.getText().toString())) {
            Toast.makeText(activity, "Please enter confirm password", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!binding.etNewPassword.getText().toString().equals(binding.etConfirmPassword.getText().toString())) {
            Toast.makeText(activity, "Password not match", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    private void getOTP(String email, String otp, String newPassword, String confirmPassword) {
        View progressView = activity.showProgressBar();
        ApiClient.getAPiAuthHelper(activity).getOTP("otp_verified", email, otp, newPassword, confirmPassword,ApiClient.API_VERSION).enqueue(new Callback<LoginResponce>() {
            @Override
            public void onResponse(Call<LoginResponce> call, Response<LoginResponce> response) {
                activity.cancelProgressBar(progressView);
                if (response.body() == null || response.body().getResponce() == null) {
                    Toast.makeText(activity, activity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (response.body().getStatus().equals("1")) {
                    activity.startActivity(new Intent(activity, LoginActivity.class));
                    activity.finish();
                }
                activity.showToast(activity, response.body().getResponseMessege());
            }

            @Override
            public void onFailure(Call<LoginResponce> call, Throwable t) {
                activity.cancelProgressBar(progressView);
            }
        });
    }

    private void forgotPassword(String email) {
        View progressView = activity.showProgressBar();
        ApiClient.getAPiAuthHelper(activity).forgotPassword("forgot_password", email,ApiClient.API_VERSION).enqueue(new Callback<LoginResponce>() {
            @Override
            public void onResponse(Call<LoginResponce> call, Response<LoginResponce> response) {
                activity.cancelProgressBar(progressView);
                if (response.body() == null || response.body().getResponce() == null) {
                    Toast.makeText(activity, activity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                activity.showToast(activity, response.body().getResponseMessege());
            }

            @Override
            public void onFailure(Call<LoginResponce> call, Throwable t) {
                activity.cancelProgressBar(progressView);
            }
        });
    }
}
