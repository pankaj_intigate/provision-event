package provision.com.app.binder;

import android.content.Intent;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.text.TextUtils;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.activity.LoginActivity;
import provision.com.app.activity.WebViewActivity;
import provision.com.app.databinding.FragmentSettingBinding;
import provision.com.app.fragment.MoreFragment;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.network.api.ApiClient;

import static provision.com.app.app.AppConstant.ABOUT_US_EVENT_TOKEN;
import static provision.com.app.app.AppConstant.PRIVACY_POLICY_EVENT_TOKEN;
import static provision.com.app.app.AppConstant.TERMS_AND_CONDITIONS_EVENT_TOKEN;
import static provision.com.app.utils.CommonUtils.onTrackEventClick;

/**
 * Created by gauravg on 7/17/2018.
 */

public class SettingFragmentBinder {
    public static final int LOGIN_REQUEST_CODE = 1002;
    private BaseActivity activity;
    private FragmentSettingBinding binding;
    private MoreFragment moreFragment;

    public SettingFragmentBinder(BaseActivity activity, FragmentSettingBinding binding) {
        this.activity = activity;
        this.binding = binding;

        onCreate();
    }

    private void onCreate() {
        binding.rlUserProfile.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(activity, PrefUtils.USER_ID))) {
                //  activity.startActivityForResult(new Intent(activity, MoreActivity.class),1001);
            } else {
                //   activity.startActivity(new Intent(activity, LoginActivity.class));
            }
        });

        binding.rlSignInSignUp.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(activity, PrefUtils.USER_ID))) {
                //  moreFragment = MoreFragment.newInstance();
                //  openFragment(moreFragment);
            } else {
                activity.startActivityForResult(new Intent(activity, LoginActivity.class), LOGIN_REQUEST_CODE);
            }
        });

        binding.rlHelp.setOnClickListener(v -> {
            activity.startActivity(new Intent(activity, WebViewActivity.class).putExtra("URL", ApiClient.HELP_URL).putExtra("TitleName", "Help"));
        });

        binding.rlAboutUs.setOnClickListener(v -> {
            activity.startActivity(new Intent(activity, WebViewActivity.class).putExtra("URL", ApiClient.ABOUT_US_URL).putExtra("TitleName", "About Us"));
            onTrackEventClick(ABOUT_US_EVENT_TOKEN);
        });

        binding.rlPrivacyPolicy.setOnClickListener(v -> {
            activity.startActivity(new Intent(activity, WebViewActivity.class).putExtra("URL", ApiClient.PRIVACY_POLICY_URL).putExtra("TitleName", "Privacy Policy"));
            onTrackEventClick(PRIVACY_POLICY_EVENT_TOKEN);
        });

        binding.rlTermsAndConditions.setOnClickListener(v -> {
            activity.startActivity(new Intent(activity, WebViewActivity.class).putExtra("URL", ApiClient.TERMS_N_CONDITIONS_URL).putExtra("TitleName", "Terms & Conditions"));
            onTrackEventClick(TERMS_AND_CONDITIONS_EVENT_TOKEN);
        });
    }

    public void openFragment(Fragment fragment) {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.commit();
    }
}
