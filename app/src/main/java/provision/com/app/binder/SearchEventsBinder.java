package provision.com.app.binder;

import android.content.Context;

import provision.com.app.activity.SearchEventsActivity;
import provision.com.app.databinding.ActivitySearchEventsBinding;

/**
 * Created by gauravg on 7/13/2018.
 */

public class SearchEventsBinder {
    Context context;
    ActivitySearchEventsBinding binding;

    public SearchEventsBinder(Context context, ActivitySearchEventsBinding binding) {
        this.context = context;
        this.binding = binding;

        onCreate();
    }

    private void onCreate() {
        binding.back.setOnClickListener(v -> {
            ((SearchEventsActivity) context).finish();
        });
    }
}
