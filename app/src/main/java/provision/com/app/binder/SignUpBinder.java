package provision.com.app.binder;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.core.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.activity.SignUpActivity;
import provision.com.app.activity.WebViewActivity;
import provision.com.app.apiResponse.getProfileResponse.CountryCode;
import provision.com.app.apiResponse.login.LoginResponce;
import provision.com.app.apiResponse.login.UserResponce;
import provision.com.app.apiResponse.my_account.MyAccountDetails;
import provision.com.app.databinding.ActivitySignUpBinding;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.Utils;
import provision.com.app.utils.network.api.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static provision.com.app.app.AppConstant.PRIVACY_POLICY_EVENT_TOKEN;
import static provision.com.app.app.AppConstant.SIGNUP_EVENT_TOKEN;
import static provision.com.app.app.AppConstant.TERMS_AND_CONDITIONS_EVENT_TOKEN;
import static provision.com.app.utils.CommonUtils.onTrackEventClick;

/**
 * Created by gauravg on 7/6/2018.
 */

public class SignUpBinder {
    Context context;
    ActivitySignUpBinding binding;
    private BaseActivity baseActivity;
    private List<CountryCode> countryCodeList;
    private String selectCountryCode = "";
    private String email;

    public SignUpBinder(Context context, ActivitySignUpBinding binding, String email) {
        this.context = context;
        this.binding = binding;
        this.email = email;
        baseActivity = (BaseActivity) context;
        baseActivity.hideKeyboard();
        onCreate();
    }

    private void onCreate() {
//        getCountryCode();
        binding.headerLayout.headerDividerLine.setVisibility(View.GONE);
        binding.emailEditText.setText(email);
        binding.emailEditText.setSelection(binding.emailEditText.getText().length());
        binding.headerLayout.back.setVisibility(View.VISIBLE);
        binding.headerLayout.back.setOnClickListener(v -> {
            ((SignUpActivity) context).finish();
        });


        binding.btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = binding.emailEditText.getText().toString();
                if (email.isEmpty()) {
                    Toast.makeText(context, "Enter email address", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!Utils.isValidEmail(email)) {
                    Toast.makeText(context, "Enter valid email address", Toast.LENGTH_SHORT).show();
                    return;
                }
                String name = binding.nameEditText.getText().toString();
                if (name.isEmpty() || name.length() < 3) {
                    Toast.makeText(context, "Please enter valid name", Toast.LENGTH_SHORT).show();
                    return;
                }
                String password = binding.passwordEditText.getText().toString();
                if (password.isEmpty()) {
                    Toast.makeText(context, "Enter password", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!binding.tearmsCheckBox.isChecked()) {
                    Toast.makeText(context, "Please accept terms of service and privacy policy", Toast.LENGTH_SHORT).show();
                    return;
                }
//                if (TextUtils.isEmpty(binding.spnCountryCode.getText().toString())) {
//                    Toast.makeText(context, "Select country code", Toast.LENGTH_SHORT).show();
//                    return;
//                }
//                String mobileNo = binding.mobileEditText.getText().toString();
//                if (mobileNo.isEmpty() || mobileNo.length() < 10) {
//                    Toast.makeText(context, "Enter valid mobile no", Toast.LENGTH_SHORT).show();
//                    return;
//                }
//
                doRegister(email, name, password, "", selectCountryCode);
            }
        });
        setSpannable(binding.tvSignIn, context.getResources().getString(R.string.already_have_an_account_sign_in), 25, 32);
        setTermsConditionSpannable(binding.tvTermsAndConditions, context.getResources().getString(R.string.by_signing_up_you_agree_to_our_terms_of_service_and_privacy_policy));
    }

    private void doRegister(String email, String name, String password, String mobileno, String code) {

                String newToken = PrefUtils.getFromPrefs(context,PrefUtils.TOKEN);
                Log.d("MyFirebaseMsgService", "Refreshed token: " + newToken);
                View progressView = baseActivity.showProgressBar();
                ApiClient.getAPiAuthHelper(baseActivity).doRegister("GetRegister", name, email, password, mobileno, "android", code, newToken,ApiClient.API_VERSION).enqueue(new Callback<LoginResponce>() {
                    @Override
                    public void onResponse(@NotNull Call<LoginResponce> call, Response<LoginResponce> response) {
                        baseActivity.cancelProgressBar(progressView);
                        if (response.body() == null || response.body().getResponce() == null) {
                            onFailure(call, new Throwable());
                            return;
                        }
                        Toast.makeText(context, response.body().getResponseMessege(), Toast.LENGTH_SHORT).show();
                        if (response.body().getStatus().equals("1")) {
                            UserResponce userResponce = response.body().getResponce();
                            PrefUtils.saveToPrefs(baseActivity, PrefUtils.USER_ID, userResponce.getId());
                            PrefUtils.saveToPrefs(baseActivity, PrefUtils.USER_NAME, userResponce.getName());
                            PrefUtils.saveToPrefs(baseActivity, PrefUtils.EMAIL_ID, userResponce.getEmail());
                            PrefUtils.saveToPrefs(baseActivity, PrefUtils.PASSWORD, password);
                            PrefUtils.saveToPrefs(baseActivity, PrefUtils.MOBILE_NO, mobileno);
                            baseActivity.setResult(Activity.RESULT_OK);
//                    Intent intent = new Intent(baseActivity, LoginActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    baseActivity.startActivity(intent);
                            baseActivity.finish();
                            onTrackEventClick(SIGNUP_EVENT_TOKEN);
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponce> call, Throwable t) {
                        Toast.makeText(context, context.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        baseActivity.cancelProgressBar(progressView);
                    }
                });

    }

    private void setTermsConditionSpannable(TextView tv, String text) {
        SpannableString ss = new SpannableString(text);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                context.startActivity(new Intent(context, WebViewActivity.class).putExtra("URL", ApiClient.TERMS_N_CONDITIONS_URL).putExtra("TitleName", "Terms of Service"));
                onTrackEventClick(TERMS_AND_CONDITIONS_EVENT_TOKEN);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(ContextCompat.getColor(context, R.color.colorAccent));
            }
        };
        ss.setSpan(clickableSpan, 32, 48, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        ClickableSpan clickable = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                context.startActivity(new Intent(context, WebViewActivity.class).putExtra("URL", ApiClient.PRIVACY_POLICY_URL).putExtra("TitleName", "Privacy Policy"));
                onTrackEventClick(PRIVACY_POLICY_EVENT_TOKEN);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(ContextCompat.getColor(context, R.color.colorAccent));
            }
        };
        ss.setSpan(clickable, 53, 68, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv.setMovementMethod(LinkMovementMethod.getInstance());
        tv.setText(ss, TextView.BufferType.SPANNABLE);
    }

    private void setSpannable(TextView tv, String text, int start, int end) {
        SpannableString ss = new SpannableString(text);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                ((SignUpActivity) context).finish();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(ContextCompat.getColor(context, R.color.colorAccent));
            }
        };

        ss.setSpan(clickableSpan, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv.setMovementMethod(LinkMovementMethod.getInstance());
        tv.setText(ss, TextView.BufferType.SPANNABLE);
    }

    private void getCountryCode() {
        ApiClient.getAPiAuthHelper(context).getCountryCode("getCountryCode",ApiClient.API_VERSION).enqueue(new Callback<MyAccountDetails>() {
            @Override
            public void onResponse(Call<MyAccountDetails> call, Response<MyAccountDetails> response) {
                if (response.body() == null || response.body().getResponse() == null) {
                    Toast.makeText(context, context.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (response.body().getStatus().equals("1")) {

                    //Set Country-Code'Data.
                    countryCodeList = new ArrayList<>();
//                    countryCodeList.addAll(response.body().getResponse().getCountryCode());
                    CountryCode countryCode;
                    for (int i = 0; i < response.body().getResponse().getCountryCode().size(); i++) {
                        countryCode = new CountryCode();
                        countryCode.setDialCode(response.body().getResponse().getCountryCode().get(i).getDialCode());
                        countryCode.setCode(response.body().getResponse().getCountryCode().get(i).getCode());
                        countryCode.setName(response.body().getResponse().getCountryCode().get(i).getName());
                        countryCode.setCountryCodeEnable(true);
                        countryCodeList.add(countryCode);
                    }
                    ArrayAdapter<CountryCode> countryCodeArrayAdapter = new ArrayAdapter<>(context,
                            android.R.layout.simple_dropdown_item_1line, countryCodeList);
                    binding.spnCountryCode.setAdapter(countryCodeArrayAdapter);

                    binding.spnCountryCode.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            selectCountryCode = countryCodeList.get(position).getDialCode();
                            baseActivity.showToast(baseActivity, selectCountryCode);
                        }
                    });

                }
            }

            @Override
            public void onFailure(Call<MyAccountDetails> call, Throwable t) {

            }
        });
    }
}
