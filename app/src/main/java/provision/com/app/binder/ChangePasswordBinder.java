package provision.com.app.binder;

import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.apiResponse.my_account.MyAccountDetails;
import provision.com.app.databinding.ActivityChangePasswordBinding;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.network.api.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gauravg on 7/17/2018.
 */

public class ChangePasswordBinder {
    private BaseActivity activity;
    private ActivityChangePasswordBinding binding;

    public ChangePasswordBinder(BaseActivity activity, ActivityChangePasswordBinding binding) {
        this.activity = activity;
        this.binding = binding;

        onCreate();
    }

    private void onCreate() {
        binding.headerLayout.imgHeader.setVisibility(View.VISIBLE);
        binding.headerLayout.back.setVisibility(View.VISIBLE);
        binding.headerLayout.back.setOnClickListener(v -> {
            activity.finish();
        });

        binding.btnConfirm.setOnClickListener(v -> {
            if (isValidate()) {
                changePassword();
            }
        });
    }

    private boolean isValidate() {
        if (TextUtils.isEmpty(binding.oldPassword.getText().toString())) {
            activity.showToast(activity, "Please enter old password.");
            return false;
        }
        if (TextUtils.isEmpty(binding.newPassword.getText().toString())) {
            activity.showToast(activity, "Please enter new password.");
            return false;
        }
        if (TextUtils.isEmpty(binding.confirmPassword.getText().toString())) {
            activity.showToast(activity, "Please enter confirm password.");
            return false;
        }
        if (!binding.newPassword.getText().toString().equals(binding.confirmPassword.getText().toString())) {
            activity.showToast(activity, "Password does not match.");
            return false;
        }
        return true;
    }

    private void changePassword() {
        View progressView = activity.showProgressBar();
        ApiClient.getAPiAuthHelper(activity).changePassword("setPassword", PrefUtils.getFromPrefs(activity, PrefUtils.USER_ID), binding.oldPassword.getText().toString(), binding.confirmPassword.getText().toString(),ApiClient.API_VERSION).enqueue(new Callback<MyAccountDetails>() {

            @Override
            public void onResponse(Call<MyAccountDetails> call, Response<MyAccountDetails> response) {
                activity.cancelProgressBar(progressView);
                if (response.body() == null || response.body().getResponse() == null) {
                    Toast.makeText(activity, activity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (response.body().getStatus().equals("1")) {
                    activity.finish();
                }
                activity.showToast(activity, response.body().getResponseMessege());
            }

            @Override
            public void onFailure(Call<MyAccountDetails> call, Throwable t) {
                activity.cancelProgressBar(progressView);
            }
        });
    }
}
