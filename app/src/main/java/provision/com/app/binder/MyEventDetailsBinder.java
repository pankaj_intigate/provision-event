package provision.com.app.binder;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.activity.MyEventDetailsActivity;
import provision.com.app.adapter.EventDetailsAdapter;
import provision.com.app.apiResponse.receipt_details.InvoiceInfo;
import provision.com.app.apiResponse.receipt_details.ParticipateInfo;
import provision.com.app.apiResponse.receipt_details.ReceiptDetails;
import provision.com.app.databinding.ActivityMyEventsDetailsBinding;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.Utils;
import provision.com.app.utils.network.api.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gauravg on 7/10/2018.
 */

public class MyEventDetailsBinder {
    Context context;
    ActivityMyEventsDetailsBinding binding;
    private List<ParticipateInfo> dataModelList;
    private EventDetailsAdapter adapter;
    private String bookingId;

    public MyEventDetailsBinder(Context context, ActivityMyEventsDetailsBinding binding, String bookingId) {
        this.context = context;
        this.binding = binding;
        this.bookingId = bookingId;
        onCreate();
    }

    private void onCreate() {
        dataModelList = new ArrayList<>();
        binding.rcvEventDetails.setLayoutManager(new LinearLayoutManager(context));
        binding.rcvEventDetails.setNestedScrollingEnabled(false);
       /* adapter = new EventDetailsAdapter(context, dataModelList);
        binding.rcvEventDetails.setAdapter(adapter);*/
        binding.headerLayout.back.setVisibility(View.VISIBLE);
        binding.headerLayout.imgHeader.setVisibility(View.GONE);
        binding.headerLayout.tvTitle.setText("Booking Details");
        binding.headerLayout.back.setOnClickListener(v -> {
            ((MyEventDetailsActivity) context).finish();
        });
        getReceiptData();
    }


    private void getReceiptData() {
        View progressView = ((BaseActivity) context).showProgressBar();
        ApiClient.getAPiAuthHelper(context).getReceipt("getReceipt", bookingId, PrefUtils.getFromPrefs(context, PrefUtils.COUNTRY_CODE),ApiClient.API_VERSION).enqueue(new Callback<ReceiptDetails>() {
            @Override
            public void onResponse(Call<ReceiptDetails> call, Response<ReceiptDetails> response) {
                ReceiptDetails receiptDetails = response.body();
                ((BaseActivity) context).cancelProgressBar(progressView);
                if (response.body() == null || response.body().getResponse() == null) {
                    Toast.makeText(context, context.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (receiptDetails.getStatus().equals("1")) {
                    InvoiceInfo invoiceInfo = receiptDetails.getResponse().getInvoiceInfo();
                    Glide.with(context)
                            .load(invoiceInfo.getBarCode())
                            .into(binding.barCodeImageView);
                    binding.nameTextView.setText(invoiceInfo.getName());
                    binding.mobileTextView.setText(invoiceInfo.getPhoneNo());
                    binding.bookingDateTextView.setText(invoiceInfo.getBookingDate());
                    binding.regNoTextView.setText(invoiceInfo.getRegistrationNo());
                    binding.emailTextView.setText(invoiceInfo.getEmail());
                    binding.totalTextView.setText(Utils.convertPrice(response.body().getCurrencyValue(), invoiceInfo.getSubTotal(), response.body().getNumberformat()) + " " + response.body().getCurrrencyCode());
                    binding.grandTotalTextView.setText(Utils.convertPrice(response.body().getCurrencyValue(), invoiceInfo.getGrandTotal(), response.body().getNumberformat()) + " " + response.body().getCurrrencyCode());
                    dataModelList.addAll(receiptDetails.getResponse().getParticipateInfo());

                }
                adapter = new EventDetailsAdapter(context, dataModelList, response.body().getCurrencyValue(), response.body().getCurrrencyCode(), response.body().getNumberformat());
                binding.rcvEventDetails.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<ReceiptDetails> call, Throwable t) {
                ((BaseActivity) context).cancelProgressBar(progressView);
            }
        });
    }
}
