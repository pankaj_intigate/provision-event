package provision.com.app.binder;

import android.content.Intent;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.activity.ReplyActivity;
import provision.com.app.adapter.InboxDetailAdapter;
import provision.com.app.apiResponse.inboxDetailsResponse.InboxDetails;
import provision.com.app.apiResponse.inboxDetailsResponse.Response_;
import provision.com.app.databinding.ActivityInboxDetailBinding;
import provision.com.app.utils.network.api.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static provision.com.app.app.AppConstant.REPLY_EVENT_TOKEN;
import static provision.com.app.utils.CommonUtils.onTrackEventClick;

/**
 * Created by gauravg on 7/18/2018.
 */

public class InboxDetailBinder {
    String getMailId;
    private BaseActivity activity;
    private ActivityInboxDetailBinding binding;
    private InboxDetailAdapter mAdapter;
    private List<Response_> mMailList;

    public InboxDetailBinder(BaseActivity activity, ActivityInboxDetailBinding binding, String getMailId) {
        this.activity = activity;
        this.binding = binding;
        this.getMailId = getMailId;
        onCreate();
    }

    private void onCreate() {
        binding.headerLayout.imgHeader.setVisibility(View.GONE);
        binding.headerLayout.back.setVisibility(View.VISIBLE);
        binding.headerLayout.tvTitle.setText("Inbox detail");


        binding.headerLayout.back.setOnClickListener(v -> {
            activity.finish();
        });


        binding.btnReply.setOnClickListener(v -> {
            activity.startActivity(new Intent(activity, ReplyActivity.class).putExtra("MailId", getMailId));
            onTrackEventClick(REPLY_EVENT_TOKEN);
        });

        // getInboxDetails(getMailId);
    }

    public void getInboxDetails(String mailId) {
        View progress = activity.showProgressBar();
        ApiClient.getAPiAuthHelper(activity).inboxDetails("getSupportInfo", mailId,ApiClient.API_VERSION).enqueue(new Callback<InboxDetails>() {
            @Override
            public void onResponse(Call<InboxDetails> call, Response<InboxDetails> response) {
                activity.cancelProgressBar(progress);
                if (response.body() == null || response.body().getResponse() == null) {
                    Toast.makeText(activity, activity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (response.body().getStatus().equals("1")) {
                    mMailList = new ArrayList<>();
                    if (response.body().getResponse().getList().getResponse() != null && response.body().getResponse().getList().getResponse().size() > 0) {
                        mMailList.addAll(response.body().getResponse().getList().getResponse());
                        binding.rcvInboxDetailList.setLayoutManager(new LinearLayoutManager(activity));
                        mAdapter = new InboxDetailAdapter(activity, mMailList);
                        binding.rcvInboxDetailList.setAdapter(mAdapter);
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<InboxDetails> call, Throwable t) {

            }
        });
    }
}
