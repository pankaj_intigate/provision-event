package provision.com.app.binder;

import android.annotation.SuppressLint;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import provision.com.app.BuildConfig;
import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.adapter.HomeAdapter;
import provision.com.app.adapter.HomeViewPagerAdapter;
import provision.com.app.apiResponse.homeBanner.HomeBanerResponse;
import provision.com.app.apiResponse.homeBanner.ProductList;
import provision.com.app.apiResponse.upcomingEventResponse.UpcomingEventResponse;
import provision.com.app.databinding.FragmentHomeBinding;
import provision.com.app.utils.CustomAlertDialog;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.Utils;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;

/**
 * Created by amitk on 12-07-2018.
 */

public class HomeFragmentBinder {
    private List<provision.com.app.apiResponse.upcomingEventResponse.ProductList> mProductList = new ArrayList<>();
    private List<ProductList> productViewPagerList = new ArrayList<>();
    private FragmentHomeBinding fragmentHomeBinding;
    private HomeAdapter mHomeAdapter;
    private HomeViewPagerAdapter mHomeViewPagerAdapter;
    private BaseActivity mActivity;
    private boolean isHomeBannerListLoaded, isEventLoaded;
    private boolean[] refreshing = {false, false};

    public HomeFragmentBinder(BaseActivity mActivity, FragmentHomeBinding fragmentHomeBinding) {
        this.mActivity = mActivity;
        this.fragmentHomeBinding = fragmentHomeBinding;
        setAdapter();
        getHomeBanerProductList(false);
        getUpcomingEventList(false);
        setRefershLayout();
    }

    private void setRefershLayout() {
        fragmentHomeBinding.swipeRefreshLayout.setOnRefreshListener(() -> {
            getHomeBanerProductList(true);
            getUpcomingEventList(true);
        });
    }

    @SuppressLint("SetTextI18n")
    private void setHourDays(TextView tv_hour, TextView tv_day, ProductList productItem) {
        long timeDifference = Utils.getTimeRemaining(productItem.getEventDate());
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = timeDifference / daysInMilli;
        timeDifference = timeDifference % daysInMilli;
        long elapsedHours = timeDifference / hoursInMilli;

        long hours = TimeUnit.MILLISECONDS
                .toHours(timeDifference);
        Log.d("hours", hours + "");
        if (hours < 0) {
            tv_hour.setText("00");
        } else if (elapsedHours < 10) {
            tv_hour.setText("0" + hours);
        } else {
            tv_hour.setText("" + hours);
        }

        if (elapsedDays < 0) {
            tv_day.setText("00");
        } else if (elapsedDays < 10) {
            tv_day.setText("0" + elapsedDays);
        } else {
            tv_day.setText("" + elapsedDays);
        }
    }

    private void getHomeBanerProductList(boolean isRefersh) {
        if (mActivity.isOnline()) {
            View view = null;
            if (!isRefersh) {
                view = mActivity.showSplashProgress();
            }
            final View view1 = view;
            AuthApiHelper authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper.class);
            final retrofit2.Call<HomeBanerResponse> call = authApiHelper.homeBanerProductList("getHomeBanner", "1",ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<HomeBanerResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    if (view1 != null)
                        mActivity.cancelProgressBar(view1);
                    HomeBanerResponse homeBanerResponse = (HomeBanerResponse) object;
                    if (homeBanerResponse == null || homeBanerResponse.getResponse() == null) {
                        Toast.makeText(mActivity, mActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    productViewPagerList.addAll(homeBanerResponse.getResponse().getProductList());
                    setHomeviewPagerAdapter();
                    setHourDays(fragmentHomeBinding.tvHour, fragmentHomeBinding.tvDay, productViewPagerList.get(0));
                    isHomeBannerListLoaded = true;
                    refreshing[0] = true;
                    if (refreshing[0] && refreshing[1]) {

                        fragmentHomeBinding.swipeRefreshLayout.setRefreshing(false);
                    }
                }

                @Override
                protected void onError(RetroError retroError) {
                    if (view1 != null)
                        mActivity.cancelProgressBar(view1);
                    isHomeBannerListLoaded = false;
                    refreshing[0] = true;
                    if (refreshing[0] && refreshing[1]) {

                        fragmentHomeBinding.swipeRefreshLayout.setRefreshing(false);
                    }
                    mActivity.showNetworkLayout();
                }

                @Override
                protected void onFailure(String retroError) {
                    if (view1 != null)
                        mActivity.cancelProgressBar(view1);
                    refreshing[0] = true;
                    if (refreshing[0] && refreshing[1]) {

                        fragmentHomeBinding.swipeRefreshLayout.setRefreshing(false);
                    }
                    isHomeBannerListLoaded = false;
                }
            });
        } else {
            isHomeBannerListLoaded = false;
            mActivity.showNetworkLayout();
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
        }
    }

    private void setAdapter() {
        if (mHomeAdapter != null) {
            mHomeAdapter.notifyDataSetChanged();
        } else {
            fragmentHomeBinding.recyclerviewHome.setLayoutManager(new LinearLayoutManager(mActivity));
            mHomeAdapter = new HomeAdapter(mActivity, mProductList);
            fragmentHomeBinding.recyclerviewHome.setAdapter(mHomeAdapter);
            fragmentHomeBinding.recyclerviewHome.setNestedScrollingEnabled(false);
        }

    }

    private void getUpcomingEventList(boolean isReferishing) {
        if (mActivity.isOnline()) {

            View view = null;
            if (!isReferishing) {
                view = mActivity.showSplashProgress();
            }
            final View view1 = view;
            AuthApiHelper authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper.class);
            retrofit2.Call<UpcomingEventResponse> call = authApiHelper.homeUpcomingEvent("getEventsUpComing",ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<UpcomingEventResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    UpcomingEventResponse upcomingEventResponse = (UpcomingEventResponse) object;
                    if (upcomingEventResponse == null || upcomingEventResponse.getResponse() == null) {
                        Toast.makeText(mActivity, mActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    PrefUtils.saveToPrefs(mActivity, PrefUtils.VERSION_CODE, upcomingEventResponse.getVersion() + "");
                    PrefUtils.saveToPrefs(mActivity, PrefUtils.VERSION_NAME, upcomingEventResponse.getVersion() + "");
                    int versionCode = BuildConfig.VERSION_CODE;

                    if (upcomingEventResponse.getVersion() > versionCode) {
                        CustomAlertDialog.forceUpdate(mActivity);
                    }
                    mProductList.clear();
                    mProductList.addAll(upcomingEventResponse.getResponse().getProductList());
                    setAdapter();
                    if (view1 != null) {
                        mActivity.cancelProgressBar(view1);
                    }
                    refreshing[1] = true;
                    if (refreshing[0] && refreshing[1]) {
                        fragmentHomeBinding.swipeRefreshLayout.setRefreshing(false);
                    }
                    isEventLoaded = true;
                }

                @Override
                protected void onError(RetroError retroError) {
                    mActivity.cancelProgressBar(view1);
                    isEventLoaded = false;
                    refreshing[1] = true;
                    if (refreshing[0] && refreshing[1]) {
                        fragmentHomeBinding.swipeRefreshLayout.setRefreshing(false);
                    }
                    mActivity.showNetworkLayout();
                }

                @Override
                protected void onFailure(String retroError) {
                    refreshing[1] = true;
                    if (refreshing[0] && refreshing[1]) {
                        fragmentHomeBinding.swipeRefreshLayout.setRefreshing(false);
                    }
                    mActivity.cancelProgressBar(view1);
                    isEventLoaded = false;
                }
            });
        } else {
            isEventLoaded = false;
            mActivity.showNetworkLayout();
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
        }
    }

    private void setHomeviewPagerAdapter() {
        if (mHomeViewPagerAdapter != null) {
            mHomeViewPagerAdapter.notifyDataSetChanged();
        } else {
            mHomeViewPagerAdapter = new HomeViewPagerAdapter(mActivity, productViewPagerList);
            fragmentHomeBinding.homeViewPager.setAdapter(mHomeViewPagerAdapter);

            fragmentHomeBinding.homeViewPager.beginFakeDrag();

            fragmentHomeBinding.homeViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                public void onPageScrollStateChanged(int state) {

                }

                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                public void onPageSelected(int position) {
                    // Check if this is the page you want.
//                    setPagerItemData(position);
                }
            });
        }
        if (productViewPagerList != null && productViewPagerList.size() > 0) {
            setPagerItemData(0);
        }
    }


    @SuppressLint("SetTextI18n")
    private void setPagerItemData(int position) {
        long timeDifference = Utils.getTimeRemaining(productViewPagerList.get(position).getEventDate());
        long hours = ((((timeDifference / 1000) / 60) / 60) % 24);
        long days = ((((timeDifference / 1000) / 60) / 60) / 24) % 24;
        StringBuilder sb = new StringBuilder(Utils.parseDateToddMonth(productViewPagerList.get(position).getEventDate()).toLowerCase());
        sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
        fragmentHomeBinding.eventDate.setText(Utils.parseDateToDate(productViewPagerList.get(position).getEventDate()));
        fragmentHomeBinding.eventMonth.setText(sb.toString());
        fragmentHomeBinding.tvHour.setText("" + hours);
        fragmentHomeBinding.tvDay.setText("" + days);
        fragmentHomeBinding.eventAddress.setText(productViewPagerList.get(position).getEventAddress());
        // fragmentHomeBinding.eventDate.setText(Utils.parseDateToddMMyyyy(productViewPagerList.get(position).getEventDate()));
        fragmentHomeBinding.eventTitle.setText(productViewPagerList.get(position).getEventsTitle());
    }

    public void retry() {
        if (!isHomeBannerListLoaded) {
            getHomeBanerProductList(false);
        }
        if (!isEventLoaded) {
            getUpcomingEventList(false);
        }
    }
}


