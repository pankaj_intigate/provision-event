package provision.com.app.binder;

import android.app.Activity;
import android.content.Intent;
import android.util.Base64;
import android.view.View;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;

import org.jetbrains.annotations.NotNull;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.activity.CartActivity;
import provision.com.app.activity.WebViewActivity;
import provision.com.app.adapter.MyWalletAdapter;
import provision.com.app.apiResponse.myWalletResponse.MyWallet;
import provision.com.app.apiResponse.myWalletResponse.WalletDetail;
import provision.com.app.databinding.ActivityMyWalletBinding;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.Utils;
import provision.com.app.utils.network.api.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gauravg on 7/18/2018.
 */

public class MyWalletBinder {
    private BaseActivity activity;
    private ActivityMyWalletBinding binding;
    private MyWalletAdapter mAdapter;
    private List<WalletDetail> mWalletList;

    public MyWalletBinder(BaseActivity activity, ActivityMyWalletBinding binding) {
        this.activity = activity;
        this.binding = binding;

        onCreate();
    }

    private void onCreate() {
        binding.headerLayout.imgHeader.setVisibility(View.GONE);
        binding.headerLayout.back.setVisibility(View.VISIBLE);
        binding.headerLayout.tvTitle.setText("My Wallet");
        binding.headerLayout.cartRL.setVisibility(View.VISIBLE);
        binding.headerLayout.cartRL.setOnClickListener(v -> {
            Intent cartIntent = new Intent(activity, CartActivity.class);
            ((Activity) activity).startActivityForResult(cartIntent, 101);
            activity.overridePendingTransition(R.anim.bottom_up, R.anim.stay);
        });
        binding.headerLayout.back.setOnClickListener(v -> {
            activity.finish();
        });


        binding.tvAddBalance.setOnClickListener(v -> {
//            activity.startActivity(new Intent(activity, AddMoneyActivity.class));
            try {
                hitAddWalletService();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        });

       /* if (activity.isOnline()) {
            getMyWallet(PrefUtils.getFromPrefs(activity, PrefUtils.EMAIL_ID));
        } else {
            activity.showToast(activity, activity.getResources().getString(R.string.check_internet));
        }*/
    }

    private void hitAddWalletService() throws UnsupportedEncodingException {
        String userId = PrefUtils.getFromPrefs(activity, PrefUtils.USER_ID);
        String password = PrefUtils.getFromPrefs(activity, PrefUtils.PASSWORD);
        String socialId = PrefUtils.getFromPrefs(activity, PrefUtils.SOCIAL_ID);
        if (socialId.equals("")) {
            socialId = "0";
        }
        if (password.equals("")) {
            password = "0";
        }

        byte[] data = password.getBytes("UTF-8");
        String base64 = Base64.encodeToString(data, Base64.DEFAULT);
        String parms = userId + "/" + base64 + "/" + socialId;
        ApiClient.getAPiAuthHelper(activity).addMoney("getapplogin", ApiClient.API_VERSION).enqueue(new Callback<MyWallet>() {
            @Override
            public void onResponse(@NotNull Call<MyWallet> call, @NotNull Response<MyWallet> response) {
                assert response.body() != null;
                if (response.body().getStatus().equals("1")) {
                    activity.startActivity(new Intent(activity, WebViewActivity.class).putExtra("URL", response.body().getResponse().getUrl() + parms).putExtra("TitleName", "Add Credit"));
                } else {
                    activity.showToast(activity, response.body().getResponseMessege());
                }
            }

            @Override
            public void onFailure(@NotNull Call<MyWallet> call, @NotNull Throwable t) {

            }
        });
//        ApiClient.getAPiAuthHelper(activity).addWallet("addwallet", PrefUtils.getFromPrefs(activity, PrefUtils.EMAIL_ID), PrefUtils.getFromPrefs(activity, PrefUtils.USER_ID), binding.etAmount.getText().toString().trim(), "1", password, socialId, payType).enqueue(new Callback<MyWallet>() {
//            @Override
//            public void onResponse(Call<MyWallet> call, ShopItemEventResponse<MyWallet> response) {
//                if (response.body().getStatus().equals("1")) {
//                    activity.startActivity(new Intent(activity, WebViewActivity.class).putExtra("URL", response.body().getResponse().getUrl()).putExtra("TitleName", "Disclaimer"));
//
//                } else {
//                    activity.showToast(activity, response.body().getResponseMessege());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<MyWallet> call, Throwable t) {
//
//            }
//        });
    }

    public void getMyWallet(String email) {
        View progress = activity.showProgressBar();
        ApiClient.getAPiAuthHelper(activity).myWallet("getWalletTransactions", email, PrefUtils.getFromPrefs(activity, PrefUtils.USER_ID), PrefUtils.getFromPrefs(activity, PrefUtils.COUNTRY_CODE), ApiClient.API_VERSION).enqueue(new Callback<MyWallet>() {
            @Override
            public void onResponse(Call<MyWallet> call, Response<MyWallet> response) {
                activity.cancelProgressBar(progress);
                if (response.body() == null || response.body().getResponse() == null) {
                    Toast.makeText(activity, activity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (response.body().getStatus().equals("1")) {
                    if (response.body().getResponse().getWalletAmount() != 0) {
                        binding.tvMyBalance.setText(Utils.convertPrice(response.body().getCurrencyValue(), response.body().getResponse().getWalletAmount(), response.body().getNumberformat()) + " " + response.body().getCurrrencyCode());
                    } else {
                        binding.tvMyBalance.setText("0.00 " + response.body().getCurrrencyCode());
                    }
                    mWalletList = new ArrayList<>();
                    if (response.body().getResponse().getWalletDetail() != null && response.body().getResponse().getWalletDetail().size() > 0) {
                        mWalletList.addAll(response.body().getResponse().getWalletDetail());
                        binding.rcvMyWalletList.setLayoutManager(new LinearLayoutManager(activity));
                        mAdapter = new MyWalletAdapter(activity, mWalletList, response.body().getCurrencyValue(), response.body().getCurrrencyCode(), response.body().getNumberformat());
                        binding.rcvMyWalletList.setAdapter(mAdapter);
                        binding.rcvMyWalletList.setVisibility(View.VISIBLE);
                        binding.tvNoData.setVisibility(View.GONE);
                    } else {
                        binding.rcvMyWalletList.setVisibility(View.GONE);
                        binding.tvNoData.setVisibility(View.VISIBLE);
                    }
                } else {
                    binding.rcvMyWalletList.setVisibility(View.GONE);
                    binding.tvNoData.setVisibility(View.VISIBLE);
                    binding.tvNoData.setText(response.body().getResponseMessege());
                    binding.tvMyBalance.setText("KWD 0.00");
                }
            }

            @Override
            public void onFailure(Call<MyWallet> call, Throwable t) {
                activity.cancelProgressBar(progress);
            }
        });
    }

}
