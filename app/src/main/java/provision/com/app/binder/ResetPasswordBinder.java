package provision.com.app.binder;

import android.view.View;

import provision.com.app.activity.ResetPasswordActivity;
import provision.com.app.databinding.ActivityResetPasswordBinding;

/**
 * Created by pankajk on 8/21/2018.
 * All rights reserved by Intigate Technologies.
 */

public class ResetPasswordBinder {
    private ActivityResetPasswordBinding binding;
    private ResetPasswordActivity resetPasswordActivity;

    public ResetPasswordBinder(ActivityResetPasswordBinding binding, ResetPasswordActivity resetPasswordActivity) {
        this.binding = binding;
        this.resetPasswordActivity = resetPasswordActivity;
        onCreate();
    }

    private void onCreate() {
        binding.headerLayout.headerDividerLine.setVisibility(View.GONE);
        binding.headerLayout.imgHeader.setVisibility(View.GONE);
        binding.headerLayout.back.setVisibility(View.VISIBLE);
        binding.headerLayout.tvTitle.setText("");

        binding.headerLayout.back.setOnClickListener(v -> {
            resetPasswordActivity.finish();
        });



       /* if (activity.isOnline()) {
            getMyWallet(PrefUtils.getFromPrefs(activity, PrefUtils.EMAIL_ID));
        } else {
            activity.showToast(activity, activity.getResources().getString(R.string.check_internet));
        }*/
    }
}
