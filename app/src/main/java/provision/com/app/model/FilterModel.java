package provision.com.app.model;

/**
 * Created by gauravg on 7/13/2018.
 */

public class FilterModel {
    private String categoryName;
    private int categoryId;
    private String categorySelectedValue;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategorySelectedValue() {
        return categorySelectedValue;
    }

    public void setCategorySelectedValue(String categorySelectedValue) {
        this.categorySelectedValue = categorySelectedValue;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
