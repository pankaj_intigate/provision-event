package provision.com.app.model;

/**
 * Created by gauravg on 7/5/2018.
 */

public class DataModel {
    String title, date, time, date_time, address;
    String amount;
    int type;

    public DataModel(String title, String date, String time, String date_time, String address, int type) {
        this.title = title;
        this.date = date;
        this.time = time;
        this.date_time = date_time;
        this.address = address;
        this.type = type;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
