package provision.com.app.interfaces

interface OnItemFilterSelectListener {
     fun onItemSelected(position:Int,itemId:String,filterType:String,itemName:String,flag:String)
     fun onItemUnSelected(position:Int,itemId:String,filterType:String,itemName:String,flag:String)
}