package provision.com.app.interfaces

interface MinMaxPrice {
    fun onMinMaxPrice(minPrice:String, maxPrice:String)
}