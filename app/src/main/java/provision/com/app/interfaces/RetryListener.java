package provision.com.app.interfaces;

/**
 * Created by pankajk on 9/28/2018.
 * All rights reserved by Intigate Technologies.
 */
public interface RetryListener {
    void onRetry();
}
