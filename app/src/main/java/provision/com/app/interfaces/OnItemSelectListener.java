package provision.com.app.interfaces;

import provision.com.app.adapter.ItemSizeAdapter;

/**
 * Created by pankajk on 10/18/2018.
 * All rights reserved by Intigate Technologies.
 */
public interface OnItemSelectListener {
    void onItemSelect(int parentPosition, int childPosition,ItemSizeAdapter itemSizeAdapter);
}
