package provision.com.app.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import provision.com.app.R;
import provision.com.app.apiResponse.recomded_itm.ItemSize;
import provision.com.app.interfaces.OnItemSelectListener;

/**
 * Created by pankajk on 10/17/2018.
 * All rights reserved by Intigate Technologies.
 */
public class ItemSizeAdapter extends RecyclerView.Adapter<ItemSizeAdapter.ViewHolder> {
    private Context context;
    private List<ItemSize> mList;
    private int selectedItemPosition = -1;
    private int parentItemPosition;
    private OnItemSelectListener onItemSelectListener;


    public ItemSizeAdapter(Context context, List<ItemSize> mList, int parentItemPosition, OnItemSelectListener onItemSelectListener) {
        this.context = context;
        this.mList = mList;
        this.parentItemPosition = parentItemPosition;
        this.onItemSelectListener = onItemSelectListener;
    }

    public int getSelectedItemPosition() {
        return selectedItemPosition;
    }

    public void setSelectedItemPosition(int selectedItemPosition) {
        this.selectedItemPosition = selectedItemPosition;
        mList.get(selectedItemPosition).setSelected(true);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ItemSizeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_size_layout, parent, false);

        return new ItemSizeAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemSizeAdapter.ViewHolder holder, int position) {
        holder.tvItemSize.setText(mList.get(position).getVValue());
        if (mList.get(position).isSelected()) {
            selectedItemPosition = position;
            holder.tvItemSize.setBackground(ContextCompat.getDrawable(context, R.drawable.selected_size));
            holder.tvItemSize.setTextColor(ContextCompat.getColor(context, R.color.white));
        } else {
            holder.tvItemSize.setBackground(ContextCompat.getDrawable(context, R.drawable.unselected_size));
            holder.tvItemSize.setTextColor(ContextCompat.getColor(context, R.color.inputHint));
        }
        holder.tvItemSize.setOnClickListener(v -> {
            onItemSelectListener.onItemSelect(parentItemPosition, position, this);
            if (selectedItemPosition != -1) {
                mList.get(selectedItemPosition).setSelected(false);

            }

            mList.get(position).setSelected(true);
            selectedItemPosition = position;
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvItemSize;

        public ViewHolder(View itemView) {
            super(itemView);
            tvItemSize = itemView.findViewById(R.id.tv_item_size);
        }
    }
}
