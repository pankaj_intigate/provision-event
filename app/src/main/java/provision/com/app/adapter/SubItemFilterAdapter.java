package provision.com.app.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import provision.com.app.R;
import provision.com.app.model.DataModel;

/**
 * Created by gauravg on 7/13/2018.
 */

public class SubItemFilterAdapter extends RecyclerView.Adapter<SubItemFilterAdapter.ViewHolder> {
    Context context;
    List<DataModel> list;

    public SubItemFilterAdapter(Context context, List<DataModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_sub_filter_data, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvValue;

        public ViewHolder(View itemView) {
            super(itemView);
            tvValue = itemView.findViewById(R.id.tvValue);
        }
    }
}
