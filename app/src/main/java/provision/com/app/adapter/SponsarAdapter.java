package provision.com.app.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.apiResponse.sponserResponse.Sponser;

/**
 * Created by amitk on 16-07-2018.
 */

public class SponsarAdapter extends RecyclerView.Adapter<SponsarAdapter.ViewHolder> {
    private Context context;
    private List<Sponser> sponsorList;
    private BaseActivity baseActivity;

    public SponsarAdapter(Context context, List<Sponser> organizersList) {
        this.context = context;
        this.sponsorList = organizersList;
        baseActivity = (BaseActivity) context;
    }

    @NonNull
    @Override
    public SponsarAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_organiser, parent, false);
        return new SponsarAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SponsarAdapter.ViewHolder holder, int position) {
        holder.itemView.post(new Runnable() {
            @Override
            public void run() {

                int cellWidth = holder.itemView.getWidth();// this will give you cell width dynamically
                holder.itemView.getLayoutParams().height = cellWidth;

            }
        });
     /*   Glide.with(context)
                .load(sponsorList.get(position).getThumbImg())
                .into(holder.organiserLogo);*/
        DisplayMetrics displayMetrics = new DisplayMetrics();
        baseActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels * 18 / 100;
        int width = displayMetrics.widthPixels * 40 / 100;
        holder.organiserLogo.getLayoutParams().height = height;
        holder.organiserLogo.getLayoutParams().width = width;
        Picasso.get()
                .load(TextUtils.isEmpty(sponsorList.get(position).getThumbImg()) ? "null" : sponsorList.get(position).getThumbImg())
                .placeholder(R.drawable.footer_logo)
                .into(holder.organiserLogo);

    }

    @Override
    public int getItemCount() {
        return sponsorList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView organiserLogo;

        public ViewHolder(View itemView) {
            super(itemView);
            organiserLogo = itemView.findViewById(R.id.organiserLogo);
        }
    }
}
