package provision.com.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.NotificationDetailsActivity;
import provision.com.app.apiResponse.notification.NotificationData;
import provision.com.app.utils.CustomTextView;

/**
 * Created by pankajk on 9/21/2018.
 * All rights reserved by Intigate Technologies.
 */
public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.ViewHolder> {
    private Context context;
    private List<NotificationData> list;

    public NotificationListAdapter(Context context, List<NotificationData> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.notification_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NotificationData notificationData = list.get(position);
        holder.tvTitle.setText(notificationData.getTitle());
        holder.tvMessage.setText(notificationData.getContent());
        holder.tvDate.setText(notificationData.getCreateDate());
        if (notificationData.getRead()==0){
            holder.tvTitle.setTypeface(Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf"));
            holder.tvMessage.setTypeface(Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf"));
            holder.tvDate.setTypeface(Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf"));
        }else {
            holder.tvTitle.setTypeface(Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf"));
            holder.tvMessage.setTypeface(Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf"));
            holder.tvDate.setTypeface(Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf"));

        }
        holder.parentPanel.setOnClickListener(v -> {
            notificationData.setRead(1);
            notifyDataSetChanged();
            (context).startActivity(new Intent(context, NotificationDetailsActivity.class).putExtra("notification", notificationData));
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout parentPanel;
        CustomTextView tvTitle, tvMessage, tvDate;

        public ViewHolder(View itemView) {
            super(itemView);
            parentPanel = itemView.findViewById(R.id.parentPanel);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvMessage = itemView.findViewById(R.id.tvMessage);
            tvDate = itemView.findViewById(R.id.tvDate);
        }
    }
}
