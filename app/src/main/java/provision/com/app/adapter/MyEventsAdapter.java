package provision.com.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.provider.CalendarContract;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.Calendar;
import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.EventDialogActivity;
import provision.com.app.activity.MyEventDetailsActivity;
import provision.com.app.apiResponse.event_list.EventList;
import provision.com.app.utils.Utils;

/**
 * Created by gauravg on 7/10/2018.
 */

public class MyEventsAdapter extends RecyclerView.Adapter<MyEventsAdapter.ViewHolder> {
    private Context context;
    private List<EventList> list;

    public MyEventsAdapter(Context context, List<EventList> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_my_events, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        EventList eventList = list.get(position);
        if (eventList.getVirtualEvent() == 1) {
            holder.eventDateTextView.setText(Utils.parseDateToddMMyyyy(eventList.getEventStartDate()) + "-" + Utils.parseDateToddMMyyyy(eventList.getEventEndDate()));
        }else {
            holder.eventDateTextView.setText(Utils.parseDateToddMMyyyy(eventList.getEventDate()));
        }
        holder.eventNameTextView.setText(eventList.getEventName());
        holder.eventLocationTextView.setText(eventList.getAddress());
        holder.tvParticipateName.setText(eventList.getParticipateName());
        holder.ll_parent.setTag(position);
        holder.addToCalendar.setOnClickListener(v -> {
            addToCalender(eventList.getEventDate(), eventList.getEventName(), eventList.getAddress());
        });
        Glide.with(context)
                .load(eventList.getQrCode())
                .into(holder.imageViewQrCode);
        holder.ll_parent.setOnClickListener(v -> {
            context.startActivity(new Intent(context, EventDialogActivity.class).putExtra("participateId", list.get(position).getParticipateId()));

        });
        holder.tvInvoice.setOnClickListener(v -> {
            context.startActivity(new Intent(context, MyEventDetailsActivity.class).putExtra("bookingId", list.get(position).getBookingId()));


        });
    }

    @Override
    public int getItemCount() {
        return list.size();
//        return 10;
    }

    private void addToCalender(String date, String title, String location) {
        Calendar beginTime = Calendar.getInstance();
        beginTime.set(Utils.getSaprateDate(date, "yyyy"), Utils.getSaprateDate(date, "MM") - 1, Utils.getSaprateDate(date, "dd"),
                Utils.getSaprateDate(date, "HH"), Utils.getSaprateDate(date, "mm"));
//        Calendar endTime = Calendar.getInstance();
//        endTime.set(2012, 0, 19, 8, 30);
        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
//                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis())
                .putExtra(CalendarContract.Events.TITLE, title)
                .putExtra(CalendarContract.Events.DESCRIPTION, "")
                .putExtra(CalendarContract.Events.EVENT_LOCATION, location)
                .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY)
                .putExtra(Intent.EXTRA_EMAIL, "");
        context.startActivity(intent);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout ll_parent;
        TextView eventNameTextView, eventDateTextView, eventLocationTextView, tvParticipateName, addToCalendar, tvInvoice;
        ImageView imageViewQrCode;

        public ViewHolder(View itemView) {
            super(itemView);
            tvInvoice = itemView.findViewById(R.id.tvInvoice);
            addToCalendar = itemView.findViewById(R.id.addToCalendar);
            tvParticipateName = itemView.findViewById(R.id.tvParticipateName);
            imageViewQrCode = itemView.findViewById(R.id.imageViewQrCode);
            ll_parent = itemView.findViewById(R.id.ll_parent);
            eventNameTextView = itemView.findViewById(R.id.eventNameTextView);
            eventDateTextView = itemView.findViewById(R.id.eventDateTextView);
            eventLocationTextView = itemView.findViewById(R.id.eventLocationTextView);
        }
    }
}
