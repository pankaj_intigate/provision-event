package provision.com.app.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.ImageDialogActivity;
import provision.com.app.apiResponse.raceTrackResponse.Racetrack;

/**
 * Created by gauravg on 7/12/2018.
 */

public class RaceTrackAdapter extends RecyclerView.Adapter<RaceTrackAdapter.ViewHolder> {
    private Context context;
    private List<Racetrack> racetrackList;

    public RaceTrackAdapter(Context context, List<Racetrack> racetrackList) {
        this.context = context;
        this.racetrackList = racetrackList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_race_track, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
      /*  Glide.with(context)
                .load(racetrackList.get(position).getThumbImg())
                .into(holder.raceTrackImage);*/
        Picasso.get()
                .load(TextUtils.isEmpty(racetrackList.get(position).getThumbImg()) ? "null" : racetrackList.get(position).getThumbImg())
                .placeholder(R.drawable.placeholder_logo)
                .into(holder.raceTrackImage);

        holder.ll_parent.setOnClickListener(v -> {
            context.startActivity(new Intent(context, ImageDialogActivity.class).putExtra("Image_Key", racetrackList.get(position).getThumbImg()));
        });
    }

    @Override
    public int getItemCount() {
        return racetrackList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView raceTrackImage;
        private RelativeLayout ll_parent;

        public ViewHolder(View itemView) {
            super(itemView);
            raceTrackImage = itemView.findViewById(R.id.raceTrackImage);
            ll_parent = itemView.findViewById(R.id.ll_parent);
        }
    }
}
