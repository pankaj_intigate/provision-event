package provision.com.app.adapter;

import android.content.Context;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import provision.com.app.R;
import provision.com.app.apiResponse.faqResponse.EventFaq;

/**
 * Created by gauravg on 7/12/2018.
 */

public class FAQAdapter extends RecyclerView.Adapter<FAQAdapter.ViewHolder> {
    Context context;
    List<EventFaq> list;
    private boolean isClick = true;
    private Typeface font;

    public FAQAdapter(Context context, List<EventFaq> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_faq, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.tvQuestion.setText(list.get(position).getQuestion());
        holder.tvAnswer.setText(list.get(position).getAnswer());
        font = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
        holder.tvQuestion.setTypeface(font);
        holder.ll_fag.setOnClickListener(v -> {
            if (isClick) {
                font = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
                holder.tvAnswer.setVisibility(View.VISIBLE);
                isClick = false;
                holder.ivArrow.setRotation(90);
                holder.tvQuestion.setTextColor(context.getResources().getColor(R.color.black));
                holder.tvQuestion.setTypeface(font);
                holder.tvQuestion.setMaxLines(10);
                holder.tvQuestion.setEllipsize(TextUtils.TruncateAt.END);
            } else {
                font = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
                holder.tvAnswer.setVisibility(View.GONE);
                isClick = true;
                holder.ivArrow.setRotation(0);
                holder.tvQuestion.setTextColor(context.getResources().getColor(R.color.black));
                holder.tvQuestion.setTypeface(font);
                holder.tvQuestion.setMaxLines(1);
                holder.tvQuestion.setEllipsize(TextUtils.TruncateAt.END);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvQuestion, tvAnswer;
        LinearLayout ll_fag;
        ImageView ivArrow;

        public ViewHolder(View itemView) {
            super(itemView);
            tvQuestion = itemView.findViewById(R.id.tvQuestion);
            tvAnswer = itemView.findViewById(R.id.tvAnswer);
            ll_fag = itemView.findViewById(R.id.ll_fag);
            ivArrow = itemView.findViewById(R.id.ivArrow);
        }
    }
}
