package provision.com.app.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import provision.com.app.R;
import provision.com.app.apiResponse.RacekitResponse.Racekit;

/**
 * Created by gauravg on 7/11/2018.
 */

public class RaceKitAdapter extends RecyclerView.Adapter<RaceKitAdapter.ViewHolder> {
    private Context context;
    private List<Racekit> racekitList;

    public RaceKitAdapter(Context context, List<Racekit> racekitList) {
        this.context = context;
        this.racekitList = racekitList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_racekit, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.trackitItemName.setText(racekitList.get(position).getKitName());
        Glide.with(context.getApplicationContext())
                .load(racekitList.get(position).getThumbImg())
                .placeholder(R.drawable.footer_logo)
                .into((holder.ivItem));

//        holder.ll_parent.setOnClickListener(v -> {
//            context.startActivity(new Intent(context, ImageDialogActivity.class).putExtra("Image_Key", racekitList.get(position).getThumbImg()));
//        });
    }

    @Override
    public int getItemCount() {
        return racekitList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivItem;
        private TextView trackitItemName;
        private LinearLayout ll_parent;

        public ViewHolder(View itemView) {
            super(itemView);
            ivItem = itemView.findViewById(R.id.ivItem);
            trackitItemName = itemView.findViewById(R.id.trackitItemName);
            ll_parent = itemView.findViewById(R.id.ll_parent);
        }
    }
}
