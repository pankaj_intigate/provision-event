package provision.com.app.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.WebViewActivity;
import provision.com.app.apiResponse.resultResponse.EventList;
import provision.com.app.utils.Utils;

import static provision.com.app.app.AppConstant.RESULTS_DETAILS_EVENT_TOKEN;
import static provision.com.app.utils.CommonUtils.onTrackEventClick;

/**
 * Created by amitk on 19-07-2018.
 */

public class ResultAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private List<EventList> productLists;

    public ResultAdapter(Context context, List<EventList> productLists) {
        this.mContext = context;
        this.productLists = productLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_result, parent, false);
        return new ResultAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ResultAdapter.ViewHolder holder1 = (ResultAdapter.ViewHolder) holder;
        EventList producIttem = productLists.get(position);
        holder1.eventTitle.setText(producIttem.getEventsTitle());
        holder1.eventDate.setText(Utils.parseDateToddMMyyyy(producIttem.getEventDate()));
        if (producIttem.getEventAddress() == null || producIttem.getEventAddress().isEmpty()) {
            holder1.eventAddress.setVisibility(View.GONE);
        } else {
            holder1.eventAddress.setVisibility(View.VISIBLE);
            holder1.eventAddress.setText(producIttem.getEventAddress());
        }
    }

    @Override
    public int getItemCount() {
        return productLists.size() > 0 ? productLists.size() : 0;
    }

    private void setHourDays(ResultAdapter.ViewHolder homeViewHolderSecond, EventList productItem) {
        long timeDifference = Utils.getTimeRemaining(productItem.getEventDate());
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = timeDifference / daysInMilli;
        timeDifference = timeDifference % daysInMilli;
        long elapsedHours = timeDifference / hoursInMilli;


        if (elapsedHours < 10) {
//            homeViewHolderSecond.hourText.setText("0" + elapsedHours);
        } else {
//            homeViewHolderSecond.hourText.setText("" + elapsedHours);
        }

        if (elapsedDays < 10) {
//            homeViewHolderSecond.dayText.setText("0" + elapsedDays);
        } else {
//            homeViewHolderSecond.dayText.setText("" + elapsedDays);
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private TextView eventTitle, eventDate, eventAddress;
        private RelativeLayout rlParent;

        public ViewHolder(View itemView) {
            super(itemView);
            eventTitle = itemView.findViewById(R.id.eventTitle);
            eventDate = itemView.findViewById(R.id.eventDate);
            eventAddress = itemView.findViewById(R.id.eventAddress);
            rlParent = itemView.findViewById(R.id.rlParent);


            rlParent.setOnClickListener(v -> {
                Intent intent = new Intent(mContext, WebViewActivity.class);
                intent.putExtra("TitleName", "Results");
                intent.putExtra("URL", productLists.get(getAdapterPosition()).getResultCodeUrl());
                mContext.startActivity(intent);
                onTrackEventClick(RESULTS_DETAILS_EVENT_TOKEN);
            });

        }
    }

}
