package provision.com.app.adapter;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

import provision.com.app.model.TabFragmentData;
import provision.com.app.utils.AppConstant;

/**
 * Created by amitk on 13-07-2018.
 */

public class EventDetailPagerAdapter extends FragmentPagerAdapter {

    ArrayList<TabFragmentData> tabFragmentDataList;

    public EventDetailPagerAdapter(FragmentManager fm, ArrayList<TabFragmentData> tabFragmentDataList) {
        super(fm);
        this.tabFragmentDataList = tabFragmentDataList;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = tabFragmentDataList.get(position).getmFragment();
        if (tabFragmentDataList.get(position).getTabId() != "13") {
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.KEY_SELECTED_TAB_ID, tabFragmentDataList.get(position).getTabId());
            bundle.putString(AppConstant.KEY_SELECTED_TAB_NAME, tabFragmentDataList.get(position).getTabTitle());
            bundle.putString(AppConstant.KEY_SELECTED_TAB_ID, tabFragmentDataList.get(position).getTabId());
            bundle.putString(AppConstant.KEY_SELECTED_TAB_EVENT_ID, tabFragmentDataList.get(position).getEventId());
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return tabFragmentDataList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabFragmentDataList.get(position).getTabTitle();
    }


}