package provision.com.app.adapter

import android.content.Context
import androidx.viewpager.widget.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_product_image_layout.view.*
import provision.com.app.R
import provision.com.app.apiResponse.event_shop.ItemImage

class EventProductImageAdapter (var context: Context, var productImageList:ArrayList<ItemImage>): PagerAdapter() {
    override fun isViewFromObject(p0: View, p1: Any): Boolean {
        return p0 == p1
    }

    override fun getCount(): Int {
        return productImageList.size
    }


    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val layout: ViewGroup = LayoutInflater.from(context).inflate(R.layout.item_product_image_layout,container,false) as ViewGroup
        container.addView(layout)

        Glide.with(context)
                .load(productImageList.get(position).imageUrl)
                .fitCenter()
                .into(layout.product_image)
        return layout
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}