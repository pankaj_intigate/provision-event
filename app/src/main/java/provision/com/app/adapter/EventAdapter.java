package provision.com.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.EventDetailsActivity;
import provision.com.app.apiResponse.eventSearchResponse.ProductList;
import provision.com.app.utils.Utils;
import provision.com.app.utils.network.api.ApiConstant;

/**
 * Created by gauravg on 7/9/2018.
 */

public class EventAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private List<ProductList> productLists;

    public EventAdapter(Context context, List<ProductList> productLists) {
        this.mContext = context;
        this.productLists = productLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_home_second, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder holder1 = (ViewHolder) holder;
        ProductList producIttem = productLists.get(position);
        Glide.with(mContext)
                .load(producIttem.getEventImage())
                .placeholder(R.drawable.placeholder_logo)
                .into(holder1.imageMain);
        holder1.eventTitle.setText(producIttem.getEventsTitle());
        if (!producIttem.getEventAddress().isEmpty()) {
            StringBuilder sb = new StringBuilder(producIttem.getEventAddress());
            sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
            holder1.eventAddress.setText(sb.toString());
        }
        holder1.eventDate.setText(Utils.parseDateToDate(producIttem.getEventDate()));
//        holder1.eventMonth.setText(Utils.parseDateToddMonth(producIttem.getEventDate()));
        StringBuilder sb = new StringBuilder(Utils.parseDateToddMonth(producIttem.getEventDate()).toLowerCase());
        sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
        holder1.eventMonth.setText(sb.toString());
        if (TextUtils.isEmpty(producIttem.getIsRegisteration())) {
            holder1.isResgestrationText.setVisibility(View.GONE);
        }
        holder1.isResgestrationText.setText(producIttem.getIsRegisteration());
        if (!TextUtils.isEmpty(producIttem.getIsColor())) {
            holder1.isResgestrationText.setBackgroundColor(Color.parseColor(producIttem.getIsColor()));
        }

        setHourDays(holder1, producIttem);
    }

    @Override
    public int getItemCount() {
        return productLists.size();
    }

    private void setHourDays(ViewHolder homeViewHolderSecond, ProductList productItem) {
        long timeDifference = Utils.getTimeRemaining(productItem.getEventDate());
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = timeDifference / daysInMilli;
        timeDifference = timeDifference % daysInMilli;
        long elapsedHours = timeDifference / hoursInMilli;


        if (elapsedHours < 0) {
            homeViewHolderSecond.hourText.setText("00");
        } else if (elapsedHours < 10) {
            homeViewHolderSecond.hourText.setText("0" + elapsedHours);
        } else {
            homeViewHolderSecond.hourText.setText("" + elapsedHours);
        }

        if (elapsedDays < 0) {
            homeViewHolderSecond.dayText.setText("00");
        } else if (elapsedDays < 10) {
            homeViewHolderSecond.dayText.setText("0" + elapsedDays);
        } else {
            homeViewHolderSecond.dayText.setText("" + elapsedDays);
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private TextView dayText, hourText, isResgestrationText, eventMonth;
        private ImageView imageMain;
        private TextView eventTitle, eventDate, eventAddress;
        private RelativeLayout rlParent;

        public ViewHolder(View itemView) {
            super(itemView);
            dayText = itemView.findViewById(R.id.tv_day);
            hourText = itemView.findViewById(R.id.tv_hour);
            imageMain = itemView.findViewById(R.id.img_main);
            isResgestrationText = itemView.findViewById(R.id.isResgestrationText);
            eventTitle = itemView.findViewById(R.id.eventTitle);
            eventDate = itemView.findViewById(R.id.eventDate);
            eventAddress = itemView.findViewById(R.id.eventAddress);
            eventMonth = itemView.findViewById(R.id.eventMonth);
            rlParent = itemView.findViewById(R.id.rlParent);
            rlParent.setOnClickListener(v -> {
                Intent eventDetailIntent = new Intent(mContext, EventDetailsActivity.class);
                eventDetailIntent.putExtra(ApiConstant.KEY_EVENT_ID, productLists.get(getAdapterPosition()).getId());
                eventDetailIntent.putExtra(ApiConstant.KEY_EVENT_NAME, productLists.get(getAdapterPosition()).getEventsTitle());
                ((Activity) mContext).startActivityForResult(eventDetailIntent, 101);
            });

        }
    }

}
