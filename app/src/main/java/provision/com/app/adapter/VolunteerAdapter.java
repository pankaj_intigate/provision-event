package provision.com.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import provision.com.app.R;
import provision.com.app.apiResponse.volunteerResponse.Volunteer;

/**
 * Created by gauravg on 7/12/2018.
 */

public class VolunteerAdapter extends RecyclerView.Adapter<VolunteerAdapter.ViewHolder> {
    private Context mContext;
    private List<Volunteer> mVolunteerList;

    public VolunteerAdapter(Context context, List<Volunteer> mVolunteerList) {
        this.mContext = context;
        this.mVolunteerList = mVolunteerList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_volunteer, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mDescription.setText(mVolunteerList.get(position).getDescription());
        holder.ivVisitWebsite.setOnClickListener((View v) -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mVolunteerList.get(position).getUrl()));
            mContext.startActivity(browserIntent);
        });
   /*     Glide.with(mContext)
                .load(mVolunteerList.get(position).getThumbImg())
                .into(holder.volunteerImage);*/

        Picasso.get()
                .load(TextUtils.isEmpty(mVolunteerList.get(position).getThumbImg()) ? "null" : mVolunteerList.get(position).getThumbImg())
                .placeholder(R.drawable.footer_logo)
                .into(holder.volunteerImage);

    }

    @Override
    public int getItemCount() {
        return mVolunteerList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView volunteerImage;
        private TextView mDescription, ivVisitWebsite;

        public ViewHolder(View itemView) {
            super(itemView);
            if (itemView != null) {
                volunteerImage = itemView.findViewById(R.id.volunteerImage);
                ivVisitWebsite = itemView.findViewById(R.id.ivVisitWebsite);
                mDescription = itemView.findViewById(R.id.tv_description);
            }
        }
    }
}
