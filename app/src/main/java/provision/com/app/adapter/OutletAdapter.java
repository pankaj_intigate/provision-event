package provision.com.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import provision.com.app.R;
import provision.com.app.apiResponse.outletResponse.Outlet;

/**
 * Created by gauravg on 7/12/2018.
 */

public class OutletAdapter extends RecyclerView.Adapter<OutletAdapter.ViewHolder> {
    private Context context;
    private List<Outlet> outLetList;

    public OutletAdapter(Context context, List<Outlet> outLetList) {
        this.context = context;
        this.outLetList = outLetList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_outlet, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Outlet outlet = outLetList.get(position);
        holder.locationText.setText(outlet.getAddress());
        holder.emailText.setText(outlet.getEmail());
        holder.contactNumberText.setText(outlet.getPhoneNo());
        holder.provisionCenterText.setText(outlet.getName());
        holder.emailText.setOnClickListener(v -> {
//            Intent intent = new Intent(Intent.ACTION_SENDTO);
//            intent.setType("plain/text");
//            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{holder.emailText.getText().toString()});
//            intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
//            intent.putExtra(Intent.EXTRA_TEXT, "mail body");
//            context.startActivity(Intent.createChooser(intent, ""));
            try {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{holder.emailText.getText().toString()});
                intent.putExtra(Intent.EXTRA_SUBJECT, "App feedback");
               context. startActivity(intent);
            } catch (android.content.ActivityNotFoundException ex) {

            }
        });
        holder.contactNumberText.setOnClickListener(v -> {
            Uri uri = Uri.parse("smsto:" + outlet.getPhoneNo());
            Intent i = new Intent(Intent.ACTION_SENDTO, uri);
            i.putExtra("sms_body", "Hello");
            i.setPackage("com.whatsapp");
            context.startActivity(i);
        });

    }

    @Override
    public int getItemCount() {
        return outLetList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView locationText, emailText, contactNumberText, provisionCenterText;

        public ViewHolder(View itemView) {
            super(itemView);
            locationText = itemView.findViewById(R.id.locationText);
            emailText = itemView.findViewById(R.id.emailText);
            contactNumberText = itemView.findViewById(R.id.contactNumberText);
            provisionCenterText = itemView.findViewById(R.id.provisionCenterText);


        }
    }
}
