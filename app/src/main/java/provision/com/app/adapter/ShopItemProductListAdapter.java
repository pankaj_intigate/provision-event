package provision.com.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import provision.com.app.R;
import provision.com.app.apiResponse.shop_product_list.ProductDetails;
import provision.com.app.shop.productdetails.ProductDetailsActivity;
import provision.com.app.utils.Utils;

public class ShopItemProductListAdapter extends RecyclerView.Adapter<ShopItemProductListAdapter.ViewHolder> {
    private Context context;
    private double currencyValue;
    private String currencyCode;
    private ArrayList<ProductDetails> productList;

    public ShopItemProductListAdapter(Context context, ArrayList<ProductDetails> productList, double currencyValue, String currencyCode) {
        this.context = context;
        this.productList = productList;
        this.currencyValue = currencyValue;
        this.currencyCode = currencyCode;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.shop_item_adaptor, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.product_name_tv.setText(productList.get(i).getItemTitle());
        viewHolder.tv_price.setText(Utils.convertPrice(productList.get(i).getSalesPrice(), currencyValue, 3, "")+" " + currencyCode);
        viewHolder.tv_old_price.setText(Utils.convertPrice(productList.get(i).getBasePrice(), currencyValue, 3, "")+" "  + currencyCode);
        viewHolder.tv_old_price.setPaintFlags(viewHolder.tv_old_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        if (productList.get(i).getBasePrice() == productList.get(i).getSalesPrice()) {
            viewHolder.tv_old_price.setVisibility(View.GONE);

        } else {
            viewHolder.tv_old_price.setVisibility(View.VISIBLE);
            if (productList.get(i).getBasePrice() <= 0) {
                viewHolder.tv_old_price.setVisibility(View.GONE);
            }
        }
        if (productList.get(i).getBasePrice() <= 0) {
            viewHolder.tv_old_price.setVisibility(View.GONE);
        }
        Glide.with(context).load(productList.get(i).getItemImg())
                .placeholder(R.drawable.placeholder_logo)
                .into(viewHolder.item_image);

        viewHolder.rlParent.setOnClickListener(v -> {
            ((Activity) context).startActivityForResult(new Intent(context, ProductDetailsActivity.class).putExtra("ProductSubId", productList.get(i).getSubid()),101);
        });
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView product_name_tv, tv_price, tv_old_price;
        RelativeLayout rlParent;
        ImageView item_image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            product_name_tv = itemView.findViewById(R.id.product_name_tv);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_old_price = itemView.findViewById(R.id.tv_old_price);
            rlParent = itemView.findViewById(R.id.rlParent);
            item_image = itemView.findViewById(R.id.item_image);
        }
    }


}
