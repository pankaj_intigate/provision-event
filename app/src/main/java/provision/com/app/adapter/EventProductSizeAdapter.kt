package provision.com.app.adapter

import android.content.Context
import android.graphics.Paint
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_product_size_layout.view.*
import provision.com.app.R
import provision.com.app.apiResponse.event_shop.ItemSize
import provision.com.app.shop.event_shop_item.RefreshItemOnChanage

class EventProductSizeAdapter(var context: Context, var colorAttr: ArrayList<ItemSize>,var parrentPosition:Int,var refreshItemOnChanage: RefreshItemOnChanage) : RecyclerView.Adapter<EventProductSizeAdapter.ViewHolder>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_product_size_layout,p0,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return colorAttr.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, p1: Int) {
//        viewHolder.itemView.tvProductItemSize.text = colorAttr[p1].vValue
//        if (colorAttr[p1].isselected == 1) {
//            viewHolder.itemView.tvProductItemSize.background = ContextCompat.getDrawable(context, R.drawable.size_selected)
//        } else {
//            viewHolder.itemView.tvProductItemSize.background = ContextCompat.getDrawable(context, R.drawable.size_unselected)
//        }
//        viewHolder.itemView.tvProductItemSize.setOnClickListener(object :View.OnClickListener{
//            override fun onClick(v: View?) {
//                refreshItemOnChanage.onRefresh(parrentPosition,"",colorAttr[p1].sizeId.toString(),false,p1)
//            }
//
//        })


        if (colorAttr[p1].isselected == 1) {
            viewHolder.itemView.tvProductItemSize.background = ContextCompat.getDrawable(context, R.drawable.size_selected)
            viewHolder.itemView.tvProductItemSize.setPaintFlags(0)
            viewHolder.itemView.tvProductItemSize.setTextColor(ContextCompat.getColor(context,R.color.black))
        } else if (colorAttr[p1].isAvailability == 1) {
            viewHolder.itemView.tvProductItemSize.background = ContextCompat.getDrawable(context, R.drawable.size_unselected)
            viewHolder.itemView.tvProductItemSize.setPaintFlags(0)
            viewHolder.itemView.tvProductItemSize.setTextColor(ContextCompat.getColor(context,R.color.black))
        } else if (colorAttr[p1].isAvailability == 2) {
            viewHolder.itemView.tvProductItemSize.background = ContextCompat.getDrawable(context, R.drawable.item_not_available)
            viewHolder.itemView.tvProductItemSize.setPaintFlags(viewHolder.itemView.tvProductItemSize.getPaintFlags() or  Paint.STRIKE_THRU_TEXT_FLAG)
            viewHolder.itemView.tvProductItemSize.setTextColor(ContextCompat.getColor(context,R.color.colorLightGray))
        } else if (colorAttr[p1].isAvailability == 3) {
            viewHolder.itemView.tvProductItemSize.background = ContextCompat.getDrawable(context, R.drawable.size_unselected)
            viewHolder.itemView.tvProductItemSize.setPaintFlags(0)
            viewHolder.itemView.tvProductItemSize.setTextColor(ContextCompat.getColor(context,R.color.black))
        }
        viewHolder.itemView.tvProductItemSize.text = colorAttr.get(p1).vValue

        viewHolder.itemView.tvProductItemSize.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
//                if (colorAttr[p1].isAvailability == 3) {
//                    return
//                }
                refreshItemOnChanage.onRefresh(parrentPosition, "", colorAttr[p1].sizeId.toString(),false,p1)
            }

        })
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}