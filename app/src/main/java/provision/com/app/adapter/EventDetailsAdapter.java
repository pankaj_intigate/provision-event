package provision.com.app.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.EventDialogActivity;
import provision.com.app.apiResponse.receipt_details.ParticipateInfo;
import provision.com.app.utils.Utils;

/**
 * Created by gauravg on 7/10/2018.
 */

public class EventDetailsAdapter extends RecyclerView.Adapter<EventDetailsAdapter.ViewHolder> {
    private Context context;
    private List<ParticipateInfo> list;
    private double currencyValue;
    private String currencyCode;
    private int numberFormat;

    public EventDetailsAdapter(Context context, List<ParticipateInfo> list, double currencyValue, String currencyCode, int numberFormat) {
        this.context = context;
        this.list = list;
        this.currencyValue = currencyValue;
        this.currencyCode = currencyCode;
        this.numberFormat = numberFormat;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_event_details, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ParticipateInfo participateInfo = list.get(position);
        holder.eventNameTextView.setText(participateInfo.getEventName());
        holder.nameTextView.setText(participateInfo.getFirstName());
        holder.priceTextView.setText(Utils.convertPrice(currencyValue, participateInfo.getPrice(), numberFormat) + " " + currencyCode);
        if (participateInfo.getIsCancelled() == 0) {
            holder.rl_parent.setOnClickListener(v -> {
                context.startActivity(new Intent(context, EventDialogActivity.class).putExtra("participateId", list.get(position).getParticipateId()));
            });
        } else {
            holder.rl_parent.setOnClickListener(null);
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout rl_parent;
        TextView priceTextView, eventNameTextView, nameTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            rl_parent = itemView.findViewById(R.id.rl_parent);
            priceTextView = itemView.findViewById(R.id.priceTextView);
            eventNameTextView = itemView.findViewById(R.id.eventNameTextView);
            nameTextView = itemView.findViewById(R.id.nameTextView);
        }
    }


}
