package provision.com.app.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.InboxDetailActivity;
import provision.com.app.apiResponse.inbox.InboxList;

/**
 * Created by gauravg on 7/18/2018.
 */

public class InboxAdapter extends RecyclerView.Adapter<InboxAdapter.ViewHolder> {
    private Context mContext;
    private List<InboxList> inboxLists;

    public InboxAdapter(Context mContext, List<InboxList> inboxLists) {
        this.mContext = mContext;
        this.inboxLists = inboxLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_inbox, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        InboxList inboxList = inboxLists.get(position);
        holder.tv_subject.setText("Subject :- " + inboxList.getSubject());
        holder.tvSenderName.setText(inboxList.getName());
        holder.tvMessage.setText(inboxList.getBody());
        holder.tvDate.setText(inboxList.getSendDate());
        holder.rl_parent.setOnClickListener(v -> {
            mContext.startActivity(new Intent(mContext, InboxDetailActivity.class).putExtra("MailId", inboxList.getId()));
        });
    }

    @Override
    public int getItemCount() {
        return inboxLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout rl_parent;
        TextView tvSenderName, tv_subject, tvMessage, tvDate;

        public ViewHolder(View itemView) {
            super(itemView);
            rl_parent = itemView.findViewById(R.id.rl_parent);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvSenderName = itemView.findViewById(R.id.tvSenderName);
            tv_subject = itemView.findViewById(R.id.tv_subject);
            tvMessage = itemView.findViewById(R.id.tvMessage);
        }
    }
}
