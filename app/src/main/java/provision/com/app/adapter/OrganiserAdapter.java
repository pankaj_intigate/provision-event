package provision.com.app.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.apiResponse.organiserResponse.Organizer;

/**
 * Created by gauravg on 7/12/2018.
 */

public class OrganiserAdapter extends RecyclerView.Adapter<OrganiserAdapter.ViewHolder> {
    private Context context;
    private List<Organizer> organizersList;
    private BaseActivity baseActivity;

    public OrganiserAdapter(Context context, List<Organizer> organizersList) {
        this.context = context;
        this.organizersList = organizersList;
        baseActivity = (BaseActivity) context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_organiser, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
      /*  Glide.with(context)
                .load(organizersList.get(position).getThumbImg())
                .into(holder.organiserLogo);*/
        DisplayMetrics displayMetrics = new DisplayMetrics();
        baseActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels * 18 / 100;
        int width = displayMetrics.widthPixels * 40 / 100;
        holder.organiserLogo.getLayoutParams().height = height;
        holder.organiserLogo.getLayoutParams().width = width;
        Picasso.get()
                .load(TextUtils.isEmpty(organizersList.get(position).getThumbImg()) ? "null" : organizersList.get(position).getThumbImg())
                .placeholder(R.drawable.footer_logo)
                .into(holder.organiserLogo);

    }

    @Override
    public int getItemCount() {
        return organizersList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView organiserLogo;

        public ViewHolder(View itemView) {
            super(itemView);
            organiserLogo = itemView.findViewById(R.id.organiserLogo);
        }
    }
}
