package provision.com.app.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import provision.com.app.R;
import provision.com.app.apiResponse.myWalletResponse.WalletDetail;
import provision.com.app.utils.Utils;

/**
 * Created by gauravg on 7/18/2018.
 */

public class MyWalletAdapter extends RecyclerView.Adapter<MyWalletAdapter.ViewHolder> {
    private Context mContext;
    private List<WalletDetail> mWalletList;
    private double currencyValue;
    private String currencyCode;
    private int numberFormat;

    public MyWalletAdapter(Context mContext, List<WalletDetail> mWalletList, double currencyValue, String currencyCode, int numberFormat) {
        this.mContext = mContext;
        this.mWalletList = mWalletList;
        this.currencyValue = currencyValue;
        this.currencyCode = currencyCode;
        this.numberFormat = numberFormat;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_my_wallet, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvTransactionId.setText("Transaction ID :- " + mWalletList.get(position).getTransactionId());
        holder.tvDate.setText(mWalletList.get(position).getDated());
        if (mWalletList.get(position).getAmount() >= 0) {
            holder.tvBalance.setTextColor(mContext.getResources().getColor(R.color.colorAmountGreen));
            holder.tvBalance.setText("+ " + Utils.convertPrice(currencyValue, mWalletList.get(position).getAmount(), numberFormat, "") + " " + currencyCode);
        } else {
            holder.tvBalance.setTextColor(mContext.getResources().getColor(R.color.colorAmountRed));
            holder.tvBalance.setText(Utils.convertPrice(currencyValue, mWalletList.get(position).getAmount(), numberFormat, "") + " " + currencyCode);
        }
    }

    @Override
    public int getItemCount() {
        return mWalletList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout ll_parent;
        TextView tvTransactionId, tvBalance, tvDate;

        public ViewHolder(View itemView) {
            super(itemView);
            ll_parent = itemView.findViewById(R.id.ll_parent);
            tvTransactionId = itemView.findViewById(R.id.tvTransactionId);
            tvBalance = itemView.findViewById(R.id.tvBalance);
            tvDate = itemView.findViewById(R.id.tvDate);
        }
    }
}
