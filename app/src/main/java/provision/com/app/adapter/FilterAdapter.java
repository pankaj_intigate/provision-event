package provision.com.app.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.model.FilterModel;

/**
 * Created by gauravg on 7/13/2018.
 */

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.ViewHolder> {
    BaseActivity context;
    List<FilterModel> list;
    private boolean isClick = true;
    private ClickHelper clickHelper;

    public FilterAdapter(BaseActivity context, List<FilterModel> list, ClickHelper clickHelper) {
        this.context = context;
        this.list = list;
        this.clickHelper = clickHelper;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_filter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvCategory.setText(list.get(position).getCategoryName());
        holder.ll_parent.setOnClickListener(v -> {
            clickHelper.callBack(list.get(position).getCategoryId());
        });

        if (TextUtils.isEmpty(list.get(position).getCategorySelectedValue())) {
            holder.tvSelectedValue.setVisibility(View.GONE);
        } else {
            holder.tvSelectedValue.setVisibility(View.VISIBLE);
            holder.tvSelectedValue.setText(list.get(position).getCategorySelectedValue());
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface ClickHelper {
        void callBack(int selectorId);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout ll_parent;
        TextView tvCategory, tvSelectedValue;

        public ViewHolder(View itemView) {
            super(itemView);
            ll_parent = itemView.findViewById(R.id.ll_parent);
            tvCategory = itemView.findViewById(R.id.tvCategory);
            tvSelectedValue = itemView.findViewById(R.id.tvSelectedValue);
        }
    }


}
