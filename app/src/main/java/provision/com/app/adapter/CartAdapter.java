package provision.com.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import provision.com.app.R;
import provision.com.app.apiResponse.cartDataResponse.DeliveryInfo;
import provision.com.app.apiResponse.cartDataResponse.EventsData;
import provision.com.app.utils.Utils;

/**
 * Created by gauravg on 7/10/2018.
 */
//for event list adapter
public class CartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<EventsData> cartDataList;
    private RemoveCartItem removeCartItemInterface;
    private double currencyValue;
    private String currencyCode;
    private int numberFormat;
    private ArrayList<DeliveryInfo> shopDataList;
    private static int VIEW_TYPE_EVENT_DATA = 111;
    private static int VIEW_TYPE_SHOP_HEADER = 112;
    private static int VIEW_TYPE_SHOP_DATA = 113;

    public CartAdapter(Context context, ArrayList<EventsData> list, ArrayList<DeliveryInfo> shopData, RemoveCartItem removeCartItemInterface, double currencyValue, String currencyCode, int numberFormat) {
        this.context = context;
        this.cartDataList = list;
        this.shopDataList = shopData;
        this.removeCartItemInterface = removeCartItemInterface;
        this.currencyValue = currencyValue;
        this.currencyCode = currencyCode;
        this.numberFormat = numberFormat;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_EVENT_DATA) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_cart, parent, false);
            return new ViewHolderEvent(view);
        } else if (viewType == VIEW_TYPE_SHOP_HEADER) {
            View view = LayoutInflater.from(context).inflate(R.layout.shop_header, parent, false);
            return new ViewHolderShopHeader(view);
        } else if (viewType == VIEW_TYPE_SHOP_DATA) {
            View view = LayoutInflater.from(context).inflate(R.layout.cart_shop_item, parent, false);
            return new ViewHolderShop(view);
        }
        View view = LayoutInflater.from(context).inflate(R.layout.item_cart, parent, false);
        return new ViewHolderEvent(view);
    }

    @Override
    public int getItemViewType(int position) {
        if (position < cartDataList.size()) {
            return VIEW_TYPE_EVENT_DATA;
        }
        if (position == cartDataList.size()) {
            return VIEW_TYPE_SHOP_HEADER;
        }
        if (position > cartDataList.size()) {
            return VIEW_TYPE_SHOP_DATA;
        }
        return VIEW_TYPE_EVENT_DATA;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof ViewHolderEvent) {
            EventsData eventsDatum = cartDataList.get(position);
            ((ViewHolderEvent) viewHolder).tv_name.setText(eventsDatum.getName());
            ((ViewHolderEvent) viewHolder).eventCategoryName.setText(eventsDatum.getFeeCategory());
            ((ViewHolderEvent) viewHolder).eventNameText.setText(eventsDatum.getEventName());
            ((ViewHolderEvent) viewHolder).all_details_textview.setText("");
            ((ViewHolderEvent) viewHolder).tv_amount.setText(Utils.convertPrice(currencyValue, eventsDatum.getFeePrice(), numberFormat, "") + " " + currencyCode);
            if (eventsDatum.getPrintTshirt() > 0) {
                ((ViewHolderEvent) viewHolder).tshirtPrintingLayout.setVisibility(View.VISIBLE);
                ((ViewHolderEvent) viewHolder).tvAmountTshirtPrinting.setText(Utils.convertPrice(currencyValue, eventsDatum.getPrintTshirt(), numberFormat, "") + " " + currencyCode);

            } else {
                ((ViewHolderEvent) viewHolder).tshirtPrintingLayout.setVisibility(View.GONE);
            }
            if (eventsDatum.getDeliveryAmount() > 0) {
                ((ViewHolderEvent) viewHolder).raceKitLayout.setVisibility(View.VISIBLE);
                ((ViewHolderEvent) viewHolder).raceKitDeliveryChargeTV.setText(Utils.convertPrice(currencyValue, eventsDatum.getDeliveryAmount(), numberFormat, "") + " " + currencyCode);

            } else {
                ((ViewHolderEvent) viewHolder).raceKitLayout.setVisibility(View.GONE);
            }
            if (eventsDatum.getMedalEnv() > 0) {
                ((ViewHolderEvent) viewHolder).medalEngravingLayout.setVisibility(View.VISIBLE);
                ((ViewHolderEvent) viewHolder).tvAmountMedalEngraving.setText(Utils.convertPrice(currencyValue, eventsDatum.getMedalEnv(), numberFormat, "") + " " + currencyCode);
            } else {
                ((ViewHolderEvent) viewHolder).medalEngravingLayout.setVisibility(View.GONE);
            }

            if (eventsDatum.getDob() != null && !eventsDatum.getDob().equals("null") && !eventsDatum.getDob().equals("0")) {
                ((ViewHolderEvent) viewHolder).all_details_textview.append(eventsDatum.getDob() + " yrs , ");
            }
            if (eventsDatum.getGender() != null && !eventsDatum.getGender().equals("null")) {
                String gender = "";
                if (eventsDatum.getGender().equalsIgnoreCase("male")) {
                    gender = "Male";
                }
                if (eventsDatum.getGender().equalsIgnoreCase("female")) {
                    gender = "Female";

                }
                ((ViewHolderEvent) viewHolder).all_details_textview.append(gender + " , ");
            }
            if (eventsDatum.getTShirtSize() != null && !eventsDatum.getTShirtSize().equals("null") && !TextUtils.isEmpty(eventsDatum.getTShirtSize()) && !eventsDatum.getTShirtSize().equals("Select")) {
                ((ViewHolderEvent) viewHolder).all_details_textview.append("T-shirt Size : " + eventsDatum.getTShirtSize() + " , ");
            }
            ((ViewHolderEvent) viewHolder).all_details_textview.append(eventsDatum.getEmail());
            if (eventsDatum.getTeamName() != null && !eventsDatum.getTeamName().equals("null")) {
                ((ViewHolderEvent) viewHolder).teamNameText.setText("Team : " + eventsDatum.getTeamName());
                ((ViewHolderEvent) viewHolder).teamNameText.setVisibility(View.VISIBLE);
            } else {
                ((ViewHolderEvent) viewHolder).teamNameText.setVisibility(View.GONE);
            }
//        holder.refundAmountText.setText(Utils.convertPrice(currencyValue, eventsDatum.getRefundableAmount(), numberFormat) + " " + currencyCode);

            ((ViewHolderEvent) viewHolder).ll_parent.setOnClickListener(v -> {
//            if (isClick) {
//                holder.ll_sub_details.setVisibility(View.VISIBLE);
//                isClick = false;
//                holder.img_arrow.setRotation(180);
//                holder.eventNameText.setTextColor(context.getResources().getColor(R.color.black));
//            } else {
//                holder.ll_sub_details.setVisibility(View.GONE);
//                isClick = true;
//                holder.img_arrow.setRotation(0);
//                holder.eventNameText.setTextColor(context.getResources().getColor(R.color.inputHint));
//            }
            });
            return;
        }
        if (viewHolder instanceof ViewHolderShop) {
            DeliveryInfo deliveryInfo = shopDataList.get(position - (cartDataList.size() + 1));
            ((ViewHolderShop) viewHolder).totalPriceTv.setText(deliveryInfo.getItemAmount() + " " + currencyCode);
            ((ViewHolderShop) viewHolder).product_name_tv.setText(deliveryInfo.getItemName());
            if ((TextUtils.isEmpty(deliveryInfo.getItemAttrVal()) && !deliveryInfo.getItemAttrVal().equalsIgnoreCase("NA")) && (!TextUtils.isEmpty(deliveryInfo.getColorCode()) && deliveryInfo.getColorCode().equalsIgnoreCase("NA"))) {
                ((ViewHolderShop) viewHolder).sellPriceTv.setVisibility(View.GONE);
            } else {
                ((ViewHolderShop) viewHolder).sellPriceTv.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(deliveryInfo.getItemAttrVal()) && !deliveryInfo.getItemAttrVal().equalsIgnoreCase("NA")) {
                    ((ViewHolderShop) viewHolder).sellPriceTv.setText("Item Size : " + deliveryInfo.getItemAttrVal());

                }

                if (!deliveryInfo.getColorCode().equals("NA") && !TextUtils.isEmpty(deliveryInfo.getColorCode())) {


                    if (((ViewHolderShop) viewHolder).sellPriceTv.getText().toString().length() > 0) {
                        ((ViewHolderShop) viewHolder).sellPriceTv.append(" |");
                    }
                    ((ViewHolderShop) viewHolder).sellPriceTv.append(" Item Color ");
                    Drawable myDrawable = ContextCompat.getDrawable(context, R.drawable.product_color_rounded_corner).mutate();
                    ((GradientDrawable) myDrawable).setColor(Color.parseColor(deliveryInfo.getColorCode().trim()));
                    ((ViewHolderShop) viewHolder).imageColor.setBackgroundDrawable(myDrawable);




                } else {
                    ((ViewHolderShop) viewHolder).imageColor.setVisibility(View.GONE);
                }
            }


            ((ViewHolderShop) viewHolder).qtyTV.setText(deliveryInfo.getQuantity());
            Glide.with(context)
                    .load(deliveryInfo.getItemImg())
                    .fitCenter()
                    .placeholder(R.drawable.placeholder_logo)
                    .into(((ViewHolderShop) viewHolder).productImage);

//            Drawable myDrawable = ContextCompat.getDrawable(context, R.drawable.product_color_rounded_corner).mutate();
//            myDrawable.setColorFilter(Color.parseColor(deliveryInfo.getColorCode().trim()), PorterDuff.Mode.SRC_IN);
//            ((ViewHolderShop) viewHolder).imageColor.setImageDrawable(myDrawable);
            ((ViewHolderShop) viewHolder).removeCartItem.setOnClickListener(v -> {
                String bookingId = deliveryInfo.getBookingItemId();
                removeCartItemInterface.callbackClick(bookingId);
            });
            ((ViewHolderShop) viewHolder).plusCartImg.setOnClickListener(v -> {
                String bookingId = deliveryInfo.getBookingItemId();
                String qty = ((ViewHolderShop) viewHolder).qtyTV.getText().toString();
                int cartIntVal = Integer.parseInt(qty);
                cartIntVal = cartIntVal + 1;
                if (deliveryInfo.getMaxQtyAvaliable() < cartIntVal) {
                    Toast.makeText(context, "Quantity  must be less than " + cartIntVal, Toast.LENGTH_SHORT).show();
                    return;
                }
                ((ViewHolderShop) viewHolder).qtyTV.setText(cartIntVal + "");
                removeCartItemInterface.updateCartQty(bookingId, cartIntVal + "");
            });
            ((ViewHolderShop) viewHolder).minusCartIcon.setOnClickListener(v -> {
                String bookingId = deliveryInfo.getBookingItemId();
                String qty = ((ViewHolderShop) viewHolder).qtyTV.getText().toString();
                int cartIntVal = Integer.parseInt(qty);
                cartIntVal = cartIntVal - 1;
                if (cartIntVal < 1) {
                    Toast.makeText(context, "Quantity must be greater than 1", Toast.LENGTH_SHORT).show();
                    return;
                }
                ((ViewHolderShop) viewHolder).qtyTV.setText(cartIntVal + "");
                removeCartItemInterface.updateCartQty(bookingId, cartIntVal + "");
            });
        }

    }

    @Override
    public int getItemCount() {
        if (shopDataList.size() == 0) {
            return cartDataList.size();
        } else {
            return cartDataList.size() + shopDataList.size() + 1;
        }

    }

    public interface RemoveCartItem {
        void callbackClick(String cartItemId, String participantId);

        void callbackClick(String cartItemId);

        void updateCartQty(String bookingId, String qty);
    }

    public class ViewHolderEvent extends RecyclerView.ViewHolder {
        private TextView tv_name, eventNameText, tv_amount, all_details_textview, teamNameText, raceKitDeliveryChargeTV;
        private LinearLayout ll_parent, medalEngravingLayout, tshirtPrintingLayout, raceKitLayout;
        private TextView eventCategoryName, tvAmountMedalEngraving, tvAmountTshirtPrinting;
        private ImageView removeCartItem;

        ViewHolderEvent(View itemView) {
            super(itemView);
            medalEngravingLayout = itemView.findViewById(R.id.medal_engraving_layout);
            tshirtPrintingLayout = itemView.findViewById(R.id.tshirt_printing_layout);
            raceKitLayout = itemView.findViewById(R.id.raceKitLayout);
            tvAmountTshirtPrinting = itemView.findViewById(R.id.tv_amount_tshirt_printing);
            tvAmountMedalEngraving = itemView.findViewById(R.id.tv_amount_medal_engraving);
            tv_name = itemView.findViewById(R.id.tv_name);
            teamNameText = itemView.findViewById(R.id.teamNameText);
            raceKitDeliveryChargeTV = itemView.findViewById(R.id.raceKitDeliveryChargeTV);
            all_details_textview = itemView.findViewById(R.id.all_details_textview);
            eventCategoryName = itemView.findViewById(R.id.eventCategoryName);
            eventNameText = itemView.findViewById(R.id.eventNameText);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            ll_parent = itemView.findViewById(R.id.ll_parent);
            removeCartItem = itemView.findViewById(R.id.tv_remove);

            removeCartItem.setOnClickListener(v -> {
                String bookingId = cartDataList.get(getAdapterPosition()).getBookingId();
                removeCartItemInterface.callbackClick(bookingId, cartDataList.get(getAdapterPosition()).getParticipantId());
            });
        }
    }


    public static class ViewHolderShop extends RecyclerView.ViewHolder {
        private TextView product_name_tv, sellPriceTv, totalPriceTv, qtyTV;
        private AppCompatImageView productImage, imageColor, plusCartImg, minusCartIcon;
        ImageView removeCartItem;

        ViewHolderShop(View itemView) {
            super(itemView);
            product_name_tv = itemView.findViewById(R.id.product_name_tv);
            sellPriceTv = itemView.findViewById(R.id.sellPriceTv);
            totalPriceTv = itemView.findViewById(R.id.totalPriceTv);
            qtyTV = itemView.findViewById(R.id.qtyTV);
            productImage = itemView.findViewById(R.id.item_image);
            imageColor = itemView.findViewById(R.id.imageColor);
            removeCartItem = itemView.findViewById(R.id.removeTv);
            plusCartImg = itemView.findViewById(R.id.qtyPluseTV);
            minusCartIcon = itemView.findViewById(R.id.qtyMinsTV);


        }
    }

    public static class ViewHolderShopHeader extends RecyclerView.ViewHolder {

        ViewHolderShopHeader(View itemView) {
            super(itemView);

        }
    }
}
