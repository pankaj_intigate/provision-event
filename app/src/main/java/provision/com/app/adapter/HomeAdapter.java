package provision.com.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.EventDetailsActivity;
import provision.com.app.activity.HomeActivity;
import provision.com.app.apiResponse.upcomingEventResponse.ProductList;
import provision.com.app.utils.Utils;
import provision.com.app.utils.network.api.ApiConstant;

/**
 * Created by gauravg on 7/5/2018.
 */

public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private List<provision.com.app.apiResponse.upcomingEventResponse.ProductList> productLists;

    public HomeAdapter(Context context, List<provision.com.app.apiResponse.upcomingEventResponse.ProductList> productLists) {
        this.mContext = context;
        this.productLists = productLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(mContext).inflate(R.layout.item_home_second, parent, false);
        return new HomeViewHolderSecond(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ProductList productItem = productLists.get(position);
        HomeViewHolderSecond holder2 = (HomeViewHolderSecond) holder;
        if (productItem.getVirtualEvent() == 1) {
            holder2.endllDate.setVisibility(View.VISIBLE);
            holder2.ivMinus.setVisibility(View.VISIBLE);
            StringBuilder sb = new StringBuilder(Utils.parseDateToddMonth(productItem.getEventStartDate()).toLowerCase());
            sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
            holder2.eventDate.setText(Utils.parseDateToDate(productItem.getEventStartDate()));
            holder2.eventMonth.setText(sb.toString());
            StringBuilder sb2 = new StringBuilder(Utils.parseDateToddMonth(productItem.getEventEndDate()).toLowerCase());
            sb2.setCharAt(0, Character.toUpperCase(sb2.charAt(0)));
            holder2.eventEndDate.setText(Utils.parseDateToDate(productItem.getEventEndDate()));
            holder2.eventEndMonth.setText(sb2.toString());
        } else {
            holder2.endllDate.setVisibility(View.GONE);
            holder2.ivMinus.setVisibility(View.GONE);
            StringBuilder sb3 = new StringBuilder(Utils.parseDateToddMonth(productItem.getEventDate()).toLowerCase());
            sb3.setCharAt(0, Character.toUpperCase(sb3.charAt(0)));
            holder2.eventDate.setText(Utils.parseDateToDate(productItem.getEventDate()));
            holder2.eventMonth.setText(sb3.toString());
        }
        setHourDays(holder2, productItem);
        Glide.with(mContext.getApplicationContext())
                .load(productItem.getEventImage())
                .placeholder(R.drawable.placeholder_logo)
                .into(((HomeViewHolderSecond) holder).img_main);

        holder2.rlParent.setOnClickListener(v -> {
            Intent eventDetailIntent = new Intent(mContext, EventDetailsActivity.class);
            eventDetailIntent.putExtra(ApiConstant.KEY_EVENT_ID, productItem.getId());
            eventDetailIntent.putExtra(ApiConstant.KEY_EVENT_NAME, productItem.getEventsTitle());
            ((HomeActivity) mContext).startActivityForResult(eventDetailIntent, 101);
        });
        if (!productItem.getEventAddress().isEmpty()) {
            StringBuilder sb = new StringBuilder(productItem.getEventAddress());
            sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
            holder2.eventAddress.setText(sb.toString());
        }
//        StringBuilder sb = new StringBuilder(Utils.parseDateToddMonth(productItem.getEventDate()).toLowerCase());
//        sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
//        holder2.eventDate.setText(Utils.parseDateToDate(productItem.getEventDate()));
//        holder2.eventMonth.setText(sb.toString());
        holder2.eventTitle.setText(productItem.getEventsTitle());
        if (TextUtils.isEmpty(productItem.getIsRegisteration())) {
            holder2.isResgestrationText.setVisibility(View.GONE);
        }
        holder2.isResgestrationText.setText(productItem.getIsRegisteration());
        if (!TextUtils.isEmpty(productItem.getIsColor())) {
            holder2.isResgestrationText.setBackgroundColor(Color.parseColor(productItem.getIsColor()));
        }
    }


    @Override
    public int getItemCount() {
        return productLists.size();
    }

    private void setHourDays(HomeViewHolderSecond homeViewHolderSecond, ProductList productItem) {
        long timeDifference = Utils.getTimeRemaining(productItem.getEventDate());

        if (productItem.getVirtualEvent()==1){
            timeDifference = Utils.getTimeRemaining(productItem.getEventEndDate());
        }
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = timeDifference / daysInMilli;
        timeDifference = timeDifference % daysInMilli;
        long elapsedHours = timeDifference / hoursInMilli;

        if (elapsedHours < 0) {
            homeViewHolderSecond.tv_hour.setText("00");
        } else if (elapsedHours < 10) {
            homeViewHolderSecond.tv_hour.setText("0" + elapsedHours);
        } else {
            homeViewHolderSecond.tv_hour.setText("" + elapsedHours);
        }
        if (elapsedDays < 0) {
            homeViewHolderSecond.tv_day.setText("00");
        } else if (elapsedDays < 10) {
            homeViewHolderSecond.tv_day.setText("0" + elapsedDays);
        } else {
            homeViewHolderSecond.tv_day.setText("" + elapsedDays);
        }


    }

    private class HomeViewHolderSecond extends RecyclerView.ViewHolder {
        private TextView tv_day, tv_hour, btnBookNow, eventTitle, eventDate, eventAddress, isResgestrationText,
                eventMonth, eventEndDate, eventEndMonth, ivMinus;
        private ImageView img_main;
        private RelativeLayout rlParent;
        public LinearLayout endllDate;

        public HomeViewHolderSecond(View view) {
            super(view);
            endllDate = view.findViewById(R.id.endllDate);
            ivMinus = view.findViewById(R.id.ivMinus);
            tv_day = view.findViewById(R.id.tv_day);
            tv_hour = view.findViewById(R.id.tv_hour);
            img_main = view.findViewById(R.id.img_main);
            eventMonth = view.findViewById(R.id.eventMonth);
            rlParent = view.findViewById(R.id.rlParent);
            eventTitle = view.findViewById(R.id.eventTitle);
            eventDate = view.findViewById(R.id.eventDate);
            eventAddress = view.findViewById(R.id.eventAddress);
            eventEndDate = (TextView) view.findViewById(R.id.eventEndDate);
            eventEndMonth = (TextView) view.findViewById(R.id.eventEndMonth);
            isResgestrationText = view.findViewById(R.id.isResgestrationText);
        }
    }
}
