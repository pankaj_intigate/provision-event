package provision.com.app.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import provision.com.app.R;

/**
 * Created by pankajk on 10/17/2018.
 * All rights reserved by Intigate Technologies.
 */

//for recommendedItem show in cart
public class CartItemsAdapter extends RecyclerView.Adapter<CartItemsAdapter.ViewHolder> {
    private List<String> itemsData;
    private Context context;
    private boolean isClick = true;
    private double currencyValue;
    private String currencyCode;
    private int numberFormat;
    private RemoveCartItem removeCartItemInterface;
    public CartItemsAdapter(Context context, List<String> list, double currencyValue, String currencyCode, int numberFormat,RemoveCartItem removeCartItemInterface) {
        this.context = context;
        this.itemsData = list;
        this.currencyValue = currencyValue;
        this.currencyCode = currencyCode;
        this.numberFormat = numberFormat;
        this.removeCartItemInterface = removeCartItemInterface;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_cart_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
//        holder.tvName.setText(itemsData.get(position).getItemName());
//        holder.productSize.setText("Item Size : " + itemsData.get(position).getItemAttrVal());
//        holder.tvAmount.setText(Utils.convertPrice(currencyValue, itemsData.get(position).getItemAmount(), numberFormat, "") + " " + currencyCode);
    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    public interface RemoveCartItem {
        void callbackClick(String cartItemId);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, productSize, tvAmount;
        ImageView removeCartItem;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            productSize = itemView.findViewById(R.id.product_size);
            tvAmount = itemView.findViewById(R.id.tv_amount);
            removeCartItem = itemView.findViewById(R.id.tv_remove);
            removeCartItem.setOnClickListener(v -> {
//                String bookingId = itemsData.get(getAdapterPosition()).getBookingItemId();
//                removeCartItemInterface.callbackClick(bookingId);
            });
        }
    }
}
