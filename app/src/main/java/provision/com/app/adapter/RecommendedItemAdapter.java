package provision.com.app.adapter;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import provision.com.app.R;
import provision.com.app.activity.BaseActivity;
import provision.com.app.apiResponse.recomded_itm.Response;
import provision.com.app.apiResponse.savetocartresponse.SaveToCartResponse;
import provision.com.app.interfaces.OnItemSelectListener;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.Utils;
import provision.com.app.utils.network.api.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by pankajk on 10/16/2018.
 * All rights reserved by Intigate Technologies.
 */
public class RecommendedItemAdapter extends RecyclerView.Adapter<RecommendedItemAdapter.ViewHolder> implements OnItemSelectListener {
    private BaseActivity context;
    private ArrayList<Response> mList;
    private double currencyValue;
    private int numberFormat;
    private String currencyCode;
    private int position, childPosition;

    public RecommendedItemAdapter(BaseActivity context, ArrayList<Response> mList) {
        this.context = context;
        this.mList = mList;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public void setCurrencyValue(double currencyValue) {
        this.currencyValue = currencyValue;
    }

    public void setNumberFormat(int numberFormat) {
        this.numberFormat = numberFormat;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recommended_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso.get()
                .load(mList.get(position).getItemImg())
                .placeholder(R.drawable.placeholder_event)
                .into(holder.itemImage);
        holder.tvPrice.setText(Utils.convertPrice(currencyValue, mList.get(position).getItemPrice(), numberFormat, "") + " " + currencyCode);
        holder.tvItemName.setText(mList.get(position).getItemTitle());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        ItemSizeAdapter itemSizeAdapter = new ItemSizeAdapter(context, (mList.get(position).getItemSize()), position, this);
        holder.itemSizeRecycler.setLayoutManager(linearLayoutManager);
        holder.itemSizeRecycler.setAdapter(itemSizeAdapter);
        holder.addCartBtn.setOnClickListener(v -> {
            if (itemSizeAdapter.getSelectedItemPosition() == -1) {
                Toast.makeText(context, "Please select size", Toast.LENGTH_SHORT).show();
                return;
            }
            String sessionId = PrefUtils.getFromPrefs(context, PrefUtils.SESSION_ID);
            saveItemCart(mList.get(position).getIId(),
                    sessionId, mList.get(position).getItemPrice(),
                    mList.get(position).getItemSize().get(itemSizeAdapter.getSelectedItemPosition()).getVValue(),
                    mList.get(position).getItemSize().get(itemSizeAdapter.getSelectedItemPosition()).getIId());
        });
    }

    private void saveItemCart(String id, String sessionId, double price, String tsize, String tsizeId) {
        if (context.isOnline()) {
            View view = context.showProgressBar();

            ApiClient.getAPiAuthHelper(context).saveCartItem("savecartItem", id, sessionId, "","","", "", "",ApiClient.API_VERSION).enqueue(new Callback<SaveToCartResponse>() {
                @Override
                public void onResponse(Call<SaveToCartResponse> call, retrofit2.Response<SaveToCartResponse> response) {

                    for (int i = 0; i < mList.size(); i++) {
                        for (int j = 0; j < mList.get(i).getItemSize().size(); j++) {
                            mList.get(i).getItemSize().get(j).setSelected(false);
                        }
                    }
                    RecommendedItemAdapter.this.notifyDataSetChanged();
                    showDialogBox();
                    context.cancelProgressBar(view);
                }

                @Override
                public void onFailure(Call<SaveToCartResponse> call, Throwable t) {
                    context.cancelProgressBar(view);
                }
            });
        } else {
            context.showToast(context, context.getResources().getString(R.string.check_internet));
        }
    }

    private void showDialogBox() {
        LayoutInflater inflater = context.getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.go_cart_layout, null);

        TextView addNewParticipent = alertLayout.findViewById(R.id.add_new_participent);
        TextView gotocartText = alertLayout.findViewById(R.id.gotocartText);
        gotocartText.setText("Go to cart");
        addNewParticipent.setText("+Add more item");
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        AlertDialog dialog = alert.create();
        addNewParticipent.setOnClickListener(v -> {
            dialog.dismiss();
        });
        gotocartText.setOnClickListener(v -> {
            dialog.dismiss();
            context.finish();
        });
        dialog.show();
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public void onItemSelect(int parentPosition, int childPosition, ItemSizeAdapter itemSizeAdapter) {
        if (childPosition == -1) {
            this.childPosition = childPosition;
            position = parentPosition;
            return;
        }
        mList.get(position).getItemSize().get(this.childPosition).setSelected(false);
        this.childPosition = childPosition;
        position = parentPosition;
        mList.get(position).getItemSize().get(childPosition).setSelected(true);
        notifyDataSetChanged();

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView itemImage, removeCartItem;
        private RecyclerView itemSizeRecycler;
        private TextView tvPrice, tvItemName;
        private Button addCartBtn;

        public ViewHolder(View itemView) {
            super(itemView);
            addCartBtn = itemView.findViewById(R.id.add_cart);
            itemSizeRecycler = itemView.findViewById(R.id.item_size_recycler);
            itemImage = itemView.findViewById(R.id.item_image);
            tvPrice = itemView.findViewById(R.id.tv_price);
            tvItemName = itemView.findViewById(R.id.tv_item_name);
        }
    }
}
