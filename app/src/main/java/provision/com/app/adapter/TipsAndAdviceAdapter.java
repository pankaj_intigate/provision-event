package provision.com.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import provision.com.app.R;
import provision.com.app.apiResponse.tipsAdviceResponse.Tip;

/**
 * Created by gauravg on 7/12/2018.
 */

public class TipsAndAdviceAdapter extends RecyclerView.Adapter<TipsAndAdviceAdapter.ViewHolder> {
    private Context context;
    private List<Tip> tipList;
    private CallbackInterface callbackInterface;

    public TipsAndAdviceAdapter(Context context, List<Tip> tipList, CallbackInterface callbackInterface) {
        this.context = context;
        this.tipList = tipList;
        this.callbackInterface = callbackInterface;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_tips, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

      /*  Glide.with(context)
                .load(tipList.get(position).getThumbImg())
                .into(holder.tipsImage);*/

        Picasso.get()
                .load(TextUtils.isEmpty(tipList.get(position).getThumbImg()) ? "null" : tipList.get(position).getThumbImg())
                .placeholder(R.drawable.footer_logo)
                .into(holder.tipsImage);

        holder.description.setText(tipList.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return tipList.size();
    }

    public interface CallbackInterface {
        void callBack(String fileUrl);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView tipsImage;
        private TextView description, ivDownload;

        public ViewHolder(View itemView) {
            super(itemView);
            tipsImage = itemView.findViewById(R.id.tips_image);
            description = itemView.findViewById(R.id.tv_description);
            ivDownload = itemView.findViewById(R.id.ivDownload);

            ivDownload.setOnClickListener(v -> {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(tipList.get(getAdapterPosition()).getFileToDownload()));
                context.startActivity(browserIntent);
//                    callbackInterface.callBack(tipList.get(getAdapterPosition()).getFileToDownload());
            });
        }
    }
}
