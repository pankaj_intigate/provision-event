package provision.com.app.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import provision.com.app.R;
import provision.com.app.apiResponse.feeDataResponse.EventFee;
import provision.com.app.utils.Utils;

/**
 * Created by gauravg on 7/12/2018.
 */

public class FeeAdapter extends RecyclerView.Adapter<FeeAdapter.ViewHolder> {
    Context context;
    private List<EventFee> eventFeesList;
    private double currencyValue;
    private int numberFormat;
    private String currencyCode;

    public FeeAdapter(Context context, List<EventFee> eventFeesList) {
        this.context = context;
        this.eventFeesList = eventFeesList;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public void setCurrencyValue(double currencyValue) {
        this.currencyValue = currencyValue;
    }

    public void setNumberFormat(int numberFormat) {
        this.numberFormat = numberFormat;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_fee, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        EventFee eventFee = eventFeesList.get(position);
        holder.categoryText.setText(eventFee.getCategory());
        if (eventFee.getShowTeamPrice() == 0) {
            holder.teamPriceText.setVisibility(View.GONE);
        } else {
            holder.teamPriceText.setVisibility(View.VISIBLE);
            holder.teamPriceText.setText("Team - " + Utils.convertPrice(currencyValue, eventFee.getTeamPrice(), numberFormat, "") + " " + currencyCode);
        }

        if (eventFee.getShowIndivisualPrice() == 0) {
            holder.individualPriceText.setVisibility(View.GONE);
        } else {
            holder.individualPriceText.setVisibility(View.VISIBLE);
            holder.individualPriceText.setText("Individual - " + Utils.convertPrice(currencyValue, eventFee.getIndivisualPrice(), numberFormat, "") + " " + currencyCode);
        }
    }

    @Override
    public int getItemCount() {
        return eventFeesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView categoryText, individualPriceText, teamPriceText;

        public ViewHolder(View itemView) {
            super(itemView);
            categoryText = itemView.findViewById(R.id.categoryText);
            individualPriceText = itemView.findViewById(R.id.individualPriceText);
            teamPriceText = itemView.findViewById(R.id.teamPriceText);
        }
    }
}
