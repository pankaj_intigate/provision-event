package provision.com.app.adapter;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import provision.com.app.R;
import provision.com.app.apiResponse.eventDetailResponse.EventImg;

/**
 * Created by amitk on 17-07-2018.
 */

public class EventDetailViewPagerAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    private Context mContext;
    private List<EventImg> eventImageList;

    public EventDetailViewPagerAdapter(Context context, List<EventImg> eventImageList) {
        this.mContext = context;
        this.eventImageList = eventImageList;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.event_detail_pager_item, container, false);
        container.addView(view);

        ImageView itemImage = view.findViewById(R.id.item_image);

        Glide.with(mContext)
                .load(eventImageList.get(position).getAppImg())
                .placeholder(R.drawable.placeholder_logo)
                .into(itemImage);

        return view;
    }

    @Override
    public int getCount() {
        return eventImageList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}