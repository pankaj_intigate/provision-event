package provision.com.app.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.viewpager.widget.PagerAdapter;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import provision.com.app.R;
import provision.com.app.activity.EventDetailsActivity;
import provision.com.app.apiResponse.homeBanner.ProductList;
import provision.com.app.utils.network.api.ApiConstant;

/**
 * Created by amitk on 12-07-2018.
 */

public class HomeViewPagerAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    private Context mContext;
    private List<ProductList> productLists;

    public HomeViewPagerAdapter(Context context, List<ProductList> productLists) {
        this.mContext = context;
        this.productLists = productLists;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.item_home, container, false);
        view.setOnClickListener(v -> {
            Intent eventDetailIntent = new Intent(mContext, EventDetailsActivity.class);
            eventDetailIntent.putExtra(ApiConstant.KEY_EVENT_ID, productLists.get(position).getId());
            eventDetailIntent.putExtra(ApiConstant.KEY_EVENT_NAME, productLists.get(position).getEventsTitle());
            ((AppCompatActivity) mContext).startActivityForResult(eventDetailIntent, 101);
        });
        container.addView(view);
        ImageView imageEvent = view.findViewById(R.id.img_main);
        Glide.with(mContext)
                .load(productLists.get(position).getEventImage())
                .placeholder(R.drawable.placeholder_logo)
                .into(imageEvent);
        return view;
    }



    @Override
    public int getCount() {
        return productLists.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}