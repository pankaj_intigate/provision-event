package provision.com.app.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import provision.com.app.R;
import provision.com.app.apiResponse.inboxDetailsResponse.Response_;

/**
 * Created by gauravg on 7/18/2018.
 */

public class InboxDetailAdapter extends RecyclerView.Adapter<InboxDetailAdapter.ViewHolder> {
    private Context mContext;
    private List<Response_> mMailList;

    public InboxDetailAdapter(Context mContext, List<Response_> mMailList) {
        this.mContext = mContext;
        this.mMailList = mMailList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_inbox_details, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvSenderName.setText(mMailList.get(position).getSenderName());
        holder.tvDate.setText(mMailList.get(position).getCreateOn());
        holder.tv_subject.setText(mMailList.get(position).getSubject());
        holder.tvMessage.setText(mMailList.get(position).getBody());
    }

    @Override
    public int getItemCount() {
        return mMailList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvSenderName, tvDate, tv_subject, tvMessage;

        public ViewHolder(View itemView) {
            super(itemView);
            tvSenderName = itemView.findViewById(R.id.tvSenderName);
            tvDate = itemView.findViewById(R.id.tvDate);
            tv_subject = itemView.findViewById(R.id.tv_subject);
            tvMessage = itemView.findViewById(R.id.tvMessage);
        }
    }
}
