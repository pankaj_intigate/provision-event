package provision.com.app.upload_result

import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.upload_result_item.view.*
import provision.com.app.R
import provision.com.app.activity.WebViewActivity
import provision.com.app.apiResponse.resultResponse.EventList
import provision.com.app.utils.Utils
import java.util.*
import kotlin.jvm.internal.Intrinsics

class UploadResultAdapter(var context: Context, var list: ArrayList<EventList>) : RecyclerView.Adapter<UploadResultAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.upload_result_item, p0, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        val producIttem: EventList = list[p1]
        p0.itemView.eventTitle.text = list[p1].eventsTitle
        p0.itemView.eventDate.setText(Utils.parseDateToddMMyyyy(producIttem.eventEndDate))
        if (producIttem.getEventAddress() == null || producIttem.getEventAddress().isEmpty()) {
            p0.itemView.eventAddress.setVisibility(View.GONE)
        } else {
            p0.itemView.eventAddress.setVisibility(View.VISIBLE)
            p0.itemView.eventAddress.setText(producIttem.getEventAddress())
        }
        p0.itemView.rlParent.setOnClickListener {
            val intent = Intent(context, WebViewActivity::class.java)
            intent.putExtra("TitleName", context.getString(R.string.upload_result))

            intent.putExtra("URL", producIttem.resultCodeUrl)
            context.startActivity(intent)
        }
    }
}