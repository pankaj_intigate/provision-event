package provision.com.app.upload_result

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_upload_result.view.*
import provision.com.app.R
import provision.com.app.activity.BaseActivity
import provision.com.app.apiResponse.resultResponse.EventList
import provision.com.app.apiResponse.resultResponse.ResultResponse
import provision.com.app.fragment.BaseFragment
import provision.com.app.upload_result.filter.FilterUploadResultActivity
import provision.com.app.utils.AppConstant
import provision.com.app.utils.CustomEditText
import provision.com.app.utils.network.api.ApiClient
import provision.com.app.utils.network.api.AuthApiHelper
import provision.com.app.utils.network.api.CallbackManager
import provision.com.app.utils.network.api.RetroError
import java.util.*
import kotlin.jvm.internal.Intrinsics


class UploadResultFragment : BaseFragment() {
    lateinit var rootView: View
    private var mActivity: BaseActivity? = null
    private val productLists: ArrayList<EventList> = ArrayList()
    private var isFilterActive = false
    var catId: String? = null
    var months: String? = null
    var year: String? = null
    var staus: String? = null

    //
    private lateinit var uploadResultAdapter: UploadResultAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_upload_result, container, false)
        mActivity = activity as BaseActivity?
        rootView.rcv_events.layoutManager = LinearLayoutManager(requireContext())
        uploadResultAdapter = UploadResultAdapter(requireContext(), productLists)
        rootView.rcv_events.adapter = uploadResultAdapter
        getSearchEventData("", "", "", "", "")
        rootView.icFilter.setOnClickListener {
            startActivityForResult(Intent(requireContext(), FilterUploadResultActivity::class.java), 777)
        }
        searchEvent()
        return rootView
    }

    companion object {

        @JvmStatic
        fun newInstance() =
                UploadResultFragment()
    }


    fun getSearchEventData(searchText: String?, categoryId: String?, yearId: String?, monthId: String?, statusId: String?) {
        if (mActivity!!.isOnline()) {
            productLists.clear()
            val view: View = mActivity!!.showProgressBar()
            val authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper::class.java)
            val call = authApiHelper.getUploadResultEventList("uploadresultEventsList", searchText, categoryId, yearId, monthId, statusId
                    , ApiClient.API_VERSION)
            call.enqueue(object : CallbackManager<ResultResponse?>() {
                override fun onSuccess(`object`: Any, message: String) {
                    mActivity!!.cancelProgressBar(view)
                    val eventDetailResponse = `object` as ResultResponse
                    if (eventDetailResponse.response.eventList != null) {
                        productLists.clear()
                        productLists.addAll(eventDetailResponse.response.eventList)
                    }
                    uploadResultAdapter.notifyDataSetChanged()
                    if (productLists != null && productLists.size > 0) {
                        rootView.noDataFoundText.visibility = View.GONE
                        rootView.rcv_events.visibility = View.VISIBLE
                    } else {
                        rootView.noDataFoundText.visibility = View.VISIBLE
                        rootView.rcv_events.visibility = View.GONE
                    }
                }

                override fun onError(retroError: RetroError) {
                    mActivity!!.cancelProgressBar(view)
                }

                override fun onFailure(retroError: String) {
                    mActivity!!.cancelProgressBar(view)
                }
            })
        } else {
            mActivity!!.showToast(mActivity, mActivity!!.getResources().getString(R.string.check_internet))
        }
    }


    private fun searchEvent() {
        rootView.tvSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(text: Editable) {
                val searchText = text.toString()
                if (!TextUtils.isEmpty(searchText) && searchText.length > 2) {
                    getSearchEventData(searchText, "", year, "", "")
                } else if (TextUtils.isEmpty(searchText)) {
                    getSearchEventData("", "", year, "", "")
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val str = "rootView"
        if (resultCode == Activity.RESULT_OK) {
            if (data == null) {
                Intrinsics.throwNpe()
            }
            val categoryId = data!!.getStringExtra(AppConstant.KEY_CATEGORY)
            val yearId = data.getStringExtra("year")
            val monthId = data.getStringExtra("month")
            val statusId = data.getStringExtra("status")
            isFilterActive = true

            getSearchEventData(rootView.tvSearch.text.toString(), categoryId, yearId, monthId, statusId)
            return
        }
        isFilterActive = true
        getSearchEventData(rootView.tvSearch.text.toString(), "", "", "", "")
    }
}