package provision.com.app.upload_result.filter

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_filter_upload_result.*
import provision.com.app.R
import provision.com.app.activity.BaseActivity
import provision.com.app.adapter.FilterAdapter
import provision.com.app.apiResponse.filterContentResponse.FilterContentResponse
import provision.com.app.model.FilterModel
import provision.com.app.utils.AppConstant
import provision.com.app.utils.network.api.ApiClient
import provision.com.app.utils.network.api.AuthApiHelper
import provision.com.app.utils.network.api.CallbackManager
import provision.com.app.utils.network.api.RetroError
import java.util.*
import kotlin.collections.ArrayList

class FilterUploadResultActivity : BaseActivity(), FilterAdapter.ClickHelper {
    private val REQUEST_CODE = 103
    private var adapter: FilterAdapter? = null
    private var filterModelList: ArrayList<FilterModel>? = ArrayList()
    private var filterContentResponse: FilterContentResponse? = null
    private lateinit var categoryArray: Array<String?>
    private lateinit var yearArray: Array<String?>
    private lateinit var monthArray: Array<String?>
    private lateinit var statusArray: Array<String?>
    private var yearSelectedPosition: Int = -1
    private var monthSelectedPosition: Int = -1
    private var statusSelectedPosition: Int = -1
    private var categorySelectedPosition = -1
    private var selectedIdVal = 0
    private var categoryId = ""
    private var yearId: String? = ""
    private var monthId: String? = ""
    private var statusId: String? = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter_upload_result)
        yearId = Calendar.getInstance()[Calendar.YEAR].toString()
        setAdapterFilterItem()
        getFilterContentData()
        back.setOnClickListener { v -> finish() }

        tvReset.setOnClickListener { v ->
            inVisiblePickerAnimation()
            categorySelectedPosition = -1
            yearSelectedPosition = -1
            monthSelectedPosition = -1
            statusSelectedPosition = -1
            setAdapterFilterItem()
            numberPicker.setDisplayedValues(null)
        }


        tvApply.setOnClickListener { v ->
            if (filterContentResponse != null) {
                val categories = filterContentResponse!!.response.category
                val yearList = filterContentResponse!!.response.year
                val monthList = filterContentResponse!!.response.month
                val statusList = filterContentResponse!!.response.status
                if (categorySelectedPosition >= 0) {
                    categoryId = if (categories != null && categories.size > 0) categories[categorySelectedPosition].iId else ""
                }
                if (yearSelectedPosition >= 0) {
                    yearId = if (yearList != null && yearList.size > 0) "" + yearList[yearSelectedPosition] else ""
                }
                if (monthSelectedPosition >= 0) {
                    monthId = if (monthList != null && monthList.size > 0) monthList[monthSelectedPosition] else ""
                }
                if (statusSelectedPosition >= 0) {
                    statusId = if (statusList != null && statusList.size > 0) statusList[statusSelectedPosition].id else ""
                }
            }
            val intent = Intent()
            intent.putExtra(AppConstant.KEY_CATEGORY, categoryId)
            intent.putExtra(AppConstant.KEY_YEAR, yearId)
            intent.putExtra(AppConstant.KEY_MONTH, monthId)
            intent.putExtra(AppConstant.KEY_STATUS, statusId)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    private fun getFilterContentData() {
        if (isOnline) {
            val view = showProgressBar()
            val authApiHelper = ApiClient.getClient(this).create(AuthApiHelper::class.java)
            val call = authApiHelper.getFilterContent("GetEventFilterData", ApiClient.API_VERSION)
            call.enqueue(object : CallbackManager<FilterContentResponse?>() {
                override fun onSuccess(`object`: Any, message: String) {
                    cancelProgressBar(view)
                    filterContentResponse = `object` as FilterContentResponse
                    if (filterContentResponse == null && filterContentResponse!!.getResponse() == null) {
                        Toast.makeText(this@FilterUploadResultActivity, getString(R.string.response_msg), Toast.LENGTH_SHORT).show()
                        return
                    }
                }

                override fun onError(retroError: RetroError) {
                    cancelProgressBar(view)
                }

                override fun onFailure(retroError: String) {
                    cancelProgressBar(view)
                }
            })
        } else {
            showToast(this, resources.getString(R.string.check_internet))
        }
    }

    private fun setCategory() {
        var filterModel = FilterModel()
        filterModel.categoryId = 1
        filterModel.categoryName = "Category"
        filterModelList!!.add(filterModel)
        filterModel = FilterModel()
        filterModel.categoryId = 2
        filterModel.categoryName = "Select Year"
        filterModelList!!.add(filterModel)
        filterModel = FilterModel()
        filterModel.categoryId = 3
        filterModel.categoryName = "Select Month"
        filterModelList!!.add(filterModel)
        filterModel = FilterModel()
        filterModel.categoryId = 4
//        filterModel.categoryName = "Select Status"
//        filterModelList!!.add(filterModel)
    }

    private fun setAdapterFilterItem() {
        filterModelList = ArrayList()
        setCategory()
        rcvFilterList.setLayoutManager(LinearLayoutManager(this))
        adapter = FilterAdapter(this, filterModelList, this)
        rcvFilterList.setAdapter(adapter)
    }

    private fun getCategoryArray() {
        val categories = filterContentResponse!!.response.category
        categoryArray = arrayOfNulls(categories.size)
        for (i in categories.indices) {
            categoryArray[i] = categories[i].vTitle
        }
    }

    private fun getYearArray() {
        val yearList = filterContentResponse!!.response.year
        yearArray = arrayOfNulls(yearList.size)
        for (i in yearList.indices) {
            yearArray[i] = "" + yearList[i]
        }
    }

    private fun getMonthArray() {
        val monthList = filterContentResponse!!.response.month
        monthArray = arrayOfNulls(monthList.size)
        for (i in monthList.indices) {
            monthArray!![i] = "" + monthList[i]
        }
    }

    private fun getStatusArray() {
        val statusList = filterContentResponse!!.response.status
        statusArray = arrayOfNulls(statusList.size)
        for (i in statusList.indices) {
            statusArray!![i] = "" + statusList[i].type
        }
    }

    private fun visiblePickerAnimation() {
        val bottomUp = AnimationUtils.loadAnimation(this,
                R.anim.bottom_up)
        selectedLayout.startAnimation(bottomUp)
        selectedLayout.setVisibility(View.VISIBLE)
    }

    private fun inVisiblePickerAnimation() {
        if (selectedLayout.getVisibility() === View.VISIBLE) {
            val bottomUp = AnimationUtils.loadAnimation(this,
                    R.anim.bottom_down)
            selectedLayout.startAnimation(bottomUp)
            selectedLayout.setVisibility(View.GONE)
        }
    }

    private object ClickHelper {
        const val CLICK_CATEGORY = 1
        const val CLICK_YEAR = 2
        const val CLICK_MONTH = 3
        const val CLICK_STATUS = 4
    }

    override fun callBack(selectorId: Int) {
        this.selectedIdVal = selectorId
      numberPicker.setDisplayedValues(null)
        setNumberPicker()
    }
    private fun setNumberPicker() {
        if (selectedIdVal == ClickHelper.CLICK_CATEGORY) {
            getCategoryArray()
            numberPicker.setMinValue(0)
            numberPicker.setMaxValue(categoryArray.size - 1)
            numberPicker.setDisplayedValues(categoryArray)
            numberPicker.setValue(categorySelectedPosition)
        } else if (selectedIdVal == ClickHelper.CLICK_YEAR) {
            getYearArray()
            numberPicker.setMinValue(0)
            numberPicker.setMaxValue(yearArray.size - 1)
            numberPicker.setDisplayedValues(yearArray)
            numberPicker.setValue(yearSelectedPosition)
        } else if (selectedIdVal == ClickHelper.CLICK_MONTH) {
            getMonthArray()
            numberPicker.minValue = 0
            numberPicker.maxValue = monthArray.size - 1
            numberPicker.displayedValues = monthArray
            numberPicker.value = monthSelectedPosition
        }
//        else if (selectedIdVal == ClickHelper.CLICK_STATUS) {
//            getStatusArray()
//            numberPicker.minValue = 0
//            numberPicker.maxValue = statusArray.size - 1
//            numberPicker.displayedValues = statusArray
//            numberPicker.value = statusSelectedPosition
//        }
        visiblePickerAnimation()
        cancelPicker.setOnClickListener(View.OnClickListener { inVisiblePickerAnimation() })
        confirmPicker.setOnClickListener(View.OnClickListener {
            inVisiblePickerAnimation()
            if (selectedIdVal == ClickHelper.CLICK_CATEGORY) {
                categorySelectedPosition = numberPicker.getValue()
                val filterModel = FilterModel()
                filterModel.categoryId = 1
                filterModel.categoryName = "Category"
                filterModel.categorySelectedValue = filterContentResponse!!.response.category[categorySelectedPosition].vTitle
                filterModelList!!.set(0, filterModel)
            } else if (selectedIdVal == ClickHelper.CLICK_YEAR) {
                yearSelectedPosition = numberPicker.getValue()
                val filterModel = FilterModel()
                filterModel.categoryId = 2
                filterModel.categoryName = "Select Year"
                filterModel.categorySelectedValue = "" + filterContentResponse!!.response.year[yearSelectedPosition]
                filterModelList!!.set(1, filterModel)
            } else if (selectedIdVal == ClickHelper.CLICK_MONTH) {
                monthSelectedPosition = numberPicker.getValue()
                val filterModel = FilterModel()
                filterModel.categoryId = 3
                filterModel.categoryName = "Select Month"
                filterModel.categorySelectedValue = "" + filterContentResponse!!.response.month[monthSelectedPosition]
                filterModelList!!.set(2, filterModel)
            }
//            else if (selectedIdVal == ClickHelper.CLICK_STATUS) {
//                statusSelectedPosition = numberPicker.getValue()
//                val filterModel = FilterModel()
//                filterModel.categoryId = 4
//                filterModel.categoryName = "Select Status"
//                filterModel.categorySelectedValue = "" + filterContentResponse!!.response.status[statusSelectedPosition].type
//                filterModelList!!.set(3, filterModel)
//            }
            if (adapter != null) {
                adapter!!.notifyDataSetChanged()
            }
        })
    }
}