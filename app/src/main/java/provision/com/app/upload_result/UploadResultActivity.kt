package provision.com.app.upload_result

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import android.view.View
import kotlinx.android.synthetic.main.header_layout.*
import provision.com.app.R
import provision.com.app.activity.BaseActivity

class UploadResultActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_result)
        if (savedInstanceState == null) {
            val fragment = UploadResultFragment.newInstance()
            if (intent.extras != null) {
                fragment.arguments = intent.extras
            }
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, fragment)
                    .commitNow()
        }
        back.visibility= View.VISIBLE
        back.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.close_cart));
        back.setOnClickListener { finish() }
        tvTitle.setText(R.string.upload_result);
    }
}
