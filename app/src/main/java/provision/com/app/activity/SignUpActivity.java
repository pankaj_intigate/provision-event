package provision.com.app.activity;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;

import provision.com.app.R;
import provision.com.app.binder.SignUpBinder;
import provision.com.app.databinding.ActivitySignUpBinding;

/**
 * Created by gauravg on 7/6/2018.
 */

public class SignUpActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivitySignUpBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);
        String email = getIntent().getStringExtra("email");
        SignUpBinder signUpBinder = new SignUpBinder(this, binding, email);
        binding.setSignUp(signUpBinder);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
