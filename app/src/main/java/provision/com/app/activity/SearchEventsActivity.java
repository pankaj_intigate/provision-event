package provision.com.app.activity;

import android.app.Activity;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import provision.com.app.R;
import provision.com.app.adapter.EventAdapter;
import provision.com.app.apiResponse.eventSearchResponse.EventSearchResponse;
import provision.com.app.apiResponse.eventSearchResponse.ProductList;
import provision.com.app.binder.SearchEventsBinder;
import provision.com.app.databinding.ActivitySearchEventsBinding;
import provision.com.app.utils.AppConstant;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;

/**
 * Created by gauravg on 7/13/2018.
 */

public class SearchEventsActivity extends BaseActivity {
    private final int REQUEST_CODE = 103;
    private ActivitySearchEventsBinding binding;
    private List<ProductList> productLists = new ArrayList<>();
    private EventAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search_events);
        SearchEventsBinder binder = new SearchEventsBinder(this, binding);
        binding.setSearchEvents(binder);
        binding.icFilter.setOnClickListener(v -> {
            Intent intent = new Intent(this, FilterActivity.class);
            startActivityForResult(intent, REQUEST_CODE);
        });
        binding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable text) {
                String searchText = text.toString();
                if (!TextUtils.isEmpty(searchText) && searchText.length() > 2) {
                    getSearchEventData(searchText, "", 2018, "", "");
                }
//                else if (!TextUtils.isEmpty(searchText) && searchText.length() > 1) {
//                    getSearchEventData("", "", year, "", "");
//                }

            }
        });
        binding.etSearch.setOnEditorActionListener(new EditText.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER
                        || actionId == EditorInfo.IME_ACTION_DONE) {
                    getSearchEventData(binding.etSearch.getText().toString(), "", 2018, "", "");
                    return true;
                }
                return false;
            }
        });
        setEventAdapter();
    }

    public void getSearchEventData(String searchText, String categoryId, int yearId, String monthId, String statusId) {
        if (isOnline()) {
            productLists.clear();
            View view = showProgressBar();
            AuthApiHelper authApiHelper = ApiClient.getClient(this).create(AuthApiHelper.class);
            retrofit2.Call<EventSearchResponse> call = authApiHelper.searchEventResponse("GetAjaxEventsListing", searchText, categoryId, yearId, monthId, statusId,ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<EventSearchResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    cancelProgressBar(view);
                    EventSearchResponse eventDetailResponse = (EventSearchResponse) object;
                    productLists.clear();
                    if (eventDetailResponse.getResponse().getProductList() != null) {
                        productLists.addAll(eventDetailResponse.getResponse().getProductList());
                        binding.rcvSearchList.setVisibility(View.VISIBLE);
                        binding.noDataFoundText.setVisibility(View.GONE);
                    } else {
                        binding.rcvSearchList.setVisibility(View.GONE);
                        binding.noDataFoundText.setVisibility(View.VISIBLE);
                    }
                    if (productLists.size() > 0) {
                        binding.icFilter.setVisibility(View.VISIBLE);
                    } else {
                        binding.icFilter.setVisibility(View.GONE);
                    }
                    if (mAdapter != null) {
                        mAdapter.notifyDataSetChanged();
                    } else {
                        setEventAdapter();
                    }
                }

                @Override
                protected void onError(RetroError retroError) {
                    cancelProgressBar(view);
                }

                @Override
                protected void onFailure(String retroError) {
                    cancelProgressBar(view);
                }
            });
        } else {
            showToast(this, getResources().getString(R.string.check_internet));
        }
    }

    private void setEventAdapter() {
        binding.rcvSearchList.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new EventAdapter(this, productLists);
        binding.rcvSearchList.setAdapter(mAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == REQUEST_CODE) {
            String categoryId = intent.getStringExtra(AppConstant.KEY_CATEGORY);
            String yearId = intent.getStringExtra(AppConstant.KEY_YEAR);
            String monthId = intent.getStringExtra(AppConstant.KEY_MONTH);
            String statusId = intent.getStringExtra(AppConstant.KEY_STATUS);
            getSearchEventData(binding.etSearch.getText().toString(), categoryId, Integer.parseInt(yearId), monthId, statusId);
        } else if (requestCode == 101) {
            setResult(Activity.RESULT_OK, intent);
            finish();

        }
    }
}
