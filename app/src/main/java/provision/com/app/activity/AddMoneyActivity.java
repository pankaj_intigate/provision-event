package provision.com.app.activity;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;

import provision.com.app.R;
import provision.com.app.binder.AddMoneyBinder;
import provision.com.app.databinding.ActivityAddMoneyBinding;

/**
 * Created by gauravg on 7/18/2018.
 */

public class AddMoneyActivity extends BaseActivity {
    private ActivityAddMoneyBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_money);
        AddMoneyBinder addMoneyBinder = new AddMoneyBinder(this, binding);
        binding.setAddMoney(addMoneyBinder);
    }
}
