package provision.com.app.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import provision.com.app.R;
import provision.com.app.adapter.NotificationListAdapter;
import provision.com.app.apiResponse.notification.NotificationData;
import provision.com.app.apiResponse.notification.NotificationEntity;
import provision.com.app.databinding.ActivityNotificationListBinding;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;

import static provision.com.app.app.AppConstant.NOTIFICATION_EVENT_TOKEN;
import static provision.com.app.utils.CommonUtils.onTrackEventClick;

public class NotificationListActivity extends BaseActivity {
    ActivityNotificationListBinding binding;
    private List<NotificationData> list = new ArrayList<>();
    private NotificationListAdapter notificationListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification_list);
        binding.headerLayout.back.setVisibility(View.VISIBLE);
        binding.headerLayout.back.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.close_cart));
        binding.headerLayout.back.setOnClickListener(v -> {
            finish();
        });
        binding.headerLayout.tvTitle.setText(R.string.notification);
        binding.notificationRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        notificationListAdapter = new NotificationListAdapter(this, list);
        binding.notificationRecyclerView.setAdapter(notificationListAdapter);
        getNotification();
    }

    @Override
    protected void onResume() {
        super.onResume();
        onTrackEventClick(NOTIFICATION_EVENT_TOKEN);
    }

    private void getNotification() {
        //device android then device is 1 and 2 for ios
        if (isOnline()) {
            View view = showProgressBar();

            String newToken = PrefUtils.getFromPrefs(this, PrefUtils.TOKEN);
            Log.d("MyFirebaseMsgService", "Refreshed token: " + newToken);
            ApiClient.getClient(NotificationListActivity.this).create(AuthApiHelper.class).getNotificationList("getNotification", "1", newToken, ApiClient.API_VERSION).enqueue(new CallbackManager<NotificationEntity>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    NotificationEntity notificationEntity = (NotificationEntity) object;

                    if (notificationEntity == null || notificationEntity.getResponse() == null) {
                        Toast.makeText(NotificationListActivity.this, NotificationListActivity.this.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (notificationEntity.getResponse().getList() != null && notificationEntity.getResponse().getList().size() > 0) {
                        list.addAll(notificationEntity.getResponse().getList());
                        notificationListAdapter.notifyDataSetChanged();
                        binding.notificationRecyclerView.setVisibility(View.VISIBLE);
                        binding.noDataFoundText.setVisibility(View.GONE);
                    } else {
                        binding.notificationRecyclerView.setVisibility(View.GONE);
                        binding.noDataFoundText.setVisibility(View.VISIBLE);
                    }
                    cancelProgressBar(view);
                }

                @Override
                protected void onError(RetroError retroError) {
                    cancelProgressBar(view);
                }

                @Override
                protected void onFailure(String retroError) {
                    cancelProgressBar(view);
                }
            });
        }


    }
}