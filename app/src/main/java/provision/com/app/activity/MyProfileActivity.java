package provision.com.app.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import androidx.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import provision.com.app.R;
import provision.com.app.apiResponse.updateProfileImageResponse.UpdateProfileImageResponse;
import provision.com.app.binder.MyProfileBinder;
import provision.com.app.databinding.ActivityMyProfileBinding;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.Utils;
import provision.com.app.utils.network.api.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static provision.com.app.app.AppConstant.EDIT_PROFILE_EVENT_TOKEN;
import static provision.com.app.app.AppConstant.VIEW_PROFILE_EVENT_TOKEN;
import static provision.com.app.utils.CommonUtils.onTrackEventClick;

/**
 * Created by gauravg on 7/10/2018.
 */

public class MyProfileActivity extends BaseActivity {
    static final int DATE_PICKER_ID = 999;
    final String[] selectFrom = {"Take Photo", "Choose from Gallery", "Cancel"};
    private final Context mContext = this;
    File file;
    private ActivityMyProfileBinding binding;
    private int year = 2018;
    private int month;
    private int day;
    private int PICK_IMAGE_REQUEST = 1;
    private int REQUEST_CAMERA = 2;
    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            // Show selected date
            long c = Calendar.getInstance().getTimeInMillis();


            StringBuilder dateStr = new StringBuilder().append(year).append("-").append(month + 1).append("-").append(day)
                    .append(" ");


            try {
                SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd");
                Date dateObj = curFormater.parse(dateStr.toString());
                dateObj.getTime();
                long result = (c - dateObj.getTime()) / 3600000;
                Log.d("system.out.println11", " " + result);
                if (result < 24) {
                    showToast(MyProfileActivity.this, "Please select valid date.");
                    return;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            binding.etDob.setText(dateStr);

        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_profile);
        MyProfileBinder myProfileBinder = new MyProfileBinder(this, binding);
        binding.setMyProfile(myProfileBinder);

        //  Glide.with(MyProfileActivity.this).load(TextUtils.isEmpty(PrefUtils.getFromPrefs(this, PrefUtils.USER_IMAGE)) ? R.drawable.profile_p : PrefUtils.getFromPrefs(this, PrefUtils.USER_IMAGE)).placeholder(R.drawable.profile_p).into(binding.ivProfile);

        Picasso.get()
                .load(TextUtils.isEmpty(PrefUtils.getFromPrefs(this, PrefUtils.USER_IMAGE)) ? "null" : PrefUtils.getFromPrefs(this, PrefUtils.USER_IMAGE))
                .placeholder(R.drawable.profile_p)
                .into(binding.ivProfile);

        binding.etDob.setOnClickListener(v -> {
            SelectDate();
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            View view = window.getDecorView();
            Utils.setDarkStatusBar(view, this);
        }

        binding.tvEditPhoto.setOnClickListener(v -> {
            if (Utils.isCheckWritePermission(this)) {
                // galleryIntent();
                AlertDialog.Builder builder = new AlertDialog.Builder(MyProfileActivity.this);
                builder.setTitle("Profile photo");
                builder.setItems(selectFrom, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (selectFrom[item].equals("Take Photo")) {
                            cameraIntent();
                        } else if (selectFrom[item].equals("Choose from Gallery")) {
                            galleryIntent();
                        } else if (selectFrom[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            }
        });

    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onResume() {
        super.onResume();
        onTrackEventClick(VIEW_PROFILE_EVENT_TOKEN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                binding.ivProfile.setImageBitmap(bitmap);

                if (uri != null) {
                    String filePath = getRealPathFromUri(uri);
                    if (filePath != null && !filePath.isEmpty()) {
                        file = new File(filePath);
                        file = new Compressor(this).compressToFile(file);
                        hitUpdateImageService(file, PrefUtils.getFromPrefs(this, PrefUtils.USER_ID));
                    }
                }
                hitUpdateImageService(file, PrefUtils.getFromPrefs(this, PrefUtils.USER_ID));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            File destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".jpg");
            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            binding.ivProfile.setImageBitmap(thumbnail);

            Uri uri = Uri.fromFile(destination);
            if (uri != null) {
                String filePath = getRealPathFromUri(uri);
                if (filePath != null && !filePath.isEmpty()) {
                    file = new File(filePath);
                    try {
                        file = new Compressor(this).compressToFile(file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    hitUpdateImageService(file, PrefUtils.getFromPrefs(this, PrefUtils.USER_ID));
                }
            }
            hitUpdateImageService(file, PrefUtils.getFromPrefs(this, PrefUtils.USER_ID));
        }
    }

    private void hitUpdateImageService(File imageFile, String userId) {
        View progressBar = showProgressBar();
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), imageFile);
        MultipartBody.Part body = MultipartBody.Part.createFormData("Image", file.getName(), requestBody);
        RequestBody id = RequestBody.create(MediaType.parse("text/plain"), userId);
        RequestBody command = RequestBody.create(MediaType.parse("text/plain"), "ChangeUserProfile");
        RequestBody version = RequestBody.create(MediaType.parse("text/plain"), ApiClient.API_VERSION);

        ApiClient.getAPiAuthHelper(this).updateProfileImage(command, body, id, version).enqueue(new Callback<UpdateProfileImageResponse>() {
            @Override
            public void onResponse(Call<UpdateProfileImageResponse> call, Response<UpdateProfileImageResponse> response) {
                cancelProgressBar(progressBar);
                if (response.body() == null || response.body().getResponse() == null) {
                    Toast.makeText(MyProfileActivity.this, MyProfileActivity.this.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (response.body().getStatus().equals("1")) {
                    showToast(MyProfileActivity.this, response.body().getResponseMessege());
                    //  Glide.with(MyProfileActivity.this).load(TextUtils.isEmpty(PrefUtils.getFromPrefs(MyProfileActivity.this,PrefUtils.USER_IMAGE))?R.drawable.profile_p:response.body().getResponse().getUserProfile()).placeholder(R.drawable.profile_p).into(binding.ivProfile);
                    PrefUtils.saveToPrefs(MyProfileActivity.this, PrefUtils.USER_IMAGE, response.body().getResponse().getUserProfile());
                    onTrackEventClick(EDIT_PROFILE_EVENT_TOKEN);
                } else {
                    showToast(MyProfileActivity.this, response.body().getResponseMessege());
                }
            }

            @Override
            public void onFailure(Call<UpdateProfileImageResponse> call, Throwable t) {

            }
        });
    }

    public String getRealPathFromUri(final Uri uri) {
        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(mContext, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(mContext, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } /*else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }*/

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(mContext, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(mContext, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private String getDataColumn(Context context, Uri uri, String selection,
                                 String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    private void SelectDate() {

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);


        showDialog(DATE_PICKER_ID);

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:

                // open datepicker dialog.
                // set date picker for current date
                // add pickerListener listner to date picker
                return new DatePickerDialog(this, pickerListener, year, month, day);
        }
        return null;
    }

}
