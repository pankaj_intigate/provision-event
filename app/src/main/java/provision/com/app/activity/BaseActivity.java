package provision.com.app.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import provision.com.app.R;
import provision.com.app.interfaces.RetryListener;

/**
 * Created by gauravg on 7/5/2018.
 */

public class BaseActivity extends AppCompatActivity {
    NetworkChangeReceiver mReceiver;
    TextView retry = null;
    private ViewGroup viewGroup;
    private View noConnectionView;
    private RetryListener retryListener;
    private boolean isRetryEnable;

    public void setRetryEnable(boolean retryEnable) {
        isRetryEnable = retryEnable;
    }

    public void setRetryListener(RetryListener retryListener) {
        this.retryListener = retryListener;
    }

    public void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public View showProgressBar() {
        viewGroup = findViewById(android.R.id.content);
        View view = LayoutInflater.from(this).inflate(R.layout.progress_layout, null);
        viewGroup.addView(view);
        return view;
    }

    public View showSplashProgress() {
        viewGroup = findViewById(android.R.id.content);
        View view = LayoutInflater.from(this).inflate(R.layout.progress_layout_white, null);
        viewGroup.addView(view);
        return view;
    }

    public void cancelProgressBar(View view) {
        if (view != null) {
            viewGroup.removeView(view);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        filter.addAction("android.net.conn.Ne");
        // Your BroadcastReceiver class
        mReceiver = new NetworkChangeReceiver();
        registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        setRetryEnable(false);
        unregisterReceiver(mReceiver);
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    public void hideKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // PrefUtils.saveToPrefs(BaseActivity.this, PrefUtils.session_id, "");
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public void showNetworkLayout() {
        if (viewGroup == null) {
            viewGroup = findViewById(android.R.id.content);
        }
        if (noConnectionView == null) {
            noConnectionView = LayoutInflater.from(BaseActivity.this).inflate(R.layout.no_internet_connection, null);

        }
        retry = noConnectionView.findViewById(R.id.retryTextView);
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            if (viewGroup.getChildAt(i) == noConnectionView) {
                viewGroup.removeView(noConnectionView);
            }
        }
        viewGroup.addView(noConnectionView);
        if (retry != null) {
            retry.setOnClickListener(v -> {
                ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

                if (retryListener != null) {
                    if (isOnline()) {
                        if (manager.getActiveNetworkInfo() != null
                                && manager.getActiveNetworkInfo().isAvailable()
                                && manager.getActiveNetworkInfo().isConnected()) {
                            // txt_status.setText("Internet i return true;s working");
                            viewGroup.removeView(noConnectionView);
                            retryListener.onRetry();

                        }

                    }
                }

            });
        }
    }

    class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {

            ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = manager.getActiveNetworkInfo();
            boolean isConnected = networkInfo != null &&
                    networkInfo.isConnectedOrConnecting();
            if (!isConnected && isRetryEnable) {
                showNetworkLayout();
            }


        }
    }
}
