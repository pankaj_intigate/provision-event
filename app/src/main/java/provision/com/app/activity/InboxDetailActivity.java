package provision.com.app.activity;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;

import provision.com.app.R;
import provision.com.app.binder.InboxDetailBinder;
import provision.com.app.databinding.ActivityInboxDetailBinding;

import static provision.com.app.app.AppConstant.INBOX_DETAIL_EVENT_TOKEN;
import static provision.com.app.utils.CommonUtils.onTrackEventClick;

/**
 * Created by gauravg on 7/18/2018.
 */

public class InboxDetailActivity extends BaseActivity {
    String getMailId;
    InboxDetailBinder inboxDetailBinder;
    private ActivityInboxDetailBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_inbox_detail);
        if (getIntent() != null) {
            getMailId = getIntent().getStringExtra("MailId");
        }
        inboxDetailBinder = new InboxDetailBinder(this, binding, getMailId);
        binding.setInboxDetail(inboxDetailBinder);
    }

    @Override
    protected void onResume() {
        super.onResume();
        inboxDetailBinder.getInboxDetails(getMailId);
        onTrackEventClick(INBOX_DETAIL_EVENT_TOKEN);
    }
}
