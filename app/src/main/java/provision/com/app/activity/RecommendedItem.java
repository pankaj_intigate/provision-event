package provision.com.app.activity;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import provision.com.app.R;
import provision.com.app.adapter.RecommendedItemAdapter;
import provision.com.app.apiResponse.recomded_itm.RecomdedItem;
import provision.com.app.databinding.ActivityRecommendedItemBinding;
import provision.com.app.utils.GridSpacingItemDecoration;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.network.api.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecommendedItem extends BaseActivity {
    ArrayList<provision.com.app.apiResponse.recomded_itm.Response> mList = new ArrayList<>();
    RecommendedItemAdapter recommendedItemAdapter;
    ActivityRecommendedItemBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

         binding = DataBindingUtil.setContentView(this, R.layout.activity_recommended_item);
        binding.itemRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        int spacing = (int) getResources().getDimension(R.dimen.dim_8dp); // 50px
        boolean includeEdge = true;
        binding.itemRecyclerView.addItemDecoration(new GridSpacingItemDecoration(2, spacing, includeEdge));
        binding.headerLayout.tvTitle.setText("Recommended Item");
        binding.headerLayout.back.setVisibility(View.VISIBLE);
        binding.headerLayout.back.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.close_cart));
        binding.headerLayout.back.setOnClickListener(v -> {
            finish();
        });
        recommendedItemAdapter = new RecommendedItemAdapter(this, mList);
        binding.itemRecyclerView.setAdapter(recommendedItemAdapter);
        getRecomdedItme(getIntent().getStringExtra("eventIds"));
    }


    private void getRecomdedItme(String eventId) {
        if (isOnline()) {
            View view = showProgressBar();


            ApiClient.getAPiAuthHelper(this).getRecomdedItem("addcartItem", eventId, TextUtils.isEmpty(PrefUtils.getFromPrefs(this, PrefUtils.COUNTRY_CODE)) ? "KWD" : PrefUtils.getFromPrefs(this, PrefUtils.COUNTRY_CODE),ApiClient.API_VERSION).enqueue(new Callback<RecomdedItem>() {
                @Override
                public void onResponse(Call<RecomdedItem> call, Response<RecomdedItem> response) {
                    mList.clear();
                    cancelProgressBar(view);
                    if (response.body() == null || response.body().getResponse() == null) {
                        Toast.makeText(RecommendedItem.this, RecommendedItem.this.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    recommendedItemAdapter.setCurrencyCode(response.body().getCurrrencyCode());
                    recommendedItemAdapter.setCurrencyValue(response.body().getCurrencyValue());
                    recommendedItemAdapter.setNumberFormat(response.body().getNumberformat());
                    mList.addAll(response.body().getResponse());
                    recommendedItemAdapter.notifyDataSetChanged();
                    if (mList.size()>0){
                        binding.noDataFoundText.setVisibility(View.GONE);
                        binding.itemRecyclerView.setVisibility(View.VISIBLE);
                    }else {
                        binding.noDataFoundText.setVisibility(View.VISIBLE);
                        binding.itemRecyclerView.setVisibility(View.GONE);
                    }

                }

                @Override
                public void onFailure(Call<RecomdedItem> call, Throwable t) {
                    cancelProgressBar(view);
                }
            });
        } else {
            showToast(this, getResources().getString(R.string.check_internet));
        }
    }
}
