package provision.com.app.activity;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import android.view.View;

import provision.com.app.R;
import provision.com.app.databinding.ActivityMoreBinding;
import provision.com.app.fragment.MoreFragment;

/**
 * Created by gauravg on 7/18/2018.
 */

public class MoreActivity extends BaseActivity {
    private ActivityMoreBinding binding;
    private MoreFragment moreFragment;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_more);

        binding.headerLayout.imgHeader.setVisibility(View.VISIBLE);
        binding.headerLayout.back.setVisibility(View.VISIBLE);

        binding.headerLayout.back.setOnClickListener(v -> {
            finish();
        });

        moreFragment = new MoreFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, moreFragment);
        transaction.commit();
    }
}
