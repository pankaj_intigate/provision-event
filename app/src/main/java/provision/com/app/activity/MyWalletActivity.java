package provision.com.app.activity;

import android.app.Activity;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import provision.com.app.R;
import provision.com.app.binder.MyWalletBinder;
import provision.com.app.databinding.ActivityMyWalletBinding;
import provision.com.app.utils.PrefUtils;

import static provision.com.app.app.AppConstant.MY_WALLET_EVENT_TOKEN;
import static provision.com.app.utils.CommonUtils.onTrackEventClick;

/**
 * Created by gauravg on 7/18/2018.
 */

public class MyWalletActivity extends BaseActivity {
    private ActivityMyWalletBinding binding;
    private MyWalletBinder myWalletBinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_wallet);
        myWalletBinder = new MyWalletBinder(this, binding);
        binding.setMyWallet(myWalletBinder);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM)) && !PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM).equals("0")) {
            binding.headerLayout.cartCountTextView.setVisibility(View.VISIBLE);
            binding.headerLayout.cartCountTextView.setText(PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM));
        } else {
            binding.headerLayout.cartCountTextView.setVisibility(View.GONE);
        }
        if (isOnline()) {
            myWalletBinder.getMyWallet(PrefUtils.getFromPrefs(this, PrefUtils.EMAIL_ID));
        } else {
            showToast(this, getResources().getString(R.string.check_internet));
        }

        onTrackEventClick(MY_WALLET_EVENT_TOKEN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK);
            finish();
        }
    }
}
