package provision.com.app.activity;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;

import com.squareup.picasso.Picasso;

import provision.com.app.R;
import provision.com.app.databinding.ActivityImageDialogBinding;

/**
 * Created by gauravg on 8/14/2018.
 */

public class ImageDialogActivity extends BaseActivity {
    private ActivityImageDialogBinding binding;
    private String getImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_image_dialog);

        if (getIntent() != null) {
            getImage = getIntent().getStringExtra("Image_Key");
        }

        binding.ivClose.setOnClickListener(v -> {
            finish();
        });

        Picasso.get().load(getImage).into(binding.ivFullImage);
    }
}
