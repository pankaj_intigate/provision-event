package provision.com.app.activity;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;

import provision.com.app.R;
import provision.com.app.binder.MyEventDetailsBinder;
import provision.com.app.databinding.ActivityMyEventsDetailsBinding;

import static provision.com.app.app.AppConstant.MY_BOOKING_DETAIL_EVENT_TOKEN;
import static provision.com.app.utils.CommonUtils.onTrackEventClick;

/**
 * Created by gauravg on 7/10/2018.
 */

public class MyEventDetailsActivity extends BaseActivity {
    private ActivityMyEventsDetailsBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_events_details);
        String bookingId = getIntent().getStringExtra("bookingId");
        MyEventDetailsBinder myEventDetailsBinder = new MyEventDetailsBinder(this, binding, bookingId);
        binding.setMyEventDetails(myEventDetailsBinder);


    }

    @Override
    protected void onResume() {
        super.onResume();
        onTrackEventClick(MY_BOOKING_DETAIL_EVENT_TOKEN);
    }
}
