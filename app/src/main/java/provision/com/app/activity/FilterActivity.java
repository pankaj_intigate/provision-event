package provision.com.app.activity;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;

import provision.com.app.R;
import provision.com.app.binder.FilterBinder;
import provision.com.app.databinding.ActivityFilterBinding;

/**
 * Created by gauravg on 7/13/2018.
 */

public class FilterActivity extends BaseActivity {
    ActivityFilterBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_filter);
        FilterBinder binder = new FilterBinder(FilterActivity.this, binding);
        binding.setFilter(binder);
    }
}
