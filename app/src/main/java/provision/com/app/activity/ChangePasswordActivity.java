package provision.com.app.activity;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;

import provision.com.app.R;
import provision.com.app.binder.ChangePasswordBinder;
import provision.com.app.databinding.ActivityChangePasswordBinding;

/**
 * Created by gauravg on 7/17/2018.
 */

public class ChangePasswordActivity extends BaseActivity {
    private ActivityChangePasswordBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);
        ChangePasswordBinder changePasswordBinder = new ChangePasswordBinder(this, mBinding);
        mBinding.setChangePassword(changePasswordBinder);
    }
}
