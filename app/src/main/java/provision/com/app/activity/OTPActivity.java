package provision.com.app.activity;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;

import provision.com.app.R;
import provision.com.app.binder.OTPBinder;
import provision.com.app.databinding.ActivityOtpBinding;

/**
 * Created by gauravg on 7/17/2018.
 */

public class OTPActivity extends BaseActivity {
    private ActivityOtpBinding binding;
    private String mGetEmail = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_otp);
        if (getIntent() != null) {
            mGetEmail = getIntent().getStringExtra("EmailId");
        }
        OTPBinder otpBinder = new OTPBinder(this, binding, mGetEmail);
        binding.setOtp(otpBinder);

    }

}
