package provision.com.app.activity;

import android.app.Activity;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import provision.com.app.BuildConfig;
import provision.com.app.R;
import provision.com.app.adapter.EventDetailPagerAdapter;
import provision.com.app.apiResponse.eventDetailResponse.EventDetailResponse;
import provision.com.app.apiResponse.eventDetailResponse.Feature;
import provision.com.app.databinding.ActivityEventDetailsBinding;
import provision.com.app.fragment.EventDeatilsFragment;
import provision.com.app.fragment.FAQFragment;
import provision.com.app.fragment.FeeFragment;
import provision.com.app.fragment.MapFragment;
import provision.com.app.fragment.OrganiserFragment;
import provision.com.app.fragment.OutletFragment;
import provision.com.app.fragment.RaceTrackFragment;
import provision.com.app.fragment.RacekitFragment;
import provision.com.app.fragment.SponsorsFragment;
import provision.com.app.fragment.TipsAndAdviceFragment;
import provision.com.app.interfaces.RetryListener;
import provision.com.app.model.TabFragmentData;
import provision.com.app.utils.AppConstant;
import provision.com.app.utils.CustomAlertDialog;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.ApiConstant;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;
import static provision.com.app.app.AppConstant.EVENT_DETAILS_EVENT_TOKEN;
import static provision.com.app.app.AppConstant.SHARE_EVENT_EVENT_TOKEN;
import static provision.com.app.utils.CommonUtils.onTrackEventClick;
import static provision.com.app.utils.CommonUtils.onTrackEventWithDetails;

public class EventDetailsActivity extends BaseActivity implements RetryListener {
    int overAllPosition = 0;
    private ActivityEventDetailsBinding eventDetailsBinding;
    private EventDetailsActivity mActivity;
    private EventDetailPagerAdapter eventDetailPagerAdapter;
    private String shareUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        eventDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_event_details);
//        setRetryEnable(true);
        setRetryListener(this);
        mActivity = EventDetailsActivity.this;
        eventDetailsBinding.headerLayout.ivShareEvent.setOnClickListener(v -> {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, shareUrl);
            sendIntent.setType("text/plain");
            startActivity(sendIntent);

            onTrackEventClick(SHARE_EVENT_EVENT_TOKEN);
        });
        eventDetailsBinding.headerLayout.ivShareEvent.setVisibility(View.VISIBLE);
        eventDetailsBinding.headerLayout.cartRL.setOnClickListener(v -> {
            Intent cartIntent = new Intent(this, CartActivity.class);
            startActivityForResult(cartIntent, 101);
            overridePendingTransition(R.anim.bottom_up, R.anim.stay);
        });
        eventDetailsBinding.headerLayout.back.setOnClickListener(v -> {
            finish();
        });
        String eventId = "0";
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            eventId = intent.getStringExtra(AppConstant.KEY_EVENT_ID);
            String eventName = intent.getStringExtra(ApiConstant.KEY_EVENT_NAME);
            eventDetailsBinding.headerLayout.tvHeader.setText(eventName);
            onTrackEventWithDetails(EVENT_DETAILS_EVENT_TOKEN,"EVENT_NAME",eventName);
            getEventDetail(eventId);
        }
        String finalEventId = eventId;
        eventDetailsBinding.btnRegisterNow.setOnClickListener((View v) -> {
//            Intent intent1 = new Intent(this, HomeActivity.class);
//            intent1.putExtra("eventId", finalEventId);
//            startActivityForResult(intent1, 101);
            Intent intent1 = new Intent();
            intent1.putExtra(AppConstant.KEY_EVENT_ID, finalEventId);
            setResult(Activity.RESULT_OK, intent1);
            finish();
        });
//        eventDetailsBinding.btnRegisterNow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
    }


    /**
     * A placeholder fragment containing a simple view.
     */


    private void getEventDetail(String eventId) {
        if (mActivity.isOnline()) {
            View view = mActivity.showSplashProgress();
            AuthApiHelper authApiHelper = ApiClient.getClient(mActivity).create(AuthApiHelper.class);
            retrofit2.Call<EventDetailResponse> call = authApiHelper.eventDetail("GetEventdetail", eventId,ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<EventDetailResponse>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    mActivity.cancelProgressBar(view);
                    EventDetailResponse eventDetailResponse = (EventDetailResponse) object;
                    if (eventDetailResponse == null || eventDetailResponse.getResponse() == null) {
                        Toast.makeText(mActivity, mActivity.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    int versionCode = BuildConfig.VERSION_CODE;
                    if (eventDetailResponse.getVersion() > versionCode) {
                        CustomAlertDialog.forceUpdate(mActivity);
                    }
                    shareUrl = eventDetailResponse.getResponse().getShareUrl();
                    setEventTab(eventDetailResponse);

                }

                @Override
                protected void onError(RetroError retroError) {
                    showNetworkLayout();
                    mActivity.cancelProgressBar(view);
                }

                @Override
                protected void onFailure(String retroError) {
                    showNetworkLayout();
                    mActivity.cancelProgressBar(view);
                }
            });
        } else {
            mActivity.showToast(mActivity, mActivity.getResources().getString(R.string.check_internet));
        }
    }

    private void setEventTab(EventDetailResponse eventDetailResponse) {
        if (eventDetailResponse.getResponse().getEventDetail().getIsRegistration().equals("1") && eventDetailResponse.getResponse().getEventDetail().getIsRegisterShow().equals("1")) {
            eventDetailsBinding.btnRegisterNow.setVisibility(View.VISIBLE);
        }
        List<Feature> eventFetures = eventDetailResponse.getResponse().getFeature();
        ArrayList<TabFragmentData> tabFragmentDataList = new ArrayList<>();
        for (int i = 0; i < eventFetures.size(); i++) {
            if (eventFetures.get(i).getShow() == 1) {
                Bundle bundle = new Bundle();
                bundle.putString(AppConstant.KEY_EVENT_ID, eventFetures.get(i).getEventId());

                if (eventFetures.get(i).getTabId().equalsIgnoreCase("0")) {
                    TabFragmentData tabFragmentData = new TabFragmentData();
                    EventDeatilsFragment eventDeatilsFragment = EventDeatilsFragment.getInstance();
//                eventDeatilsFragment.setArguments(bundle);
                    tabFragmentData.setmFragment(eventDeatilsFragment);
                    tabFragmentData.setTabTitle(eventFetures.get(i).getTabName());
                    tabFragmentData.setTabId(eventFetures.get(i).getTabId());
                    tabFragmentData.setEventId(eventFetures.get(i).getEventId());
                    tabFragmentDataList.add(tabFragmentData);
                } else if (eventFetures.get(i).getTabId().equalsIgnoreCase("1")) {
                    TabFragmentData tabFragmentData = new TabFragmentData();

                    FeeFragment feeFragment = FeeFragment.getInstance();
//                feeFragment.setArguments(bundle);

                    tabFragmentData.setmFragment(feeFragment);
                    tabFragmentData.setTabTitle(eventFetures.get(i).getTabName());
                    tabFragmentData.setTabId(eventFetures.get(i).getTabId());
                    tabFragmentData.setEventId(eventFetures.get(i).getEventId());
                    tabFragmentDataList.add(tabFragmentData);
                } else if (eventFetures.get(i).getTabId().equalsIgnoreCase("2")) {
                    TabFragmentData tabFragmentData = new TabFragmentData();
                    RacekitFragment racekitFragment = RacekitFragment.getInstance();
//                racekitFragment.setArguments(bundle);
                    tabFragmentData.setmFragment(racekitFragment);
                    tabFragmentData.setTabTitle(eventFetures.get(i).getTabName());
                    tabFragmentData.setTabId(eventFetures.get(i).getTabId());
                    tabFragmentData.setEventId(eventFetures.get(i).getEventId());
                    tabFragmentDataList.add(tabFragmentData);
                } else if (eventFetures.get(i).getTabId().equalsIgnoreCase("3")) {
                } else if (eventFetures.get(i).getTabId().equalsIgnoreCase("4")) {
                    TabFragmentData tabFragmentData = new TabFragmentData();
                    OutletFragment outletFragment = OutletFragment.getInstance();
//                outletFragment.setArguments(bundle);
                    tabFragmentData.setmFragment(outletFragment);
                    tabFragmentData.setTabTitle(eventFetures.get(i).getTabName());
                    tabFragmentData.setTabId(eventFetures.get(i).getTabId());
                    tabFragmentData.setEventId(eventFetures.get(i).getEventId());
                    tabFragmentDataList.add(tabFragmentData);
                } else if (eventFetures.get(i).getTabId().equalsIgnoreCase("5")) {
                    TabFragmentData tabFragmentData = new TabFragmentData();

                    OrganiserFragment onOrganiserFragment = OrganiserFragment.getInstance();
//                onOrganiserFragment.setArguments(bundle);
                    tabFragmentData.setmFragment(onOrganiserFragment);
                    tabFragmentData.setTabTitle(eventFetures.get(i).getTabName());
                    tabFragmentData.setTabId(eventFetures.get(i).getTabId());
                    tabFragmentData.setEventId(eventFetures.get(i).getEventId());
                    tabFragmentDataList.add(tabFragmentData);
                } else if (eventFetures.get(i).getTabId().equalsIgnoreCase("6")) {
                    TabFragmentData tabFragmentData = new TabFragmentData();

                    SponsorsFragment sponsorsFragment = SponsorsFragment.getInstance();
//                sponsorsFragment.setArguments(bundle);
                    tabFragmentData.setmFragment(sponsorsFragment);
                    tabFragmentData.setTabTitle(eventFetures.get(i).getTabName());
                    tabFragmentData.setTabId(eventFetures.get(i).getTabId());
                    tabFragmentData.setEventId(eventFetures.get(i).getEventId());
                    tabFragmentDataList.add(tabFragmentData);
                } else if (eventFetures.get(i).getTabId().equalsIgnoreCase("7")) {
                    TabFragmentData tabFragmentData = new TabFragmentData();

                    RaceTrackFragment raceTrackFragment = RaceTrackFragment.getInstance();
                    raceTrackFragment.setArguments(bundle);
                    tabFragmentData.setmFragment(raceTrackFragment);
                    tabFragmentData.setTabTitle(eventFetures.get(i).getTabName());
                    tabFragmentData.setTabId(eventFetures.get(i).getTabId());
                    tabFragmentData.setEventId(eventFetures.get(i).getEventId());
                    tabFragmentDataList.add(tabFragmentData);
                }
                //volunteer hiddeen
//            else if (eventFetures.get(i).getTabId().equalsIgnoreCase("8")) {
//                TabFragmentData tabFragmentData = new TabFragmentData();
//                VolunteerFragment volunteerFragment = VolunteerFragment.getInstance();
////                volunteerFragment.setArguments(bundle);
//                tabFragmentData.setmFragment(volunteerFragment);
//                tabFragmentData.setTabTitle(eventFetures.get(i).getTabName());
//                tabFragmentData.setTabId(eventFetures.get(i).getTabId());
//                tabFragmentData.setEventId(eventFetures.get(i).getEventId());
//                tabFragmentDataList.add(tabFragmentData);
//            }
                else if (eventFetures.get(i).getTabId().equalsIgnoreCase("9")) {
                    TabFragmentData tabFragmentData = new TabFragmentData();
                    TipsAndAdviceFragment tipsAndAdviceFragment = TipsAndAdviceFragment.getInstance();
                    tipsAndAdviceFragment.setArguments(bundle);

                    tabFragmentData.setmFragment(tipsAndAdviceFragment);
                    tabFragmentData.setTabTitle(eventFetures.get(i).getTabName());
                    tabFragmentData.setTabId(eventFetures.get(i).getTabId());
                    tabFragmentData.setEventId(eventFetures.get(i).getEventId());
                    tabFragmentDataList.add(tabFragmentData);
                }

                //tearms and condition hidden
//            else if (eventFetures.get(i).getTabId().equalsIgnoreCase("10")) {
//                TabFragmentData tabFragmentData = new TabFragmentData();
//                TermsAndConditionsFragment termsAndConditionsFragment = TermsAndConditionsFragment.getInstance();
//                termsAndConditionsFragment.setArguments(bundle);
//                tabFragmentData.setmFragment(termsAndConditionsFragment);
//                tabFragmentData.setTabTitle(eventFetures.get(i).getTabName());
//                tabFragmentData.setTabId(eventFetures.get(i).getTabId());
//                tabFragmentData.setEventId(eventFetures.get(i).getEventId());
//                tabFragmentDataList.add(tabFragmentData);
//            }
                else if (eventFetures.get(i).getTabId().equalsIgnoreCase("11")) {
                    TabFragmentData tabFragmentData = new TabFragmentData();
                    FAQFragment faqFragment = FAQFragment.getInstance();
                    faqFragment.setArguments(bundle);
                    tabFragmentData.setmFragment(faqFragment);
                    tabFragmentData.setTabTitle(eventFetures.get(i).getTabName());
                    tabFragmentData.setTabId(eventFetures.get(i).getTabId());
                    tabFragmentData.setEventId(eventFetures.get(i).getEventId());
                    tabFragmentDataList.add(tabFragmentData);
                } else if (eventFetures.get(i).getTabId().endsWith("13")) {
                    TabFragmentData tabFragmentData = new TabFragmentData();
//                MapFragment mapFragment = new MapFragment(eventDetailResponse.getResponse().getEventDetail().getVAddressLong(), eventDetailResponse.getResponse().getEventDetail().getVAddressLat());
                    Bundle bundle1 = new Bundle();
                    bundle1.putString("address", eventDetailResponse.getResponse().getEventDetail().getVAddress());
                    bundle1.putString("latitude", eventDetailResponse.getResponse().getEventDetail().getVAddressLat());
                    bundle1.putString("longitude", eventDetailResponse.getResponse().getEventDetail().getVAddressLong());
                    MapFragment mapFragment = new MapFragment();
                    mapFragment.setArguments(bundle1);
                    tabFragmentData.setmFragment(mapFragment);
//                    tabFragmentData.setTabTitle(eventFetures.get(i).getTabName());
                    tabFragmentData.setTabTitle("Map");
                    tabFragmentData.setTabId("13");
                    tabFragmentDataList.add(tabFragmentData);
                }
            }
        }

        eventDetailsBinding.sliderRight.setOnClickListener(v -> {
            eventDetailsBinding.tabs.setSmoothScrollingEnabled(true);
            int right = ((ViewGroup) eventDetailsBinding.tabs.getChildAt(0)).getChildAt(eventDetailsBinding.tabs.getTabCount() - 1).getRight();
            eventDetailsBinding.tabs.scrollTo(right, 0);
        });
        eventDetailsBinding.sliderLeft.setOnClickListener(v -> {
            int right = ((ViewGroup) eventDetailsBinding.tabs.getChildAt(0)).getChildAt(0).getLeft();
            eventDetailsBinding.tabs.scrollTo(right, 0);
        });
        eventDetailPagerAdapter = new EventDetailPagerAdapter(getSupportFragmentManager(), tabFragmentDataList);
        eventDetailsBinding.container.setAdapter(eventDetailPagerAdapter);
        eventDetailsBinding.tabs.setupWithViewPager(eventDetailsBinding.container);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            eventDetailsBinding.tabs.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    overAllPosition = overAllPosition + scrollX;
                    int[] location = new int[2];
                    ((ViewGroup) eventDetailsBinding.tabs.getChildAt(0)).getChildAt(eventDetailsBinding.tabs.getTabCount() - 1).getLocationOnScreen(location);
                    Log.d("onScrollChange", scrollX + "  " + location[0]);
                    if (((ViewGroup) eventDetailsBinding.tabs.getChildAt(0)).getChildAt(0).getWidth() <= scrollX) {
                        eventDetailsBinding.sliderLeft.setVisibility(View.VISIBLE);
                    } else {
                        eventDetailsBinding.sliderLeft.setVisibility(View.GONE);
                    }
                    int rightDr = eventDetailsBinding.tabs.getWidth() - ((ViewGroup) eventDetailsBinding.tabs.getChildAt(0)).getChildAt(eventDetailsBinding.tabs.getTabCount() - 1).getWidth();
                    if (location[0] == rightDr) {
                        eventDetailsBinding.sliderRight.setVisibility(View.GONE);
                    } else {
                        eventDetailsBinding.sliderRight.setVisibility(View.VISIBLE);
                    }

//                    if (EventDetailsActivity.this.getResources().getDisplayMetrics().widthPixels < (eventDetailsBinding.tabs.getChildAt(0)).getWidth()) {
//                    eventDetailsBinding.sliderRight.setVisibility(View.VISIBLE);
//                } else {
//                    eventDetailsBinding.sliderRight.setVisibility(View.GONE);
//                }
                }
            });
        }
        eventDetailsBinding.tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                eventDetailsBinding.container.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        ViewTreeObserver vto = eventDetailsBinding.tabs.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    eventDetailsBinding.tabs.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    eventDetailsBinding.tabs.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
                int width = eventDetailsBinding.tabs.getChildAt(0).getMeasuredWidth();
                if (EventDetailsActivity.this.getResources().getDisplayMetrics().widthPixels < width) {
                    eventDetailsBinding.sliderRight.setVisibility(View.VISIBLE);
                } else {
                    eventDetailsBinding.sliderRight.setVisibility(View.GONE);
                }

            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        eventDetailsBinding.headerLayout.cartRL.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM)) && !PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM).equals("0")) {
            eventDetailsBinding.headerLayout.cartCountTextView.setVisibility(View.VISIBLE);
            eventDetailsBinding.headerLayout.cartCountTextView.setText(PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM));
        } else {
            eventDetailsBinding.headerLayout.cartCountTextView.setVisibility(View.GONE);
        }

       // onTrackEventClick(EVENT_DETAILS_EVENT_TOKEN);
    }

    @Override
    public void onRetry() {
        String eventId = getIntent().getStringExtra(AppConstant.KEY_EVENT_ID);
        getEventDetail(eventId);
    }
}
