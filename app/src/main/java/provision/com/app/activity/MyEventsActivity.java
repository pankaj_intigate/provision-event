package provision.com.app.activity;

import android.app.Activity;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import provision.com.app.R;
import provision.com.app.binder.MyEventsBinder;
import provision.com.app.databinding.ActivityMyEventsBinding;
import provision.com.app.utils.PrefUtils;

import static provision.com.app.app.AppConstant.MY_BOOKING_EVENT_TOKEN;
import static provision.com.app.utils.CommonUtils.onTrackEventClick;

/**
 * Created by gauravg on 7/10/2018.
 */

public class MyEventsActivity extends BaseActivity {
    private ActivityMyEventsBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_events);
        MyEventsBinder myEventsBinder = new MyEventsBinder(this, binding);
        binding.setMyEvents(myEventsBinder);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM)) && !PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM).equals("0")) {
            binding.headerLayout.cartCountTextView.setVisibility(View.VISIBLE);
            binding.headerLayout.cartCountTextView.setText(PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM));
        } else {
            binding.headerLayout.cartCountTextView.setVisibility(View.GONE);
        }

        onTrackEventClick(MY_BOOKING_EVENT_TOKEN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK);
            finish();
        }
    }
}
