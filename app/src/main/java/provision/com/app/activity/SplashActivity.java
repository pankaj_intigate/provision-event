package provision.com.app.activity;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;


import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;

import org.json.JSONObject;

import java.util.UUID;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;

import provision.com.app.R;
import provision.com.app.apiResponse.create_token.NotificateTokenUpdate;
import provision.com.app.databinding.ActivitySplashBinding;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;

/**
 * Created by gauravg on 7/5/2018.
 */

public class SplashActivity extends AppCompatActivity {
    ActivitySplashBinding binding;
    Trace myTrace;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
         myTrace = FirebasePerformance.getInstance().newTrace("test_trace");
        myTrace.start();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancelAll();
        myTrace.incrementMetric("test_trace", 1);

//        final Fabric fabric = new Fabric.Builder(this)
//                .kits(new Crashlytics())
//                .debuggable(true)  // Enables Crashlytics debugger
//                .build();
//        Fabric.with(fabric);
        updateToken();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String timeInMillis = System.currentTimeMillis() + "-" + UUID.randomUUID().toString();
                Log.d("SESSION_ID", timeInMillis);
                PrefUtils.saveToPrefs(SplashActivity.this, PrefUtils.SESSION_ID, timeInMillis);
                PrefUtils.saveToPrefs(SplashActivity.this, PrefUtils.CART_ITEM, "0");
                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                finish();
            }
        }, 2000);
    }


    @Override
    protected void onStart() {
        super.onStart();
        // Branch init
        Branch.getInstance().initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
               /* if (error == null) {
                    Log.i("BRANCH SDK", referringParams.toString());
                    // Retrieve deeplink keys from 'referringParams' and evaluate the values to determine where to route the user
                    // Check '+clicked_branch_link' before deciding whether to use your Branch routing logic
                    try {
                        String title = referringParams.getString("$og_title");
                        String link = referringParams.getString("$canonical_url");
                        String[] bits = link.split("/");
                        String lastOne = bits[bits.length-1];
                        Log.d("DeepLink is",lastOne);
                        Intent eventDetailIntent = new Intent(SplashActivity.this, EventDetailsActivity.class);
                        eventDetailIntent.putExtra(ApiConstant.KEY_EVENT_ID, lastOne);
                        eventDetailIntent.putExtra(ApiConstant.KEY_EVENT_NAME, title);
                        startActivityForResult(eventDetailIntent, 101);
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.i("BRANCH SDK", error.getMessage());
                }*/
            }
        }, this.getIntent().getData(), this);
    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }

    private void updateToken() {

            String newToken = PrefUtils.getFromPrefs(this,PrefUtils.TOKEN);;
            Log.d("MyFirebaseMsgService", "Refreshed token: " + newToken);
            ApiClient.getClient(SplashActivity.this).create(AuthApiHelper.class).createToken("createToken", "android", newToken,ApiClient.API_VERSION).enqueue(new CallbackManager<NotificateTokenUpdate>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    NotificateTokenUpdate notificateTokenUpdate = (NotificateTokenUpdate) object;
                    PrefUtils.saveToPrefs(SplashActivity.this, PrefUtils.NOTIFICATION_ENABLE, notificateTokenUpdate.getResponse().getIsNotify());

                }

                @Override
                protected void onError(RetroError retroError) {

                }

                @Override
                protected void onFailure(String retroError) {

                }
            });

    }

    @Override
    protected void onResume() {
        super.onResume();
        myTrace.stop();
    }
}

