package provision.com.app.activity;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import provision.com.app.R;
import provision.com.app.apiResponse.BaseResponce;
import provision.com.app.apiResponse.notification.NotificationData;
import provision.com.app.databinding.ActivityNotificationDetailsBinding;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.network.api.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static provision.com.app.app.AppConstant.NOTIFICATION_DETAILS_EVENT_TOKEN;
import static provision.com.app.utils.CommonUtils.onTrackEventClick;

public class NotificationDetailsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_details);
        ActivityNotificationDetailsBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_notification_details);
        binding.headerLayout.back.setVisibility(View.VISIBLE);
        binding.headerLayout.back.setOnClickListener(v -> {
            finish();
        });
        binding.headerLayout.tvTitle.setText(R.string.notification);
        NotificationData notificationData = getIntent().getParcelableExtra("notification");
        binding.tvTitle.setText(notificationData.getTitle());
        binding.tvMessage.setText(notificationData.getContent());
        binding.tvDate.setText(notificationData.getCreateDate());
        readNotification(notificationData.getId());
    }

    @Override
    protected void onResume() {
        super.onResume();
        onTrackEventClick(NOTIFICATION_DETAILS_EVENT_TOKEN);
    }

    void readNotification(String id) {

                String newToken = PrefUtils.getFromPrefs(this,PrefUtils.TOKEN);;
                ApiClient.getAPiAuthHelper(NotificationDetailsActivity.this).readNotification("checkRead", id, newToken,ApiClient.API_VERSION).enqueue(new Callback<BaseResponce>() {
                    @Override
                    public void onResponse(Call<BaseResponce> call, Response<BaseResponce> response) {

                    }

                    @Override
                    public void onFailure(Call<BaseResponce> call, Throwable t) {

                    }
                });
            }

}
