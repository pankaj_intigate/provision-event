package provision.com.app.activity;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;

import provision.com.app.R;
import provision.com.app.binder.InboxBinder;
import provision.com.app.databinding.ActivityInboxBinding;

import static provision.com.app.app.AppConstant.INBOX_EVENT_TOKEN;
import static provision.com.app.utils.CommonUtils.onTrackEventClick;

/**
 * Created by gauravg on 7/18/2018.
 */

public class InboxActivity extends BaseActivity {
    InboxBinder inboxBinder;
    private ActivityInboxBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_inbox);
        inboxBinder = new InboxBinder(this, binding);
        binding.setInbox(inboxBinder);
    }

    @Override
    protected void onResume() {
        super.onResume();
        inboxBinder.getInboxList();
        onTrackEventClick(INBOX_EVENT_TOKEN);
    }
}
