package provision.com.app.activity;

import android.app.Activity;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.TextUtils;
import android.view.View;
import android.view.Window;

import provision.com.app.R;
import provision.com.app.databinding.ActivityRegisterEventBinding;
import provision.com.app.fragment.RegisterNowFragment;
import provision.com.app.interfaces.UpdateCartCount;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.Utils;

public class RegisterEventActivity extends BaseActivity implements UpdateCartCount {
    private ActivityRegisterEventBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register_event);
        binding.headerLayout.tvTitle.setText("Registration");
        binding.headerLayout.back.setVisibility(View.VISIBLE);
        binding.headerLayout.back.setOnClickListener((View v) -> {
            Intent intent = new Intent();
            setResult(Activity.RESULT_OK, intent);
            finish();
        });
        binding.headerLayout.cartRL.setVisibility(View.VISIBLE);
        binding.headerLayout.cartRL.setOnClickListener(v -> {
//            startActivity(new Intent(RegisterEventActivity.this, CartActivity.class).putExtra("fromRegisterActivity", true));
//            overridePendingTransition(R.anim.bottom_up, R.anim.stay);
        });
        String eventId = "0";
        eventId = getIntent().getStringExtra("eventId");
        RegisterNowFragment registerNowFragment = RegisterNowFragment.newInstance(eventId, this);
        openFragment(registerNowFragment);
        binding.headerLayout.rlParent.setVisibility(View.VISIBLE);
        binding.headerLayout.tvCountry.setVisibility(View.GONE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            View view = window.getDecorView();
            Utils.setLightStatusBar(view, this);
        }
    }

    public void openFragment(Fragment fragment) {
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        upDateCartCount();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        super.onBackPressed();
    }

    @Override
    public void upDateCartCount() {
        if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM)) && !PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM).equals("0")) {
            binding.headerLayout.cartRL.setVisibility(View.VISIBLE);
            binding.headerLayout.cartCountTextView.setText(PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM));
        } else {
            binding.headerLayout.cartRL.setVisibility(View.GONE);
        }
    }
}
