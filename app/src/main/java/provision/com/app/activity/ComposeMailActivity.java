package provision.com.app.activity;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;

import provision.com.app.R;
import provision.com.app.binder.ComposeMailBinder;
import provision.com.app.databinding.ActivityComposeMailBinding;

/**
 * Created by gauravg on 7/18/2018.
 */

public class ComposeMailActivity extends BaseActivity {
    private ActivityComposeMailBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_compose_mail);
        ComposeMailBinder composeMailBinder = new ComposeMailBinder(this, binding);
        binding.setComposeMail(composeMailBinder);
    }
}
