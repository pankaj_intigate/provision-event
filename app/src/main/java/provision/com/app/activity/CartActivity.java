package provision.com.app.activity;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.view.Window;

import provision.com.app.R;
import provision.com.app.databinding.ActivityCartBinding;
import provision.com.app.fragment.CartFragment;
import provision.com.app.utils.Utils;

import static provision.com.app.app.AppConstant.CART_EVENT_TOKEN;
import static provision.com.app.utils.CommonUtils.onTrackEventClick;

public class CartActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityCartBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_cart);
        binding.headerLayout.tvTitle.setText("Cart");
        binding.headerLayout.back.setVisibility(View.VISIBLE);
        binding.headerLayout.back.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.close_cart));
        binding.headerLayout.back.setOnClickListener((View v) -> {
//            Intent intent = new Intent();
//            setResult(Activity.RESULT_CANCELED, intent);
            finish();
        });


        boolean isFinishOnAddClick = false;
        if (getIntent().hasExtra("fromRegisterActivity")) {
            isFinishOnAddClick = true;
        }
        CartFragment cartFragment = CartFragment.newInstance(isFinishOnAddClick);
        openFragment(cartFragment);
        binding.headerLayout.rlParent.setVisibility(View.VISIBLE);
        binding.headerLayout.tvCountry.setVisibility(View.GONE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            View view = window.getDecorView();
            Utils.setLightStatusBar(view, this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        onTrackEventClick(CART_EVENT_TOKEN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    public void openFragment(Fragment fragment) {
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.commit();
    }

}
