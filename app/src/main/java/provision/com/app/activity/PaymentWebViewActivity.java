package provision.com.app.activity;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;

import provision.com.app.R;
import provision.com.app.databinding.ActivityPaymentWebViewBinding;

public class PaymentWebViewActivity extends AppCompatActivity {
    ActivityPaymentWebViewBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_web_view);
         binding = DataBindingUtil.setContentView(this, R.layout.activity_payment_web_view);
        binding.headerLayout.tvTitle.setText(getIntent().getStringExtra("title"));
        binding.headerLayout.back.setVisibility(View.VISIBLE);
        binding.headerLayout.back.setOnClickListener((View v) -> {
//            Intent intent = new Intent();
//            setResult(Activity.RESULT_OK, intent);
            finish();
        });
        String url = getIntent().getStringExtra("url");
        binding.webView.loadUrl(url);
        binding.webView.getSettings().setJavaScriptEnabled(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        binding.webView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        binding.webView.onResume();
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding.webView.destroy();
    }
}
