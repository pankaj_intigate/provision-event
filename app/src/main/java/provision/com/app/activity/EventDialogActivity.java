package provision.com.app.activity;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.provider.CalendarContract;
import androidx.annotation.Nullable;

import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.Calendar;

import provision.com.app.R;
import provision.com.app.apiResponse.participate_info.ParticipateInfoEntity;
import provision.com.app.databinding.ActivityEventDialogBinding;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.Utils;
import provision.com.app.utils.network.api.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static provision.com.app.utils.Utils.convertPrice;


/**
 * Created by gauravg on 7/10/2018.
 */

public class EventDialogActivity extends BaseActivity {
    private ActivityEventDialogBinding binding;
    private String date;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_event_dialog);
        binding.ivCancel.setOnClickListener(v -> {
            finish();
        });
        String participateId = getIntent().getStringExtra("participateId");
        binding.cancelAction.setOnClickListener(v -> {
            cancelParticipate(participateId);
        });
        getParticipateInfo(participateId);
    }

    private void cancelParticipate(String participateId) {
        View progressView = showProgressBar();
        ApiClient.getAPiAuthHelper(this).cancelParticipateInfo("cancelparticipateInfo", participateId,ApiClient.API_VERSION).enqueue(new Callback<ParticipateInfoEntity>() {
            @Override
            public void onResponse(Call<ParticipateInfoEntity> call, Response<ParticipateInfoEntity> response) {
                ParticipateInfoEntity receiptDetails = response.body();
                if (response.body() == null || response.body().getResponse() == null) {
                    Toast.makeText(EventDialogActivity.this, EventDialogActivity.this.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(EventDialogActivity.this, receiptDetails.getResponseMessege(), Toast.LENGTH_SHORT).show();
                if (receiptDetails.getStatus().equals("1")) {
                    binding.cancelAction.setText(R.string.cancelled);
                    binding.cancelAction.setEnabled(false);
//                    binding.nameTextView.setText(receiptDetails.getResponse().getParticipateDetails().getFullName());
//                    binding.ageTextView.setText(receiptDetails.getResponse().getParticipateDetails().getAge());
//                    binding.tshirtTextView.setText(receiptDetails.getResponse().getParticipateDetails().getTshirt());
//                    binding.priceTextView.setText(receiptDetails.getResponse().getParticipateDetails().getFeename());
//                    binding.eventNameTextView.setText(receiptDetails.getResponse().getParticipateDetails().getEventName());
//                    binding.eventDateTextView.setText(receiptDetails.getResponse().getParticipateDetails().getEventDate());
//                    binding.eventLocationTextView.setText(receiptDetails.getResponse().getParticipateDetails().getEventAddress());
                }
                cancelProgressBar(progressView);
            }

            @Override
            public void onFailure(Call<ParticipateInfoEntity> call, Throwable t) {
                cancelProgressBar(progressView);
            }
        });
    }

    private void getParticipateInfo(String participateId) {
        View progressView = showProgressBar();
        ApiClient.getAPiAuthHelper(this).getParticipateInfo("getparticipateInfo", participateId, PrefUtils.getFromPrefs(EventDialogActivity.this, PrefUtils.COUNTRY_CODE),ApiClient.API_VERSION).enqueue(new Callback<ParticipateInfoEntity>() {
            @Override
            public void onResponse(Call<ParticipateInfoEntity> call, Response<ParticipateInfoEntity> response) {
                ParticipateInfoEntity receiptDetails = response.body();
                cancelProgressBar(progressView);
                if (response.body() == null || response.body().getResponse() == null) {
                    Toast.makeText(EventDialogActivity.this, EventDialogActivity.this.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (receiptDetails.getStatus().equals("1")) {
                    Glide.with(EventDialogActivity.this)
                            .load(receiptDetails.getResponse().getParticipateDetails().getQrCode())
                            .into(binding.barCodeImageView);
                    binding.barCodeTextView.setText("#" + receiptDetails.getResponse().getParticipateDetails().getRegistrationNo());
                    binding.nameTextView.setText(receiptDetails.getResponse().getParticipateDetails().getFullName());
                    if (receiptDetails.getResponse().getParticipateDetails().getAge().equals("0")) {
                        binding.ageTextView.setVisibility(View.GONE);
                    }
                    binding.ageTextView.setText(receiptDetails.getResponse().getParticipateDetails().getAge() + " " + "year");
                    if (receiptDetails.getResponse().getParticipateDetails().getTshirt().equals("Select")) {
                        binding.tshirtTextView.setVisibility(View.GONE);
                    }
                    binding.tshirtTextView.setText("T-Shirt size" + " " + receiptDetails.getResponse().getParticipateDetails().getTshirt());
                    binding.priceTextView.setText(convertPrice(receiptDetails.getCurrencyValue(), receiptDetails.getResponse().getParticipateDetails().getPrice(), response.body().getNumberformat()) + " " + receiptDetails.getCurrrencyCode());
                    binding.eventNameTextView.setText(receiptDetails.getResponse().getParticipateDetails().getEventName());
                    date = receiptDetails.getResponse().getParticipateDetails().getEventDate();

                    if (receiptDetails.getResponse().getParticipateDetails().getVirtualEvent() == 1) {
                        binding.eventDateTextView.setText(Utils.parseDateToddMMyyyy(receiptDetails.getResponse().getParticipateDetails().getEventStartDate()) + "-" + Utils.parseDateToddMMyyyy(receiptDetails.getResponse().getParticipateDetails().getEventEndDate()));
                    }else {
                        binding.eventDateTextView.setText(Utils.parseDateToddMMyyyy(receiptDetails.getResponse().getParticipateDetails().getEventDate()));
                    }
//                    binding.eventDateTextView.setText(Utils.parseDateToddMMyyyyHHMM(receiptDetails.getResponse().getParticipateDetails().getEventDate()));
                    binding.eventLocationTextView.setText(receiptDetails.getResponse().getParticipateDetails().getEventAddress());
                    binding.feeNameTextView.setText(receiptDetails.getResponse().getParticipateDetails().getFeename());

                    if (receiptDetails.getResponse().getParticipateDetails().getIsCancelled() == 1 && receiptDetails.getResponse().getParticipateDetails().getCancelled() == 0) {

                    } else {
                        binding.cancelAction.setText(R.string.cancelled);
                        binding.cancelAction.setEnabled(false);
                    }

                }
            }

            @Override
            public void onFailure(Call<ParticipateInfoEntity> call, Throwable t) {
                cancelProgressBar(progressView);
            }
        });
    }

    private void addToCalender() {
        Calendar beginTime = Calendar.getInstance();
        beginTime.set(Utils.getSaprateDate(date, "yyyy"), Utils.getSaprateDate(date, "MM") - 1, Utils.getSaprateDate(date, "dd"),
                Utils.getSaprateDate(date, "HH"), Utils.getSaprateDate(date, "mm"));
        Calendar endTime = Calendar.getInstance();
        endTime.set(2012, 0, 19, 8, 30);
        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis())
                .putExtra(CalendarContract.Events.TITLE, binding.eventNameTextView.getText())
                .putExtra(CalendarContract.Events.DESCRIPTION, binding.eventNameTextView.getText())
                .putExtra(CalendarContract.Events.EVENT_LOCATION, binding.eventLocationTextView.getText())
                .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY)
                .putExtra(Intent.EXTRA_EMAIL, "");
        startActivity(intent);
    }

}
