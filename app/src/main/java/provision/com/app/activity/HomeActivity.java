package provision.com.app.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.LabelVisibilityMode;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.firebase.messaging.FirebaseMessaging;

import provision.com.app.BuildConfig;
import provision.com.app.R;
import provision.com.app.apiResponse.unread_count.UnReadCount;
import provision.com.app.binder.HomeBinder;
import provision.com.app.databinding.ActivityHomeBinding;
import provision.com.app.fragment.HomeFragment;
import provision.com.app.fragment.MoreFragment;
import provision.com.app.fragment.RegisterNowFragment;
import provision.com.app.fragment.ResultFragment;
import provision.com.app.interfaces.RetryListener;
import provision.com.app.interfaces.UpdateCartCount;
import provision.com.app.shop.fragments.ShopFragment;
import provision.com.app.upload_result.UploadResultActivity;
import provision.com.app.utils.AppConstant;
import provision.com.app.utils.CustomAlertDialog;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.Utils;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;

import static provision.com.app.binder.SettingFragmentBinder.LOGIN_REQUEST_CODE;

/**
 * Created by gauravg on 7/5/2018.
 */
//////
public class HomeActivity extends BaseActivity implements UpdateCartCount, RetryListener {
    HomeBinder homeBinder;
    private ActivityHomeBinding binding;
    private HomeFragment homeFragment;
    private MoreFragment moreFragment;
    private ResultFragment resultFragment;
    private ShopFragment shopFragment;
    private RegisterNowFragment registerNowFragment;
    private String eventId = "0";


//    public  void disableShiftMode(BottomNavigationView view) {
//        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
//        try {
//            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
//            shiftingMode.setAccessible(true);
//            shiftingMode.setBoolean(menuView, false);
//            shiftingMode.setAccessible(false);
//            for (int i = 0; i < menuView.getChildCount(); i++) {
//                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
//                //noinspection RestrictedApi
////                item.setShiftingMode(false);
//                item.setPadding(0, 15, 0, 0);
//                // set once again checked value, so view will be updated
//                //noinspection RestrictedApi
//                item.setChecked(item.getItemData().isChecked());
//            }
//        } catch (NoSuchFieldException e) {
//            Log.e("BNVHelper", "Unable to get shift mode field", e);
//        } catch (IllegalAccessException e) {
//            Log.e("BNVHelper", "Unable to change value of shift mode", e);
//        }
//    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        homeBinder = new HomeBinder(this, binding);
        //Fabric.with(this, new Crashlytics());
        // Operations on FirebaseCrashlytics.
        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();

        binding.setHome(homeBinder);
        if (TextUtils.isEmpty(PrefUtils.getFromPrefs(this, PrefUtils.COUNTRY_CODE)) || PrefUtils.getFromPrefs(this, PrefUtils.COUNTRY_CODE).equals("")) {
            PrefUtils.saveToPrefs(this, PrefUtils.COUNTRY_CODE, "KWD");
            PrefUtils.saveToPrefs(this, PrefUtils.COUNTRY_NAME, "Kuwait");
        }
        binding.headerLayout.notificationImageView.setOnClickListener(v -> {
                    startActivity(new Intent(this, NotificationListActivity.class));
                    overridePendingTransition(R.anim.bottom_up, R.anim.stay);
                }
        );
        binding.headerLayout.tvUploadResult.setOnClickListener(v -> {
                    startActivity(new Intent(this, UploadResultActivity.class));
                    overridePendingTransition(R.anim.bottom_up, R.anim.stay);
                }
        );
//        BottomNavigationViewHelper.disableShiftMode(binding.bottomView);
        FirebaseMessaging.getInstance().subscribeToTopic("androidPushEvent");
        binding.bottomView.setSelectedItemId(R.id.action_home);
        binding.bottomView.setItemIconTintList(null);
        binding.headerLayout.notificationImageView.setVisibility(View.VISIBLE);
        homeFragment = HomeFragment.newInstance();
        binding.headerLayout.cartRL.setVisibility(View.VISIBLE);
        binding.headerLayout.cartRL.setOnClickListener(v -> {
            Intent intent = new Intent(HomeActivity.this, CartActivity.class);
            startActivityForResult(intent, 101);
            overridePendingTransition(R.anim.bottom_up, R.anim.stay);
        });
        binding.bottomView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_home:
                    homeFragment = HomeFragment.newInstance();
                    openFragment(homeFragment);
                    //  binding.headerLayout.tvTitle.setText("Home");
                    binding.headerLayout.tvUploadResult.setVisibility(View.GONE);
                    binding.headerLayout.tvTitle.setVisibility(View.GONE);
                    binding.headerLayout.imgHeader.setVisibility(View.VISIBLE);
                    binding.headerLayout.rlParent.setVisibility(View.VISIBLE);
                    binding.headerLayout.notificationImageView.setVisibility(View.VISIBLE);
                    binding.headerLayout.searchImg.setVisibility(View.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        Window window = getWindow();
                        View view = window.getDecorView();
                        Utils.setLightStatusBar(view, this);
                    }
                    break;
//                case R.id.action_events:
//                    if (binding.headerLayout.back.getVisibility() == View.GONE) {
//                        binding.headerLayout.back.setVisibility(View.VISIBLE);
//                    }
//                    eventFragment = EventFragment.newInstance();
//                    binding.headerLayout.tvTitle.setVisibility(View.VISIBLE);
//                    binding.headerLayout.tvTitle.setText("Event");
//                    binding.headerLayout.imgHeader.setVisibility(View.GONE);
//                    openFragment(eventFragment);
//                    binding.headerLayout.rlParent.setVisibility(View.VISIBLE);
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        Window window = getWindow();
//                        View view = window.getDecorView();
//                        Utils.setLightStatusBar(view, this);
//                    }
//                    break;
                case R.id.action_register:
                    binding.headerLayout.tvTitle.setVisibility(View.VISIBLE);
                    binding.headerLayout.imgHeader.setVisibility(View.GONE);
                    binding.headerLayout.tvTitle.setText(R.string.registration);
                    registerNowFragment = RegisterNowFragment.newInstance(eventId, this);
                    openFragment(registerNowFragment);
                    binding.headerLayout.tvUploadResult.setVisibility(View.GONE);
                    binding.headerLayout.rlParent.setVisibility(View.VISIBLE);
                    binding.headerLayout.tvCountry.setVisibility(View.GONE);
                    binding.headerLayout.searchImg.setVisibility(View.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        Window window = getWindow();
                        View view = window.getDecorView();
                        Utils.setLightStatusBar(view, this);
                    }

                    break;
                case R.id.action_result:
                    binding.headerLayout.tvTitle.setVisibility(View.VISIBLE);
                    binding.headerLayout.tvTitle.setText(R.string.result);
                    resultFragment = ResultFragment.newInstance();
                    binding.headerLayout.searchImg.setVisibility(View.GONE);
                    binding.headerLayout.tvUploadResult.setVisibility(View.VISIBLE);
                    openFragment(resultFragment);
                    binding.headerLayout.rlParent.setVisibility(View.VISIBLE);
                    binding.headerLayout.tvCountry.setVisibility(View.GONE);
                    binding.headerLayout.imgHeader.setVisibility(View.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        Window window = getWindow();
                        View view = window.getDecorView();
                        Utils.setLightStatusBar(view, this);
                    }

                    break;

                case R.id.action_shop:
                    binding.headerLayout.tvTitle.setVisibility(View.VISIBLE);
                    binding.headerLayout.tvTitle.setText(R.string.shop);
                    binding.headerLayout.rlParent.setVisibility(View.VISIBLE);

                    binding.headerLayout.imgHeader.setVisibility(View.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        Window window = getWindow();
                        View view = window.getDecorView();
                        Utils.setLightStatusBar(view, this);
                    }
                    binding.headerLayout.searchImg.setVisibility(View.VISIBLE);
                    binding.headerLayout.tvUploadResult.setVisibility(View.GONE);
                    shopFragment = ShopFragment.newInstance();
                    shopFragment.onSearchClick(binding.headerLayout.searchImg);
                    openFragment(shopFragment);
                    break;
                case R.id.action_more:
                    binding.headerLayout.searchImg.setVisibility(View.GONE);
                    binding.headerLayout.tvTitle.setVisibility(View.VISIBLE);
                    binding.headerLayout.tvTitle.setText("More");
                    moreFragment = MoreFragment.newInstance(this);
                    openFragment(moreFragment);
                    binding.headerLayout.rlParent.setVisibility(View.VISIBLE);
                    binding.headerLayout.tvCountry.setVisibility(View.GONE);
                    binding.headerLayout.imgHeader.setVisibility(View.GONE);
                    binding.headerLayout.tvUploadResult.setVisibility(View.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        Window window = getWindow();
                        View view = window.getDecorView();
                        Utils.setLightStatusBar(view, this);
                    }

                    break;
            }
            return true;
        });

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, homeFragment);
        transaction.commit();
    }


    @Override
    protected void onResume() {
        super.onResume();
        setRetryEnable(true);
        setRetryListener(this);
        getNotificationCount();
        if (!TextUtils.isEmpty(PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM)) && !PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM).equals("0")) {
            binding.headerLayout.cartCountTextView.setVisibility(View.VISIBLE);
            binding.headerLayout.cartCountTextView.setText(PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM));
        } else {
            binding.headerLayout.cartCountTextView.setVisibility(View.GONE);
        }
        binding.bottomView.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);
//        disableShiftMode(binding.bottomView);
        homeBinder.setCountryCode();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 222) {
            try {
                if (TextUtils.isEmpty(PrefUtils.getFromPrefs(this, PrefUtils.VERSION_CODE))) {
                    return;
                }
                int currentVersion = Integer.parseInt(PrefUtils.getFromPrefs(this, PrefUtils.VERSION_CODE));
                int versionCode = BuildConfig.VERSION_CODE;

                if (currentVersion > versionCode) {
                    CustomAlertDialog.forceUpdate(this);
                }
            } catch (Exception e) {

            }

        }
        if (resultCode == Activity.RESULT_CANCELED) {
            return;
        }

        if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
            if (data != null && data.hasExtra(AppConstant.KEY_EVENT_ID)) {
                eventId = data.getStringExtra(AppConstant.KEY_EVENT_ID);
            }
            binding.bottomView.setSelectedItemId(R.id.action_register);
            eventId = "0";
        } else if (requestCode == 1001 && resultCode == Activity.RESULT_OK) {
            openFragment(HomeFragment.newInstance());
            binding.bottomView.setSelectedItemId(R.id.action_home);
        } else if (requestCode == LOGIN_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            openFragment(HomeFragment.newInstance());
            binding.bottomView.setSelectedItemId(R.id.action_more);
        }
    }

    public void openFragment(Fragment fragment) {
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.commit();
    }

    public void onBottomItemSelected(int position) {
        binding.bottomView.setSelectedItemId(position);
    }


    @Override
    public void upDateCartCount() {
        if (TextUtils.isEmpty(PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM)) || PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM).equals("0")) {
            binding.headerLayout.cartCountTextView.setVisibility(View.GONE);
            return;
        }
        binding.headerLayout.cartCountTextView.setVisibility(View.VISIBLE);
        binding.headerLayout.cartCountTextView.setText(PrefUtils.getFromPrefs(this, PrefUtils.CART_ITEM));
    }

    private void exitDialog() {
        new CustomAlertDialog(this, getString(R.string.exit_confirmation_msg), getString(R.string.dialog_ok),
                new CustomAlertDialog.onAlertDialogCustomListener(
                ) {
                    @Override
                    public void onSuccessListener(DialogInterface dialog) {
                        dialog.dismiss();
                        System.exit(0);
                    }

                    @Override
                    public void onCancelListener() {
                    }
                });
    }


    @Override
    public void onBackPressed() {
        exitDialog();
    }

    private void getNotificationCount() {
        //device android then device is 1 and 2 for ios
        if (isOnline()) {

            String newToken = PrefUtils.getFromPrefs(this, PrefUtils.TOKEN);
            ;
            Log.d("MyFirebaseMsgService", "Refreshed token: " + newToken);
            ApiClient.getClient(HomeActivity.this).create(AuthApiHelper.class).getNotificationCount("unReadCount", "1", newToken, ApiClient.API_VERSION).enqueue(new CallbackManager<UnReadCount>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    UnReadCount unReadCount = (UnReadCount) object;
                    if (unReadCount == null) {
                        Toast.makeText(HomeActivity.this, HomeActivity.this.getString(R.string.response_msg), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (unReadCount.getResponse() != null && unReadCount.getResponse().getUnReadCount() != 0) {
                        binding.headerLayout.notificationCountTextView.setText(unReadCount.getResponse().getUnReadCount() + "");
                        binding.headerLayout.notificationCountTextView.setVisibility(View.VISIBLE);
                    } else {
                        binding.headerLayout.notificationCountTextView.setVisibility(View.GONE);
                    }
                }

                @Override
                protected void onError(RetroError retroError) {
                }

                @Override
                protected void onFailure(String retroError) {
                }
            });

        } else {
            showToast(this, getResources().getString(R.string.check_internet));
        }
    }

    @Override
    public void onRetry() {
        getNotificationCount();
        int selectedItem = binding.bottomView.getSelectedItemId();
        switch (selectedItem) {
            case R.id.action_home:
                homeFragment.onRetryCall();
                break;
            case R.id.action_result:
                resultFragment.onRetryCall();
                break;
            case R.id.action_register:
                registerNowFragment.onRetryCall();
                break;
            case R.id.action_more:
                moreFragment.onRetryCall();
                break;
            default:
                homeFragment.onRetryCall();
        }
    }


}
