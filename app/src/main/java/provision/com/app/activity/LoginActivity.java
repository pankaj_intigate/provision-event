package provision.com.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.facebook.FacebookSdk;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import provision.com.app.R;
import provision.com.app.binder.LoginBinder;
import provision.com.app.databinding.ActivityLoginBinding;

import static provision.com.app.binder.LoginBinder.RC_SIGN_IN;

/**
 * Created by gauravg on 7/6/2018.
 */
public class LoginActivity extends BaseActivity {
    ActivityLoginBinding binding;
    private LoginBinder loginBinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        if (getIntent().hasExtra("IsCheckOut")) {
            binding.loginRG.setVisibility(View.VISIBLE);
            binding.loginTitleTV.setText("Login to continue");
        } else {
            binding.loginRG.setVisibility(View.GONE);
        }

        loginBinder = new LoginBinder(this, binding, getIntent().hasExtra("IsCheckOut"), getIntent().getIntExtra("isAddressRequired", 0));
        binding.setLogin(loginBinder);

        try {
            PackageInfo info = getPackageManager().getPackageInfo("provision.com.app", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String sign = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.e("MY KEY HASH:", sign);
                //textInstructionsOrLink = (TextView)findViewById(R.id.textstring);
                //textInstructionsOrLink.setText(sign);
//                Toast.makeText(getApplicationContext(),sign, Toast.LENGTH_LONG).show();
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("nope", "nope");
        } catch (NoSuchAlgorithmException ignored) {
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 555 && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK);
            finish();
        }
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else {
            loginBinder.getCallbackManager().onActivityResult(requestCode, resultCode, data);
        }
    }

    ;

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            loginBinder.updateUI(account);
        } catch (ApiException e) {

            Log.w("TAG", "signInResult:failed code=" + e.getStatusCode() + e.getMessage());
            //loginBinder.updateUI(null);
        }
    }
}
