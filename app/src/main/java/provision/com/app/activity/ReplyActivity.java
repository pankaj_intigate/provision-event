package provision.com.app.activity;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;

import provision.com.app.R;
import provision.com.app.binder.ReplyBinder;
import provision.com.app.databinding.ActivityReplyBinding;

/**
 * Created by gauravg on 7/18/2018.
 */

public class ReplyActivity extends BaseActivity {
    private ActivityReplyBinding binding;
    private String getId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_reply);
        if (getIntent() != null) {
            getId = getIntent().getStringExtra("MailId");
        }
        ReplyBinder replyBinder = new ReplyBinder(this, binding, getId);
        binding.setReply(replyBinder);
    }
}
