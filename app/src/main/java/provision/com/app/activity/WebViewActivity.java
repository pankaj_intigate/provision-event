package provision.com.app.activity;

import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.adjust.sdk.webbridge.AdjustBridge;

import java.util.UUID;

import provision.com.app.R;
import provision.com.app.apiResponse.BaseResponce;
import provision.com.app.databinding.ActivityWebViewBinding;
import provision.com.app.utils.PrefUtils;
import provision.com.app.utils.network.api.ApiClient;
import provision.com.app.utils.network.api.AuthApiHelper;
import provision.com.app.utils.network.api.CallbackManager;
import provision.com.app.utils.network.api.RetroError;

/**
 * Created by gauravg on 7/13/2018.
 */

public class WebViewActivity extends BaseActivity {
    ActivityWebViewBinding binding;
    String url, titleName;
    View progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_web_view);

        if (getIntent() != null) {
            url = getIntent().getStringExtra("URL");
            titleName = getIntent().getStringExtra("TitleName");
            Log.d("url", url);
        }
        progressBar = showProgressBar();
        if (isOnline()) {
            binding.headerLayout.tvHeader.setText(titleName);
            binding.headerLayout.tvHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.dim_15sp));
            binding.webView.setWebViewClient(new MyWebViewClient());
            binding.webView.getSettings().setJavaScriptEnabled(true);
            AdjustBridge.registerAndGetInstance(getApplication(), binding.webView);
            // Log.d("payment Url : ",url);
            binding.webView.loadUrl(url);
        } else {
            showToast(this, "Please check internet connection");
            cancelProgressBar(progressBar);
        }
        binding.webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress == 100) {
                    progressBar.setVisibility(View.GONE);

                } else {
                    progressBar.setVisibility(View.VISIBLE);

                }
            }
        });
        binding.headerLayout.back.setOnClickListener(v -> {
            if (getIntent().hasExtra("isCartReset")) {
                resetCart();
                return;
            }
            finish();
        });
    }

    @Override
    public void onBackPressed() {
        if (getIntent().hasExtra("isCartReset")) {
            resetCart();
            return;
        }
        super.onBackPressed();
    }

    private void resetCart() {
        if (isOnline()) {
            String sessionId = PrefUtils.getFromPrefs(this, PrefUtils.SESSION_ID);
            View view = showProgressBar();
            AuthApiHelper authApiHelper = ApiClient.getClient(this).create(AuthApiHelper.class);
            retrofit2.Call<BaseResponce> call = authApiHelper.cancelOrderForResetCart("cancelOrderForResetCart", sessionId, ApiClient.API_VERSION);
            call.enqueue(new CallbackManager<BaseResponce>() {
                @Override
                protected void onSuccess(Object object, String message) {
                    cancelProgressBar(view);

                    finish();
//                    Toast.makeText(mActivity, cartDataResponse.getResponseMessege(), Toast.LENGTH_SHORT).show();
                }

                @Override
                protected void onError(RetroError retroError) {
                    cancelProgressBar(view);
                }

                @Override
                protected void onFailure(String retroError) {
                    cancelProgressBar(view);
                }
            });
        } else {
            showToast(this, getResources().getString(R.string.check_internet));
        }
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            Log.d("urlLoading", url);
            if (url.contains("receipt/view/")) {
                String timeInMillis = System.currentTimeMillis() + "-" + UUID.randomUUID().toString();
                Log.d("SESSION_ID", timeInMillis);
                PrefUtils.saveToPrefs(WebViewActivity.this, PrefUtils.SESSION_ID, timeInMillis);
                PrefUtils.saveToPrefs(WebViewActivity.this, PrefUtils.CART_ITEM, "0");
            } else if (url.contains("cart/appfailpayment")) {
                paymentFails(WebViewActivity.this);
            }
            return true;
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        binding.webView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        binding.webView.onResume();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding.webView.destroy();
    }


    public static void paymentFails(BaseActivity mActivity) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
        } else {
            builder = new AlertDialog.Builder(mActivity);
        }

        builder.setTitle(mActivity.getString(R.string.payment_fails));

        builder.setMessage(mActivity.getString(R.string.payment_fails_message));
        builder.setCancelable(false);
        builder.setPositiveButton("Retry", (dialog, which) -> {
            mActivity.finish();
        });
        builder.show();
    }
}
