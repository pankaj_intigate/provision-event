package provision.com.app.activity;

import androidx.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.View;

import provision.com.app.R;
import provision.com.app.databinding.ActivityCouponInfoBinding;

public class CouponInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityCouponInfoBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_coupon_info);
        binding.headerLayout.tvTitle.setText("Coupon Code");
        binding.headerLayout.back.setOnClickListener(v -> {
            finish();
        });
        String firstTearms = "Regular Coupon code users: Once attempted to use the coupon code, it will be frozen for 60 minutes until you can try again للعلم: لدى محاولة استعمال رقم الخصم الخاص بك، سيتم تجميده لفترة 60 دقيقة قبل الاستطاعة من محاولة استخدامه مرة أخرى\n";
        SpannableStringBuilder sb = new SpannableStringBuilder(firstTearms);
        StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
        StyleSpan iss = new StyleSpan(Typeface.NORMAL); //Span to make text italic
        sb.setSpan(bss, 0, 26, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
        sb.setSpan(iss, 27, firstTearms.length() - 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make last 2 characters Italic

        binding.firstTerms.setText(sb);
        String secondTerms = "Gulf Bank employees: please enter your Civil I.D number followed by your employee code to avail discount. Example: 000000000000-000000 موظفي بنك الخليج: يرجى ادخال الرقم المدني و الرقم الوظيفي للحصول على الخصم. مثال: 000000000000-000000";
        binding.headerLayout.back.setVisibility(View.VISIBLE);
        sb = new SpannableStringBuilder(secondTerms);
        sb.setSpan(bss, 0, 20, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
        sb.setSpan(iss, 21, secondTerms.length() - 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make last 2 characters Italic
        binding.secondTerms.setText(sb);
    }
}
