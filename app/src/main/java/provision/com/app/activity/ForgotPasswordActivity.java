package provision.com.app.activity;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;

import provision.com.app.R;
import provision.com.app.binder.ForgotPasswordBinder;
import provision.com.app.databinding.ActivityForgotPasswordBinding;

/**
 * Created by gauravg on 7/9/2018.
 */

public class ForgotPasswordActivity extends BaseActivity {
    ActivityForgotPasswordBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);
        ForgotPasswordBinder forgotPasswordBinder = new ForgotPasswordBinder(this, binding);
        binding.setForgotPassword(forgotPasswordBinder);
        binding.emailEditText.setText(getIntent().getStringExtra("EmailId"));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
