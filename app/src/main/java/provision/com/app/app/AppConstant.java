package provision.com.app.app;

public class AppConstant {
    //For Adjust Analytics
    public static String HOME_EVENT_TOKEN = "gm9bkv"; //HomeFragment
    public static String REGISTRATION_EVENT_TOKEN = "mssv10"; //RegisterNowFragment
    public static String RESULTS_EVENT_TOKEN = "n0hzkq"; //ResultFragment
    public static String RESULTS_DETAILS_EVENT_TOKEN = "assvc5";
    public static String MY_ACCOUNT_EVENT_TOKEN = "nd0gmk"; //MoreFragment
    public static String LOGIN_EVENT_TOKEN = "424mku"; //LoginActivity
    public static String CART_EVENT_TOKEN = "gi5uxp"; //CartActivity
    public static String SIGNUP_EVENT_TOKEN = "hur40d"; //SignUpActivity
    public static String NOTIFICATION_EVENT_TOKEN = "5indef"; //NotificationListActivity
    public static String NOTIFICATION_DETAILS_EVENT_TOKEN = "n026td";
    public static String ABOUT_US_EVENT_TOKEN = "u04r27";
    public static String CONTACT_US_EVENT_TOKEN = "6tif0j";
    public static String TERMS_AND_CONDITIONS_EVENT_TOKEN = "722nxc";
    public static String PRIVACY_POLICY_EVENT_TOKEN = "9jide7";
    public static String INSTAGRAM_EVENT_TOKEN = "k59m2o";
    public static String FORGOT_PASSWORD_EVENT_TOKEN = "fsun4m";
    public static String MY_BOOKING_EVENT_TOKEN = "5ode34";
    public static String MY_WALLET_EVENT_TOKEN = "b7r3us";
    public static String CHANGE_PASSWORD_EVENT_TOKEN = "toerv3";
    public static String INBOX_EVENT_TOKEN = "uext77";
    public static String INBOX_DETAIL_EVENT_TOKEN = "rjvan5";
    public static String REPLY_EVENT_TOKEN = "qggl15";
    public static String MY_LOCATION_EVENT_TOKEN = "9nfj6t";
    public static String LOGOUT_EVENT_TOKEN = "hm40ap";
    public static String EDIT_PROFILE_EVENT_TOKEN = "fkok6k";
    public static String VIEW_PROFILE_EVENT_TOKEN = "pi8b3e";
    public static String MY_BOOKING_DETAIL_EVENT_TOKEN = "lp0bll";
    public static String EVENT_DETAILS_EVENT_TOKEN = "ay772l";
    public static String SHARE_EVENT_EVENT_TOKEN = "acogrq";
    public static String SEND_TO_CART_EVENT_TOKEN = "lf3p31";
    public static String DISCLAIMER_EVENT_TOKEN = "1ezlyb";
    public static String PROCEED_TO_PAYMENT_EVENT_TOKEN = "wcv2wr";
}
