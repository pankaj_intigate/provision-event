package provision.com.app.app;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import androidx.multidex.MultiDex;

import com.adjust.sdk.Adjust;
import com.adjust.sdk.AdjustConfig;
import com.adjust.sdk.AdjustEventSuccess;
import com.adjust.sdk.LogLevel;
import com.adjust.sdk.OnEventTrackingSucceededListener;

import io.branch.referral.Branch;
import provision.com.app.utils.CommonUtils;


/**
 * Created by gauravg on 7/11/2018.
 */

public class ProvisionApplication extends Application {
    public static final String ADJUST_ANALYTICS_APP_TOKEN = "9li7x1fp8dfk";
    private String environment;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);

    }

    @Override
    public void onCreate() {
        super.onCreate();

        if(CommonUtils.isDebug()) {
            environment = AdjustConfig.ENVIRONMENT_SANDBOX;//Development Mode
        }else{
            environment = AdjustConfig.ENVIRONMENT_PRODUCTION;//Live Mode
        }
        AdjustConfig config = new AdjustConfig(this, ADJUST_ANALYTICS_APP_TOKEN, environment);
        config.setLogLevel(LogLevel.VERBOSE); // enable all logs
        // Set event success tracking delegate.
        config.setOnEventTrackingSucceededListener(new OnEventTrackingSucceededListener() {
            @Override
            public void onFinishedEventTrackingSucceeded(AdjustEventSuccess eventSuccessResponseData) {
              //  Log.d("example", "Event success callback called!");
              //  Log.d("example", "Event success data: " + eventSuccessResponseData.toString());
            }
        });
        Adjust.onCreate(config);

        registerActivityLifecycleCallbacks(new AdjustLifecycleCallbacks());

        // Branch logging for debugging
        Branch.enableDebugMode();

        // Branch object initialization
        Branch.getAutoInstance(this);
    }

    private class AdjustLifecycleCallbacks implements ActivityLifecycleCallbacks {
        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

        }

        @Override
        public void onActivityStarted(Activity activity) {

        }

        @Override
        public void onActivityResumed(Activity activity) {
            Adjust.onResume();
           // Log.d("Adjust Analytics", "Resumed");
        }

        @Override
        public void onActivityPaused(Activity activity) {
            Adjust.onPause();
          //  Log.d("Adjust Analytics", "Paused");
        }

        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {

        }
    }

    /*
    Next, you must set the environment to either sandbox or production mode:
String environment = AdjustConfig.ENVIRONMENT_SANDBOX;
String environment = AdjustConfig.ENVIRONMENT_PRODUCTION;
Important: Set the value to AdjustConfig.ENVIRONMENT_SANDBOX if (and only if) you or someone else is testing your app. Make sure to set the environment to AdjustConfig.ENVIRONMENT_PRODUCTION before you publish the app.
 Set it back to AdjustConfig.ENVIRONMENT_SANDBOX if you start developing and testing it again.
 */
}
